<div class="cabinet-menu">
    <a href="/cabinet/" class="<?=($APPLICATION->GetCurPage(false) == '/cabinet/')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">Общая информация</a>
    <?
    if(in_array(9, $USER->GetUserGroupArray())){
       $class_active= ($APPLICATION->GetCurPage(false) == '/cabinet/make-product/')?"active-cabinet-item cabinet-menu-item":"cabinet-menu-item";
        echo '<a href="/cabinet/make-product/" class="'.$class_active.' ">Мои товары</a>';
    }
    ?>
    <?php if(in_array(9, $USER->GetUserGroupArray())): ?>
    <a href="/cabinet/order/list/?filter_history=Y"
       class="<?=($APPLICATION->GetCurPageParam('',array(), null) == '/cabinet/order/list/?filter_history=Y')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">
       История заказов
    </a>
    <?php endif; ?>
    <?php if(in_array(9, $USER->GetUserGroupArray())): ?>
    <a href="/cabinet/order/list/"
       class="<?=($APPLICATION->GetCurPageParam('',array(), null) == '/cabinet/order/list/')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">
       Текущие заказы
    </a>
    <?php endif; ?>
    <a href="#" class="<?=($APPLICATION->GetCurPage(false) == '#')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">Адрес доставки</a>
    <a href="#" class="<?=($APPLICATION->GetCurPage(false) == '#')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">Привязанные карты</a>
    <a href="#" class="<?=($APPLICATION->GetCurPage(false) == '#')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">Привязанные соц сети</a>
    <a href="#" class="<?=($APPLICATION->GetCurPage(false) == '#')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?> chat-item">Чат с поставщиком</a>
    <?php if(in_array(9, $USER->GetUserGroupArray())): ?>
    <a href="/cabinet/wishlist/" class="<?=($APPLICATION->GetCurPage(false) == '/cabinet/wishlist/')?"cabinet-menu-item active-cabinet-item":"cabinet-menu-item";?>">Избранное</a>
    <?php endif; ?>
</div>
