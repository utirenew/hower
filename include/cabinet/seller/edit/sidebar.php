<?php
$current_url_path = $APPLICATION->GetCurPageParam('', array(), null);
//exit('U');
$link_data_collection = array(
    array(
        'href' => '/cabinet/seller/edit/info/main/',
        'text' => 'Общая информация'
    ),
    array(
        'href' => '/cabinet/seller/edit/catalog/',
        'text' => 'Мои товары'
    ),
	array(
		'href' => '/cabinet/seller/edit/order/history/?filter_history=Y',
		'text' => 'История заказов'
	),
    array(
        'href' => '/cabinet/seller/edit/catalog/wishlist/',
        'text' => 'Избранные заявки'
    ),
    array(
        'href' => '/cabinet/seller/edit/info/address/',
        'text' => 'Адреса магазинов'
    ),
    array(
        'href' => '/cabinet/seller/edit/info/deliveri/',
        'text' => 'Адреса доставки'
    ),
    array(
        'href' => '/cabinet/seller/edit/info/deliveris/',
        'text' => 'Службы доставки'
    ),
    array(
        'href' => '/cabinet/seller/edit/info/payment/',
        'text' => 'Финансовый кабинет'
    ),
    array(
        'href' => '/cabinet/seller/edit/subscription/',
        'text' => 'Тарифный план'
    ),
    array(
        'href' => '/cabinet/seller/edit/info/social/',
        'text' => 'Привязанные соц. сети'
    ),
    array(
        'href' => '/cabinet/seller/edit/user/message/',
        'text' => 'Чат с покупателем'
    ),
    array(
        'href' => '/cabinet/seller/edit/user/review/',
        'text' => 'Отзывы о покупателях'
    ),
	array(
		'href' => '/cabinet/seller/edit/user/wishlist/',
		'text' => 'Избранные покупатели',
	),
	array(
		'href' => '/cabinet/seller/edit/help/message/',
		'text' => 'Чат с администрацией',
	),
);
?>
<div class="cabinet-menu">
	 <?php foreach ($link_data_collection as $link_data):
        if ($current_url_path === $link_data['href']) {
            $class_active = 'active-cabinet-item';
        } else {
            $class_active = '';
        }
        if ($link_data['href'] === '/cabinet/seller/edit/order/history/?filter_history=Y') {
            if ($current_url_path === '/cabinet/seller/edit/order/history/') {
                $class_active = 'active-cabinet-item';
            }
        }
    ?> <a href="<?php echo $link_data['href']; ?>" class="cabinet-menu-item <?php echo $class_active; ?>
	 "> <?php echo $link_data['text']; ?> </a>
	<?php endforeach; ?>
</div>
 <br>