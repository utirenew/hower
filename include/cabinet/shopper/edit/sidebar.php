<?php
$current_url_path = $APPLICATION->GetCurPageParam('', array(), null);

$link_data_collection = array(
	array(
		'href' => '/cabinet/shopper/edit/info/main/',
		'text' => 'Общая информация',
	),
	array(
		'href' => '/cabinet/shopper/edit/catalog/',
		'text' => 'Мои заявки',
	),
	array(
		'href' => '/cabinet/shopper/edit/order/history/?filter_history=Y',
		'text' => 'История покупок',
	),
	array(
		'href' => '/cabinet/shopper/edit/catalog/wishlist/',
		'text' => 'Избранные товары',
	),
	array(
		'href' => '/cabinet/shopper/edit/info/address/',
		'text' => 'Адреса доставки',
	),
	array(
		'href' => '/cabinet/shopper/edit/info/payment/',
		'text' => 'Финансовый кабинет',
	),
	array(
		'href' => '/cabinet/shopper/edit/info/social/',
		'text' => 'Привязанные соц. сети',
	),
	array(
		'href' => '/cabinet/shopper/edit/user/message/',
		'text' => 'Чат с продавцом',
	),
	array(
		'href' => '/cabinet/shopper/edit/user/review/',
		'text' => 'Отзывы о продавцах',
	),
	array(
		'href' => '/cabinet/shopper/edit/user/wishlist/',
		'text' => 'Избранные продавцы',
	),
	array(
		'href' => '/cabinet/shopper/edit/help/message/',
		'text' => 'Чат с администрацией',
	),
);
?>

<div class="cabinet-menu">
	<?php foreach($link_data_collection as $link_data):
		if($current_url_path === $link_data['href']){
			$class_active = 'active-cabinet-item';
		}
		else{
			$class_active = '';
		}
		if($link_data['href'] === '/cabinet/shopper/edit/order/list/?filter_history=Y'){
			if($current_url_path === '/cabinet/shopper/edit/order/list/'){
				$class_active = 'active-cabinet-item';
			}
		}
		?>
		<a href="<?php echo $link_data['href']; ?>" class="cabinet-menu-item <?php echo $class_active; ?>">
			<?php echo $link_data['text']; ?>
		</a>
	<?php endforeach; ?>
</div>
