<div class="search-catalog">
	<div>
		<div class="row">
			<div class="col-12">
				<div class="search-catalog-form-wrapper">
					<form action="" method="post" class="search-form">
						<div class="search-up-line">
							<div class="search-block1">
								<span class="search-block1-text">Вы ищите:</span>
								<label class="d-flex align-items-center">
									<input name="object" type="radio" value="0" class="d-none product-fermer">
									<span class="icon-fermer choose-radio"></span> <span class="search-item-name">Фермера</span></label>
								<label>
									<input name="object" type="radio" value="1" class="d-none product-fermer">
									<span class="icon-product choose-radio"></span> <span class="search-item-name">Товар</span></label>
							</div>
							<div class="search-text-block">
								<div class="search-text-wrap">
									<input type="text" class="search-text" placeholder="Поиск">
									<button type="submit" class="search-btn">найти</button>
								</div>
							</div>
							<div class="under-search-btn">
							</div>
						</div>
						<div class="under-search-wrap">
							<div class="search-under-line">
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="10" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/banka.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Соленья</span> </span></label>
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="20" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/meat.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Мясо</span> </span></label>
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="30" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/chicken.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Птица</span> </span></label>
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="40" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/fish.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Рыба</span> </span></label>
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="50" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/milk.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Молочные продукты</span> </span></label>
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="60" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/fruit.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Овощи, фрукты</span> </span></label>
								<label class="product-choose-wrap">
									<input name="product" type="checkbox" value="70" class="hidden-input">
									<span class="search-product"> <img src="/local/templates/ferma_24/image/svg/bakaley.svg" alt="" class="product-svg"> <span
												class="product-choose-name">Бакалея</span> </span></label>
							</div>
							<div class="search-sub-line">
								<div class="sub-product">
									Баранина
								</div>
								<div class="sub-product active-sub-product">
									Говядина
								</div>
								<div class="sub-product">
									Стейки
								</div>
								<div class="sub-product">
									Деликатесы
								</div>
								<div class="sub-product">
									Дичь
								</div>
								<div class="sub-product">
									Козлятина
								</div>
								<div class="sub-product">
									Колбасы
								</div>
								<div class="sub-product">
									Фарш
								</div>
								<div class="sub-product">
									Полуфабрикаты
								</div>
								<div class="sub-product">
									Копчения
								</div>
								<div class="sub-product">
									Крольчатина
								</div>
								<div class="sub-product">
									Мясные закуски
								</div>
								<div class="sub-product">
								</div>
								<div class="sub-product">
								</div>
								<div class="sub-product">
								</div>
								<div class="sub-product">
								</div>
								<div class="sub-product">
								</div>
								<div class="sub-product">
								</div>
								<div class="sub-product">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
