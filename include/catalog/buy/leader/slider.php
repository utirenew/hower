<div class="lider-slider-block">
	<div>
		<div class="row">
			<div class="col-12 p-0">
				<div class="slider-head">
					<div class="slider-title">
 						<span class="head-icon">top</span>
						Лидеры покупок
					</div>
					<div class="slider-controls-block">
						<div class="lider-arrows">
 							<a class="carousel-control lider-control prev" href="#main-carouselIndicators" role="button" data-slide="slickPrev">
								назад
								<span class="left-btn control-btn" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control lider-control next" href="#main-carouselIndicators" role="button" data-slide="slickNext">
								вперёд
								<span class="right-btn control-btn" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
						<div class="product-dots">
						</div>
					</div>
				</div>
				<div class="product-slider">
					<!-- item -->
					<div class="col relative">
						<div class="product-wrap">
							<div class="product">
								<div class="top-block">
 									<a href="/product.php" class="product-pic"> <img src="/local/templates/ferma_24/image/slider-p1.jpg" alt=""> </a>
									<div class="product-desc">
										<div class="fermer-name">
											 Коптильня "Ретивый кабанчик"
										</div>
										<div class="product-name">
											 Окорок копчено-вяленый по-вестфальски нарезка
										</div>
									</div>
									<div class="product-sale akcija">
									</div>
									 <!--class=""akcija class="discont"-->
									<div class="like">
									</div>
									 <!--class="active-like"-->
								</div>
								<div class="bottom-block">
									<div class="product-price">
 										<span class="price">1050</span> <span class="unit">/ за кг.</span>
									</div>
									 <!--<div class="product-old-price">860</div>-->
								</div>
 								<a href="#" class="add-product"></a>
							</div>
						</div>
					</div>
					<!-- item -->
				</div>
			</div>
		</div>
	</div>
</div>
