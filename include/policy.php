<div id="policy" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100 text-center">Политика конфиденциальности</h4>
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Политика конфиденциальности персональных данных (далее – Политика
                    конфиденциальности)
            </div>
        </div>
    </div>
</div>
