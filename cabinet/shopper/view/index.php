<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?
$arResult["PATH_TO_USER"]               = '/cabinet/?page=user&user_id=#user_id#';
$arResult["VARIABLES"]["page"] 			= 'user';
$arResult["VARIABLES"]["user_id"] 		= intval($_GET['user_id']);
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.profile",
	"public_profile_shopper",
	Array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CHECK_RIGHTS" => "N",
		"SEND_INFO" => "Y"
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
