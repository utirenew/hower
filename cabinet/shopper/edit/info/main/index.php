<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<? $APPLICATION->IncludeComponent("bitrix:main.profile", "profile_seller", array(
	"AJAX_MODE"              => "N",
	"AJAX_OPTION_ADDITIONAL" => "",
	"AJAX_OPTION_HISTORY"    => "N",
	"AJAX_OPTION_JUMP"       => "N",
	"AJAX_OPTION_SHADOW"     => "Y",
	"AJAX_OPTION_STYLE"      => "Y",
	"CHECK_RIGHTS"           => "N",
	"SEND_INFO"              => "Y",
), false, array('ACTIVE_COMPONENT' => 'Y')); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
