<? define('NEED_AUTH', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle(""); ?>



<div class="order-item">
	<div class="order-header">
		<span style="font-size: 25px;">Службы доставки</span>
	</div>



	
	<form name="iblock_add" class="delivformbe" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		<?


		global $USER;
		$cur_user_id = $USER->GetID();



		$rsUser = CUser::GetByID($cur_user_id);
		$arUser = $rsUser->Fetch();



		?>


<?

if($_POST['save-shop-delivery'] =="Y" && $_POST[IDEDIT] >0) {

$checks = unserialize($arUser[UF_DOSTAVKAS]);
$idedits = intval($_POST[IDEDIT]);

if(in_array($idedits,$checks)) { 

$name = strip_tags($_POST['NAME']);
$desc = strip_tags($_POST['DESCRIPTION']);
$arFields2 = array(
    "NAME" => $name,
    "ACTIVE" => "Y",
    "DESCRIPTION" => $desc,
);

if (!CSaleDelivery::Update($idedits, $arFields2)) {
   echo "Ошибка изменения доставки";

} else {
echo '
<div id="change-delivery-id" class="alert text-center alert-success" >Изменения успешно применены!</div>
';
}
}

}
else if($_POST['save-shop-delivery'] =="Y") {
\Bitrix\Main\Loader::includeModule('sale');

$name = strip_tags($_POST['NAME']);
$desc = strip_tags($_POST['DESCRIPTION']);

$arDeliveryFields = array(
 "LID" => "s1",
    "NAME" => $name,
    "ACTIVE" => "Y",
    "DESCRIPTION" => $desc,
    "SORT" => 100,
    "CURRENCY" => "RUB",
    "PRICE" => 0,
   "ORDER_CURRENCY" => "RUB",

    "CLASS_NAME" => "\Bitrix\Sale\Delivery\Services\Configurable",
///    "CLASS_NAME" => "\Sale\Handlers\Delivery\SimpleHandler",
);
$rsDelivery = \Bitrix\Sale\Delivery\Services\Manager::add($arDeliveryFields);

 
if (!$rsDelivery->isSuccess())
{
    $errors = $rsDelivery->getErrorMessages();

foreach($errors as $errors2) {
echo '
<div id="messages" class="alert alert-danger mt-3 text-center" >'.$errors2.'</div>
';
}

}

if ($rsDelivery->isSuccess())
{
    $id = $rsDelivery->getId();

if(empty($arUser[UF_DOSTAVKAS])) 
{
//$arUser[UF_DOSTAVKAS]
$edelivs[] = $id;
} else {
$edelivs = unserialize($arUser[UF_DOSTAVKAS]);
$edelivs[] = $id;


}

$edelivsnew = serialize($edelivs);


$userupd = new CUser;
$fieldsupd = Array( 
"UF_DOSTAVKAS" => $edelivsnew, 
); 
$userupd->Update($USER->GetID(), $fieldsupd);



		$rsUser = CUser::GetByID($cur_user_id);
		$arUser = $rsUser->Fetch();

echo '
<div id="change-delivery-id" class="alert text-center alert-success" >Изменения успешно применены!</div>
';

}
 


}
//////////////////





if(!empty($arUser[UF_DOSTAVKAS]))  {
$eids = unserialize($arUser[UF_DOSTAVKAS]);

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");




$db_dtype = CSaleDelivery::GetList(
    array(
            "ID" => "ASC",
        ),
    array(
            "ID" => $eids,
            "ACTIVE" => "Y"
        ),
    false,
    false,
    array()
);
if ($ar_dtype = $db_dtype->Fetch())
{
   echo "Вам пренадлежат следующие способы доставки:<br>";
   do
   {
if($ar_dtype[ID] == $_GET[ID]) {
$evars = $ar_dtype;
 $sel = 'selected'; } else { $sel = ''; }
echo '
<a class="edilvs '.$sel.'" href="?ID='.$ar_dtype[ID].'">
<div class=" bx-soa-pp-company col-lg-4 col-sm-4 col-xs-6 bx-selected">
<div class="bx-soa-pp-company-smalltitle">'.$ar_dtype["NAME"].' </div>

<div class="bx-soa-pp-company-graf-container">
 <div class="bx-soa-pp-company-image" ></div><div class="bx-soa-pp-delivery-cost"> '.CurrencyFormat($ar_dtype["PRICE"], $ar_dtype["CURRENCY"]).' руб.</div>
</div>
</div>
</a>
';
      
   }
   while ($ar_dtype = $db_dtype->Fetch());
}
else
{
///  echo "Доступных способов доставки не найдено<br>";
}
		
}

?>


		<div id="added-location-search" style="display: none;">
		</div>

		<br>
		<button style="display:none;" type="button" id="add-shop-delivery" name="add-shop-delivery" class="btn btn-secondary btn-sm">Добавить +</button>
		<div id="messages" class="alert alert-danger mt-3 text-center" style="display: none;">Произошли ошибки добавления. Попытайтесь позже.</div>
		
		<hr>


<div class="cabinet-user-block ">		
<div class="user-block-head">Наминование доставки:</div>								
<div class="long-input-block">					
<input required type="text" name="NAME" class="input-custom mid-input"  value="<? if(!empty($evars[NAME])) { echo $evars[NAME]; } else { ?>Доставка от продавца <? } ?>">									
</div>
</div>

<? /*
<div class="cabinet-user-block ">		
<div class="user-block-head">Стоимость доставки:</div>								
<div class="long-input-block">					
<input required type="number" name="PRICE" placeholder="100" class="input-custom mid-input"  value="">									
</div>
</div>
*/ ?>

<?
if($_GET[ID] >0) {
echo '<input type="hidden" value="'.$_GET[ID].'" name="IDEDIT">';
}
?>

<div class="cabinet-user-block">
				<div class="user-block-head">Информация о доставке:</div>

				<div class="long-input-block">
					<textarea required name="DESCRIPTION" class="input-custom w-100" placeholder="Описание"><? if(!empty($evars[DESCRIPTION])) { echo $evars[DESCRIPTION]; } else { ?><? } ?></textarea>
				</div>
			</div>

		<div class="text-center">
			<button type="submit" name="save-shop-delivery" value="Y" class="btn btn-primary">Сохранить</button>
		</div>
	</form>
</div>



<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>