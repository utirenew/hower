<? define('NEED_AUTH', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>


<div class="order-item">
	<div class="order-header">
		<span style="font-size: 25px;">Адреса доставки</span>
	</div>
	<br><br>
	<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		<?


		global $USER;
		$cur_user_id = $USER->GetID();


		if(key_exists('save-shop-delivery', $_POST) && key_exists('LOCATION', $_POST)){

			if($USER->Update($cur_user_id, ['UF_DELIVERY_ID' => $_POST['LOCATION']], false)){
				$class   = 'alert-success';
				$message = 'Изменения успешно применены!';
			}
			else{
				$class   = 'alert-danger';
				$message = 'Произошли ошибки:' . $USER->LAST_ERROR;
			}

			$change_delivery_id = true;
			echo '<div id="change-delivery-id" class="alert text-center ' . $class . '">' . $message . '</div>';
		}


		$rsUser = CUser::GetByID($cur_user_id);
		$arUser = $rsUser->Fetch();


		if(key_exists('UF_DELIVERY_ID', $arUser) && is_array($arUser['UF_DELIVERY_ID']) && !empty($arUser['UF_DELIVERY_ID'])){

			foreach($arUser['UF_DELIVERY_ID'] as $idx => $loc_id){

				$APPLICATION->IncludeComponent(
					"bitrix:sale.location.selector.search",
					"cabinet_city_search",
					array(
						"COMPONENT_TEMPLATE"         => "cabinet_city_search",
						"ID"                         => "$loc_id",
						"CODE"                       => "",
						"INPUT_NAME"                 => "LOCATION[$idx]",
						"PROVIDE_LINK_BY"            => "id",
						"JSCONTROL_GLOBAL_ID"        => "",
						"JS_CALLBACK"                => "",
						"FILTER_BY_SITE"             => "Y",
						"SHOW_DEFAULT_LOCATIONS"     => "Y",
						"CACHE_TYPE"                 => "A",
						"CACHE_TIME"                 => "36000000",
						"FILTER_SITE_ID"             => "s1",
						"INITIALIZE_BY_GLOBAL_EVENT" => "",
						"SUPPRESS_ERRORS"            => "N",
						"JS_CONTROL_GLOBAL_ID"       => "as",
					),
					false
				);
				echo '<br>';
			}
		}
		?>

		<div id="added-location-search" style="display: none;">
		</div>

		<br>
		<button type="button" id="add-shop-delivery" name="add-shop-delivery" class="btn btn-secondary btn-sm">Добавить +</button>
		<div id="messages" class="alert alert-danger mt-3 text-center" style="display: none;">Произошли ошибки добавления. Попытайтесь позже.</div>
		<br>
		<br>
		<hr>
		<div class="text-center">
			<button type="submit" name="save-shop-delivery" class="btn btn-primary">Сохранить</button>
		</div>
	</form>
</div>

<script type="text/javascript">

	BX.ready(function () {
		BX.bind(BX('add-shop-delivery'), 'click', get_search);
		<? if (isset($change_delivery_id)) : ?>
		$('#change-delivery-id').delay(3000).fadeOut();
		<? endif; ?>
	});


	function get_search() {
		BX.ajax({
			url: '/local/templates/ferma_24/components/bitrix/sale.location.selector.search/delivery_address_add/ajax.php',
			method: 'POST',
			data: {},
			dataType: 'html',
			timeout: 60,
			async: true,
			processData: false,
			scriptsRunFirst: false,
			emulateOnload: false,
			start: true,
			cache: false,
			onsuccess: function (response) {

				var container = $('#added-location-search');
				if (container.children('div').length) {
					container.append('<br>');
				}
				container.append(response).fadeIn();

				// var ob = BX.processHTML(response);
				// var html = BX('added-location-search').innerHTML;
				// BX('added-location-search').innerHTML = html + ob.HTML;
				// BX.ajax.processScripts(ob.SCRIPT);

			},
			onfailure: function (response) {
				$('#messages').fadeIn().delay(5000).fadeOut();
			}
		});
	}
</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
