<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>

<?$APPLICATION->IncludeComponent(
"bitrix:sale.personal.order",
"",
Array(
"ACTIVE_DATE_FORMAT" => "d.m.Y",
"ALLOW_INNER" => "N",
"CACHE_GROUPS" => "Y",
"CACHE_TIME" => "3600",
"CACHE_TYPE" => "A",
"CUSTOM_SELECT_PROPS" => array(""),
"DETAIL_HIDE_USER_INFO" => array("0"),
"HISTORIC_STATUSES" => array("F"),
"NAV_TEMPLATE" => "",
"ONLY_INNER_FULL" => "N",
"ORDERS_PER_PAGE" => "20",
"ORDER_DEFAULT_SORT" => "ID",
"PATH_TO_BASKET" => "/cabinet/seller/edit/catalog/cart/",
"PATH_TO_CATALOG" => "/buy/",
"PATH_TO_PAYMENT" => "/cabinet/seller/edit/order/payment/",
"REFRESH_PRICES" => "N",
"RESTRICT_CHANGE_PAYSYSTEM" => array("0"),
"SAVE_IN_SESSION" => "Y",
"SEF_MODE" => "N",
"SET_TITLE" => "Y",
"STATUS_COLOR_F" => "gray",
"STATUS_COLOR_N" => "green",
"STATUS_COLOR_P" => "yellow",
"STATUS_COLOR_PSEUDO_CANCELLED" => "red"
),
false,
Array(
'ACTIVE_COMPONENT' => 'Y'
)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
