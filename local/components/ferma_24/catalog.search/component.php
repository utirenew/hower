<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponent $this
 * @var array            $arParams
 * @var array            $arResult
 * @var string           $componentPath
 * @var string           $componentName
 * @var string           $componentTemplate
 * @global CDatabase     $DB
 * @global CUser         $USER
 * @global CMain         $APPLICATION
 */


use Bitrix\Main\Loader;

// Создание массива фильтра
global ${$this->arParams['FILTER_NAME']};
if(!empty($_POST['filter'])){
	${$this->arParams['FILTER_NAME']}['INCLUDE_SUBSECTIONS'] = 'Y';

	if(!empty($_POST['filter']['product'])){
		if(is_array($_POST['filter']['product'])){
			${$this->arParams['FILTER_NAME']}['SECTION_ID'] = $_POST['filter']['product'];
		}
		else{
			${$this->arParams['FILTER_NAME']}['SECTION_ID'] = array($_POST['filter']['product']);
		}
	}

	if(!empty($_POST['filter']['search_text'])){
		${$this->arParams['FILTER_NAME']}['NAME'] = '%' . $_POST['filter']['search_text'] . '%';
	}

	$_SESSION['filter'] = ${$this->arParams['FILTER_NAME']};
	LocalRedirect($APPLICATION->GetCurPage());
}
elseif(!empty($_SESSION['filter'])){
	${$this->arParams['FILTER_NAME']} = $_SESSION['filter'];
}
else{
	${$this->arParams['FILTER_NAME']} = array('INCLUDE_SUBSECTIONS' => 'Y');
}
$arResult['arFilter'] = ${$this->arParams['FILTER_NAME']};

if(!empty($arResult['arFilter']['SECTION_ID'])) $arResult['open_form'] = true;
else $arResult['open_form'] = false;

if(IS_HOME){
	$arResult['action'] = '/sell/';
}
else{
	$arResult['action'] = '/'.explode('/', $APPLICATION->GetCurPage())[1].'/';
}

// Создание массива $arResult
if(Loader::includeModule('iblock')){
	$res = CIBlock::GetList(Array(), Array(
		'TYPE' => $this->arParams['IBLOCK_TYPE'],
		'ID'   => $this->arParams['IBLOCK_ID'],
	));

	while($ar_res = $res->Fetch()){
		$arResult['IBLOCKS'][$ar_res['ID']] = $ar_res;

		// Получение категорий
		$arFilter = array(
			'IBLOCK_ID'     => $ar_res['ID'],
			'ACTIVE'        => 'Y',
			'GLOBAL_ACTIVE' => 'Y',
		);
		$rsSect   = CIBlockSection::GetList(array('depth_level' => 'asc', 'sort' => 'asc'), $arFilter, false, array('UF_*'));
		while($arSect = $rsSect->GetNext()){
			if(!empty($arSect['UF_ICO'])){
				$arSect['UF_ICO'] = CFile::GetFileArray($arSect['UF_ICO'])['SRC'];
			}

			// Разделение категорий на основные и под категории
			$arSect['checked'] = 'N';
			switch($arSect['DEPTH_LEVEL']){
				case 1:
					if(!empty($arResult['arFilter']['SECTION_ID']) && in_array($arSect['ID'], $arResult['arFilter']['SECTION_ID'])){
						$arSect['checked'] = 'Y';
					}
					$arResult['IBLOCKS'][$arSect['IBLOCK_ID']]['SECTIONS'][$arSect['ID']] = $arSect;
					break;

				case 2:
					if(!empty($arResult['arFilter']['SECTION_ID']) && in_array($arSect['ID'], $arResult['arFilter']['SECTION_ID'])){
						$arSect['checked']                                                                              = 'Y';
						$arResult['IBLOCKS'][$arSect['IBLOCK_ID']]['SECTIONS'][$arSect['IBLOCK_SECTION_ID']]['checked'] = 'Y';
					}
					$arResult['IBLOCKS'][$arSect['IBLOCK_ID']]['SECTIONS'][$arSect['IBLOCK_SECTION_ID']]['children'][$arSect['ID']] = $arSect;
					break;
			}
		}
	}
}

// Вывод шаблона
$this->IncludeComponentTemplate();