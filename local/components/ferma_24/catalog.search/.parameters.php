<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

if(!Loader::includeModule('iblock')) return;

$catalogIncluded = Loader::includeModule('catalog');
$iblockExists    = (!empty($arCurrentValues['IBLOCK_ID']) && (int)$arCurrentValues['IBLOCK_ID'] > 0);

$arIBlockType = CIBlockParameters::GetIBlockTypes();
$arIBlock     = array();
$iblockFilter = (!empty($arCurrentValues['IBLOCK_TYPE'])?array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y'):array('ACTIVE' => 'Y'));
$rsIBlock     = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while($arr = $rsIBlock->Fetch()){
	$arIBlock[$arr['ID']] = '[' . $arr['ID'] . '] ' . $arr['NAME'];
}
unset($arr, $rsIBlock, $iblockFilter);


$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_TYPE"     => array(
			"PARENT"            => "DATA_SOURCE",
			"NAME"              => "Тип инфоблока",
			"TYPE"              => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES"            => $arIBlockType,
			"REFRESH"           => "Y",
		),
		"IBLOCK_ID"       => array(
			"PARENT"            => "DATA_SOURCE",
			"NAME"              => "Инфоблок",
			"TYPE"              => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES"            => $arIBlock,
			"MULTIPLE"          => "Y",
		),
		"FILTER_NAME"     => array(
			"PARENT"  => "DATA_SOURCE",
			"NAME"    => "Имя выходящего массива для фильтрации",
			"TYPE"    => "STRING",
			"DEFAULT" => "arrFilter",
		),
		"SAVE_IN_SESSION" => array(
			"PARENT"  => "ADDITIONAL_SETTINGS",
			"NAME"    => "Сохранять установки фильтра в сессии пользователя",
			"TYPE"    => "CHECKBOX",
			"DEFAULT" => "N",
		),
	),
);



if(empty($arPrice)){
	unset($arComponentParameters["PARAMETERS"]["PRICE_CODE"]);
}