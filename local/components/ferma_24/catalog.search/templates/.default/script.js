$(function () {
	let search_form = $('#search-filter-form'),
		top_categories = $('.search-under-line input');

	check_options(search_form);

	$('.product-fermer').change(function () {
		let type = $(this).val(),
			id = $(this).data('id');
		search_form.prop('action', '/' + type + '/')
			.find('.under-search-wrap').removeClass('active').end()
			.find('#iblock-' + id).addClass('active');
		check_options(search_form);
	});

	$('.under-search-btn').click(function () {
		search_form.toggleClass('open-form');
		check_options(search_form);
	});

	top_categories.change(check_sub_categories);
});

function check_options(form = null) {
	form.find('.under-search-wrap input').prop('disabled', true);
	if (form.hasClass('open-form')) form.find('.under-search-wrap.active input').prop('disabled', false);
}

function check_sub_categories() {
	let id = $(this).val(),
		all_sub_categories = $('.search-sub-line'),
		sub_category = $('#sub-cat-' + id);

	all_sub_categories.addClass('d-none').find('input').prop('disabled', true);
	sub_category.removeClass('d-none').find('input').prop('disabled', false);
}