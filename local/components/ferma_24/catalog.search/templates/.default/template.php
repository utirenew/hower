<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="search-catalog">
	<div class="row">
		<div class="col-12">
			<? if(!IS_HOME): ?>
			<div class="search-catalog-form-wrapper">
				<? endif; ?>
				<form action="<?=$arResult['action']?>" method="post" class="search-form <?=$arResult['open_form'] && !IS_HOME?'open-form':''?>"
				      id="search-filter-form">
					<div class="search-up-line">
						<div class="search-block1">
							<span class="search-block1-text">Вы ищите:</span>
							<label class="d-flex align-items-center">
								<input name="object" type="radio" value="sell" data-id="5" class="d-none product-fermer" <?=$arParams['DEFAULT_IBLOCK_ID'] == 5?'checked':''?>>
								<span class="icon-fermer choose-radio"></span> <span class="search-item-name">Фермера</span></label>
							<label>
								<input name="object" type="radio" value="buy" data-id="6" class="d-none product-fermer" <?=$arParams['DEFAULT_IBLOCK_ID'] == 6?'checked':''?>>
								<span class="icon-product choose-radio"></span> <span class="search-item-name">Товар</span></label>
						</div>
						<div class="search-text-block">
							<div class="search-text-wrap">
								<input name="filter[search_text]" type="text" class="search-text" placeholder="Поиск">
								<button type="submit" class="search-btn">найти</button>
							</div>
						</div>
						<div class="under-search-btn"></div>
					</div>

					<? if(!empty($arResult['IBLOCKS'])): ?>
						<? foreach($arResult['IBLOCKS'] as $iblock): ?>
							<div class="under-search-wrap <?=$iblock['ID'] == $arParams['DEFAULT_IBLOCK_ID']?'active':''?>" id="iblock-<?=$iblock['ID']?>">
								<div class="search-under-line">
									<? if(!empty($iblock['SECTIONS'])): ?>
										<? foreach($iblock['SECTIONS'] as $section): ?>
											<label class="product-choose-wrap">
												<input name="filter[product]" type="radio" value="<?=$section['ID']?>"
												       class="hidden-input" <?=$section['checked'] == 'Y'?'checked':''?>>
												<span class="search-product">
											<? if(!empty($section['UF_ICO'])): ?>
												<img src="<?=$section['UF_ICO']?>" alt="" class="product-svg">
											<? endif; ?>
											<span class="product-choose-name"><?=$section['NAME']?></span>
										</span>
											</label>
										<? endforeach; ?>
										<? unset($section); ?>
									<? endif; ?>
								</div>

								<? if(!empty($iblock['SECTIONS'])): ?>
									<? foreach($iblock['SECTIONS'] as $section): ?>
										<? if(isset($section['children']) && is_array($section['children'])): ?>
											<div class="search-sub-line <?=$section['checked'] == 'Y'?'':'d-none'?>" id="sub-cat-<?=$section['ID']?>">
												<? foreach($section['children'] as $sub): ?>
													<label class="sub-product">
														<input type="checkbox" name="filter[product][]" value="<?=$sub['ID']?>" <?=$sub['checked'] == 'Y'?'checked':''?>>
														<span class="check_bg"></span><span class="sub-product-name"><?=$sub['NAME']?></span>
													</label>
												<? endforeach; ?>
												<? unset($sub); ?>
											</div>
										<? endif; ?>
									<? endforeach; ?>
									<? unset($section); ?>
								<? endif; ?>

							</div>
						<? endforeach; ?>
					<? endif; ?>

				</form>
				<? if(!IS_HOME): ?>
			</div>
		<? endif; ?>
		</div>
	</div>
</div>