$(function () {
	$('#ajaxContainer').on('click', '#last button', function (ev) {
		ev.preventDefault();

		if (typeof window.currentMap !== 'object') window.currentMap = {};
		currentMap.action = 'add';
		currentMap.id = +currentMap.lastCount + 1;

		jsYandexSearch_searchmap.obOut.innerHTML = '';
		jsYandexSearch_searchmap.clearSearchResults();
		$.fancybox.open({
			src: '#search_map',
			type: 'inline',
			opts: {
				closeExisting: false,
				smallBtn: false,
				hideScrollbar: true,
				touch: false
			}
		});
	});
});
$(document).on('afterClose.fb', function (e, instance, slide) {
	currentMap.action = '';
	currentMap.id = 0;
});