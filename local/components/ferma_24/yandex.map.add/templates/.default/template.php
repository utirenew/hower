<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="row" id="ajaxContainer">
	<? if(!empty($arResult['SHOPS'])): ?>
		<? foreach($arResult['SHOPS'] as $key => $val): ?>
			<? $APPLICATION->IncludeComponent("bitrix:map.yandex.view", "shop_maps", Array(
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA"      => serialize($val),
				"MAP_WIDTH"     => "auto",
				"MAP_HEIGHT"    => "200",
				"CONTROLS"      => array(),
				"OPTIONS"       => array(),
				"MAP_ID"        => "yam_" . $key,
			)); ?>
		<? endforeach; ?>
	<? endif; ?>
	<div class="col-3 mb-4" id="last">
		<button class="btn btn-primary mt-0">Добавить +</button>
	</div>
</div>
<div class="d-none">
	<div id="search_map">
		<? $APPLICATION->IncludeComponent("bitrix:map.yandex.search", "shops", Array(
			"INIT_MAP_TYPE"      => "MAP",    // Стартовый тип карты
			"MAP_WIDTH"          => "auto",    // Ширина карты
			"MAP_HEIGHT"         => "500",    // Высота карты
			"CONTROLS"           => array(    // Элементы управления
				0 => "ZOOM",
			),
			"OPTIONS"            => array(
				"ENABLE_DBLCLICK_ZOOM",
				"ENABLE_DRAGGING",
				"ENABLE_SCROLL_ZOOM",
			),
			"MAP_ID"             => "searchmap",    // Идентификатор карты
			"COMPONENT_TEMPLATE" => "shops",
			"API_KEY"            => "",    // Ключ API
		), false); ?>
	</div>
</div>