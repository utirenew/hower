<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */

/** @global CMain $APPLICATION */

use Bitrix\Main\UserTable;

$this->setFrameMode(false);


// if user has access
if($USER->IsAuthorized()){

	$uid = $USER->GetID();
	$res = UserTable::getList([
		'select' => ['UF_SHOPS', 'WORK_COMPANY', 'NAME', 'LAST_NAME', 'SECOND_NAME'],
		'filter' => ['ID' => $uid],
	])->fetch();

	$unser_res         = array_map('unserialize', $res['UF_SHOPS']);
	$full_name         = empty($res['WORK_COMPANY'])?trim(preg_replace('/\s+/  ', ' ', ($res['LAST_NAME'] . ' ' . $res['NAME']))):$res['WORK_COMPANY'];
	$arResult['SHOPS'] = [];

	foreach($unser_res as $cord){
		$arResult['SHOPS'][] = array(
			'yandex_lat'   => $cord['lat'],
			'yandex_lon'   => $cord['lon'],
			'yandex_scale' => 14,
			'PLACEMARKS'   => array(
				array(
					'LAT'  => $cord['lat'],
					'LON'  => $cord['lon'],
					'TEXT' => $full_name,
				),
			),
		);
	}

	$arResult['FULL_NAME'] = $full_name;

	$this->IncludeComponentTemplate();
}
else{
	$APPLICATION->AuthForm("");
}

