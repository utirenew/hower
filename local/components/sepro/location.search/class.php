<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Entity,
    \Bitrix\Main\UI;

class LocationSearchComponent extends CBitrixComponent
{
    protected $cacheKeys = array(); // кешируемые ключи arResult
    protected $checkParams = array(
        'LOCATION_TOKEN' => array('type' => 'string'),
        'JS_OBJECT_NAME' => array('type' => 'string'),
        'JS_CLASS_VARIANTS' => array('type' => 'string')
    );

    // ПРОВЕРЯЕТ ОБЯЗАТЕЛЬНЫЙ ВВОД ПАРАМЕТРОВ ИЗ $this->checkParams
    private function checkAutomaticParams()
    {
        foreach ($this->checkParams as $key => $params)
        {
            switch ($params['type'])
            {
                case 'int':

                    if (intval($this->arParams[$key]) <= 0)
                    {
                        throw new \Bitrix\Main\ArgumentTypeException($key, 'integer');
                    }
                    break;

                case 'string':

                    $this->arParams[$key] = htmlspecialchars(trim($this->arParams[$key]));
                    if (empty($this->arParams[$key]))
                    {
                        throw new \Bitrix\Main\ArgumentNullException($key);
                    }
                    break;

                case 'array':

                    if (!is_array($this->arParams[$key]))
                    {
                        throw new \Bitrix\Main\ArgumentTypeException($key, 'array');
                    }
                    break;

                default:
                    throw new \Bitrix\Main\ArgumentTypeException($key);
            }
        }
    }

    // ПОДКЛЮЧАЕМ ЯЗЫКОВЫЕ ФАЙЛЫ
    public function onIncludeComponentLang()
    {
        Loc::setCurrentLang(LANGUAGE_ID);
        $this->includeComponentLang(basename(__FILE__), LANGUAGE_ID);
        Loc::loadMessages(__FILE__);
    }

    // ПРОВЕРЯЕТ PARAMS
    public function onPrepareComponentParams($params)
    {
        $params = array_replace_recursive(
            $params,
            array(
                'JS_OBJECT_NAME' => trim((string) $params['JS_OBJECT_NAME']),
                'JS_CLASS_VARIANTS' => trim((string) $params['JS_CLASS_VARIANTS']),
                'CURRENT_LOCATION' => trim((string) $params['CURRENT_LOCATION']),
                'CACHE_TYPE' => $params['CACHE_TYPE'] == 'Y' ? 'Y' : 'N',
                'CACHE_ADDON' => is_array($params['CACHE_ADDON']) ? $params['CACHE_ADDON'] : array(),
                'CACHE_GROUPS' => $params['CACHE_GROUPS'] == 'Y' ? 'Y' : 'N',
                'CACHE_TIME' => intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600
            )
        );

        $this->arResult['PARAMS'] = $params;
        return $params;
    }

    // ВЫПОЛНЯЕТ ДЕЙСТВИЯ НАД КЕШИРОВАНИЕМ
    protected function executeProlog()
    {
        //$this->setFrameMode(true); // Голосуем за автокомпозит. Динамическая компонента

        $arCacheAddon = $this->arParams['CACHE_ADDON'];

        if($this->arParams['CACHE_GROUPS'] == 'Y')
        {
            $arCacheAddon[] = \Sepro\User::getInstance()->GetGroups();
        }

        if (!empty($arCacheAddon))
        {
            $this->cacheAddon = $arCacheAddon;
            $this->arResult['CACHE_ADDON'] = $arCacheAddon;
        }
    }

    // ПРОВЕРЯЕТ ЧИТАТЬ ДАННЫЕ ИЗ КЕША
    protected function readDataFromCache()
    {
        if($this->arParams['CACHE_TYPE'] == 'N' || !empty($_SERVER['HTTP_X_REQUESTED_WITH']))
        {
            return true;
        }

        return $this->StartResultCache(false, is_array($this->cacheAddon) ? $this->cacheAddon : array());
    }

    // КЭШИРУЕТ КЛЮЧИ МАССИВА
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && !empty($this->cacheKeys))
        {
            $this->arResult['CACHE_KEYS'] = $this->cacheKeys;
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    protected function getResult()
    {
        $this->arResult['COUNT'] = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array(
                '=TYPE_ID' => 5 // Город
            ),
            'select' => array('ID')
        ))->getSelectedRowsCount();

        if(!empty($this->arParams['CURRENT_LOCATION']))
        {
            $this->arResult['LOCATION'] = \Bitrix\Sale\Location\Search\Finder::find(array(
                'filter' => array(
                    '=PHRASE' => $this->arParams['CURRENT_LOCATION'],
                    '=NAME.LANGUAGE_ID' => 'ru'
                ),
                'select' => array(
                    'CODE',
                    'TYPE_ID',
                    'ID',
                    'DISPLAY' => 'NAME.NAME'
                )
            ))->fetch();
        }
    }

    protected function checkAjax()
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && \Sepro\User::getRequest('LOCATION_TOKEN') == $this->arParams['LOCATION_TOKEN'])
        {
            require_once DOCUMENT_ROOT.'/bitrix/components/bitrix/sale.location.selector.search/class.php';
            $this->arResult['LOCATIONS'] = \CBitrixLocationSelectorSearchComponent::processSearchRequestV2(array(
                'select' => array(
                    'CODE',
                    'TYPE_ID',
                    'VALUE' => 'ID',
                    'DISPLAY' => 'NAME.NAME'
                ),
                'additionals' => array('PATH'),
                'filter' => array(
                    '=PHRASE' => \Sepro\User::getRequest('VALUE'),
                    '=TYPE_ID' => 5,
                    '=NAME.LANGUAGE_ID' => 'ru'
                ),
                'PAGE_SIZE' => 50,
                'PAGE' => 0
            ));
        }
    }

    // ИНИЦИАЛИЗАЦИЯ КОМПОНЕНТЫ
    public function executeComponent()
    {
        try
        {
            $this->checkAutomaticParams();
            $this->executeProlog();
            $this->checkAjax();

            if ($this->readDataFromCache())
            {
                $this->getResult();
                $this->putDataToCache();
                $this->includeComponentTemplate();
            }
        }
        catch (Exception $e)
        {
            $this->AbortResultCache();
            \Sepro\Log::add2log($e->getMessage());
            echo $e->getMessage();
        }

        return true;
    }
}