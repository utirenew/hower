$(function () {
    sliderControl();
    picBlockSize();

    $('#mainSlider').slick({
        arrows: false,
        appendDots: $('.dots-block'),
        dots: true
    });
    $('.hot-product-slider').slick({
        arrows: false,
        dots: true,
        appendDots: $('.hot-product-dots'),
        slidesToShow: 4,
        slidesToScroll: 4
    });
    $('.product-slider').slick({
        arrows: false,
        dots: true,
        appendDots: $('.lider-dots'),
        slidesToShow: 4,
        slidesToScroll: 4
    });
    $('.new-product-slider').slick({
        arrows: false,
        dots: true,
        appendDots: $('.new-product-dots'),
        slidesToShow: 4,
        slidesToScroll: 4
    });
    $('.news-slider').slick({
        arrows: false,
        dots: true,
        appendDots: $('.news-dots'),
        slidesToShow: 3,
        slidesToScroll: 1
    });
    $('.seller-goods-slider').slick({
        slidesToShow: 3,
        arrows: true
    });

    $('.main-control').click(function (ev) {
        ev.preventDefault();
        $('#mainSlider').slick($(this).data('slide'));
    });
    $('.hot-control').click(function (ev) {
        ev.preventDefault();
        $('.hot-product-slider').slick($(this).data('slide'));
    });
    $('.lider-control').click(function (ev) {
        ev.preventDefault();
        $('.product-slider').slick($(this).data('slide'));
    });
    $('.new-product-control').click(function (ev) {
        ev.preventDefault();
        $('.new-product-slider').slick($(this).data('slide'));
    });
    $('.news-control').click(function (ev) {
        ev.preventDefault();
        $('.news-slider').slick($(this).data('slide'));
    });
});

function sliderControl() {
    let screenWidth = $(window).innerWidth(),
        containerWidth = $('.container').innerWidth(),
        offset = (screenWidth - containerWidth) / 2;
    $('.slider-control-block').css({"left": (offset + 15)});
    // console.log(screenWidth,offset);
}
function picBlockSize() {
    let widthBlock = $('.product-pic').innerWidth(),
        heightBlock = $('.product-pic').css({"height": widthBlock}),
        bigPicWidth = $('.big-pic').innerWidth(),
        bigHeightBlock = $('.big-pic').css({"height": bigPicWidth}),
        heightBtn = $('.region-btn').innerHeight(),
        widthBtn = $('.region-btn').css({"width": heightBtn});

}


$(document).ready(function(){
    var linksCollection = $('[data-jquery-selector="link-anchor-behavior-disable"]');

    linksCollection.each(function (index, el) {
        $(el).click(function (e) {
            e.preventDefault();
        });
    });
});
