
function add2wish(p_id, pp_id, p, name, dpu, userRole, th){
        $.ajax({
            type: "POST",
            url: "/local/templates/ferma_24/ajax/wishlist/add.php",
            data: {
                "p_id": p_id,
                "pp_id": pp_id,
                "p": p,
                "name": name,
                "dpu": dpu,
            },
            success: function(result){
                let resultParse = JSON.parse(result),
                    favorites = $('.header .favorites');

                // console.log('resultParse.debug = ', resultParse.debug);
                // console.log('resultParse = ', resultParse);

                $(th).removeClass('js-wishbtn-add');
                $(th).addClass('js-wishbtn-delete');
                $(th).addClass('in_wishlist');
                $(th).attr('onclick', "delete4wish('" + resultParse.id + "', this)");

                if (favorites.prop("tagName") === 'SPAN') {
                    favorites.remove();
                    $('.header .favorites-wrapper').append('<a href="/cabinet/' + userRole + '/edit/catalog/wishlist/" class="favorites"><span class="have-favorites"></span></a>');
                }
            }
        });
    };

function delete4wish(p_id, th){
        $.ajax({
            type: "POST",
            url: "/local/templates/ferma_24/ajax/wishlist/delete.php",
            data: "p_id=" + p_id,
            success: function(result){
                var resultParse = JSON.parse(result);
                var data = resultParse.itemData;

                // console.log('resultParse = ', resultParse);

                $(th).removeClass('js-wishbtn-delete');
                $(th).removeClass('in_wishlist');
                $(th).addClass('js-wishbtn-add');
                $(th).attr('onclick', "add2wish('" + data.PRODUCT_ID + "', '" + data.PRODUCT_PRICE_ID + "', '" + data.PRICE + "', '" + data.NAME + "', '" + data.DETAIL_PAGE_URL + "', '" + data.USER_ROLE + "', this)");

                if (resultParse.count === 0) {
                    $('.header .favorites').remove();
                    $('.header .favorites-wrapper').append('<span class="favorites empty"><span class="have-favorites"></span></span>');
                }

                $(th).parents('.product-item').find('.js-wishbtn-add').removeClass('in_wishlist');
            }
        });
    };
