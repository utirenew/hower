<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix Framework
 * @package    bitrix
 * @subpackage main
 * @copyright  2001-2013 Bitrix
 */

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 */

$bWasSelect = false;
?>

<div class="col-sm-6 col-12 mb-3 middle-column">
	<div class="font-weight-bold mb-2"><?=$arParams["arUserField"]["LIST_COLUMN_LABEL"]?></div>
	<input type="hidden" name="<?=$arParams["arUserField"]["FIELD_NAME"]?>" value="">
	<?
	if($arParams["arUserField"]["SETTINGS"]["DISPLAY"] == "CHECKBOX"): ?>
		<? foreach($arParams["arUserField"]["USER_TYPE"]["FIELDS"] as $key => $val): ?>
			<?
			$bSelected  = in_array($key, $arResult["VALUE"]) && ((!$bWasSelect) || ($arParams["arUserField"]["MULTIPLE"] == "Y"));
			$bWasSelect = $bWasSelect || $bSelected;
			?>

			<? if($arParams["arUserField"]["MULTIPLE"] == "Y"): ?>
				<label>
					<input type="checkbox" value="<?=$key?>" name="<?=$arParams["arUserField"]["FIELD_NAME"]?>" <?=($bSelected?"checked":"")?> ><?=$val?>
				</label><br/>
			<? else: ?>
				<? if($val != 'нет'): ?>
					<label>
						<input type="radio" value="<?=$key?>" name="<?=$arParams["arUserField"]["FIELD_NAME"]?>" <?=($bSelected?"checked":"")?>>
						<?=$val?>
					</label><br/>
				<? endif; ?>
			<? endif; ?>

		<? endforeach; ?>
	<? else: ?>
		<select class="bx-user-field-enum" name="<?=$arParams["arUserField"]["FIELD_NAME"]?>"
			<? if($arParams["arUserField"]["SETTINGS"]["LIST_HEIGHT"] > 1): ?>
				size="<?=$arParams["arUserField"]["SETTINGS"]["LIST_HEIGHT"]?>"
			<? endif; ?>
			<? if($arParams["arUserField"]["MULTIPLE"] == "Y"): ?>
				multiple="multiple"
			<? endif; ?>
		>
			<?
			$values = $arResult["VALUE"];
			if(!is_array($values)) $values = array($values);
			$values = array_map("strval", $values);
			if(count($values) > 1) $values = array_filter($values);
			?>
			<? foreach($arParams["arUserField"]["USER_TYPE"]["FIELDS"] as $key => $val): ?>
				<?
				$bSelected  = in_array(strval($key), $values, true) && ((!$bWasSelect) || ($arParams["arUserField"]["MULTIPLE"] == "Y"));
				$bWasSelect = $bWasSelect || $bSelected;

				?>
				<option value="<?=$key?>"<?=($bSelected?" selected":"")?>><?=$val?></option>
			<? endforeach; ?>
		</select>
	<? endif; ?>

</div>