<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 */
//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
global $APPLICATION;

//delayed function must return a string
if(empty($arResult)) return "";

$strReturn = '<div class="breadcrumb-block">
		<div class="header-back">
			<div class="small-cart-back"></div>
		</div>
		<div class="breadcrumb-custom">
			<div class="container">
				<div class="row">
					<div class="col-12">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++){
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0?'<i class="fa fa-angle-right"></i>':'');

	if($arResult[$index]["LINK"] <> "") $strReturn .= '<span class="crumb-wrap"><a href="' . $arResult[$index]["LINK"] . '" class="crumb-link">' . $title . '</a></span>';
	else $strReturn .= '<span class="crumb-wrap"><span class="current-crumb">' . $title . '</span></span>';

}

$strReturn .= '</div></div></div></div></div>';

return $strReturn;
