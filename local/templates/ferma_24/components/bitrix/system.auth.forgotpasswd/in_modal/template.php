<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

ShowMessage($arParams["~AUTH_RESULT"]);
?>
<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

	<div class="container-fluid input-block-modal">
		<div class="row">
			<? if(strlen($arResult["BACKURL"]) > 0): ?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>"/>
			<? endif; ?>
			<input type="hidden" name="AUTH_FORM" value="Y">
			<input type="hidden" name="TYPE" value="SEND_PWD">

			<div class="col-12 mb-3 middle-column"><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></div>

			<div class="col-sm-6 col-12 mb-3 left-column">
				<input type="text" name="USER_EMAIL" class="form-control" maxlength="255" placeholder="<?=GetMessage("AUTH_EMAIL")?>" required>
			</div>
			<div class="col-sm-6 col-12 mb-3 right-column">
				<input type="submit" name="send_account_info" class="modal-btn" value="<?=GetMessage("AUTH_SEND")?>"/>
			</div>

			<div class="col-sm-6 col-12 mb-3 mx-auto middle-column">
				<a data-fancybox data-src="#enter" href="javascript:"><?=GetMessage("AUTH_LOGIN_BUTTON")?></a> <br>
			</div>
			<div class="col-sm-6 col-12 mb-3 mx-auto middle-column">
				<a data-fancybox data-src="#register" href="javascript:"><?=GetMessage("AUTH_REGISTER")?></a>
			</div>
		</div>
	</div>
</form>
