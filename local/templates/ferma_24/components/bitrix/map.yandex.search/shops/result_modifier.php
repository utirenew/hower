<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\UserTable;

$uid = $USER->GetID();
$res = UserTable::getList([
	'select' => ['WORK_COMPANY', 'NAME', 'LAST_NAME', 'SECOND_NAME'],
	'filter' => ['ID' => $uid],
])->fetch();

$arResult['FULL_NAME'] = empty($res['WORK_COMPANY'])?trim(preg_replace('/\s+/  ', ' ', ($res['LAST_NAME'] . ' ' . $res['NAME']))):$res['WORK_COMPANY'];