<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Loader;

$response = ['success' => false];

if(Loader::includeModule('sepro.helper')){
	if(!empty($_REQUEST['lat']) && !empty($_REQUEST['lon']) && is_numeric($_REQUEST['lat']) && is_numeric($_REQUEST['lon'])){
		$value  = serialize(['lat' => $_REQUEST['lat'], 'lon' => $_REQUEST['lon']]);
		$action = empty($_REQUEST['action'])?'add':$_REQUEST['action'];
		$key    = empty($_REQUEST['key'])?'':$_REQUEST['key'];
	}
	elseif(!empty($_REQUEST['key']) && !empty($_REQUEST['action']) && $_REQUEST['action'] === 'del'){
		$value  = true;
		$action = $_REQUEST['action'];
		$key    = $_REQUEST['key'];
	}
	else{
		$value  = false;
		$action = 'add';
		$key    = '';
	}

	$result = \Sepro\User::setShop($action, $value, $key);
	if($result) $response['text'] = $result;
	else $response['success'] = true;
}


return print json_encode($response);