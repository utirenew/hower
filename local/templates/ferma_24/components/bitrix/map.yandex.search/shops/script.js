let JCBXYandexSearch = function (map_id, obOut, jsMess) {
	this.map_id = map_id;
	this.map = GLOBAL_arMapObjects[this.map_id];

	this.update = false;
	this.fullName = '';

	this.obOut = obOut;

	if (null == this.map)
		return false;

	this.arSearchResults = [];
	this.jsMess = jsMess;
};

JCBXYandexSearch.prototype.__searchResultsLoad = function (res) {
	if (null == this.obOut) return;

	let i,
		str = '',
		obList = null,
		len = res.geoObjects.getLength();

	this.obOut.innerHTML = '';
	this.clearSearchResults();


	if (len > 0) {
		obList = document.createElement('UL');
		obList.className = 'bx-yandex-search-results';

		str += this.jsMess.mess_search + ': <b>' + len + '</b> ' + this.jsMess.mess_found + '.';

		// i rly dont khow why this doesnt work in one loop ;-(
		for (i = 0; i < len; i++) {
			this.arSearchResults.push(res.geoObjects.get(i));
		}

		for (i = 0; i < this.arSearchResults.length; i++) {
			this.map.geoObjects.add(this.arSearchResults[i]);
			obList.appendChild(BX.create('LI', {
				children: [
					BX.create('A', {
						attrs: {
							href: "javascript:void(0)"
						},
						events: {
							click: BX.proxy(this.__showSearchResult, this.arSearchResults[i])
						},
						text: this.arSearchResults[i].properties.get('metaDataProperty').GeocoderMetaData.text
					})
				]
			}));
		}
	}
	else {
		str = this.jsMess.mess_search_empty;
	}

	this.obOut.innerHTML = str;

	if (null != obList)
		this.obOut.appendChild(obList);
};

// called in the context of ymaps.Placement.
JCBXYandexSearch.prototype.__showSearchResult = function () {
	let obList = document.getElementById('results_searchmap'),
		button = document.getElementById('save_button');

	if (button !== null) button.remove();

	obList.append(BX.create('button', {
			attrs: {
				class: 'btn btn-primary',
				id: 'save_button'
			},
			events: {click: BX.proxy(saveNode, this)},
			text: 'Сохранить'
		})
	);
	this.balloon.open();
	this.getMap().panTo(this.geometry.getCoordinates());
};
JCBXYandexSearch.prototype.searchByAddress = function (str) {
	str = str.replace(/^[\s\r\n]+/g, '').replace(/[\s\r\n]+$/g, '');

	if (str === '')
		return;

	ymaps.geocode(str).then(
		BX.proxy(this.__searchResultsLoad, this),
		this.handleError
	);
};

JCBXYandexSearch.prototype.handleError = function (error) {
	alert(this.jsMess.mess_error + ': ' + error.message);
};

JCBXYandexSearch.prototype.clearSearchResults = function () {
	for (let i = 0; i < this.arSearchResults.length; i++) {
		this.arSearchResults[i].balloon.close();
		this.map.geoObjects.remove(this.arSearchResults[i]);
		delete this.arSearchResults[i];
	}

	this.arSearchResults = [];
};

saveNode = function () {
	let coord = this.geometry.getCoordinates(),
		action = currentMap.action,
		id = currentMap.id,
		text = jsYandexSearch_searchmap.fullName;

	$.post(
		'/local/templates/ferma_24/components/bitrix/map.yandex.search/shops/ajax.php',
		{
			action: action,
			lat: coord[0],
			lon: coord[1],
			key: id,
		},
		function (response) {
			response = JSON.parse(response);
			if (response.success) {
				jsYandexSearch_searchmap.obOut.innerHTML = '';
				jsYandexSearch_searchmap.clearSearchResults();
				$.fancybox.close();
				init_yam(id, coord, text)
			}
		}
	);
};


function init_yam(id, coordinates = [], text = '') {
	let node, map, mark,
		newBlock = true,
		mapId = 'yam_' + id;

	if (!id) return;
	if (!window.ymaps) return;
	if (!window.GLOBAL_arMapObjects) window.GLOBAL_arMapObjects = {};
	if (typeof window.GLOBAL_arMapObjects['yam_' + id] !== "undefined") {
		window.GLOBAL_arMapObjects[mapId].destroy();
		newBlock = false;
	}

	if (newBlock) {
		document.getElementById('last').before(BX.create('div', {
			attrs: {class: 'col-3 mb-4'},
			children: [
				BX.create('div', {
					attrs: {class: 'bx-yandex-view-layout'},
					children: [
						BX.create('div', {
							attrs: {class: 'bx-yandex-view-map'},
							children: [
								BX.create('div', {
									attrs: {
										id: 'BX_YMAP_yam_' + id,
										class: 'bx-yandex-map'
									},
									style: {
										height: '200px',
										width: 'auto'
									}
								})
							]
						})
					]
				}),
				BX.create('div', {
					attrs: {class: 'btn-group w-100'},
					children: [
						BX.create('button', {
							attrs: {class: 'btn btn-secondary btn-sm mt-2'},
							text: 'Изменить'
						}),
						BX.create('button', {
							attrs: {class: 'btn btn-danger btn-sm mt-2'},
							text: 'Удалить'
						}),
					]
				})
			]
		}));
	}

	node = BX("BX_YMAP_yam_" + id);
	node.innerHTML = '';
	map = window.GLOBAL_arMapObjects[mapId] = new ymaps.Map(node, {
		center: coordinates,
		zoom: 14,
		type: 'yandex#map'
	});

	if (map.behaviors.isEnabled("scrollZoom"))
		map.behaviors.disable("scrollZoom");
	if (map.behaviors.isEnabled("dblClickZoom"))
		map.behaviors.disable("dblClickZoom");
	if (map.behaviors.isEnabled("drag"))
		map.behaviors.disable("drag");
	if (map.behaviors.isEnabled("rightMouseButtonMagnifier"))
		map.behaviors.disable("rightMouseButtonMagnifier");

	mark = new ymaps.Placemark(coordinates, {balloonContent: text});
	map.geoObjects.add(mark);

	if (window.GLOBAL_arMapObjects[mapId] !== undefined)
		window.GLOBAL_arMapObjects[mapId].container.fitToViewport();
}