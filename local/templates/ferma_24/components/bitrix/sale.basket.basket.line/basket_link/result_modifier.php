<?php

    $basket_line_id_selection = array();
foreach ($arResult['CATEGORIES']['READY'] as $item) {
    $basket_line_id_selection[] = $item['PRODUCT_ID'];
}

$db_res = CCatalogProduct::GetList(
                array(
                    'ID' => 'DESC',
                ),
                array(
                    "@ID" => $basket_line_id_selection,
                ),
                false,
                false,
                array(
                    'ID',
                    'QUANTITY',
                    'MEASURE'
                )
        );

    $items_base_quantity = array();
while ($ar_res = $db_res->Fetch())
{
    $items_base_quantity[$ar_res['ID']] = $ar_res;
}

foreach ($arResult['CATEGORIES']['READY'] as $key=>$item) {
    $arResult['CATEGORIES']['READY'][$key]['QUANTITY_BASE'] = $items_base_quantity[$item['PRODUCT_ID']]['QUANTITY'];
    $arResult['CATEGORIES']['READY'][$key]['MEASURE_ID'] = $items_base_quantity[$item['PRODUCT_ID']]['MEASURE'];
}

    $measure_selection = array();
if(CModule::IncludeModule("catalog"))
{
    $measure_list = CCatalogMeasure::getList();
    while($ar_result = $measure_list->GetNext())
    {
        $measure_selection[$ar_result['~ID']] = $ar_result['MEASURE_TITLE'];
    }
}
$arParams['MEASURE_SELECTION'] = $measure_selection;

foreach ($arResult['CATEGORIES']['READY'] as $key2=>$item2) {
    $arResult['CATEGORIES']['READY'][$key2]['MEASURE_TITLE_FULL'] = $measure_selection[$arResult['CATEGORIES']['READY'][$key2]['MEASURE_ID']];
}
