<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];
?>

<? if($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY']))): ?>
	<div data-role="basket-item-list" class="">
		<div style="display:none" data-wrapper="basket-item-list-data">
			<span data-item="price">
				<? if($arParams['SHOW_TOTAL_PRICE'] == 'Y'): ?>
					<?=$arResult['TOTAL_PRICE']?>
				<? endif; ?>
			</span>
			<span data-item="quantity">
				<? if($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')): ?>
					<?=$arResult['NUM_PRODUCTS']?>
				<? endif; ?>
			</span>
		</div>

		<?
		if($arParams["POSITION_FIXED"] == "Y"):?>
			<div id="<?=$cartId?>status" class="bx-basket-item-list-action" onclick="<?=$cartId?>.toggleOpenCloseCart()"><?=GetMessage("TSB1_COLLAPSE")?></div>
		<? endif ?>

		<div id="<?=$cartId?>products" class="bx-basket-item-list-container">
			<? foreach($arResult["CATEGORIES"] as $category => $items): ?>
				<? if(empty($items) || $category === 'DELAY') continue; ?>

				<? foreach($items as $v): ?>
					<div class="small-cart-item  js-small-cart-item">
						<div class="small-cart-pic">
							<? if($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]): ?>
								<? if($v["DETAIL_PAGE_URL"]): ?>
									<a href="<?=$v["DETAIL_PAGE_URL"]?>">
										<img src="<?=$v["PICTURE_SRC"]?>" alt="<?=$v["NAME"]?>">
									</a>
								<? else: ?>
									<img src="<?=$v["PICTURE_SRC"]?>" alt="<?=$v["NAME"]?>"/>
								<? endif ?>
							<? endif ?>
						</div>
						<div class="small-cart-name">
							<? if($v["DETAIL_PAGE_URL"]): ?>
								<a href="<?=$v["DETAIL_PAGE_URL"]?>"><?=$v["NAME"]?></a>
							<? else: ?>
								<?=$v["NAME"]?>
							<? endif ?>
						</div>
						<div class="small-cart-price-wrapper">
							<? if($arParams["SHOW_PRICE"] == "Y"): ?>
								<div class="small-cart-price"><?=$v["PRICE_FMT"]?></div>
								<div class="price-text">/ за <?=$v["MEASURE_TITLE_FULL"]?></div>
							<? endif ?>
						</div>
						<div class="cart-count-block">
							<div class="button-item-counter minus js-count-minus-basket-line"></div>
							<?// data-basket-item-quantity-base="<?=$v['QUANTITY_BASE']" устанавливает максимально допустимое значение данного товара в корзине пока просто меняю на 100?>
<!--							<input data-basket-item-quantity-base="--><?//=$v['QUANTITY_BASE']?><!--" data-basket-item-id="--><?//=$v['ID']?><!--" value="--><?php //echo $v['QUANTITY']; ?><!--" type="text"-->
							<input data-basket-item-quantity-base="100" data-basket-item-id="<?=$v['ID']?>" value="<?php echo $v['QUANTITY']; ?>" type="text"
							       class="cart-count-value" size="1"/>
							<div class="button-item-counter plus js-count-plus-basket-line"></div>
						</div>
						<div data-basket-item-id="<?=$v['ID']?>" class="close-btn js-close-btn"></div>
					</div>
				<? endforeach ?>
			<? endforeach ?>
		</div>

		<? if($arParams["PATH_TO_ORDER"] && $arResult["CATEGORIES"]["READY"]): ?>
			<div class="small-total-line">
				<div class="total-price">
					<? if($arParams['SHOW_TOTAL_PRICE'] == 'Y'): ?>
						<?=$arResult['TOTAL_PRICE']?>
					<? endif; ?>
				</div>
				<? if(!$arResult["DISABLE_USE_BASKET"]): ?>
					<a class="in-cart-btn" href="<?=$arParams['PATH_TO_BASKET']?>"><?=GetMessage('TSB1_CART')?></a>
				<? endif; ?>
				<a href="<?=$arParams["PATH_TO_ORDER"]?>" class="pay-btn"><?=GetMessage("TSB1_2ORDER")?></a>
			</div>
		<? endif ?>
	</div>

	<script>
		BX.ready(function () {
			<?=$cartId?>.
			fixCart();
		});
	</script>
<? endif; ?>
