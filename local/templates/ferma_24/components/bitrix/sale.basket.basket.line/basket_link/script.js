'use strict';

function BitrixSmallCart(){}

BitrixSmallCart.prototype = {

	userRole: 'guest',

	activate: function ()
	{
		this.cartElement = BX(this.cartId);
		this.fixedPosition = this.arParams.POSITION_FIXED == 'Y';
		if (this.fixedPosition)
		{
			this.cartClosed = true;
			this.maxHeight = false;
			this.itemRemoved = false;
			this.verticalPosition = this.arParams.POSITION_VERTICAL;
			this.horizontalPosition = this.arParams.POSITION_HORIZONTAL;
			this.topPanelElement = BX("bx-panel");

			this.fixAfterRender(); // TODO onready
			this.fixAfterRenderClosure = this.closure('fixAfterRender');

			var fixCartClosure = this.closure('fixCart');
			this.fixCartClosure = fixCartClosure;

			if (this.topPanelElement && this.verticalPosition == 'top')
				BX.addCustomEvent(window, 'onTopPanelCollapse', fixCartClosure);

			var resizeTimer = null;
			BX.bind(window, 'resize', function() {
				clearTimeout(resizeTimer);
				resizeTimer = setTimeout(fixCartClosure, 200);
			});
		}
		this.setCartBodyClosure = this.closure('setCartBody');
		BX.addCustomEvent(window, 'OnBasketChange', this.closure('refreshCart', {}));

		var objCurrent = this;

		$('.js-count-minus-basket-line').click(function (eventObject) {
	        objCurrent.handlerBasketBasketLine('minus', eventObject);
	    });
	    $('.js-count-plus-basket-line').click(function (eventObject) {
	        objCurrent.handlerBasketBasketLine('plus', eventObject);
	    });
		$('.js-small-cart-item .js-close-btn').click(function (eventObject) {
		  	objCurrent.handlerBasketBasketRemoveItem(eventObject);
		});
		$('.cart-body *').click(function (eventObject) {
			// console.log(eventObject);
			$('#bootstrap-4-dropdown-keep-open').click();
		});
		$('.cart-body a').click(function (eventObject) {
			window.location.assign(eventObject.target.href);
		});
		$('#dropdownCartBodyWrapper').on('show.bs.dropdown', function () {
			$('.small-cart-back').addClass('d-block');
		})
		$('#dropdownCartBodyWrapper').on('hidden.bs.dropdown', function () {
			$('.small-cart-back').removeClass('d-block');
		})
	},

	fixAfterRender: function ()
	{
		this.statusElement = BX(this.cartId + 'status');
		if (this.statusElement)
		{
			if (this.cartClosed)
				this.statusElement.innerHTML = this.openMessage;
			else
				this.statusElement.innerHTML = this.closeMessage;
		}
		this.productsElement = BX(this.cartId + 'products');
		this.fixCart();
	},

	closure: function (fname, data)
	{
		var obj = this;
		return data
			? function(){obj[fname](data)}
			: function(arg1){obj[fname](arg1)};
	},

	toggleOpenCloseCart: function ()
	{
		if (this.cartClosed)
		{
			BX.removeClass(this.cartElement, 'bx-closed');
			BX.addClass(this.cartElement, 'bx-opener');
			this.statusElement.innerHTML = this.closeMessage;
			this.cartClosed = false;
			this.fixCart();
		}
		else // Opened
		{
			BX.addClass(this.cartElement, 'bx-closed');
			BX.removeClass(this.cartElement, 'bx-opener');
			BX.removeClass(this.cartElement, 'bx-max-height');
			this.statusElement.innerHTML = this.openMessage;
			this.cartClosed = true;
			var itemList = this.cartElement.querySelector("[data-role='basket-item-list']");
			if (itemList)
				itemList.style.top = "auto";
		}
		setTimeout(this.fixCartClosure, 100);
	},

	setVerticalCenter: function(windowHeight)
	{
		var top = windowHeight/2 - (this.cartElement.offsetHeight/2);
		if (top < 5)
			top = 5;
		this.cartElement.style.top = top + 'px';
	},

	fixCart: function()
	{
		// set horizontal center
		if (this.horizontalPosition == 'hcenter')
		{
			var windowWidth = 'innerWidth' in window
				? window.innerWidth
				: document.documentElement.offsetWidth;
			var left = windowWidth/2 - (this.cartElement.offsetWidth/2);
			if (left < 5)
				left = 5;
			this.cartElement.style.left = left + 'px';
		}

		var windowHeight = 'innerHeight' in window
			? window.innerHeight
			: document.documentElement.offsetHeight;

		// set vertical position
		switch (this.verticalPosition) {
			case 'top':
				if (this.topPanelElement)
					this.cartElement.style.top = this.topPanelElement.offsetHeight + 5 + 'px';
				break;
			case 'vcenter':
				this.setVerticalCenter(windowHeight);
				break;
		}

		// toggle max height
		if (this.productsElement)
		{
			var itemList = this.cartElement.querySelector("[data-role='basket-item-list']");
			if (this.cartClosed)
			{
				if (this.maxHeight)
				{
					BX.removeClass(this.cartElement, 'bx-max-height');
					if (itemList)
						itemList.style.top = "auto";
					this.maxHeight = false;
				}
			}
			else // Opened
			{
				if (this.maxHeight)
				{
					if (this.productsElement.scrollHeight == this.productsElement.clientHeight)
					{
						BX.removeClass(this.cartElement, 'bx-max-height');
						if (itemList)
							itemList.style.top = "auto";
						this.maxHeight = false;
					}
				}
				else
				{
					if (this.verticalPosition == 'top' || this.verticalPosition == 'vcenter')
					{
						if (this.cartElement.offsetTop + this.cartElement.offsetHeight >= windowHeight)
						{
							BX.addClass(this.cartElement, 'bx-max-height');
							if (itemList)
								itemList.style.top = 82+"px";
							this.maxHeight = true;
						}
					}
					else
					{
						if (this.cartElement.offsetHeight >= windowHeight)
						{
							BX.addClass(this.cartElement, 'bx-max-height');
							if (itemList)
								itemList.style.top = 82+"px";
							this.maxHeight = true;
						}
					}
				}
			}

			if (this.verticalPosition == 'vcenter')
				this.setVerticalCenter(windowHeight);
		}
	},

	refreshCart: function (data)
	{
		if (this.itemRemoved)
		{
			this.itemRemoved = false;
			return;
		}
		data.sessid = BX.bitrix_sessid();
		data.siteId = this.siteId;
		data.templateName = this.templateName;
		data.arParams = this.arParams;
		BX.ajax({
			url: this.ajaxPath,
			method: 'POST',
			dataType: 'html',
			data: data,
			onsuccess: this.setCartBodyClosure
		});
	},

	setCartBody: function (result)
	{
		if (this.cartElement)
			this.cartElement.innerHTML = result.replace(/#CURRENT_URL#/g, this.currentUrl);
		if (this.fixedPosition)
			setTimeout(this.fixAfterRenderClosure, 100);

		var elClassDisplayNone  = 'd-none';
		var elClassDisplayBlock = 'd-block';
		var elHeaderPrice 		= $('.header .cart-price');
		var elHeaderQuantity 	= $('.header .cart-item-number');
		var elHeaderBody 		= $('.header .cart-body');
		var elBackground 		= $('.small-cart-back');
		var elHeaderCartHeader  = $('.header .cart-header');

		var dataWrapper = $(result)
							.find('div[data-wrapper="basket-item-list-data"]');
		var totalPrice = dataWrapper
							.find('span[data-item="price"]')
							.text();
		var totalQuantity = dataWrapper
							.find('span[data-item="quantity"]')
							.text();

		if ( totalPrice === '' || totalQuantity === '' ) {
			elHeaderPrice.addClass(elClassDisplayNone);
			elHeaderQuantity.addClass(elClassDisplayNone);

			elHeaderBody.addClass(elClassDisplayNone);
			elBackground.removeClass(elClassDisplayBlock);

			elHeaderCartHeader.removeAttr('data-toggle');
		} else {
			elHeaderBody.removeClass(elClassDisplayNone);
			elHeaderCartHeader.attr('data-toggle', 'dropdown');

			$('.cart-body *').click(function (eventObject) {
				// console.log(eventObject);
				$('#bootstrap-4-dropdown-keep-open').click();
			});
			$('.cart-body a').click(function (eventObject) {
				window.location.assign(eventObject.target.href);
			});

			var objCurrent = this;

			$('.js-count-minus-basket-line').unbind();
			$('.js-count-plus-basket-line').unbind();
			$('.js-small-cart-item .js-close-btn').unbind();

			$('.js-count-minus-basket-line').click(function (eventObject) {
				objCurrent.handlerBasketBasketLine('minus', eventObject);
			});
			$('.js-count-plus-basket-line').click(function (eventObject) {
				objCurrent.handlerBasketBasketLine('plus', eventObject);
			});
			$('.js-small-cart-item .js-close-btn').click(function (eventObject) {
			  	objCurrent.handlerBasketBasketRemoveItem(eventObject);
			});

			elHeaderPrice.removeClass(elClassDisplayNone);
			elHeaderQuantity.removeClass(elClassDisplayNone);

			if ( elHeaderPrice.length !== 0 ) {
				 elHeaderPrice.text(totalPrice);
			}
			if ( elHeaderQuantity.length !== 0 ) {
				 elHeaderQuantity.text(totalQuantity);
			}
		}
	},

	removeItemFromCart: function (id)
	{
		this.refreshCart ({sbblRemoveItemFromCart: id});
		this.itemRemoved = true;
		BX.onCustomEvent('OnBasketChange');
	},

	handlerBasketBasket: function (actionName, id, value) {
		var value = parseInt(value, 10);

		switch (actionName) {
			case 'minus':
				$('#basket-item-quantity-' + id).val(value + 1);
				$('#basket-item-quantity-' + id).prev().click();
			break;
			case 'plus':
				$('#basket-item-quantity-' + id).val(value - 1);
				$('#basket-item-quantity-' + id).next().click();
			break;
		}
	},

	handlerBasketBasketLine: function (actionName, eventObject) {
		var input  			= $(eventObject.target).parent().find('input'),
			countDOM   		= parseInt(input.val(), 10),
			count 			= 1,
			id      		= input.attr('data-basket-item-id'),
			quantityBase 	= input.attr('data-basket-item-quantity-base');

		switch (actionName) {
			case 'minus':
				countDOM--;
			break;
			case 'plus':
				countDOM++;
			break;
		}

		if (countDOM >= 1 && countDOM <= quantityBase) {
			count = countDOM;
		}
		if (countDOM < 1) {
			//count = count
		}
		if (countDOM > quantityBase) {
			count = quantityBase
		}

		input.val(count);

		if ( window.location.pathname === '/cabinet/' + this.userRole + '/edit/catalog/cart/' ) {
			this.handlerBasketBasket( actionName, id, count );
		} else {
			id 		= parseInt(id, 10);
			count 	= parseInt(count, 10);
			this.updateQuantity( id, count );
		}
	},

	handlerBasketBasketRemoveItem: function (eventObject) {
		var id = $(eventObject.target).attr('data-basket-item-id');

		if ( window.location.pathname === '/cabinet/' + this.userRole + '/edit/catalog/cart/' ) {
			$('#basket-item-table #basket-item-' + id + ' .close-btn').click();
		} else {
			id 		= parseInt(id, 10);
			this.handlerBasketLineRemoveItem( id );
		}
	},

	setUserRole: function (role) {
		this.userRole = role;
	},

	updateQuantity: function (id, quantity) {
		BX.ajax({
			url: '/local/templates/ferma_24/ajax/basket/add.php',
			method: 'POST',
			dataType: 'json',
			data: {
				'basket.line.item.id': id,
				'basket.line.item.quantity': quantity
			},
			onsuccess: function (data) {
				if ( data.resultCSaleBasket === true ) {
					BX.onCustomEvent('OnBasketChange');
				}
			}
		});
	},

	handlerBasketLineRemoveItem: function (id) {
		BX.ajax({
			url: '/local/templates/ferma_24/ajax/basket/delete.php',
			method: 'POST',
			dataType: 'json',
			data: {
				'basket.line.item.id': id,
			},
			onsuccess: function (data) {
				if ( data.resultCSaleBasket === true ) {
					BX.onCustomEvent('OnBasketChange');
				}
			}
		});
	}
};
