<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component                  = $this->getComponent();
$arParams                   = $component->applyTemplateModifications();
$arParams['USER']['ID']     = $_SESSION['SESS_AUTH']['USER_ID'];

use Bitrix\Sale\Fuser;
use Bitrix\Sale\Internals\BasketTable;

if (CModule::includeModule('sale')) {
    $rs = BasketTable::getList([
        'filter' => [
	        "FUSER_ID"   => Fuser::getId(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ],
        'select' => [
            "ID",
            "PRODUCT_ID",
            "DELAY"
        ]
    ]);
    while ($ar = $rs->fetch()) {
        $arParams['BASKET_DELAYED'][$ar['PRODUCT_ID']] = $ar;
    }
}

    $measure_selection = array();
if(CModule::IncludeModule("catalog"))
{
    $measure_list = CCatalogMeasure::getList();
    while($ar_result = $measure_list->GetNext())
    {
        $measure_selection[$ar_result['~ID']] = $ar_result['MEASURE_TITLE'];
    }
}
$arParams['MEASURE_SELECTION'] = $measure_selection;


    $ib_campaigns_selection = array();
foreach ($arResult['ITEMS'] as $item) {
    $ib_campaigns_selection[] = $item['ID'];
}

$result = CIBlockElement::GetList(
    false,
    array(
        'IBLOCK_ID'     => 7,
        'ACTIVE'        => 'Y',
        'PROPERTY_CP_SL_PT_BIND_CT_SL' => $ib_campaigns_selection
    ),
    false,
    array(),
    array(
        'ID',
        'NAME',
        'PROPERTY_CP_SL_PT_BIND_CT_SL'
    )
);

    $arResult['CAMPAIGNS_SELECTION'] = array();
while ($result_item = $result->GetNext()) {
    $arResult['CAMPAIGNS_SELECTION'][$result_item['PROPERTY_CP_SL_PT_BIND_CT_SL_VALUE']] = $result_item;
}
    $arParams['CAMPAIGNS_SELECTION'] = $arResult['CAMPAIGNS_SELECTION'];


    $owners_selection = array();
foreach ($arResult['ITEMS'] as $item) {
    $owners_selection[] = $item['CREATED_BY'];
}

$by      = 'id';
$order   = "asc";
$result2 = CUser::GetList($by, $order,
    array(
        'ID' => implode('|', $owners_selection),
    ),
    array(
        'SELECT' => array(
            'UF_ORDER',
            'UF_SELLER_TYPE',
            'UF_DELIVERY_ID'
        ),
        'FIELDS' => array(
            'ID',
            'NAME',
            'LAST_NAME',
            'UF_ORDER',
            'UF_SELLER_TYPE'
        )
    )
);

    $owners = $sellers_delivery_points = [];
while ($result_item2 = $result2->GetNext()) {
    $owners[$result_item2['ID']] = $result_item2;
    if (!empty($result_item2['UF_DELIVERY_ID'])) {
        $sellers_delivery_points[$result_item2['ID']] = $result_item2['UF_DELIVERY_ID'];
    }
}


    $products_with_owner_data = array();
foreach ($arResult['ITEMS'] as $product_item ) {
    $products_with_owner_data[$product_item['ID']]['NAME']      = $owners[$product_item['CREATED_BY']]['NAME'];
    $products_with_owner_data[$product_item['ID']]['LAST_NAME'] = $owners[$product_item['CREATED_BY']]['LAST_NAME'];
    $products_with_owner_data[$product_item['ID']]['ID']        = $owners[$product_item['CREATED_BY']]['ID'];

    switch (intval($owners[$product_item['CREATED_BY']]['UF_ORDER'])) {
        case 6:
            $url_part_user_of_order = 'shopper';
            break;
        case 8:
            $url_part_user_of_order = 'seller';
            break;
        default:
            $url_part_user_of_order = 'guest';
            break;
    }
    $products_with_owner_data[$product_item['ID']]['USER_ROLE'] = $url_part_user_of_order;
}

$arParams['PRODUCT_OWNERS'] = $products_with_owner_data;

// Выведем точки доставки, если разрешено.
use \Bitrix\Main\Loader;
$arResult['DELIVERIES_PER_SELLER'] = '';
if (key_exists('VIEW_DELIVERY_ADDRESS', $arParams)  &&  $arParams['VIEW_DELIVERY_ADDRESS'] == 'Y'  &&  !empty($sellers_delivery_points)) {
    if(Loader::includeModule('sepro.helper')) {
        $arResult['DELIVERIES_PER_SELLER'] = \Sepro\User::getDeliveriesPoints($sellers_delivery_points);
    }
}


// Тегировать кеш
if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
{
    $cp =& $this->__component;
    if (strlen($cp->getCachePath()))
    {
        $GLOBALS['CACHE_MANAGER']->RegisterTag('catalog__sell');
    }
}
