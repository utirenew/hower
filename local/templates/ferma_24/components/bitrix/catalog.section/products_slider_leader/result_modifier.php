<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component                  = $this->getComponent();
$arParams                   = $component->applyTemplateModifications();
$arParams['USER']['ID']     = $_SESSION['SESS_AUTH']['USER_ID'];

use Bitrix\Sale\Fuser;
use Bitrix\Sale\Internals\BasketTable;
use \Bitrix\Sale\Location\Name\LocationTable as Location;
use \Bitrix\Main\UserTable as User;

if (CModule::includeModule('sale')) {
    $rs = BasketTable::getList([
        'filter' => [
	        "FUSER_ID"   => Fuser::getId(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ],
        'select' => [
            "ID",
            "PRODUCT_ID",
            "DELAY"
        ]
    ]);
    while ($ar = $rs->fetch()) {
        $arParams['BASKET_DELAYED'][$ar['PRODUCT_ID']] = $ar;
    }
}

    $measure_selection = array();
if(CModule::IncludeModule("catalog"))
{
    $measure_list = CCatalogMeasure::getList();
    while($ar_result = $measure_list->GetNext())
    {
        $measure_selection[$ar_result['~ID']] = $ar_result['MEASURE_TITLE'];
    }
}
$arParams['MEASURE_SELECTION'] = $measure_selection;


    $ib_campaigns_selection = array();
foreach ($arResult['ITEMS'] as $item) {
    $ib_campaigns_selection[] = $item['ID'];
}

$result = CIBlockElement::GetList(
    false,
    array(
        'IBLOCK_ID'     => 7,
        'ACTIVE'        => 'Y',
        'PROPERTY_CP_SL_PT_BIND_CT_SL' => $ib_campaigns_selection
    ),
    false,
    array(),
    array(
        'ID',
        'NAME',
        'PROPERTY_CP_SL_PT_BIND_CT_SL'
    )
);

    $arResult['CAMPAIGNS_SELECTION'] = array();
while ($result_item = $result->GetNext()) {
    $arResult['CAMPAIGNS_SELECTION'][$result_item['PROPERTY_CP_SL_PT_BIND_CT_SL_VALUE']] = $result_item;
}
    $arParams['CAMPAIGNS_SELECTION'] = $arResult['CAMPAIGNS_SELECTION'];


    $owners_selection = array();
foreach ($arResult['ITEMS'] as $item) {
    $owners_selection[] = $item['CREATED_BY'];
}
//$by      = 'id';
//$order   = "asc";
//$result2 = CUser::GetList($by, $order,
//    array(
//        'ID' => implode('|', $owners_selection),
//    ),
//    array(
//        'SELECT' => array(
//            'UF_ORDER',
//            'UF_SELLER_TYPE',
//            'UF_DELIVERY_ID'
//        ),
//        'FIELDS' => array(
//            'ID',
//            'NAME',
//            'LAST_NAME',
//            'UF_ORDER',
//            'UF_SELLER_TYPE'
//        )
//    )
//);



$result2 = \Bitrix\Main\UserTable::GetList(array(
    'filter' => array(
        'ID' => implode('|', $owners_selection)
    ),
    'select' => array(
        'UF_ORDER',
        'UF_SELLER_TYPE',
        'UF_DELIVERY_ID',
        'ID',
        'NAME',
        'LAST_NAME',
        'UF_ORDER',
        'UF_SELLER_TYPE'
    )
))->fetchAll();



$owners = $delivery_id = [];
foreach ($result2 as $item) {
    $owners[$item['ID']] = $item;
}


use \Bitrix\Main\Loader;
$arParams['DELIVERIES_PER_SELLER'] = '';
if (key_exists('VIEW_DELIVERY_ADDRESS', $arParams)  &&  $arParams['VIEW_DELIVERY_ADDRESS'] == 'Y') {
    $pointsPerSeller = [];
    foreach ($result2 as $item_2) {
        if (!empty($item_2['UF_DELIVERY_ID'])) {
            $pointsPerSeller[$item_2['ID']] = $item_2['UF_DELIVERY_ID'];
        }
    }

    if (!empty($pointsPerSeller)) {
        if (key_exists('VIEW_DELIVERY_ADDRESS', $arParams)  &&  $arParams['VIEW_DELIVERY_ADDRESS'] == 'Y') {
            if(Loader::includeModule('sepro.helper')) {
                $arParams['DELIVERIES_PER_SELLER'] = \Sepro\User::getDeliveriesPoints($pointsPerSeller);
            }
        }
    }
}

    $products_with_owner_data = array();
foreach ($arResult['ITEMS'] as &$product_item ) {
    $products_with_owner_data[$product_item['ID']]['NAME']      = $owners[$product_item['CREATED_BY']]['NAME'];
    $products_with_owner_data[$product_item['ID']]['LAST_NAME'] = $owners[$product_item['CREATED_BY']]['LAST_NAME'];
    $products_with_owner_data[$product_item['ID']]['ID']        = $owners[$product_item['CREATED_BY']]['ID'];

    switch (intval($owners[$product_item['CREATED_BY']]['UF_ORDER'])) {
        case 6:
            $url_part_user_of_order = 'shopper';
            break;
        case 8:
            $url_part_user_of_order = 'seller';
            break;
        default:
            $url_part_user_of_order = 'guest';
            break;
    }
    $products_with_owner_data[$product_item['ID']]['USER_ROLE'] = $url_part_user_of_order;
}

$arParams['PRODUCT_OWNERS'] = $products_with_owner_data;


// Тегировать кеш
if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
{
    $cp =& $this->__component;
    if (strlen($cp->getCachePath()))
    {
        $GLOBALS['CACHE_MANAGER']->RegisterTag('catalog__slider');
    }
}
