<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="row">
	<div class="col-12">
		<? if($arResult["AUTH_SERVICES"]): ?>
			<div class="bx-auth-title"><? echo GetMessage("AUTH_TITLE") ?></div>
		<? endif ?>
	</div>

	<div class="col-12 col-sm-6 col-md-4 col-lg-3">
		<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

			<input type="hidden" name="AUTH_FORM" value="Y"/>
			<input type="hidden" name="TYPE" value="AUTH"/>
			<? if(strlen($arResult["BACKURL"]) > 0): ?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>"/>
			<? endif ?>
			<? foreach($arResult["POST"] as $key => $value): ?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
			<? endforeach ?>

			<label class="d-block"><?=GetMessage("AUTH_LOGIN")?>
				<input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" class="form-control">
			</label>
			<script>
				BX.ready(function () {
					let loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
					if (loginCookie) {
						let form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
						let loginInput = form.elements["USER_LOGIN"];
						loginInput.value = loginCookie;
					}
				});
			</script>

			<label class="d-block"><?=GetMessage("AUTH_PASSWORD")?>
				<input type="password" name="USER_PASSWORD" maxlength="255" class="form-control" autocomplete="off">
			</label>
			<div class="form-group form-check">
				<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" class="form-check-input" value="Y">
				<label for="USER_REMEMBER" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?=GetMessage("AUTH_REMEMBER_SHORT")?></label>
			</div>
			<input type="submit" name="Login" class="modal-btn" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">

			<? if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"): ?>
				<!--noindex-->
				<a data-fancybox data-src="#register" href="javascript:"><?=GetMessage("AUTH_REGISTER")?></a> <br>
				<!--/noindex-->
			<? endif ?>
			<? if($arParams["NOT_SHOW_LINKS"] != "Y"): ?>
				<!--noindex-->
				<a data-fancybox data-src="#forgot" href="javascript:"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a> <br>
				<!--/noindex-->
			<? endif ?>
		</form>
	</div>
</div>


<?// if($arResult["AUTH_SERVICES"]): ?>
<!--	<div class="bx-auth-lbl">--><?//=GetMessage("socserv_as_user_form")?><!--</div>-->
<!--	--><?// $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
//		array(
//			"AUTH_SERVICES"   => $arResult["AUTH_SERVICES"],
//			"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
//			"AUTH_URL"        => $arResult["AUTH_URL"],
//			"POST"            => $arResult["POST"],
//			"SHOW_TITLES"     => $arResult["FOR_INTRANET"]?'N':'Y',
//			"FOR_SPLIT"       => $arResult["FOR_INTRANET"]?'Y':'N',
//			"AUTH_LINE"       => $arResult["FOR_INTRANET"]?'N':'Y',
//		),
//		$component,
//		array("HIDE_ICONS" => "Y")
//	); ?>
<?// endif ?>
