<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>
<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<?if($arResult['NavPageCount'] > 1): ?>
<div class="col-12">
    <div class="pagination-block">
            <!-- Prev -->
            <? if ($arResult["NavPageNomer"] > 1):?>
				<a class="pag-left" href="<?=$arResult['sUrlPath']?>?<?=$strNavQueryString?>PAGEN_<?=$arResult['NavNum']?>=<?=$arResult['NavPageNomer']-1?>">
					<span class="pag-arrow-left"></span>
				</a>
                <? if ($arResult["NavPageNomer"] > 2):?>

                <? else:?>
                    <!-- <a class="pag-left" href="<?php //$arResult['sUrlPath']?><?php //$strNavQueryStringFull?>"> -->
						<!-- <span class="pag-arrow-left"></span> -->
					<!-- </a> -->
                <?endif;?>
            <?endif;?>
            <!-- Prev -->
            <?if($arResult['NavPageNomer'] > 4):?>
                <a href="<?=$arResult['sUrlPath']?>?<?=$strNavQueryString?>PAGEN_<?=$arResult['NavNum']?>=1" class="pagination-btn">1</a><span class="pagination-item">...</span>
            <?endif;?>
            <div class="pagination-btns">
                <!-- Nums -->
                <? while($arResult['nStartPage'] <= $arResult['nEndPage']): ?>
                    <? if($arResult['nStartPage'] == $arResult['NavPageNomer']): ?>
                        <a class="pagination-btn active-pag"><?=$arResult['nStartPage']?></a>
                    <? elseif ($arResult['nStartPage'] == 1 && $arResult['bSavePage'] == false): ?>
                        <a class="pagination-btn " href="<?=$arResult['sUrlPath']?><?=$strNavQueryStringFull?>"><?=$arResult['nStartPage']?></a>
                    <? else: ?>
                        <a class="pagination-btn " href="<?=$arResult['sUrlPath']?>?<?=$strNavQueryString?>PAGEN_<?=$arResult['NavNum']?>=<?=$arResult['nStartPage']?>"><?=$arResult['nStartPage']?></a>
                    <? endif; ?>
                    <? $arResult['nStartPage']++; ?>
                <? endwhile; ?>
                <!--Nums-->
            </div>
            <?if($arResult['NavPageNomer'] > 4 AND $arResult['NavPageNomer'] < $arResult['NavPageCount']-3):?>
                <span class="pagination-item">...</span><a href="<?=$arResult['sUrlPath']?>?<?=$strNavQueryString?>PAGEN_<?=$arResult['NavNum']?>=<?=$arResult['NavPageCount']?>" class="pagination-btn"><?=$arResult['NavPageCount']?></a>
            <?endif;?>
            <!--Next-->
            <? if($arResult['NavPageNomer'] < $arResult["NavPageCount"]): ?>
                <a class="pag-right" href="<?=$arResult['sUrlPath']?>?<?=$strNavQueryString?>PAGEN_<?=$arResult['NavNum']?>=<?=($arResult['NavPageNomer']+1)?>">
					<span class="pag-arrow-right"></span>
				</a>
            <? endif; ?>
            <!--Next-->
    </div>
</div>
<?endif;?>
