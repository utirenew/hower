<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
	<div class="cart-total-block">
		<div class="total-top-block">
			<div class="total-head">Итого
				<span class="totl-count" id="cabinet-order-cart-basket-items-count">{{ORDERABLE_BASKET_ITEMS_COUNT}}</span>
			</div>
			<div class="total-row">
				<div class="total-row-name">Стоимость товаров</div>
				<div class="total-row-value" id="cabinet-order-cart-basket-alsum">{{PRICE_WITHOUT_DISCOUNT_FORMATED}}</div>
			</div>
			<div class="total-row">
				<div class="total-row-name">доставка</div>
				<div class="total-row-value"></div>
			</div>
			<div class="total-row">
				<div class="total-row-name">скидка</div>
				<div class="total-row-value" id="cabinet-order-cart-basket-discount-value">{{DISCOUNT_PRICE_FORMATED}}</div>
			</div>
			<div class="total-sum-block">
				<div class="total-sum-name">Сумма</div>
				<div class="total-sum" id="basket-total-price">{{PRICE_FORMATED}}</div>
			</div>
		</div>
		<div class="total-btn-block">
			<a class="make-order" href="#make" data-jquery-selector="link-anchor-behavior-disable" data-entity="basket-checkout-button">
				<?=Loc::getMessage('SBB_ORDER')?>
			</a>
		</div>
	</div>
	<div class="delivery-btn-block">
		<a href="#" class="delivery-btn">Доставка и оплата</a>
	</div>
</script>