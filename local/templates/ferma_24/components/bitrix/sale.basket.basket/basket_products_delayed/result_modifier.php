<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Bitrix\Main;

$defaultParams = array(
	'TEMPLATE_THEME' => 'blue'
);
$arParams = array_merge($defaultParams, $arParams);
unset($defaultParams);

$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME'])
{
	$arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
	if ('site' == $arParams['TEMPLATE_THEME'])
	{
		$templateId = (string)Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', SITE_ID);
		$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? 'eshop_adapt' : $templateId;
		$arParams['TEMPLATE_THEME'] = (string)Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', SITE_ID);
	}
	if ('' != $arParams['TEMPLATE_THEME'])
	{
		if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
			$arParams['TEMPLATE_THEME'] = '';
	}
}
if ('' == $arParams['TEMPLATE_THEME'])
	$arParams['TEMPLATE_THEME'] = 'blue';

$arResult['PRICE_WITH_DISCOUNT'] = \Bitrix\Sale\BasketComponentHelper::getFUserBasketPrice(CSaleBasket::GetBasketUserID(), SITE_ID);
$arResult['PRICE_DISCOUNT'] 	 = $arResult['PRICE_WITHOUT_DISCOUNT'] - $arResult['PRICE_WITH_DISCOUNT'];




	$ib_id_selection = array();
foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $item0) {
	$ib_id_selection[] = $item0['PRODUCT_ID'];
}

$result = CIBlockElement::GetList(
	false,
	array(
		'IBLOCK_ID'     => 5,
		'ACTIVE'        => 'Y',
		'ID' => $ib_id_selection
	),
	false,
	array(),
	array(
		'ID',
		'PROPERTY_CT_SL_PT_SET_SELLER_NAME',
		'CREATED_BY'
	)
);

	$list_basket_items_fermer_name = array();
while ($result_item = $result->GetNext()) {
	$list_basket_items_fermer_name[$result_item['ID']] = $result_item;
}

foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key1=>$item1) {
	$arResult['BASKET_ITEM_RENDER_DATA'][$key1]['SELER_NAME'] = $list_basket_items_fermer_name[$item1['PRODUCT_ID']]['PROPERTY_CT_SL_PT_SET_SELLER_NAME_VALUE'];
}


if(CModule::IncludeModule("catalog"))
{
	$product_list = CCatalogProduct::GetList(
		array(
			'ID' => 'DESC',
		),
		array(
			"@ID" => $ib_id_selection,
		),
		false,
		false,
		array(
			'ID',
			'MEASURE'
		)
	);
		$product_selection = array();
	while($product = $product_list->Fetch())
	{
		$product_selection[$product['ID']] = $product;
	}

	$measure_list = CCatalogMeasure::getList();
		$measure_selection = array();
	while($ar_result = $measure_list->GetNext())
	{
		$measure_selection[$ar_result['~ID']] = $ar_result['MEASURE_TITLE'];
	}
}

foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key2=>$item2) {
	$measure_title_full = $measure_selection[$product_selection[$item2['PRODUCT_ID']]['MEASURE']];
	$arResult['BASKET_ITEM_RENDER_DATA'][$key2]['MEASURE_TITLE_FULL'] = $measure_title_full;
}


//Add product owners
	$owners_selection = array();
foreach ($list_basket_items_fermer_name as $user_info) {
	$owners_selection[] = $user_info['CREATED_BY'];
}

$by      = 'id';
$order   = "asc";
$users_data = CUser::GetList($by, $order,
	array(
		'ID' => implode('|', $owners_selection),
	),
	array(
		'FIELDS' => array(
			'ID',
			'NAME',
			'LAST_NAME',
		)
	)
);

	$owners = array();
while ($user_data = $users_data->GetNext()) {
	$owners[$user_data['ID']] = $user_data;
}

foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key4=>$item4) {
	$owner_id = $list_basket_items_fermer_name[$item4['PRODUCT_ID']]['CREATED_BY'];
	$owner_name_full = $owners[$owner_id]['NAME'] . ' ' . $owners[$owner_id]['LAST_NAME'];
	$arResult['BASKET_ITEM_RENDER_DATA'][$key4]['OWNER_NAME_FULL'] = $owner_name_full;
}

foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key5=>$item5) {
	if (is_null($arResult['BASKET_ITEM_RENDER_DATA'][$key5]['SELER_NAME'])) {
		$arResult['BASKET_ITEM_RENDER_DATA'][$key5]['SELER_NAME'] = $arResult['BASKET_ITEM_RENDER_DATA'][$key5]['OWNER_NAME_FULL'];
	}
}
