<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array  $mobileColumns
 * @var array  $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn               = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn            = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left'   => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right'  => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top'    => 'basket-item-label-top',
);

$discountPositionClass = '';
if($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION'])){
	foreach(explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos){
		$discountPositionClass .= isset($positionClassMap[$pos])?' ' . $positionClassMap[$pos]:'';
	}
}

$labelPositionClass = '';
if(!empty($arParams['LABEL_PROP_POSITION'])){
	foreach(explode('-', $arParams['LABEL_PROP_POSITION']) as $pos){
		$labelPositionClass .= isset($positionClassMap[$pos])?' ' . $positionClassMap[$pos]:'';
	}
}
?>
<script id="basket-item-template" type="text/html">
	<div class="no-gutters align-items-center cart-page-item" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
		{{#SHOW_RESTORE}}
		<div class="basket-items-list-item-notification">
			<div class="basket-items-list-item-notification-inner basket-items-list-item-notification-removed" id="basket-item-height-aligner-{{ID}}">
				{{#SHOW_LOADING}}
				<div class="basket-items-list-item-overlay"></div>
				{{/SHOW_LOADING}}
				<div class="basket-items-list-item-removed-container">
					<div>
						<?=Loc::getMessage('SBB_GOOD_CAP')?> <strong>{{NAME}}</strong> <?=Loc::getMessage('SBB_BASKET_ITEM_DELETED')?>.
					</div>
					<div class="basket-items-list-item-removed-block">
						<a href="javascript:" data-entity="basket-item-restore-button">
							<?=Loc::getMessage('SBB_BASKET_ITEM_RESTORE')?>
						</a>
						<span class="basket-items-list-item-clear-btn" data-entity="basket-item-close-restore-button"></span>
					</div>
				</div>
			</div>
		</div>
		{{/SHOW_RESTORE}}
		{{^SHOW_RESTORE}}
		<div class="row" id="basket-item-height-aligner-{{ID}}">
			<div class="col-5 first-column">
				<? if(in_array('PREVIEW_PICTURE', $arParams['COLUMNS_LIST'])): ?>
					<div class="big-cart-pic">
						<div class="big-cart-image <?=(!isset($mobileColumns['PREVIEW_PICTURE'])?' hidden-xs':'')?>">
							{{#DETAIL_PAGE_URL}}
							<a href="{{DETAIL_PAGE_URL}}" class="basket-item-image-link">
								{{/DETAIL_PAGE_URL}}
								<img alt="{{NAME}}" src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}">
								{{#DETAIL_PAGE_URL}}
							</a>
							{{/DETAIL_PAGE_URL}}
						</div>
					</div>
				<? endif; ?>
				<div class="big-cart-info">
					<div class="big-cart-name">
						{{#DETAIL_PAGE_URL}}
						<a href="{{DETAIL_PAGE_URL}}" class="basket-item-info-name-link">
						{{/DETAIL_PAGE_URL}}
							<span data-entity="basket-item-name">{{NAME}}</span>
						{{#DETAIL_PAGE_URL}}
						</a>
						{{/DETAIL_PAGE_URL}}
					</div>
					<div class="big-cart-fermer"> {{SELER_NAME}}</div>
					<div class="big-cart-price-block">
						<div class="big-cart-price" id="basket-item-price-{{ID}}">
							<span class="big-price">{{{PRICE_FORMATED}}}</span>
							<span class="big-unit">
										/ за <span data-jquery-selector="value">{{MEASURE_TITLE_FULL}}</span>
									</span>
						</div>
						{{#SHOW_DISCOUNT_PRICE}}
							<div class="big-cart-old-price">{{{FULL_PRICE_FORMATED}}}</div>
						{{/SHOW_DISCOUNT_PRICE}}
					</div>
				</div>
			</div>
			<div class="col-4"></div>
			<div class="flex-v-center col-auto"></div>
			<div class="flex-v-center col-auto ml-auto"><span class="close-btn" data-entity="basket-item-delete"></span></div>
		</div>
		{{/SHOW_RESTORE}}
	</div>
</script>
