<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
	<a class="make-order" href="#make" data-jquery-selector="link-anchor-behavior-disable"
		data-entity="basket-checkout-button">
		<?=Loc::getMessage('SBB_ORDER')?>
	</a>
</script>
