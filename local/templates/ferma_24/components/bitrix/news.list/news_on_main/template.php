<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-slider-block">
	<div class="container">
		<div class="row">
			<div class="col-12 p-0">
				<div class="slider-head">
					<div class="slider-title">
						Новости
					</div>
					<div class="slider-controls-block">
						<div class="news-arrows">
							<a class="carousel-control news-control prev" href="#main-carouselIndicators" role="button" data-slide="slickPrev">
								назад
								<span class="left-btn control-btn" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control news-control next" href="#main-carouselIndicators" role="button" data-slide="slickNext">
								вперёд
								<span class="right-btn control-btn" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
						<div class="news-dots"></div>
					</div>
				</div>
				<div class="news-slider">
					<? foreach($arResult['ITEMS'] as $arItem): ?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						?>
						<?
						$file                             = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], ['width' => 320, 'height' => 156], BX_RESIZE_IMAGE_EXACT, true);
						$arItem['PREVIEW_PICTURE']['SRC'] = $file['src'];
						?>
						<div class="col p-0" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
							<div class="news-item">
								<div class="news-date">
									<span class="news-year"><?=date('Y', strtotime($arItem['ACTIVE_FROM']))?></span>
									<span class="news-day"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
								</div>
								<div class="news-pic">
									<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" class="news-image">
								</div>
								<div class="news-text-block">
									<div class="news-title">
										<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="news-hover-link"><?=$arItem['NAME']?></a>
									</div>
									<div class="news-line"></div>
									<div class="news-text">
										<?=$arItem['PREVIEW_TEXT']?>
									</div>
								</div>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>