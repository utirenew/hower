<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="mainSlider">
    <? foreach ($arResult['ITEMS'] as $arItem): ?>
        <div>
            <img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="<?=$arItem['DETAIL_PICTURE']['ALT']?>">
            <div class="slider-text-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-9 ml-auto">
                            <div class="slider-text"><?=$arItem['DETAIL_TEXT']?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
</div>
