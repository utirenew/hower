<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>

<div class="bx-system-auth-form d-flex flex-column" id="header_auth">

	<? if($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) ShowMessage($arResult['ERROR_MESSAGE']); ?>

	<? if($arResult["FORM_TYPE"] == "login"): ?>

		<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" id="header_auth_form" action="<?=$arResult["AUTH_URL"]?>">
			<? if($arResult["BACKURL"] <> ''): ?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>">
			<? endif ?>
			<? foreach($arResult["POST"] as $key => $value): ?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>">
			<? endforeach ?>
			<input type="hidden" name="AUTH_FORM" value="Y">
			<input type="hidden" name="TYPE" value="AUTH">

			<table width="95%">
				<tr>
					<td colspan="2">
						<?=GetMessage("AUTH_LOGIN")?>:<br>
						<input type="text" name="USER_LOGIN" maxlength="50" value="" class="form-control">
						<script>
							BX.ready(function () {
								let loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
								if (loginCookie) {
									let form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
									let loginInput = form.elements["USER_LOGIN"];
									loginInput.value = loginCookie;
								}
							});
						</script>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<?=GetMessage("AUTH_PASSWORD")?>:<br>
						<input type="password" name="USER_PASSWORD" maxlength="50" class="form-control" autocomplete="off">
						<? if($arResult["SECURE_AUTH"]): ?>
							<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?=GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
								<div class="bx-auth-secure-icon"></div>
							</span>
							<noscript>
								<span class="bx-auth-secure" title="<?=GetMessage("AUTH_NONSECURE_NOTE")?>">
									<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
								</span>
							</noscript>
							<script type="text/javascript">
								document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
							</script>
						<? endif ?>
					</td>
				</tr>
				<? if($arResult["STORE_PASSWORD"] == "Y"): ?>
					<tr>
						<td class="position-relative">
							<div class="form-group form-check">
								<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" class="form-check-input" value="Y">
								<label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?=GetMessage("AUTH_REMEMBER_SHORT")?></label>
							</div>
						</td>
					</tr>
				<? endif ?>
				<? if($arResult["CAPTCHA_CODE"]): ?>
					<tr>
						<td colspan="2">
							<?
							echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br>
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>">
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"><br><br>
							<input type="text" name="captcha_word" maxlength="50" value="">
						</td>
					</tr>
				<? endif ?>
				<tr>
					<td colspan="2">
						<input type="submit" name="Login" class="form-control" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>">
					</td>
				</tr>

				<? if($arResult["NEW_USER_REGISTRATION"] == "Y"): ?>
					<tr>
						<td colspan="2">
							<!--noindex-->
							<a data-fancybox data-src="#register" href="javascript:"><?=GetMessage("AUTH_REGISTER")?></a>
							<!--/noindex-->
							<br></td>
					</tr>
				<? endif ?>

				<tr>
					<td colspan="2">
						<!--noindex-->
						<a data-fancybox data-src="#forgot" href="javascript:"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
						<!--/noindex-->
					</td>
				</tr>

				<?// if($arResult["AUTH_SERVICES"]): ?>
				<!--	<tr>-->
				<!--		<td colspan="2">-->
				<!--			<div class="bx-auth-lbl">--><?//=GetMessage("socserv_as_user_form")?><!--</div>-->
				<!--			--><?// $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", array(
				//				"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
				//				"SUFFIX"        => "form",
				//			), $component, array("HIDE_ICONS" => "Y"));
				//			?>
				<!--		</td>-->
				<!--	</tr>-->
				<?// endif ?>
			</table>
		</form>
	<? endif ?>
</div>
