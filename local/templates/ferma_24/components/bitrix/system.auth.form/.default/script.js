if (typeof (activFancyInstace) === 'undefined') {
	let activeFancyInstance = null;

	$(document)
		.on('onInit.fb', function (e, instance) {
			if (activeFancyInstance !== null) activeFancyInstance.close();
			activeFancyInstance = instance;
		});
}

$(function () {
	let form_container = $('#header_auth');

	form_container.on('submit', '#header_auth_form', function (ev) {
		ev.preventDefault();
		let form = $(this),
			url = form.prop('action'),
			data = form.serialize();
		$.post(
			url,
			data,
			function (html) {
				let form_html = $(html).find('#header_auth');
				if (form_html.length === 0) location.href = form.prop('action');
				else form_container.html(form_html.html());
			}
		);
	});
});