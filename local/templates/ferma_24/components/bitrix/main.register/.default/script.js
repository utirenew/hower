if (typeof (activFancyInstace) === 'undefined') {
	let activeFancyInstance = null;

	$(document)
		.on('onInit.fb', function (e, instance) {
			if (activeFancyInstance !== null) activeFancyInstance.close();
			activeFancyInstance = instance;
		});
}

$(function () {
	let form_container = $('#header_reg');

	form_container.on('submit', '#header_reg_form', function (ev) {
		ev.preventDefault();
		let form = $(this),
			url = form.prop('action'),
			data = form.serialize();

		$.post(
			url,
			data,
			function (html) {
				let form_html = $(html).find('#header_reg');
				if (form_html.length === 0) {
					location.href = form.prop('action');
				}
				else {
					form_container.html(form_html.html());
				}
			}
		);
	});
});