<?
/**
 * Bitrix Framework
 * @package    bitrix
 * @subpackage main
 * @copyright  2001-2014 Bitrix
 */

/**
 * Bitrix vars
 *
 * @param array                    $arParams
 * @param array                    $arResult
 * @param CBitrixComponentTemplate $this
 *
 * @global CUser                   $USER
 * @global CMain                   $APPLICATION
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if(!$USER->IsAuthorized()): ?>
	<div id="header_reg">
		<div class="modal-window-head"> Регистрация</div>
		<form action="<?=POST_FORM_ACTION_URI?>" method="post" name="regform" class="modal-window-form" id="header_reg_form">
			<div class="container-fluid input-block-modal">
				<div class="row">
					<? if($arResult["BACKURL"] <> ''): ?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>"/>
					<? endif; ?>
					<input type="hidden" name="TYPE" value="REGISTRATION"/>
					<input type="hidden" name="register_submit_button" value="Y"/>

					<div class="col-sm-6 col-12 mb-3 left-column">
						<input type="text" class="form-control" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]['LOGIN']?>" placeholder="Логин*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 right-column">
						<input type="text" class="form-control" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]['NAME']?>" placeholder="Имя*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 left-column">
						<input type="password" class="form-control" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]['PASSWORD']?>" placeholder="Пароль*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 right-column">
						<input type="text" class="form-control" name="REGISTER[LAST_NAME]" value="<?=$arResult["VALUES"]['LAST_NAME']?>" placeholder="Фамилия*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 left-column">
						<input type="password" class="form-control" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>"
						       placeholder="Подтверждение пароля*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 right-column">
						<input type="text" class="form-control" name="REGISTER[SECOND_NAME]" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" placeholder="Отчество">
					</div>
					<div class="col-sm-6 col-12 mb-3 left-column">
						<input type="email" class="form-control" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]['EMAIL']?>" placeholder="E-mail*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 right-column">
						<input type="tel" class="form-control" name="REGISTER[PERSONAL_PHONE]" value="<?=$arResult["VALUES"]['PERSONAL_PHONE']?>" placeholder="Номер телефон*"
						       required>
					</div>

					<? if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>
						<? foreach($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
							<? $APPLICATION->IncludeComponent("bitrix:system.field.edit", $arUserField["USER_TYPE"]["USER_TYPE_ID"], array(
								"bVarsFromForm" => $arResult["bVarsFromForm"],
								"arUserField"   => $arUserField,
								"form_name"     => "regform",
							), null, array("HIDE_ICONS" => "Y")); ?>
						<? endforeach; ?>
					<? endif; ?>

					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>">
					<div class="col-sm-6 col-12 mb-3 middle-column">
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="33" alt="CAPTCHA">
					</div>
					<div class="col-sm-6 col-12 mb-3 middle-column">
						<input type="text" class="form-control" name="captcha_word" maxlength="50" value="" placeholder="Введите капчу*" required>
					</div>
					<div class="col-sm-6 col-12 mb-3 mx-auto middle-column">
						<input type="submit" class="modal-btn" value="Регистрация"/>
					</div>
					<div class="col-sm-6 col-12 mb-3 mx-auto middle-column">
						<a data-fancybox data-src="#enter" href="javascript:"><?=GetMessage("AUTH_LOGIN_BUTTON")?></a> <br>
						<a data-fancybox data-src="#forgot" href="javascript:"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
					</div>
				</div>
			</div>
			<div class="error-text-block">
				<? if(count($arResult["ERRORS"]) > 0):
					foreach($arResult["ERRORS"] as $key => $error){
						if(intval($key) == 0 && $key !== 0) $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
					}

					ShowError(implode("<br />", $arResult["ERRORS"]));

				elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"): ?>
					<p style="margin: 0 auto; text-align: center;"><? echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT") ?></p>
				<? endif ?>
			</div>
		</form>
	</div>
<? endif ?>
<? /*$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("../.default/components/bitrix/main.register/.default/registration.php"));*/ ?>
