if (!window.BX_YMapAddPlacemark) {
	window.BX_YMapAddPlacemark = function (map, arPlacemark) {
		if (null == map)
			return false;

		if (!arPlacemark.LAT || !arPlacemark.LON)
			return false;

		var props = {};
		if (null != arPlacemark.TEXT && arPlacemark.TEXT.length > 0) {
			var value_view = '';

			if (arPlacemark.TEXT.length > 0) {
				var rnpos = arPlacemark.TEXT.indexOf("\n");
				value_view = rnpos <= 0 ? arPlacemark.TEXT : arPlacemark.TEXT.substring(0, rnpos);
			}

			props.balloonContent = arPlacemark.TEXT.replace(/\n/g, '<br />');
			props.hintContent = value_view;
		}

		var obPlacemark = new ymaps.Placemark(
			[arPlacemark.LAT, arPlacemark.LON],
			props,
			{balloonCloseButton: true}
		);

		map.geoObjects.add(obPlacemark);

		return obPlacemark;
	}
}

if (!window.BX_YMapAddPolyline) {
	window.BX_YMapAddPolyline = function (map, arPolyline) {
		if (null == map)
			return false;

		if (null != arPolyline.POINTS && arPolyline.POINTS.length > 1) {
			let arPoints = [];
			for (let i = 0, len = arPolyline.POINTS.length; i < len; i++) {
				arPoints.push([arPolyline.POINTS[i].LAT, arPolyline.POINTS[i].LON]);
			}
		}
		else {
			return false;
		}

		let obParams = {clickable: true};
		if (null != arPolyline.STYLE) {
			obParams.strokeColor = arPolyline.STYLE.strokeColor;
			obParams.strokeWidth = arPolyline.STYLE.strokeWidth;
		}
		let obPolyline = new ymaps.Polyline(
			arPoints, {balloonContent: arPolyline.TITLE}, obParams
		);

		map.geoObjects.add(obPolyline);

		return obPolyline;
	}
}
$(function () {
	$('#ajaxContainer')
		.on('click', '.delete_map', function (ev) {
			let mapID = $(this).data('map-id'),
				mapBlock = $(this).parents('.col-3'),
				id = +mapID.substr(4);

			ev.preventDefault();

			if (typeof window.currentMap !== 'object') window.currentMap = {};
			currentMap.action = 'del';
			currentMap.id = id;

			$.post(
				'/local/templates/ferma_24/components/bitrix/map.yandex.search/shops/ajax.php',
				{action: 'del', key: id},
				function (res) {
					let result = JSON.parse(res);
					if (result.success) {
						window.GLOBAL_arMapObjects[mapID].destroy();
						mapBlock.remove();
						currentMap.action = '';
						currentMap.id = 0;
					}
				}
			);
		})
		.on('click', '.update_map', function (ev) {
			let mapID = $(this).data('map-id'),
				searchID = 'searchmap',
				centrSTR = GLOBAL_arMapObjects[searchID].getCenter().toString();
			ev.preventDefault();

			if (typeof window.currentMap !== 'object') window.currentMap = {};
			currentMap.action = 'update';
			currentMap.id = +mapID.substr(4);

			$.fancybox.open({
				src: '#search_map',
				type: 'inline',
				opts: {
					closeExisting: false,
					smallBtn: false,
					hideScrollbar: true,
					touch: false
				}
			});

			window['jsYandexSearch_' + searchID].searchByAddress(centrSTR);
		});
});