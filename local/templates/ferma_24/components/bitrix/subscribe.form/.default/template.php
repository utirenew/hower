<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="subscribe-form" id="subscribe-form" xmlns="http://www.w3.org/1999/html">
    <?
    $frame = $this->createFrame("subscribe-form", false)->begin();
    ?>
    <div class="column-head">Будьте в курсе событий</div>
    <div class="mail-form-head">Подпишитесь на нашу рассылку</div>
    <form action="<?=$arResult["FORM_ACTION"]?>" method="post" class="relative">
        <input class="user-mail" type="email" placeholder="Ваша почта" name="sf_EMAIL" size="20" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" required />
        <button type="submit" class="submit-btn" name="OK" value="" />подписаться</button>
    </form>
    <?
    $frame->beginStub();
    ?>
    <form action="<?=$arResult["FORM_ACTION"]?>">

        <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
            <label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
                <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" /> <?=$itemValue["NAME"]?>
            </label><br />
        <?endforeach;?>

        <table border="0" cellspacing="0" cellpadding="2" align="center">
            <tr>
                <td><input type="text" name="sf_EMAIL" size="20" value="" title="<?=GetMessage("subscr_form_email_title")?>" /></td>
            </tr>
            <tr>
                <td align="right"><input type="submit" name="OK" value="<?=GetMessage("subscr_form_button")?>" /></td>
            </tr>
        </table>
    </form>
    <?
    $frame->end();
    ?>
</div>
