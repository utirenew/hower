<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container mt-30">
    <div class="row">
        <div class="col-12">
            <div class="bx-newsdetail">
                <div class="bx-newsdetail-block" >
                    <div class="bx-newsdetail-img">
                        <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>" title="<?=$arResult['DETAIL_PICTURE']['TITLE']?>">
                    </div>
                    <div class="bx-newsdetail-content">
                        <?=$arResult['DETAIL_TEXT']?>
                    </div>
                    <div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i><?=$arResult['DISPLAY_ACTIVE_FROM']?></div>
                </div>
            </div>
        </div>
    </div>
</div>
