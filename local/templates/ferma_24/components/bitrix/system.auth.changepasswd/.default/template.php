<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform" class="row">
	<div class="col-12 mb-3">
		<div class="bx-auth-title"><? echo GetMessage("AUTH_CHANGE_PASSWORD") ?></div>
	</div>
	<div class="col-12">
		<? ShowMessage($arParams["~AUTH_RESULT"]); ?>
	</div>
	<? if(strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>"/>
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">

	<div class="col-md-5 col-sm-7 col-12">
		<div class="row">
			<div class="col-12 mb-3">
				<input type="text" name="USER_LOGIN" class="form-control" maxlength="50" placeholder="*<?=GetMessage("AUTH_LOGIN")?>" value="<?=$arResult["LAST_LOGIN"]?>">
			</div>
			<div class="col-12 mb-3">
				<input type="text" name="USER_CHECKWORD" class="form-control" maxlength="50" placeholder="*<?=GetMessage("AUTH_CHECKWORD")?>"
				       value="<?=$arResult["USER_CHECKWORD"]?>">
			</div>
			<div class="col-12 mb-3">
				<input type="password" name="USER_PASSWORD" class="form-control" maxlength="50" placeholder="*<?=GetMessage("AUTH_NEW_PASSWORD_REQ")?>"
				       value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off">
			</div>
			<div class="col-12 mb-3">
				<input type="password" name="USER_CONFIRM_PASSWORD" class="form-control" maxlength="50" placeholder="*<?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>"
				       value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off">
			</div>
			<div class="col-12 mb-3">
				<input type="submit" name="change_pwd" class="modal-btn" value="<?=GetMessage("AUTH_CHANGE")?>"/>
			</div>
		</div>
	</div>

	<div class="col-md-7 col-sm-5 col-12">
		<? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?>
		<span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?> <br>
		<a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a>
		<a data-fancybox data-src="#enter" href="javascript:"><?=GetMessage("AUTH_LOGIN_BUTTON")?></a> <br>
		<a data-fancybox data-src="#forgot" href="javascript:"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
	</div>
</form>
