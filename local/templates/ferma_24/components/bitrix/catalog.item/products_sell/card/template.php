<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain                $APPLICATION
 * @var array                   $arParams
 * @var array                   $item
 * @var array                   $actualItem
 * @var array                   $minOffer
 * @var array                   $itemIds
 * @var array                   $price
 * @var array                   $measureRatio
 * @var bool                    $haveOffers
 * @var bool                    $showSubscribe
 * @var array                   $morePhoto
 * @var bool                    $showSlider
 * @var bool                    $itemHasDetailUrl
 * @var string                  $imgTitle
 * @var string                  $productTitle
 * @var string                  $buttonSizeClass
 * @var string                  $sellerName
 * @var string                  $measureTitleFull
 * @var string                  $compaignName
 * @var string                  $ownerName
 * @var string                  $itInDelay
 * @var string                  $userId
 * @var CatalogSectionComponent $component
 */
?>

<div class="product">

	<div class="top-block">
		<div class="product-labels-wrapper">
			<?php if($compaignName !== ''): ?>
				<div class="product-sale  akcija"></div>
			<?php endif; ?>
			<?php if($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y'): ?>
				<div class="product-sale  discont"></div>
			<?php endif; ?>
		</div>

		<a class="product-pic" href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$imgTitle?>" data-entity="image-wrapper">
			<span id="<?=$itemIds['PICT_SLIDER']?>"></span>
			<span id="<?=$itemIds['PICT']?>"></span>
			<span id="<?=$itemIds['SECOND_PICT']?>"></span>
			<img src="<?=$item['CARD_PICTURE']?>" alt="">
		</a>

		<div class="product-desc">
			<div class="fermer-name">
				<?php if($sellerName === '' || is_null($sellerName)): ?>
					<a href="/cabinet/<?php echo $arResult['ITEM']['OWNER']['USER_ROLE']; ?>/view/?user_id=<?php echo $arResult['ITEM']['OWNER']['ID']; ?>"><?php echo $ownerName; ?></a>
				<?php else: ?>
					<a href="/cabinet/<?php echo $arResult['ITEM']['OWNER']['USER_ROLE']; ?>/view/?user_id=<?php echo $arResult['ITEM']['OWNER']['ID']; ?>"><?php echo $sellerName; ?></a>
				<?php endif; ?>
			</div>
			<div class="product-name">
				<?=$productTitle?>
			</div>
            <div class="product-delivery-address">
                <?
                if (key_exists('DELIVERIES_PER_SELLER', $arParams)  &&  is_array($arParams['DELIVERIES_PER_SELLER'])  &&  !empty($arParams['DELIVERIES_PER_SELLER'])) {
                    echo '<div class="address-title">Доставка в ';
                    if ($arParams['DELIVERIES_PER_SELLER'][$arResult['ITEM']['OWNER']['ID']]['TITLE_IS_LINK']) {
                        echo '<button type="button" data-toggle="popover" data-container="body" data-placement="bottom" data-content="'.$arParams['DELIVERIES_PER_SELLER'][$arResult['ITEM']['OWNER']['ID']]['LIST'].'">'.$arParams['DELIVERIES_PER_SELLER'][$arResult['ITEM']['OWNER']['ID']]['TITLE'].'</button>';
                    } else {
                        echo '<span>' . $arParams['DELIVERIES_PER_SELLER'][$arResult['ITEM']['OWNER']['ID']]['TITLE'] . '</span>';
                    }
                    echo '</div>';
                }?>
            </div>
		</div>
		<!-- button wishlist -->
		<?php if(USER_ROLE !== $arResult['ITEM']['OWNER']['USER_ROLE'] && USER_ROLE !== 'guest'): ?>
			<div>
				<?php if($arResult['ITEM']['BASKET_DELAYED'][$userId]['BASKET_IS_DELAYED_ITEM'] === 'Y'): ?>
					<a href="javascript:void(0)" class="like wishbtn js-wishbtn-delete in_wishlist"
					   onclick="delete4wish(
							   '<?=$arResult['ITEM']['BASKET_DELAYED'][$userId]['BASKET_DELAYED_ITEM_ID']?>',
							   this
							   )">
					</a>
				<?php else: ?>
					<a href="javascript:void(0)" class="like wishbtn js-wishbtn-add  <? if((in_array($item["~ID"], $delaydBasketItems)) || (isset($itInDelay))){
						echo 'in_wishlist';
					} ?>"
					   onclick="add2wish(
							   '<?=$item["~ID"]                    //p_id  - PRODUCT_ID ?>',
							   '<?=$item["~CATALOG_PRICE_ID_1"]    //pp_id - PRODUCT_PRICE_ID ?>',
							   '<?=$item["RATIO_PRICE"]['VALUE']    //p 	- PRICE ?>',
							   '<?=$item["~NAME"]                    //name  - NAME ?>',
							   '<?=$item["~DETAIL_PAGE_URL"]        //dpu   - DETAIL_PAGE_URL?>',
							   '<?=USER_ROLE?>',
							   this
							   )">
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<!-- button wishlist -->
	</div>

	<div class="bottom-block" data-entity="price-block">
		<span class="product-price">
			<span class="price" id="<?=$itemIds['PRICE']?>"><?php echo $price['PRINT_RATIO_PRICE']; ?></span>
			<span class="unit">/ за <?php echo $measureTitleFull; ?></span>
		</span>
		<?php if($arParams['SHOW_OLD_PRICE'] === 'Y'): ?>
			<span class="product-old-price" id="<?=$itemIds['PRICE_OLD']?>">
			<?=$price['PRINT_RATIO_BASE_PRICE']?>
		</span>
		<?php endif; ?>
	</div>

	<div data-entity="buttons-block">
		<? if(!$haveOffers): ?>
			<? if($actualItem['CAN_BUY'] && ((USER_ROLE !== $arResult['ITEM']['OWNER']['USER_ROLE'] && USER_ROLE !== 'guest') || (USER_ROLE == 'guest' && $arResult['ITEM']['OWNER']['USER_ROLE'] !== 'shopper'))): ?>
				<div class="product-item-button-container" id="<?=$itemIds['BASKET_ACTIONS']?>">
				<?php if($arParams['USE_PRODUCT_QUANTITY']): ?>
					<div class="product-count-block">
						<div class="count-minus" id="<?=$itemIds['QUANTITY_DOWN']?>" data-entity="basket-item-quantity-minus"></div>
						<input type="text" class="card-count-value" id="<?=$itemIds['QUANTITY']?>" value="1" size="1"/>
						<div class="count-plus" id="<?=$itemIds['QUANTITY_UP']?>" data-entity="basket-item-quantity-plus"></div>
					</div>
				<?php endif; ?>
					<a class="add-product" id="<?=$itemIds['BUY_LINK']?>" href="javascript:void(0)" rel="nofollow"> </a>
				</div>
			<? endif; ?>
		<? endif; ?>
	</div>
</div>
