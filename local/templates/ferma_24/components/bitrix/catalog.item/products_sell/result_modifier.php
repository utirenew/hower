<?php

if ( isset($arResult['ITEM']) ) {
	$userId = $arParams['USER']['ID'];

	if(!empty($arResult['ITEM']['DETAIL_PICTURE'])){
		$arResult['ITEM']['CARD_PICTURE'] = CFile::ResizeImageGet($arResult['ITEM']['DETAIL_PICTURE'], array('width' => 200, 'height' => 200), BX_RESIZE_IMAGE_EXACT)['src'];
	}
	else{
		$arResult['ITEM']['CARD_PICTURE'] = SITE_TEMPLATE_PATH . '/components/bitrix/catalog.element/product_sell/images/no_photo.png';
	}

	if (array_key_exists($arResult['ITEM']['~ID']*1, $arParams['BASKET_DELAYED'])) {
		$arResult['ITEM']['BASKET_DELAYED'][$userId]['ITEM_ADDED'] = true;
		$arResult['ITEM']['BASKET_DELAYED'][$userId]['BASKET_IS_DELAYED_ITEM'] = $arParams['BASKET_DELAYED'][ $arResult['ITEM']['~ID']*1 ]['DELAY'];
		$arResult['ITEM']['BASKET_DELAYED'][$userId]['BASKET_DELAYED_ITEM_ID'] = $arParams['BASKET_DELAYED'][ $arResult['ITEM']['~ID']*1 ]['ID'];
	}

	if (array_key_exists($arResult['ITEM']['ITEM_MEASURE']['ID'], $arParams['MEASURE_SELECTION'])) {
		$arResult['ITEM']['ITEM_MEASURE']['TITLE_FULL'] = $arParams['MEASURE_SELECTION'][ $arResult['ITEM']['ITEM_MEASURE']['ID'] ];
	} else {
		$arResult['ITEM']['ITEM_MEASURE']['TITLE_FULL'] = '';
	}

	if (array_key_exists($arResult['ITEM']['ID'], $arParams['CAMPAIGNS_SELECTION'])) {
		$arResult['ITEM']['ITEM_CAMPAIGNS']['NAME'] = $arParams['CAMPAIGNS_SELECTION'][ $arResult['ITEM']['ID'] ]['NAME'];
	} else {
		$arResult['ITEM']['ITEM_CAMPAIGNS']['NAME'] = '';
	}

	if (array_key_exists($arResult['ITEM']['ID'], $arParams['PRODUCT_OWNERS'])) {
		$arResult['ITEM']['OWNER']['NAME'] 		= $arParams['PRODUCT_OWNERS'][$arResult['ITEM']['ID']]['NAME'];
		$arResult['ITEM']['OWNER']['LAST_NAME'] = $arParams['PRODUCT_OWNERS'][$arResult['ITEM']['ID']]['LAST_NAME'];
		$arResult['ITEM']['OWNER']['ID'] 		= $arParams['PRODUCT_OWNERS'][$arResult['ITEM']['ID']]['ID'];
		$arResult['ITEM']['OWNER']['USER_ROLE'] = $arParams['PRODUCT_OWNERS'][$arResult['ITEM']['ID']]['USER_ROLE'];
	} else {
		$arResult['ITEM']['OWNER']['NAME'] 		= '';
		$arResult['ITEM']['OWNER']['LAST_NAME'] = '';
		$arResult['ITEM']['OWNER']['ID'] 		= '';
		$arResult['ITEM']['OWNER']['USER_ROLE'] = '';
	}


	if (USER_ROLE !== 'guest') {
		$dbBasketItems = CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
			),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"PRODUCT_ID" => $item["~ID"],
				"ORDER_ID" => "NULL",
				"DELAY" => "Y"
			),
			false,
			false,
			array("PRODUCT_ID")
		);
		while ($arItems = $dbBasketItems->Fetch())
		{
			$itInDelay = $arItems['PRODUCT_ID'];
		}
	}
		$arResult['ITEM']['OWNER']['itInDelay'] = $itInDelay;
		$arResult['USER_ID'] 					= $arParams['USER']['ID'];
}
