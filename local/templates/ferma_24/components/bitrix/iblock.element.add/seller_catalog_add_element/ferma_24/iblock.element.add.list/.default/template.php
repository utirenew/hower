<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$colspan = 2;
if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?ShowNote($arResult["MESSAGE"])?>
<?endif?>
    <div class="order-item">
<?if($arResult["NO_USER"] == "N"):?>
    <div class="order-header">
        <span style="font-size: 25px;"><?=GetMessage("IBLOCK_ADD_LIST_TITLE")?></span>

    <?if (in_array(9, $USER->GetUserGroupArray())):?>
        <a href="<?=$arParams["EDIT_URL"]?>?edit=Y" class="order-desc-btn 1 fl-r"><?=GetMessage("IBLOCK_ADD_LINK_TITLE")?></a>
    <?else:?>
        <?=GetMessage("IBLOCK_LIST_CANT_ADD_MORE")?>
    <?endif?>
    </div>
    <?if (count($arResult["ELEMENTS"]) > 0):?>
        <?foreach ($arResult["ELEMENTS"] as $arElement):?>
    <div class="order-info" style="width: 65%; padding-top:20px;">
        <div class="order-number-block">
            <div class="order-number-2">
				<span class="cabinet-title-2">ID ТОВАРА</span>
				<?=$arElement['ID']?>
			</div>
            <div class="order-process">
                <small><?=is_array($arResult["WF_STATUS"]) ? $arResult["WF_STATUS"][$arElement["WF_STATUS_ID"]] : $arResult["ACTIVE_STATUS"][$arElement["ACTIVE"]]?></small>
            </div>
        </div>
        <?$months = array( 1 => 'Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь' );?>
        <div class="order-date-block" style="width:135px;">
            <span class="order-year"><?=date('Y', strtotime($arElement['DATE_CREATE']));?></span>
            <span class="order-day"><?=date( 'd', strtotime($arElement['DATE_CREATE'])).' '.$months[date( 'n', strtotime($arElement['DATE_CREATE']))];?></span>
        </div>
    </div>
    <div class="order-product-block">
        <div class="order-product-pic">
            <img src="<?= CFile::GetPath($arElement['DETAIL_PICTURE']);?>" alt="">
        </div>
        <div class="order-product-name-block">
            <div class="order-product-name"><?=$arElement["NAME"]?></div>
            <div class="order-product-fermer"><?=$arElement["USER_NAME"]?></div>
        </div>
    </div>
    <div class="order-total-block">
        <?
        $res = CPrice::GetList(
            array(),
            array(
            "PRODUCT_ID" => $arElement['ID'],
            "CATALOG_GROUP_ID" => 1
            )
        );
        $price = $res->Fetch();
        ?>
        <div class="total-order-price-lk">
			<span class="cabinet-title-2">Цена</span>
			<?=$price['PRICE'];?>
		</div>
        <?if ($arElement["CAN_EDIT"] == "Y"):?>
            <?if ($arElement["CAN_EDIT"] == "Y"):?>
            <a href="<?=$arElement["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>" class="order-desc-btn cabinet-link-with-symbol-1">
                <?=GetMessage("IBLOCK_ADD_LIST_EDIT")?>
                <?else:?>&nbsp;
                <?endif?>
            </a>
        <?endif?>
        <?if ($arResult["CAN_DELETE"] == "Y"):?>
            <?if ($arElement["CAN_DELETE"] == "Y"):?>
                <a href="?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>" onClick="return confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')" class="order-desc-btn cabinet-link-with-symbol-2">
                    <?=GetMessage("IBLOCK_ADD_LIST_DELETE")?>
                </a>
            <?else:?>&nbsp;
            <?endif?>
        <?endif?>
    </div>
        <br>
        <?endforeach?>
        <?else:?>
    <?endif?>
<?endif?>
</div>
<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>
