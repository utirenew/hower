<?php
// d('DEBUG: result_modifier init');
define('MEASURE_PROPERTY', 'MEASURE');

$arResult['custom_filelds'] = [];

$arResult["PROPERTY_LIST"][]     = 'PRICE';
$arResult["PROPERTY_REQUIRED"][] = 'PRICE';
$arResult["PROPERTY_LIST"][]     = MEASURE_PROPERTY;
$arResult["PROPERTY_REQUIRED"][] = MEASURE_PROPERTY;


$measure_product_list = Bitrix\Catalog\Model\Product::getList([
		'select' => [MEASURE_PROPERTY],
		'filter' => ["ID" => $arResult['ELEMENT']['ID']],
	]
);

$measure_product_list_row = $measure_product_list->Fetch();
$measure_id               = $measure_product_list_row[MEASURE_PROPERTY];


$price_list = Bitrix\Catalog\Model\Price::getList([
	'select' => ['PRICE'],
	'filter' => [
		"PRODUCT_ID"       => $arResult['ELEMENT']['ID'],
		"CATALOG_GROUP_ID" => 1,
	],
]);

$price = $price_list->Fetch();
if(!$price) $price['PRICE'] = '';


$arResult['custom_filelds']['measure_id'] = $measure_id;
$arResult['custom_filelds']['price']      = $price['PRICE'];


$arResult["PROPERTY_LIST_FULL"][14]['MULTIPLE_CNT']  = 1;
$arResult["PROPERTY_LIST_FULL"][14]['~MULTIPLE_CNT'] = 1;
