<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$this->addExternalCss("/local/templates/ferma_24/css/chosen/chosen.min.css");
$this->addExternalJS("/local/templates/ferma_24/js/chosen.jquery.min.js");

if(CModule::IncludeModule("catalog")){
	$measure_list = CCatalogMeasure::getList();
}
if(!empty($arResult["ERRORS"])):?>
	<?
	ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;
if(strlen($arResult["MESSAGE"]) > 0):?>
	<? ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>

<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<?=bitrix_sessid_post()?>
	<? if($arParams["MAX_FILE_SIZE"] > 0): ?>
		<input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><? endif ?>
	<table class="data-table table table-striped mt-4">
		<? if(is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
			<tbody>
			<? foreach($arResult["PROPERTY_LIST"] as $propertyID): ?>
				<? $flag = false; ?>
				<tr>
					<!-- Col with titles -->
					<td class="col_titles">
						<?
						if(intval($propertyID) > 0){
							echo $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"];
						}
						else{
							echo !empty($arParams["CUSTOM_TITLE_" . $propertyID])?$arParams["CUSTOM_TITLE_" . $propertyID]:GetMessage("IBLOCK_FIELD_" . $propertyID);
						}
						if($propertyID === MEASURE_PROPERTY){
							echo "Единица измерения";
						}
						if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])){
							echo '<span class="starrequired">*</span>';
						}
						?>
					</td>
					<!-- Col with titles -->
					<!-- Col with fields -->
					<td>
						<?
						if(intval($propertyID) > 0){
							if($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T" && $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"){
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
							}
							elseif(($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S" || $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N") && $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"){
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
							}

						}
						elseif(($propertyID == "TAGS") && CModule::IncludeModule('search')) $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y"){
							$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)?count($arResult["ELEMENT_PROPERTIES"][$propertyID]):0;
							$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
						}
						else{
							$inputNum = 1;
						}

						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"]) $INPUT_TYPE = "USER_TYPE";
						else{
							$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];
						}
						if($propertyID == 'PRICE'){
							$flag       = true;
							$INPUT_TYPE = 'N';
						}
						if($propertyID == MEASURE_PROPERTY){
							$INPUT_TYPE = 'CATALOG_COMMERCE_MEASURE_TYPE';
						}


						switch($INPUT_TYPE):
							case "USER_TYPE":
								for($i = 0; $i < $inputNum; $i++){
									if($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0){
										$value       = intval($propertyID) > 0?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"]:$arResult["ELEMENT"][$propertyID];
										$description = intval($propertyID) > 0?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"]:"";
									}
									elseif($i == 0){
										$value       = intval($propertyID) <= 0?"":$arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										$description = "";
									}
									else{
										$value       = "";
										$description = "";
									}
									echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"], array(
										$arResult["PROPERTY_LIST_FULL"][$propertyID],
										array(
											"VALUE"       => $value,
											"DESCRIPTION" => $description,
										),
										array(
											"VALUE"       => "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]",
											"DESCRIPTION" => "PROPERTY[" . $propertyID . "][" . $i . "][DESCRIPTION]",
											"FORM_NAME"   => "iblock_add",
										),
									));
									?><br/><?
								}
								break;
							case "TAGS":
								$APPLICATION->IncludeComponent("bitrix:search.tags.input", "", array(
									"VALUE" => $arResult["ELEMENT"][$propertyID],
									"NAME"  => "PROPERTY[" . $propertyID . "][0]",
									"TEXT"  => 'size="' . $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] . '"',
								), null, array("HIDE_ICONS" => "Y"));
								break;
							case "HTML":
								$LHE = new CHTMLEditor();
								$LHE->Show(array(
									'name'                => "PROPERTY[" . $propertyID . "][0]",
									'id'                  => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"),
									'inputName'           => "PROPERTY[" . $propertyID . "][0]",
									'content'             => $arResult["ELEMENT"][$propertyID],
									'width'               => '100%',
									'minBodyWidth'        => 350,
									'normalBodyWidth'     => 555,
									'height'              => '200',
									'bAllowPhp'           => false,
									'limitPhpAccess'      => false,
									'autoResize'          => true,
									'autoResizeOffset'    => 40,
									'useFileDialogs'      => false,
									'saveOnBlur'          => true,
									'showTaskbars'        => false,
									'showNodeNavi'        => false,
									'askBeforeUnloadPage' => true,
									'bbCode'              => false,
									'siteId'              => SITE_ID,
									'controlsMap'         => array(
										array('id' => 'Bold', 'compact' => true, 'sort' => 80),
										array('id' => 'Italic', 'compact' => true, 'sort' => 90),
										array('id' => 'Underline', 'compact' => true, 'sort' => 100),
										array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
										array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
										array('id' => 'Color', 'compact' => true, 'sort' => 130),
										array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
										array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
										array('separator' => true, 'compact' => false, 'sort' => 145),
										array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
										array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
										array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
										array('separator' => true, 'compact' => false, 'sort' => 200),
										array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
										array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
										array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
										array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
										array('separator' => true, 'compact' => false, 'sort' => 290),
										array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
										array('id' => 'More', 'compact' => true, 'sort' => 400),
									),
								));
								break;
							case "T":
								for($i = 0; $i < $inputNum; $i++){

									if($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0){
										$value = intval($propertyID) > 0?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"]:$arResult["ELEMENT"][$propertyID];
									}
									elseif($i == 0){
										$value = intval($propertyID) > 0?"":$arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									}
									else{
										$value = "";
									}
									?>
									<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="6"
									          name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
									<?
								}
								break;
							case "S":
								$value = "";
								if($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0){
									$value = $arResult["ELEMENT"][$propertyID];
								} ?>

								<input type="text" name="PROPERTY[<?=$propertyID?>][0]" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"];?>" class="form-control"
								       value="<?=$value?>"/>
								<?
								break;
							case "N":
								if(!$flag):
									for($i = 0; $i < $inputNum; $i++){
										if($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0){
											$value = intval($propertyID) > 0?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"]:$arResult["ELEMENT"][$propertyID];
										}
										elseif($i == 0){
											$value = intval($propertyID) <= 0?"":$arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										}
										else{
											$value = "";
										}
										?>
										<input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>][][VALUE]"
										       size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"];?>"
										       class="form-control"
										       value="<?=$value?>"/>
										<? if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"): ?>
											<? $APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
												'FORM_NAME'   => 'iblock_add',
												'INPUT_NAME'  => "PROPERTY[" . $propertyID . "][" . $i . "]",
												'INPUT_VALUE' => $value,
											), null, array('HIDE_ICONS' => 'Y'));
											?>
											<br><br>
											<small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small>
											<br>
										<? endif ?>
										<?
									}
								else:
									?>
									<input type="text" name="<?=$propertyID?>" class="form-control" value="<?=$arResult['custom_filelds']['price']?>">
								<? endif;
								break;
							case "CATALOG_COMMERCE_MEASURE_TYPE":
								?>
								<select class="select-group" name="<?=$propertyID?>" data-placeholder="<?=GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?>">
									<?php
									if($arParams['ID'] === 0){
										echo '<option value=""></option>';
									}

									while($measure_list_item = $measure_list->GetNext()){
										if($measure_list_item['ID'] === $arResult['custom_filelds']['measure_id']){
											$measure_selected_option = 'selected';
										}
										else{
											$measure_selected_option = '';
										}
										echo '<option value="' . $measure_list_item['ID'] . '" ' . $measure_selected_option . '>' . $measure_list_item['MEASURE_TITLE'] . '</option>';
									}
									?>
								</select>
								<?php
								break;
							case "F":
								for($i = 0; $i < $inputNum; $i++){
									$value = intval($propertyID) > 0?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"]:$arResult["ELEMENT"][$propertyID];
									?>

									<input type="hidden"
									       name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"]?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"]:$i?>]"
									       value="<?=$value?>">
									<input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" class="form-control-file"
									       name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"]?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"]:$i?>">
									<br/>
									<? if(!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])): ?>
										<div class="form-group form-check">
											<input type="checkbox" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" class="form-check-input"
											       name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"]?$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"]:$i?>]">
											<label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label>
										</div>
										<? if($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]): ?>
											<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" class="w-50" alt="">
										<? else: ?>
											<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br/>
											<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br/>
											[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br/>
										<? endif; ?>
									<? endif; ?>
								<? } ?>

								<?php if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] === 'CT_SL_PT_SET_MORE_PHOTO'): ?>
								<button type="button" class="btn btn-outline-dark" data-jquery-selector="cabinet-add-pruduct-button-1">Добавить файл</button>
							<?php endif; ?>

								<?php
								break;
							case "L":
								if($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C") $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y"?"checkbox":"radio";
								else
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y"?"multiselect":"dropdown";

								switch($type):
									case "checkbox":
									case "radio":
										foreach($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum){
											$checked = false;
											if($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0){
												if(is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])){
													foreach($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum){
														if($arElEnum["VALUE"] == $key){
															$checked = true;
															break;
														}
													}
												}
											}
											elseif($arEnum["DEF"] == "Y") $checked = true;


											?>
											<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox"?"[" . $key . "]":""?>" value="<?=$key?>"
											       id="property_<?=$key?>" <?=$checked?" checked=\"checked\"":""?>/>
											<label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br/>
											<?
										}
										break;

									case "dropdown":
									case "multiselect":
										if($arParams['ID'] !== 0 && $propertyID == 'IBLOCK_SECTION'){
											$multiselect_allow = 'disabled';
										}
										else{
											$multiselect_allow = '';
										}

										if(intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT";

										if(!function_exists('isChecked')){
											function isChecked($arParams, $arResult, $sKey, $propertyID, $key, $arEnum){
												$checked = false;
												if($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0){
													foreach($arResult[$sKey][$propertyID] as $elKey => $arElEnum){
														if($key == $arElEnum["VALUE"]){
															$checked = true;
															break;
														}
													}
												}
												elseif($arEnum["DEF"] == "Y") $checked = true;

												return $checked;
											}
										}

										if($type == "multiselect"){
											$typeValue = '[]';
										}
										else{
											$typeValue = '';
										}
										?>

										<select name="PROPERTY[<?=$propertyID?>]<?=$type == 'multiselect'?'[]':'[][VALUE]'?>" <?php echo $multiselect_allow; ?> class="select-group"
										        data-placeholder="<?
										        echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA") ?>">
											<option value=""></option>
											<?

											$inputValue = '';
											$first      = true;

											foreach($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum){
												$dot_count = substr_count($arEnum["VALUE"], ' . ');
												$text      = substr($arEnum['VALUE'], $dot_count * 3);

												if($dot_count === 1){
													if(!$first) echo '</optgroup>';
													echo '<optgroup label="', $text, '">';
													$first = false;
												}
												else{
													$option_disabled_value = '';
													$isCheckedValue        = isChecked($arParams, $arResult, $sKey, $propertyID, $key, $arEnum);
													if($isCheckedValue) $inputValue = $key;
													?>
													<option value="<?=$key?>" <?=$isCheckedValue?"selected":""?>><?=$text?></option>
													<?
												}
											}
											echo '</optgroup>';
											?>
										</select>
										<div class="">
											<? if($multiselect_allow === 'disabled'): ?>
												<input type="hidden" name="PROPERTY[<?=$propertyID?>]<?=$typeValue?>" value="<?=$inputValue?>">
											<? endif; ?>
										</div>
										<?
										break;
								endswitch;
								break;
						endswitch;

						?>
					</td>
					<!-- Col with fields -->
				</tr>
			<? endforeach; ?>
			<!-- Captcha begin -->
			<? if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
				<tr>
					<td><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></td>
					<td>
						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"/>
					</td>
				</tr>
				<tr>
					<td><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</td>
					<td>
						<input type="text" name="captcha_word" maxlength="50" value="">
					</td>
				</tr>
			<? endif ?>
			<!-- Captcha end -->
			</tbody>
		<? endif ?>
		<!-- Buttons begin -->
		<tfoot>
		<tr>
			<td colspan="2">
				<div class="btn-group">
					<input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" class="btn btn-dark">
					<? if(strlen($arParams["LIST_URL"]) > 0): ?>
						<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" class="btn btn-dark">
						<input type="button" name="iblock_cancel" value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
						       onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';" class="btn btn-light">
					<? endif ?>
				</div>
			</td>
		</tr>
		</tfoot>
		<!-- Buttons end -->
	</table>
</form>
