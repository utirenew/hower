BX.ready(function () {
	$('[data-jquery-selector="cabinet-add-pruduct-button-1"]').click(function (e) {
		let button = $(e.target),
			wrapper = button.parent('td'),
			inputCollletion = wrapper.find('input[type="file"]'),
			inputNameNumberMax = 0,
			inputNameNumberNext;


		$(inputCollletion).each(function (index, input) {
			let inputName = $(input).attr('name'),
				inputNameNumberString = inputName.substr(inputName.lastIndexOf('_') + 1, inputName.length),
				inputNameNumber = parseInt(inputNameNumberString, 10);

			if (inputNameNumber > inputNameNumberMax) inputNameNumberMax = inputNameNumber;
		});

		inputNameNumberNext = inputNameNumberMax + 1;

		let html = '<input type="hidden" name="PROPERTY[27][' + inputNameNumberNext + ']" value=""><input type="file"  class="form-control-file" size="30" name="PROPERTY_FILE_27_' + inputNameNumberNext + '"><br>';

		$(button).before(html);
	});

	$('.select-group').chosen({
		placeholder_text_single: 'Выберите раздел',
		width: '100%'
	});
});
