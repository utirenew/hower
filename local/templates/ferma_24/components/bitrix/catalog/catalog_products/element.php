<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

if(isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') $basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION'])?array($arParams['COMMON_ADD_TO_BASKET_ACTION']):array());
else
	$basketAction = (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION'])?$arParams['DETAIL_ADD_TO_BASKET_ACTION']:array());

$componentElementParams = array(
	'IBLOCK_TYPE'                => $arParams['IBLOCK_TYPE'],
	'IBLOCK_ID'                  => $arParams['IBLOCK_ID'],
	'PROPERTY_CODE'              => (isset($arParams['DETAIL_PROPERTY_CODE'])?$arParams['DETAIL_PROPERTY_CODE']:[]),
	'META_KEYWORDS'              => $arParams['DETAIL_META_KEYWORDS'],
	'META_DESCRIPTION'           => $arParams['DETAIL_META_DESCRIPTION'],
	'BROWSER_TITLE'              => $arParams['DETAIL_BROWSER_TITLE'],
	'SET_CANONICAL_URL'          => $arParams['DETAIL_SET_CANONICAL_URL'],
	'BASKET_URL'                 => $arParams['BASKET_URL'],
	'ACTION_VARIABLE'            => $arParams['ACTION_VARIABLE'],
	'PRODUCT_ID_VARIABLE'        => $arParams['PRODUCT_ID_VARIABLE'],
	'SECTION_ID_VARIABLE'        => $arParams['SECTION_ID_VARIABLE'],
	'CHECK_SECTION_ID_VARIABLE'  => (isset($arParams['DETAIL_CHECK_SECTION_ID_VARIABLE'])?$arParams['DETAIL_CHECK_SECTION_ID_VARIABLE']:''),
	'PRODUCT_QUANTITY_VARIABLE'  => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'PRODUCT_PROPS_VARIABLE'     => $arParams['PRODUCT_PROPS_VARIABLE'],
	'CACHE_TYPE'                 => $arParams['CACHE_TYPE'],
	'CACHE_TIME'                 => $arParams['CACHE_TIME'],
	'CACHE_GROUPS'               => $arParams['CACHE_GROUPS'],
	'SET_TITLE'                  => $arParams['SET_TITLE'],
	'SET_LAST_MODIFIED'          => $arParams['SET_LAST_MODIFIED'],
	'MESSAGE_404'                => $arParams['~MESSAGE_404'],
	'SET_STATUS_404'             => $arParams['SET_STATUS_404'],
	'SHOW_404'                   => $arParams['SHOW_404'],
	'FILE_404'                   => $arParams['FILE_404'],
	'PRICE_CODE'                 => $arParams['~PRICE_CODE'],
	'USE_PRICE_COUNT'            => $arParams['USE_PRICE_COUNT'],
	'SHOW_PRICE_COUNT'           => $arParams['SHOW_PRICE_COUNT'],
	'PRICE_VAT_INCLUDE'          => $arParams['PRICE_VAT_INCLUDE'],
	'PRICE_VAT_SHOW_VALUE'       => $arParams['PRICE_VAT_SHOW_VALUE'],
	'USE_PRODUCT_QUANTITY'       => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_PROPERTIES'         => (isset($arParams['PRODUCT_PROPERTIES'])?$arParams['PRODUCT_PROPERTIES']:[]),
	'ADD_PROPERTIES_TO_BASKET'   => (isset($arParams['ADD_PROPERTIES_TO_BASKET'])?$arParams['ADD_PROPERTIES_TO_BASKET']:''),
	'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES'])?$arParams['PARTIAL_PRODUCT_PROPERTIES']:''),
	'LINK_IBLOCK_TYPE'           => $arParams['LINK_IBLOCK_TYPE'],
	'LINK_IBLOCK_ID'             => $arParams['LINK_IBLOCK_ID'],
	'LINK_PROPERTY_SID'          => $arParams['LINK_PROPERTY_SID'],
	'LINK_ELEMENTS_URL'          => $arParams['LINK_ELEMENTS_URL'],

	'OFFERS_CART_PROPERTIES' => (isset($arParams['OFFERS_CART_PROPERTIES'])?$arParams['OFFERS_CART_PROPERTIES']:[]),
	'OFFERS_FIELD_CODE'      => $arParams['DETAIL_OFFERS_FIELD_CODE'],
	'OFFERS_PROPERTY_CODE'   => (isset($arParams['DETAIL_OFFERS_PROPERTY_CODE'])?$arParams['DETAIL_OFFERS_PROPERTY_CODE']:[]),
	'OFFERS_SORT_FIELD'      => $arParams['OFFERS_SORT_FIELD'],
	'OFFERS_SORT_ORDER'      => $arParams['OFFERS_SORT_ORDER'],
	'OFFERS_SORT_FIELD2'     => $arParams['OFFERS_SORT_FIELD2'],
	'OFFERS_SORT_ORDER2'     => $arParams['OFFERS_SORT_ORDER2'],

	'ELEMENT_ID'                      => $arResult['VARIABLES']['ELEMENT_ID'],
	'ELEMENT_CODE'                    => $arResult['VARIABLES']['ELEMENT_CODE'],
	'SECTION_ID'                      => $arResult['VARIABLES']['SECTION_ID'],
	'SECTION_CODE'                    => $arResult['VARIABLES']['SECTION_CODE'],
	'SECTION_URL'                     => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['section'],
	'DETAIL_URL'                      => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['element'],
	'CONVERT_CURRENCY'                => $arParams['CONVERT_CURRENCY'],
	'CURRENCY_ID'                     => $arParams['CURRENCY_ID'],
	'HIDE_NOT_AVAILABLE'              => $arParams['HIDE_NOT_AVAILABLE'],
	'HIDE_NOT_AVAILABLE_OFFERS'       => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
	'USE_ELEMENT_COUNTER'             => $arParams['USE_ELEMENT_COUNTER'],
	'SHOW_DEACTIVATED'                => $arParams['SHOW_DEACTIVATED'],
	'USE_MAIN_ELEMENT_SECTION'        => $arParams['USE_MAIN_ELEMENT_SECTION'],
	'STRICT_SECTION_CHECK'            => (isset($arParams['DETAIL_STRICT_SECTION_CHECK'])?$arParams['DETAIL_STRICT_SECTION_CHECK']:''),
	'ADD_PICT_PROP'                   => $arParams['ADD_PICT_PROP'],
	'LABEL_PROP'                      => $arParams['LABEL_PROP'],
	'LABEL_PROP_MOBILE'               => $arParams['LABEL_PROP_MOBILE'],
	'LABEL_PROP_POSITION'             => $arParams['LABEL_PROP_POSITION'],
	'OFFER_ADD_PICT_PROP'             => $arParams['OFFER_ADD_PICT_PROP'],
	'OFFER_TREE_PROPS'                => (isset($arParams['OFFER_TREE_PROPS'])?$arParams['OFFER_TREE_PROPS']:[]),
	'PRODUCT_SUBSCRIPTION'            => $arParams['PRODUCT_SUBSCRIPTION'],
	'SHOW_DISCOUNT_PERCENT'           => $arParams['SHOW_DISCOUNT_PERCENT'],
	'DISCOUNT_PERCENT_POSITION'       => (isset($arParams['DISCOUNT_PERCENT_POSITION'])?$arParams['DISCOUNT_PERCENT_POSITION']:''),
	'SHOW_OLD_PRICE'                  => $arParams['SHOW_OLD_PRICE'],
	'SHOW_MAX_QUANTITY'               => $arParams['SHOW_MAX_QUANTITY'],
	'MESS_SHOW_MAX_QUANTITY'          => (isset($arParams['~MESS_SHOW_MAX_QUANTITY'])?$arParams['~MESS_SHOW_MAX_QUANTITY']:''),
	'RELATIVE_QUANTITY_FACTOR'        => (isset($arParams['RELATIVE_QUANTITY_FACTOR'])?$arParams['RELATIVE_QUANTITY_FACTOR']:''),
	'MESS_RELATIVE_QUANTITY_MANY'     => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY'])?$arParams['~MESS_RELATIVE_QUANTITY_MANY']:''),
	'MESS_RELATIVE_QUANTITY_FEW'      => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW'])?$arParams['~MESS_RELATIVE_QUANTITY_FEW']:''),
	'MESS_BTN_BUY'                    => (isset($arParams['~MESS_BTN_BUY'])?$arParams['~MESS_BTN_BUY']:''),
	'MESS_BTN_ADD_TO_BASKET'          => (isset($arParams['~MESS_BTN_ADD_TO_BASKET'])?$arParams['~MESS_BTN_ADD_TO_BASKET']:''),
	'MESS_BTN_SUBSCRIBE'              => (isset($arParams['~MESS_BTN_SUBSCRIBE'])?$arParams['~MESS_BTN_SUBSCRIBE']:''),
	'MESS_BTN_DETAIL'                 => (isset($arParams['~MESS_BTN_DETAIL'])?$arParams['~MESS_BTN_DETAIL']:''),
	'MESS_NOT_AVAILABLE'              => (isset($arParams['~MESS_NOT_AVAILABLE'])?$arParams['~MESS_NOT_AVAILABLE']:''),
	'MESS_BTN_COMPARE'                => (isset($arParams['~MESS_BTN_COMPARE'])?$arParams['~MESS_BTN_COMPARE']:''),
	'MESS_PRICE_RANGES_TITLE'         => (isset($arParams['~MESS_PRICE_RANGES_TITLE'])?$arParams['~MESS_PRICE_RANGES_TITLE']:''),
	'MESS_DESCRIPTION_TAB'            => (isset($arParams['~MESS_DESCRIPTION_TAB'])?$arParams['~MESS_DESCRIPTION_TAB']:''),
	'MESS_PROPERTIES_TAB'             => (isset($arParams['~MESS_PROPERTIES_TAB'])?$arParams['~MESS_PROPERTIES_TAB']:''),
	'MESS_COMMENTS_TAB'               => (isset($arParams['~MESS_COMMENTS_TAB'])?$arParams['~MESS_COMMENTS_TAB']:''),
	'MAIN_BLOCK_PROPERTY_CODE'        => (isset($arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE'])?$arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE']:''),
	'MAIN_BLOCK_OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE'])?$arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE']:''),
	'USE_VOTE_RATING'                 => $arParams['DETAIL_USE_VOTE_RATING'],
	'VOTE_DISPLAY_AS_RATING'          => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING'])?$arParams['DETAIL_VOTE_DISPLAY_AS_RATING']:''),
	'USE_COMMENTS'                    => $arParams['DETAIL_USE_COMMENTS'],
	'BLOG_USE'                        => (isset($arParams['DETAIL_BLOG_USE'])?$arParams['DETAIL_BLOG_USE']:''),
	'BLOG_URL'                        => (isset($arParams['DETAIL_BLOG_URL'])?$arParams['DETAIL_BLOG_URL']:''),
	'BLOG_EMAIL_NOTIFY'               => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY'])?$arParams['DETAIL_BLOG_EMAIL_NOTIFY']:''),
	'VK_USE'                          => (isset($arParams['DETAIL_VK_USE'])?$arParams['DETAIL_VK_USE']:''),
	'VK_API_ID'                       => (isset($arParams['DETAIL_VK_API_ID'])?$arParams['DETAIL_VK_API_ID']:'API_ID'),
	'FB_USE'                          => (isset($arParams['DETAIL_FB_USE'])?$arParams['DETAIL_FB_USE']:''),
	'FB_APP_ID'                       => (isset($arParams['DETAIL_FB_APP_ID'])?$arParams['DETAIL_FB_APP_ID']:''),
	'BRAND_USE'                       => (isset($arParams['DETAIL_BRAND_USE'])?$arParams['DETAIL_BRAND_USE']:'N'),
	'BRAND_PROP_CODE'                 => (isset($arParams['DETAIL_BRAND_PROP_CODE'])?$arParams['DETAIL_BRAND_PROP_CODE']:''),
	'DISPLAY_NAME'                    => (isset($arParams['DETAIL_DISPLAY_NAME'])?$arParams['DETAIL_DISPLAY_NAME']:''),
	'IMAGE_RESOLUTION'                => (isset($arParams['DETAIL_IMAGE_RESOLUTION'])?$arParams['DETAIL_IMAGE_RESOLUTION']:''),
	'PRODUCT_INFO_BLOCK_ORDER'        => (isset($arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER'])?$arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER']:''),
	'PRODUCT_PAY_BLOCK_ORDER'         => (isset($arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER'])?$arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER']:''),
	'ADD_DETAIL_TO_SLIDER'            => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER'])?$arParams['DETAIL_ADD_DETAIL_TO_SLIDER']:''),
	'TEMPLATE_THEME'                  => (isset($arParams['TEMPLATE_THEME'])?$arParams['TEMPLATE_THEME']:''),
	'ADD_SECTIONS_CHAIN'              => (isset($arParams['ADD_SECTIONS_CHAIN'])?$arParams['ADD_SECTIONS_CHAIN']:''),
	'ADD_ELEMENT_CHAIN'               => (isset($arParams['ADD_ELEMENT_CHAIN'])?$arParams['ADD_ELEMENT_CHAIN']:''),
	'DISPLAY_PREVIEW_TEXT_MODE'       => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'])?$arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']:''),
	'DETAIL_PICTURE_MODE'             => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE'])?$arParams['DETAIL_DETAIL_PICTURE_MODE']:array()),
	'ADD_TO_BASKET_ACTION'            => $basketAction,
	'ADD_TO_BASKET_ACTION_PRIMARY'    => (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY'])?$arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY']:null),
	'SHOW_CLOSE_POPUP'                => isset($arParams['COMMON_SHOW_CLOSE_POPUP'])?$arParams['COMMON_SHOW_CLOSE_POPUP']:'',
	'DISPLAY_COMPARE'                 => (isset($arParams['USE_COMPARE'])?$arParams['USE_COMPARE']:''),
	'COMPARE_PATH'                    => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
	'USE_COMPARE_LIST'                => 'Y',
	'BACKGROUND_IMAGE'                => (isset($arParams['DETAIL_BACKGROUND_IMAGE'])?$arParams['DETAIL_BACKGROUND_IMAGE']:''),
	'COMPATIBLE_MODE'                 => (isset($arParams['COMPATIBLE_MODE'])?$arParams['COMPATIBLE_MODE']:''),
	'DISABLE_INIT_JS_IN_COMPONENT'    => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT'])?$arParams['DISABLE_INIT_JS_IN_COMPONENT']:''),
	'SET_VIEWED_IN_COMPONENT'         => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT'])?$arParams['DETAIL_SET_VIEWED_IN_COMPONENT']:''),
	'SHOW_SLIDER'                     => (isset($arParams['DETAIL_SHOW_SLIDER'])?$arParams['DETAIL_SHOW_SLIDER']:''),
	'SLIDER_INTERVAL'                 => (isset($arParams['DETAIL_SLIDER_INTERVAL'])?$arParams['DETAIL_SLIDER_INTERVAL']:''),
	'SLIDER_PROGRESS'                 => (isset($arParams['DETAIL_SLIDER_PROGRESS'])?$arParams['DETAIL_SLIDER_PROGRESS']:''),
	'USE_ENHANCED_ECOMMERCE'          => (isset($arParams['USE_ENHANCED_ECOMMERCE'])?$arParams['USE_ENHANCED_ECOMMERCE']:''),
	'DATA_LAYER_NAME'                 => (isset($arParams['DATA_LAYER_NAME'])?$arParams['DATA_LAYER_NAME']:''),
	'BRAND_PROPERTY'                  => (isset($arParams['BRAND_PROPERTY'])?$arParams['BRAND_PROPERTY']:''),

	'USE_GIFTS_DETAIL'                => $arParams['USE_GIFTS_DETAIL']?:'Y',
	'USE_GIFTS_MAIN_PR_SECTION_LIST'  => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST']?:'Y',
	'GIFTS_SHOW_DISCOUNT_PERCENT'     => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
	'GIFTS_SHOW_OLD_PRICE'            => $arParams['GIFTS_SHOW_OLD_PRICE'],
	'GIFTS_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
	'GIFTS_DETAIL_HIDE_BLOCK_TITLE'   => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
	'GIFTS_DETAIL_TEXT_LABEL_GIFT'    => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
	'GIFTS_DETAIL_BLOCK_TITLE'        => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
	'GIFTS_SHOW_NAME'                 => $arParams['GIFTS_SHOW_NAME'],
	'GIFTS_SHOW_IMAGE'                => $arParams['GIFTS_SHOW_IMAGE'],
	'GIFTS_MESS_BTN_BUY'              => $arParams['~GIFTS_MESS_BTN_BUY'],
	'GIFTS_PRODUCT_BLOCKS_ORDER'      => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
	'GIFTS_SHOW_SLIDER'               => $arParams['LIST_SHOW_SLIDER'],
	'GIFTS_SLIDER_INTERVAL'           => isset($arParams['LIST_SLIDER_INTERVAL'])?$arParams['LIST_SLIDER_INTERVAL']:'',
	'GIFTS_SLIDER_PROGRESS'           => isset($arParams['LIST_SLIDER_PROGRESS'])?$arParams['LIST_SLIDER_PROGRESS']:'',

	'GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
	'GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'        => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
	'GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'   => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'],
);

if(isset($arParams['USER_CONSENT'])) $componentElementParams['USER_CONSENT'] = $arParams['USER_CONSENT'];
if(isset($arParams['USER_CONSENT_ID'])) $componentElementParams['USER_CONSENT_ID'] = $arParams['USER_CONSENT_ID'];
if(isset($arParams['USER_CONSENT_IS_CHECKED'])) $componentElementParams['USER_CONSENT_IS_CHECKED'] = $arParams['USER_CONSENT_IS_CHECKED'];
if(isset($arParams['USER_CONSENT_IS_LOADED'])) $componentElementParams['USER_CONSENT_IS_LOADED'] = $arParams['USER_CONSENT_IS_LOADED'];

if (key_exists('VIEW_DELIVERY_ADDRESS', $arParams)) {
    $componentElementParams['VIEW_DELIVERY_ADDRESS'] = $arParams['VIEW_DELIVERY_ADDRESS'];
} else {
    $componentElementParams['VIEW_DELIVERY_ADDRESS'] = 'N';
}

$elementId = $APPLICATION->IncludeComponent('bitrix:catalog.element', 'product_sell', $componentElementParams, $component);

$GLOBALS['CATALOG_CURRENT_ELEMENT_ID'] = $elementId;

// Блок табов. Отзывы, Сертификаты, На карте
?>
	<div class="">
		<div class="row">
			<div class="col-12">
				<div class="card-tabs-block nav nav-tabs" id="nav-tab" role="tablist">
					<a class="card-tabs-item active" id="review-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-review"
					   aria-selected="true">Отзывы</a>
					<a class="card-tabs-item" id="sertificat-tab" data-toggle="tab" href="#nav-sertificat" role="tab" aria-controls="nav-sertificat"
					   aria-selected="false">Сертификаты</a>
					<a class="card-tabs-item" id="map-tab" data-toggle="tab" href="#nav-map" role="tab" aria-controls="nav-map"
					   aria-selected="false">На карте</a>
				</div>
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-review" role="tabpanel" aria-labelledby="review-tab">
						<!-- list: comments -->
						<? $APPLICATION->IncludeComponent("bitrix:iblock.element.add.list", "comments", Array(
								"ALLOW_DELETE"     => "Y",
								"ALLOW_EDIT"       => "Y",
								"EDIT_URL"         => "",
								"ELEMENT_ASSOC"    => "CREATED_BY",
								"GROUPS"           => array("2"),
								"IBLOCK_ID"        => "9",
								"IBLOCK_TYPE"      => "comments",
								"MAX_USER_ENTRIES" => "100000",
								"NAV_ON_PAGE"      => "10",
								"SEF_MODE"         => "N",
								"STATUS"           => "ANY",
							)); ?>
						<!-- list: comments -->
						<!-- form: add comment -->
						<? $APPLICATION->IncludeComponent("ferma_24:iblock.element.add.comment.form", "", Array(
								"AJAX_MODE"                     => "N",
								"AJAX_OPTION_ADDITIONAL"        => "",
								"AJAX_OPTION_HISTORY"           => "N",
								"AJAX_OPTION_JUMP"              => "N",
								"AJAX_OPTION_STYLE"             => "Y",
								"ALLOW_DELETE"                  => "Y",
								"ALLOW_EDIT"                    => "Y",
								"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
								"CUSTOM_TITLE_DATE_ACTIVE_TO"   => "",
								"CUSTOM_TITLE_DETAIL_PICTURE"   => "",
								"CUSTOM_TITLE_DETAIL_TEXT"      => "",
								"CUSTOM_TITLE_IBLOCK_SECTION"   => "",
								"CUSTOM_TITLE_NAME"             => "",
								"CUSTOM_TITLE_PREVIEW_PICTURE"  => "",
								"CUSTOM_TITLE_PREVIEW_TEXT"     => "",
								"CUSTOM_TITLE_TAGS"             => "",
								"DEFAULT_INPUT_SIZE"            => "30",
								"DETAIL_TEXT_USE_HTML_EDITOR"   => "Y",
								"ELEMENT_ASSOC"                 => "CREATED_BY",
								"GROUPS"                        => array("1", "9", "10", "11"),
								"IBLOCK_ID"                     => "9",
								// "IBLOCK_TYPE" => "catalog",
								"LEVEL_LAST"                    => "Y",
								"MAX_FILE_SIZE"                 => "0",
								"MAX_LEVELS"                    => "100000",
								"MAX_USER_ENTRIES"              => "100000",
								"NAV_ON_PAGE"                   => "3",
								"PREVIEW_TEXT_USE_HTML_EDITOR"  => "Y",
								"PROPERTY_CODES"                => array("NAME", "DETAIL_TEXT"),
								"PROPERTY_CODES_REQUIRED"       => array("NAME", "DETAIL_TEXT"),
								"RESIZE_IMAGES"                 => "N",
								"SEF_MODE"                      => "N",
								"STATUS"                        => "ANY",
								"STATUS_NEW"                    => "N",
								"USER_MESSAGE_ADD"              => "",
								"USER_MESSAGE_EDIT"             => "",
								"USE_CAPTCHA"                   => "N",
							), false, Array(
								'ACTIVE_COMPONENT' => 'Y',
							)); ?>
						<!-- form: add comment -->

						<!-- <div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="<?php //SITE_TEMPLATE_PATH?>/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div> -->

					</div>
					<div class="tab-pane fade" id="nav-sertificat" role="tabpanel" aria-labelledby="sertificat-tab">
						<span class="exemple">сертификаты</span>
					</div>
					<div class="tab-pane fade" id="nav-map" role="tabpanel" aria-labelledby="map-tab">
						<span class="exemple">карта</span>
					</div>
				</div>
			</div>
		</div>
	</div>

<? // Блок со слайдером популярных товаров ?>

<? $APPLICATION->IncludeComponent("bitrix:catalog.section", "products_slider_leader", Array(
	"ACTION_VARIABLE"                 => "action",
	"ADD_PICT_PROP"                   => "-",
	"ADD_PROPERTIES_TO_BASKET"        => "Y",
	"ADD_SECTIONS_CHAIN"              => "N",
	"ADD_TO_BASKET_ACTION"            => "ADD",
	"AJAX_MODE"                       => "N",
	"AJAX_OPTION_ADDITIONAL"          => "",
	"AJAX_OPTION_HISTORY"             => "N",
	"AJAX_OPTION_JUMP"                => "N",
	"AJAX_OPTION_STYLE"               => "Y",
	"BACKGROUND_IMAGE"                => "-",
	"BASKET_URL"                      => $arParams["BASKET_URL"],
	"BROWSER_TITLE"                   => "-",
	"CACHE_FILTER"                    => "N",
	"CACHE_GROUPS"                    => "Y",
	"CACHE_TIME"                      => "36000000",
	"CACHE_TYPE"                      => "A",
	"COMPATIBLE_MODE"                 => "Y",
	"CONVERT_CURRENCY"                => "N",
	"CUSTOM_FILTER"                   => "",
	"DETAIL_URL"                      => "",
	"DISABLE_INIT_JS_IN_COMPONENT"    => "N",
	"DISCOUNT_PERCENT_POSITION"       => "bottom-right",
	"DISPLAY_BOTTOM_PAGER"            => "N",
	"DISPLAY_COMPARE"                 => "N",
	"DISPLAY_TOP_PAGER"               => "N",
	"ELEMENT_SORT_FIELD"              => "rand",
	"ELEMENT_SORT_FIELD2"             => "id",
	"ELEMENT_SORT_ORDER"              => "asc",
	"ELEMENT_SORT_ORDER2"             => "desc",
	"ENLARGE_PRODUCT"                 => "STRICT",
	"FILTER_NAME"                     => "arrFilter",
	"HIDE_NOT_AVAILABLE"              => "N",
	"HIDE_NOT_AVAILABLE_OFFERS"       => "N",
	"IBLOCK_ID"                       => $arParams["IBLOCK_ID"],
	"IBLOCK_TYPE"                     => "catalog",
	"INCLUDE_SUBSECTIONS"             => "Y",
	"LABEL_PROP"                      => array(),
	"LAZY_LOAD"                       => "N",
	"LINE_ELEMENT_COUNT"              => "3",
	"LOAD_ON_SCROLL"                  => "N",
	"MESSAGE_404"                     => "",
	"MESS_BTN_ADD_TO_BASKET"          => "В корзину",
	"MESS_BTN_BUY"                    => "Купить",
	"MESS_BTN_DETAIL"                 => "Подробнее",
	"MESS_BTN_SUBSCRIBE"              => "Подписаться",
	"MESS_NOT_AVAILABLE"              => "Нет в наличии",
	"META_DESCRIPTION"                => "-",
	"META_KEYWORDS"                   => "-",
	"OFFERS_LIMIT"                    => "5",
	"PAGER_BASE_LINK_ENABLE"          => "N",
	"PAGER_DESC_NUMBERING"            => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL"                  => "N",
	"PAGER_SHOW_ALWAYS"               => "N",
	"PAGER_TEMPLATE"                  => ".default",
	"PAGER_TITLE"                     => "Товары",
	"PAGE_ELEMENT_COUNT"              => "8",
	"PARTIAL_PRODUCT_PROPERTIES"      => "N",
	"PRICE_CODE"                      => array("BASE"),
	"PRICE_VAT_INCLUDE"               => "Y",
	"PRODUCT_BLOCKS_ORDER"            => "price,props,sku,quantityLimit,quantity,buttons",
	"PRODUCT_ID_VARIABLE"             => "id",
	"PRODUCT_PROPS_VARIABLE"          => "prop",
	"PRODUCT_QUANTITY_VARIABLE"       => "quantity",
	"PRODUCT_ROW_VARIANTS"            => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
	"PRODUCT_SUBSCRIPTION"            => "Y",
	"RCM_PROD_ID"                     => $_REQUEST["PRODUCT_ID"],
	"RCM_TYPE"                        => "personal",
	"SECTION_CODE"                    => "",
	"SECTION_ID"                      => $_REQUEST["SECTION_ID"],
	"SECTION_ID_VARIABLE"             => "SECTION_ID",
	"SECTION_URL"                     => "",
	"SECTION_USER_FIELDS"             => array("", ""),
	"SEF_MODE"                        => "N",
	"SET_BROWSER_TITLE"               => "Y",
	"SET_LAST_MODIFIED"               => "N",
	"SET_META_DESCRIPTION"            => "Y",
	"SET_META_KEYWORDS"               => "Y",
	"SET_STATUS_404"                  => "N",
	"SET_TITLE"                       => "Y",
	"SHOW_404"                        => "N",
	"SHOW_ALL_WO_SECTION"             => "Y",
	"SHOW_CLOSE_POPUP"                => "Y",
	"SHOW_DISCOUNT_PERCENT"           => "Y",
	"SHOW_FROM_SECTION"               => "N",
	"SHOW_MAX_QUANTITY"               => "N",
	"SHOW_OLD_PRICE"                  => "Y",
	"SHOW_PRICE_COUNT"                => "1",
	"SHOW_SLIDER"                     => "N",
	"SLIDER_INTERVAL"                 => "3000",
	"SLIDER_PROGRESS"                 => "N",
	"TEMPLATE_THEME"                  => "blue",
	"USE_ENHANCED_ECOMMERCE"          => "N",
	"USE_MAIN_ELEMENT_SECTION"        => "N",
	"USE_PRICE_COUNT"                 => "N",
	"USE_PRODUCT_QUANTITY"            => $arParams['USE_PRODUCT_QUANTITY'],
	'SECTION_TITLE'                   => 'Популярное',
	'SECTION_TEXT_1'                  => 'hot',
	'SECTION_CSS_CLASSNAME'           => 'pop-slider-block',
	'CONTAINER_CSS_CLASSNAME'         => '',
	'ITEMS_CONTAINER_CSS_CLASSNAME'   => 'product-slider',
	'CSS_CLASSNAME_PREFIX'            => 'lider',
    'VIEW_DELIVERY_ADDRESS'           => key_exists('VIEW_DELIVERY_ADDRESS', $arParams) ? $arParams['VIEW_DELIVERY_ADDRESS'] : 'N',
)); ?>