<?php

$arResult['VIEW_DELIVERY_ADDRESS'] = $arParams['VIEW_DELIVERY_ADDRESS'];

if (isset($_GET['sort'])) {
    //sort
    switch ($_GET['sort']) {
        case 'price':
            $arParams["ELEMENT_SORT_FIELD"] = 'catalog_PRICE_1';
            break;
        case 'popularity':
            $arParams["ELEMENT_SORT_FIELD"] = 'show_counter';
            break;
        case 'date':
            $arParams["ELEMENT_SORT_FIELD"] = 'created';
            break;
    }
    //order
    switch ($_GET['order']) {
        case 'asc':
            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
            break;
        case 'desc':
            $arParams["ELEMENT_SORT_ORDER"] = 'desc';
            break;
    }
} else {
            $arParams["ELEMENT_SORT_FIELD"] = 'catalog_PRICE_1';
            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
}
