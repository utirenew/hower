<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

if(!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '') $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y'?'Y':'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar        = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter         = ($arParams['USE_FILTER'] == 'Y');

if($isFilter){
	$arFilter = array(
		"IBLOCK_ID"     => $arParams["IBLOCK_ID"],
		"ACTIVE"        => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if(0 < intval($arResult["VARIABLES"]["SECTION_ID"])) $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	elseif('' != $arResult["VARIABLES"]["SECTION_CODE"]) $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
	if($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")){
		$arCurSection = $obCache->GetVars();
	}
	elseif($obCache->StartDataCache()){
		$arCurSection = array();
		if(Loader::includeModule("iblock")){
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE")){
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if($arCurSection = $dbRes->Fetch()) $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);

				$CACHE_MANAGER->EndTagCache();
			}
			else{
				if(!$arCurSection = $dbRes->Fetch()) $arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if(!isset($arCurSection)) $arCurSection = array();
}

include($_SERVER["DOCUMENT_ROOT"] . "/" . $this->GetFolder() . "/section_one.php");


$APPLICATION->IncludeComponent("bitrix:catalog.section", "products_slider_leader", Array(
	"ACTION_VARIABLE"                 => "action",
	"ADD_PICT_PROP"                   => "-",
	"ADD_PROPERTIES_TO_BASKET"        => "Y",
	"ADD_SECTIONS_CHAIN"              => "N",
	"ADD_TO_BASKET_ACTION"            => "ADD",
	"AJAX_MODE"                       => "N",
	"AJAX_OPTION_ADDITIONAL"          => "",
	"AJAX_OPTION_HISTORY"             => "N",
	"AJAX_OPTION_JUMP"                => "N",
	"AJAX_OPTION_STYLE"               => "Y",
	"BACKGROUND_IMAGE"                => "-",
	"BASKET_URL"                      => $arParams["BASKET_URL"],
	"BROWSER_TITLE"                   => "-",
	"CACHE_FILTER"                    => "N",
	"CACHE_GROUPS"                    => "Y",
	"CACHE_TIME"                      => "36000000",
	"CACHE_TYPE"                      => "A",
	"COMPATIBLE_MODE"                 => "Y",
	"CONVERT_CURRENCY"                => "N",
	"CUSTOM_FILTER"                   => "",
	"DETAIL_URL"                      => "",
	"DISABLE_INIT_JS_IN_COMPONENT"    => "N",
	"DISCOUNT_PERCENT_POSITION"       => "bottom-right",
	"DISPLAY_BOTTOM_PAGER"            => "N",
	"DISPLAY_COMPARE"                 => "N",
	"DISPLAY_TOP_PAGER"               => "N",
	"ELEMENT_SORT_FIELD"              => "rand",
	"ELEMENT_SORT_FIELD2"             => "id",
	"ELEMENT_SORT_ORDER"              => "asc",
	"ELEMENT_SORT_ORDER2"             => "desc",
	"ENLARGE_PRODUCT"                 => "STRICT",
	"FILTER_NAME"                     => "arrFilter",
	"HIDE_NOT_AVAILABLE"              => "N",
	"HIDE_NOT_AVAILABLE_OFFERS"       => "N",
	"IBLOCK_ID"                       => $arParams["IBLOCK_ID"],
	"IBLOCK_TYPE"                     => "catalog",
	"INCLUDE_SUBSECTIONS"             => "Y",
	"LABEL_PROP"                      => array(),
	"LAZY_LOAD"                       => "N",
	"LINE_ELEMENT_COUNT"              => "3",
	"LOAD_ON_SCROLL"                  => "N",
	"MESSAGE_404"                     => "",
	"MESS_BTN_ADD_TO_BASKET"          => "В корзину",
	"MESS_BTN_BUY"                    => "Купить",
	"MESS_BTN_DETAIL"                 => "Подробнее",
	"MESS_BTN_SUBSCRIBE"              => "Подписаться",
	"MESS_NOT_AVAILABLE"              => "Нет в наличии",
	"META_DESCRIPTION"                => "-",
	"META_KEYWORDS"                   => "-",
	"OFFERS_LIMIT"                    => "5",
	"PAGER_BASE_LINK_ENABLE"          => "N",
	"PAGER_DESC_NUMBERING"            => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL"                  => "N",
	"PAGER_SHOW_ALWAYS"               => "N",
	"PAGER_TEMPLATE"                  => ".default",
	"PAGER_TITLE"                     => "Товары",
	"PAGE_ELEMENT_COUNT"              => "8",
	"PARTIAL_PRODUCT_PROPERTIES"      => "N",
	"PRICE_CODE"                      => array("BASE"),
	"PRICE_VAT_INCLUDE"               => "Y",
	"PRODUCT_BLOCKS_ORDER"            => "price,props,sku,quantityLimit,quantity,buttons",
	"PRODUCT_ID_VARIABLE"             => "id",
	"PRODUCT_PROPS_VARIABLE"          => "prop",
	"PRODUCT_QUANTITY_VARIABLE"       => "quantity",
	"PRODUCT_ROW_VARIANTS"            => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
	"PRODUCT_SUBSCRIPTION"            => "Y",
	"RCM_PROD_ID"                     => $_REQUEST["PRODUCT_ID"],
	"RCM_TYPE"                        => "personal",
	"SECTION_CODE"                    => "",
	"SECTION_ID"                      => $_REQUEST["SECTION_ID"],
	"SECTION_ID_VARIABLE"             => "SECTION_ID",
	"SECTION_URL"                     => "",
	"SECTION_USER_FIELDS"             => array("", ""),
	"SEF_MODE"                        => "N",
	"SET_BROWSER_TITLE"               => "Y",
	"SET_LAST_MODIFIED"               => "N",
	"SET_META_DESCRIPTION"            => "Y",
	"SET_META_KEYWORDS"               => "Y",
	"SET_STATUS_404"                  => "N",
	"SET_TITLE"                       => "Y",
	"SHOW_404"                        => "N",
	"SHOW_ALL_WO_SECTION"             => "Y",
	"SHOW_CLOSE_POPUP"                => "Y",
	"SHOW_DISCOUNT_PERCENT"           => "Y",
	"SHOW_FROM_SECTION"               => "N",
	"SHOW_MAX_QUANTITY"               => "N",
	"SHOW_OLD_PRICE"                  => "Y",
	"SHOW_PRICE_COUNT"                => "1",
	"SHOW_SLIDER"                     => "N",
	"SLIDER_INTERVAL"                 => "3000",
	"SLIDER_PROGRESS"                 => "N",
	"TEMPLATE_THEME"                  => "blue",
	"USE_ENHANCED_ECOMMERCE"          => "N",
	"USE_MAIN_ELEMENT_SECTION"        => "N",
	"USE_PRICE_COUNT"                 => "N",
	"USE_PRODUCT_QUANTITY"            => $arParams['USE_PRODUCT_QUANTITY'],
	'SECTION_TITLE'                   => $arParams['SECTION_TITLE'],
	'SECTION_TEXT_1'                  => 'top',
	'SECTION_CSS_CLASSNAME'           => 'lider-slider-block',
	'CONTAINER_CSS_CLASSNAME'         => '',
	'ITEMS_CONTAINER_CSS_CLASSNAME'   => 'product-slider',
	'CSS_CLASSNAME_PREFIX'            => 'lider',

    'VIEW_DELIVERY_ADDRESS'           => key_exists('VIEW_DELIVERY_ADDRESS', $arResult) ? $arResult['VIEW_DELIVERY_ADDRESS'] : 'N',
));