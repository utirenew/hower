<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="seller-block">
	<div class="">
		<div class="row">
			<div class="col-4 pr-0">
				<div class="seller-block1">
					<div class="like"></div><!--class="active-like"-->
					<div class="seller-pic">
						<img src="<?=SITE_TEMPLATE_PATH?>/image/seller.jpg" alt="">
					</div>
					<div class="seller-raiting">
						<img src="<?=SITE_TEMPLATE_PATH?>/image/stars.jpg" alt="">
						<span class="raiting-count">4.1</span>
					</div>
					<div class="goods-reviews-cabinet">
						<div class="goods-count">29 товаров</div>
						<div class="reviews-count">5 отзывов</div>
					</div>
					<div class="orders-count">
						<span class="icon-ticket orders-count-icon"></span>
						<span class="orders-count-number">1055</span>
						<span class="orders-count-text">заказов</span>
					</div>
					<div class="seller-goods-slider-wrap">
						<div class="seller-goods-slider">
							<div>
								<div class="seller-product-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/seller-slider3.jpg" alt="">
								</div>
							</div>
							<div>
								<div class="seller-product-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/seller-slider2.jpg" alt="">
								</div>
							</div>
							<div>
								<div class="seller-product-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/seller-slider3.jpg" alt="">
								</div>
							</div>
							<div>
								<div class="seller-product-image">
									<img src="/image/seller-slider2.jpg" alt="">
								</div>
							</div>
							<div>
								<div class="seller-product-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/seller-slider2.jpg" alt="">
								</div>
							</div>
							<div>
								<div class="seller-product-image">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/seller-slider3.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
					<div class="send-message-btn-wrap">
						<a href="#" class="send-message-btn">сообщение</a>
					</div>
				</div>
			</div>
			<div class="col-8 pl-0">
				<div class="seller-block2">
					<div class="seller-info">
						<span class="seller-place">
							Московская область
						</span>
						<span class="product-type milk-products">Молочные продукты</span>
					</div>
					<div class="seller-name">Ферма дяди Джона</div>
					<div class="seller-tel">
						<a href="tel:88005008179" class="phone">8 800 500 81 79</a>
					</div>
					<div class="seller-about">
						Русский фермер с английскими корнями вместе с супругой возрождают агрокультурный туризм.
						Чистый воздух, заливные луга в пойме реки Клязьмы, дубовые рощи, сосновые и еловые леса…
						Красота да и только! На удивление, эти живописные места всего в 120 километрах от столицы страны.
						Все это — агрокультурный туристический комплекс «Богдарня» в деревушке Крутово Петушинского района.
						Побывав здесь однажды, вы посмотрите на сельский туризм иными глазами и обязательно приедете снова.
						Джон Кописки приехал в Россию в 90-е годы всего на три дня и вскоре вернулся, но уже насовсем.
						Все потому, что он с первого взгляда влюбился в страну и в русских людей. Чтобы уехать,
						бросил успешную работу в Лондоне, сестер и братьев. Здесь, в России, встретил свою любовь
						— жену Нину. Вместе с ней они переехали в Петушинский район, где открыли уникальный
						агротуристический комплекс «Богдарня», где с большим уважением относятся к русским традициям
						и праздникам, а еще — к работе с землей и животными. Ведь русский человек, как исторически
						сложилось, крестьянин. И Джон уверен — он именно такой.
					</div>
					<div class="seller-spec">
						<div class="seller-spec-title">Особенности:</div>
						Рецепты для колбасных изделий и деликатесов взяты из ГОСТов, ОСТов и отраслевых ТУ.
						Сырье закупается в проверенных фермерских хозяйствах.
					</div>
				</div>
			</div>
			<div class="col-12 seller-tabs-block">
				<div class="card-tabs-block nav nav-tabs" id="nav-tab" role="tablist">
					<a class="card-tabs-item active" id="product-tab" data-toggle="tab" href="#nav-product" role="tab" aria-controls="nav-product"
					   aria-selected="false">Товары</a>
					<a class="card-tabs-item" id="review-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-review"
					   aria-selected="true">Отзывы</a>
					<a class="card-tabs-item" id="sertificat-tab" data-toggle="tab" href="#nav-sertificat" role="tab" aria-controls="nav-sertificat"
					   aria-selected="false">Сертификаты/награды</a>
				</div>
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-product" role="tabpanel" aria-labelledby="product-tab">
						<div class="row">
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="/product.php" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p1.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Коптильня "Ретивый кабанчик"</div>
												<div class="product-name">Окорок копчено-вяленый по-вестфальски нарезка</div>
											</div>
											<div class="product-sale  akcija"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за кг.</span></div>
											<!--<div class="product-old-price">860</div>-->
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
									<div class="top-block">
										<a href="#" class="product-pic">
											<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p2.jpg" alt="">
										</a>
										<div class="product-desc">
											<div class="fermer-name">Ферма "Сеновал""</div>
											<div class="product-name">Сазан холодного копчения</div>
										</div>
										<div class="product-sale  discont"></div>  <!--class=""akcija class="discont"-->
										<div class="like"></div><!--class="active-like"-->

									</div>
									<div class="bottom-block">
										<div class="product-price"><span class="price">1050</span> <span class="unit">/ за кг.</span></div>
										<div class="product-old-price">860</div>
									</div>
									<a href="#" class="add-product"></a>
								</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="#" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p3.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Семейная сыроварня Калмань</div>
												<div class="product-name">Йогурт питьевой с клубникой</div>
											</div>
											<div class="product-sale  discont"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за литр</span></div>
											<div class="product-old-price">860</div>
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="#" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p4.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Студия витаминных букетов "Miss BlackBerry"</div>
												<div class="product-name">Букет из клубники №1</div>
											</div>
											<div class="product-sale"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за шт.</span></div>
											<!--<div class="product-old-price">860</div>-->
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="/product.php" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p1.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Коптильня "Ретивый кабанчик"</div>
												<div class="product-name">Окорок копчено-вяленый по-вестфальски нарезка</div>
											</div>
											<div class="product-sale  akcija"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за кг.</span></div>
											<!--<div class="product-old-price">860</div>-->
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="#" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p2.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Ферма "Сеновал""</div>
												<div class="product-name">Сазан холодного копчения</div>
											</div>
											<div class="product-sale  discont"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за кг.</span></div>
											<div class="product-old-price">860</div>
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="#" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p3.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Семейная сыроварня Калмань</div>
												<div class="product-name">Йогурт питьевой с клубникой</div>
											</div>
											<div class="product-sale  discont"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за литр</span></div>
											<div class="product-old-price">860</div>
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-3">
								<div class="product-wrap">
									<div class="product">
										<div class="top-block">
											<a href="#" class="product-pic">
												<img src="<?=SITE_TEMPLATE_PATH?>/image/slider-p4.jpg" alt="">
											</a>
											<div class="product-desc">
												<div class="fermer-name">Студия витаминных букетов "Miss BlackBerry"</div>
												<div class="product-name">Букет из клубники №1</div>
											</div>
											<div class="product-sale"></div>  <!--class=""akcija class="discont"-->
											<div class="like"></div><!--class="active-like"-->

										</div>
										<div class="bottom-block">
											<div class="product-price"><span class="price">1050</span> <span class="unit">/ за шт.</span></div>
											<!--<div class="product-old-price">860</div>-->
										</div>
										<a href="#" class="add-product"></a>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="pagination-block">
									<div class="pag-left"><span class="pag-arrow-left"></span></div>
									<div class="pagination-btns">
										<a href="#" class="pagination-btn active-pag">01</a>
										<a href="#" class="pagination-btn">02</a>
										<a href="#" class="pagination-btn">03</a>
										<a href="#" class="pagination-btn">04</a>
									</div>
									<div class="pag-right"><span class="pag-arrow-right"></span></div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="review-tab">
						<div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div>
						<div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div>
						<div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="nav-sertificat" role="tabpanel" aria-labelledby="sertificat-tab">
						<span class="exemple">сертификаты/награды</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
