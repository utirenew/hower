<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule('sale') && !CModule::IncludeModule("iblock")) return;

// Создаём поле заголовка
if(empty($arResult["arUser"]["WORK_COMPANY"])) $arResult['arUser']['title'] = $arResult["arUser"]["NAME"] . ' ' . $arResult["arUser"]["LAST_NAME"];
else $arResult['arUser']['title'] = $arResult["arUser"]["WORK_COMPANY"];

// Количество товаров опубликованных этим пользователем
$res = CIBlockElement::GetList(false, array('CREATED_BY' => $USER->GetID()), array('CREATED_BY'));

if($el = $res->Fetch()) $cnl = $el['CNT'];
else $cnl = 0;

$last_digit = substr($cnl % 100, -1);

if($last_digit > 1 && $last_digit < 5) $arResult['arUser']['product_count'] = $cnl . ' ' . 'товара';
elseif($last_digit == 1) $arResult['arUser']['product_count'] = $cnl . ' ' . 'товар';
else $arResult['arUser']['product_count'] = $cnl . ' ' . 'товаров';

// Подготовка размера аватарки пользователя
if($arResult["arUser"]["PERSONAL_PHOTO"]){
	$arResult['arUser']['img'] = CFile::ResizeImageGet($arResult["arUser"]['PERSONAL_PHOTO'], array('width' => '150', 'height' => '150'), BX_RESIZE_IMAGE_EXACT)['src'];
}
else{
	$arResult['arUser']['img'] = SITE_TEMPLATE_PATH . '/image/no_photo.png';
}