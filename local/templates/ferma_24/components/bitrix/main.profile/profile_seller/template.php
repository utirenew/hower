<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
Bitrix\Main\UI\Extension::load("ui.alerts");
?>

<? if($arResult['DATA_SAVED'] == 'Y'): ?>
	<div id="mess"></div>
	<script>
		let myAlert = new BX.UI.Alert({
			closeBtn: true,
			animate: true,
			color: BX.UI.Alert.Color.SUCCESS,
			text: '<?=GetMessage('PROFILE_DATA_SAVED') ?>'
		});

		window.onload = function () {
			document.getElementById('mess').appendChild(myAlert.getContainer());
			setTimeout(function () {
				myAlert.animateClosing();
			}, 5000);
		};
	</script>
<? endif ?>

<? if(!empty($USER->GetUserGroupArray())): ?>
	<form id="cabinet_form" class="cabinet-info-block" method="post" name="form1" action="/cabinet/seller/edit/info/main/" enctype="multipart/form-data">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>"/>
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?>/>
		<div class="avatar-block">
			<a id="add_picture" href="#photo">
				<div class="img-profile">
					<img src="<?=$arResult['arUser']['img']?>" alt="">
				</div>
			</a>
			<div class="d-none">
				<div id="photo"> <!-- class="modal-window small-modal" -->
					<div class="form-group">
						<label for="formControlFile">Выберите фото</label>
						<input name="PERSONAL_PHOTO" form="cabinet_form" id="formControlFile" class="typefile form-control-file" size="20" type="file">
						<input type="hidden" name="save" value="Сохранить"/>
					</div>
					<div class="save-btn form-group">
						<button class="btn btn-primary" onclick="$.fancybox.close();">Сохранить</button>
					</div>
				</div>
			</div>

			<div class="cabinet-raiting">
				<img src="<?=SITE_TEMPLATE_PATH . '/image/stars.jpg'?>" alt="">
				<span class="raiting-count">4.1</span>
			</div>
			<div class="goods-reviews-cabinet">
				<div class="goods-count"><?=$arResult['arUser']['product_count']?></div>
				<div class="reviews-count">5 отзывов</div>
			</div>
		</div>
		<div class="cabinet-text-block">
			<div class="cabinet-user-block">
				<div class="cabinet-user-name"><?=$arResult['arUser']['title']?></div>
				<div class="user-block-head">Личные данные:</div>
				<div class="input-mid-block">
					<input type="text" class="input-custom mid-input" name="NAME" placeholder="Имя" value="<?=$arResult["arUser"]["NAME"]?>" required>
					<input type="text" class="input-custom mid-input" name="LAST_NAME" value="<?=$arResult["arUser"]["LAST_NAME"]?>" placeholder="Фамилия" required>
				</div>
				<div class="input-short-block">
					<input type="text" class="input-custom mid-input" name="SECOND_NAME" placeholder="Отчество" value="<?=$arResult["arUser"]["SECOND_NAME"]?>">
					<? $APPLICATION->IncludeComponent('bitrix:main.calendar', 'pers', array(
						'SHOW_INPUT'  => 'Y',
						'FORM_NAME'   => 'form1',
						'INPUT_NAME'  => 'PERSONAL_BIRTHDAY',
						'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
						'SHOW_TIME'   => 'N',
					), null, array('HIDE_ICONS' => 'Y'));
					?>
					<? if(empty($arResult["arUser"]["PERSONAL_BIRTHDAY"])){
						?>
						<p style="color: red;">Дату рождения можно ввести только один раз</p>
					<? } ?>
				</div>
				<div class="sex-block">
					<span class="search-block1-text">Ваш Пол:</span>
					<label class="d-flex align-items-center">
						<input name="PERSONAL_GENDER" type="radio" value="M" class="d-none product-fermer" <?=($arResult["arUser"]['PERSONAL_GENDER'] == 'M')?"checked":""?>>
						<span class="icon-ale choose-radio"></span>
					</label>
					<label>
						<input name="PERSONAL_GENDER" type="radio" value="F" class="d-none product-fermer" <?=($arResult['arUser']['PERSONAL_GENDER'] == 'F')?"checked":""?>>
						<span class="icon-shemale choose-radio"></span>
					</label>
				</div>
			</div>
			<div class="cabinet-user-block">
				<div class="user-block-head">Контактные данные:</div>
				<div class="input-mid-block">
					<input type="email" name="EMAIL" class="input-custom mid-input" placeholder="E-mail"
					       value="<?=$arResult["arUser"]["EMAIL"]?>">
					<input type="tel" name="PERSONAL_PHONE" class="input-custom mid-input" placeholder="Телефон" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>">
				</div>
				<? $arResult["arUser"]["PERSONAL_CITY"] ?>
				<div class="long-input-block">
					<input type="text" name="WORK_COMPANY" class="input-custom mid-input" placeholder="Название компании"
					       value="<?=$arResult["arUser"]["WORK_COMPANY"]?>">
					<? /*<select name="PERSONAL_CITY" id="cities" class="<?= (empty($arResult["arUser"]["PERSONAL_CITY"]))?'':'paint_black'; ?>">
                    <option value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" >
                        <?= (empty($arResult["arUser"]["PERSONAL_CITY"]))?'Выберите город':$arResult["arUser"]["PERSONAL_CITY"]; ?>
                    </option>
                    <?
                    $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                        'filter' => array('=TYPE.ID' => '5', '=NAME.LANGUAGE_ID' => LANGUAGE_ID),
                        'select' => array('NAME_RU' => 'NAME.NAME'),
                        'order' => array('NAME.NAME' => 'asc')
                    ));
                    while ($item = $res->fetch()) {?>
                    <option value="<?=$item['NAME_RU']; ?>">
                        <?=$item['NAME_RU']; ?>
                    </option>
                    <?} ?>
                </select>*/
					?>
				</div>
				<div class="long-input-block">
					<textarea name="PERSONAL_NOTES" class="input-custom w-100" placeholder="О себе"><?=$arResult["arUser"]["PERSONAL_NOTES"]?></textarea>
				</div>
			</div>
			<div class="cabinet-user-block">
				<div class="user-block-head">Сменить пароль:</div>
				<div class="input-mid-block">
					<input type="password" name="NEW_PASSWORD" class="input-custom mid-input" placeholder="Новый пароль">
					<input type="password" name="NEW_PASSWORD_CONFIRM" class="input-custom mid-input" placeholder="Подтверждение пароля">
				</div>
			</div>
			<button type="submit" name="save" value="Сохранить" class="cabinet-btn saves">сохранить</button>
		</div>
	</form>
<? endif; ?>