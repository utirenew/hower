<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="shopper-block">
	<div class="">
		<div class="row">
			<div class="col-4 pr-0">
				<div class="shopper-block1">
					<div class="avatar-block">
						<div class="avatar"></div>
						<div class="shopper-place">Московская область, Подольск</div>
						<div class="shopper-name">Алексей Смирнов</div>
						<div class="cabinet-raiting">
							<img src="<?=SITE_TEMPLATE_PATH?>/image/stars.jpg" alt="">
							<span class="raiting-count">4.1</span>
						</div>
						<div class="shopper-reviews-count">3 отзыва</div>
						<div class="shopper-orders-count">18 оплаченых заказов</div>
					</div>
					<div class="contacts-block">
						<div class="contacts-block-head">Контактные данные</div>
						<div class="phone-male-block">
							<a href="tel:88005008179" class="phone">8 800 500 81 79</a>
							<a href="mailto:testuser@gmail.com" class="male">testuser@gmail.com</a>
						</div>
						<div class="send-message-btn-wrap">
							<a href="#" class="send-message-btn">сообщение</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-8 pl-0 shopper-tabs-block">
				<div class="card-tabs-block nav nav-tabs" id="nav-tab" role="tablist">
					<a class="card-tabs-item active" id="history-tab" data-toggle="tab" href="#nav-history" role="tab" aria-controls="nav-history"
					   aria-selected="false">История заказов</a>
					<a class="card-tabs-item" id="review-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-review"
					   aria-selected="true">Отзывы</a>
				</div>
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-history" role="tabpanel" aria-labelledby="history-tab">
						<div class="order-item">
							<div class="order-info">
								<div class="order-number-block">
									<div class="order-number">248949747</div>
									<div class="order-process">Отправлен</div>
								</div>
								<div class="order-date-block">
									<span class="order-year">2018</span>
									<span class="order-day">9 июля</span>
								</div>
							</div>
							<div class="order-product-block">
								<div class="order-product-pic">
									<img src="<?=SITE_TEMPLATE_PATH?>/image/big-pic.jpg" alt="">
								</div>
								<div class="order-product-name-block">
									<div class="order-product-name">Окорок копчено-вяленый по-вестфальски нарезка</div>
									<div class="order-product-fermer">Коптильня "Ретивый кабанчик"</div>
								</div>
							</div>
							<div class="order-total-block">
								<div class="total-order-price">7850</div>
								<a href="#" class="order-desc-btn">подробнее</a>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="review-tab">
						<div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div>
						<div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div>
						<div class="review-item">
							<div class="review-top-line">
								<div class="review-object">Семейная сыроварня Калмань</div>
								<div class="review-raiting">
									<img src="/image/stars.jpg" alt="">
									<div class="reviw-raiting-count">5.0</div>
								</div>
							</div>
							<div class="review-name-block">
								<div class="review-name">Алексей</div>
								<div class="review-date"><span class="review-year">2018</span><span class="review-day">9 июля</span></div>
							</div>
							<div class="review-text">
								Очень рад что у нас во Владимирской области есть такие фермеры. Заказывал молочную корзину, чтоб всё
								попробовать по чуть чуть так сказать. ОООООчень вкусно, творог как из детства, когда бабашку на рынке покупала.
								Йогурт питьевой даже в сравнение не идет с магазинным. В общем кто надумает покупать, берите не раздумывая.
								Очень приветливые люди, доставка быстро в оговоренное время. Спасибо еще раз!
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
