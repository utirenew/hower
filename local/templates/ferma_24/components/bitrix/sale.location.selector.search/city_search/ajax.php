<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Loader;

$response = array(
	'success' => true,
	'text'    => '',
);

if(Loader::includeModule('sepro.helper') && !empty($_REQUEST['LOCATION'])){
	$locationID = (int)htmlspecialchars($_REQUEST['LOCATION']);
	$city       = \Sepro\User::setCity($locationID);
	if($city) $response['text'] = $city;
	else $response['success'] = false;
}
else $response['success'] = false;

return print json_encode($response);