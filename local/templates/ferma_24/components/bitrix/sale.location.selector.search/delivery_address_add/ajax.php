<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');


    $APPLICATION->IncludeComponent('bitrix:sale.location.selector.search', '.default', array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "",
        "COMPONENT_TEMPLATE" => ".default",
        "ID" => "",
        "CODE" => "",
        "INPUT_NAME" => "LOCATION[]",
        "PROVIDE_LINK_BY" => "id",
        "JSCONTROL_GLOBAL_ID" => "",
        "JS_CALLBACK" => "",
        "FILTER_BY_SITE" => "Y",
        "SHOW_DEFAULT_LOCATIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "FILTER_SITE_ID" => "s1",
        "INITIALIZE_BY_GLOBAL_EVENT" => "",
        "SUPPRESS_ERRORS" => "N",
        'JS_CONTROL_GLOBAL_ID' => 'as',
    ));

