<?
$APPLICATION->IncludeComponent("bitrix:main.register", "on_page", Array(
	"AUTH"               => "Y",
	"REQUIRED_FIELDS"    => array("EMAIL", "NAME", "LAST_NAME"),
	"SET_TITLE"          => "Y",
	"SHOW_FIELDS"        => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_PHONE"),
	"SUCCESS_PAGE"       => "/",
	"USER_PROPERTY"      => array("UF_ORDER", 'UF_SELLER_TYPE'),
	"USER_PROPERTY_NAME" => "",
	"USE_BACKURL"        => "Y",
));