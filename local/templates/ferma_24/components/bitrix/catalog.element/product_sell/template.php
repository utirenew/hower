<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @global CMain                 $APPLICATION
 * @global CUser                 $USER
 * @var array                    $arParams
 * @var array                    $arResult
 * @var CatalogSectionComponent  $component
 * @var CBitrixComponentTemplate $this
 */

$this->setFrameMode(true);
CJSCore::Init(array('currency'));

$mainId  = $this->GetEditAreaId($arResult['ID']);
$obName  = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$itemIds = array(
	'ID'                    => $mainId,
	'DISCOUNT_PERCENT_ID'   => $mainId . '_dsc_pict',
	'STICKER_ID'            => $mainId . '_sticker',
	'OLD_PRICE_ID'          => $mainId . '_old_price',
	'PRICE_ID'              => $mainId . '_price',
	'DISCOUNT_PRICE_ID'     => $mainId . '_price_discount',
	'PRICE_TOTAL'           => $mainId . '_price_total',
	'QUANTITY_ID'           => $mainId . '_quantity',
	'QUANTITY_DOWN_ID'      => $mainId . '_quant_down',
	'QUANTITY_UP_ID'        => $mainId . '_quant_up',
	'QUANTITY_MEASURE'      => $mainId . '_quant_measure',
	'QUANTITY_LIMIT'        => $mainId . '_quant_limit',
	'BUY_LINK'              => $mainId . '_buy_link',
	'ADD_BASKET_LINK'       => $mainId . '_add_basket_link',
	'BASKET_ACTIONS_ID'     => $mainId . '_basket_actions',
	'NOT_AVAILABLE_MESS'    => $mainId . '_not_avail',
	'COMPARE_LINK'          => $mainId . '_compare_link',
	'TREE_ID'               => $mainId . '_skudiv',
	'DISPLAY_PROP_DIV'      => $mainId . '_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
	'OFFER_GROUP'           => $mainId . '_set_group_',
	'BASKET_PROP_DIV'       => $mainId . '_basket_prop',
	'SUBSCRIBE_LINK'        => $mainId . '_subscribe',
	'TABS_ID'               => $mainId . '_tabs',
	'TAB_CONTAINERS_ID'     => $mainId . '_tab_containers',
	'SMALL_CARD_PANEL_ID'   => $mainId . '_small_card_panel',
	'TABS_PANEL_ID'         => $mainId . '_tabs_panel',
);

$jsParams = array(
	'CONFIG'          => array(
		'USE_CATALOG'              => $arResult['CATALOG'],
		'SHOW_QUANTITY'            => true,
		'SHOW_PRICE'               => true,
		'SHOW_DISCOUNT_PERCENT'    => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
		'SHOW_OLD_PRICE'           => $arParams['SHOW_OLD_PRICE'] === 'Y',
		'USE_PRICE_COUNT'          => $arParams['USE_PRICE_COUNT'],
		'DISPLAY_COMPARE'          => $arParams['DISPLAY_COMPARE'],
		'SHOW_SKU_PROPS'           => $arResult['SHOW_OFFERS_PROPS'],
		'OFFER_GROUP'              => $arResult['OFFER_GROUP'],
		'MAIN_PICTURE_MODE'        => $arParams['DETAIL_PICTURE_MODE'],
		'ADD_TO_BASKET_ACTION'     => $arParams['ADD_TO_BASKET_ACTION'],
		'SHOW_CLOSE_POPUP'         => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
		'SHOW_MAX_QUANTITY'        => $arParams['SHOW_MAX_QUANTITY'],
		'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
		'TEMPLATE_THEME'           => $arParams['TEMPLATE_THEME'],
		'USE_STICKERS'             => true,
		'USE_SUBSCRIBE'            => $showSubscribe,
		'ALT'                      => $alt,
		'TITLE'                    => $title,
		'MAGNIFIER_ZOOM_PERCENT'   => 200,
		'USE_ENHANCED_ECOMMERCE'   => $arParams['USE_ENHANCED_ECOMMERCE'],
		'DATA_LAYER_NAME'          => $arParams['DATA_LAYER_NAME'],
		'BRAND_PROPERTY'           => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])?$arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']:null,
	),
	'PRODUCT_TYPE'    => $arResult['CATALOG_TYPE'],
	'VISUAL'          => $itemIds,
	'DEFAULT_PICTURE' => array(
		'PREVIEW_PICTURE' => $arResult['photos'][0]['SMALL'],
		'DETAIL_PICTURE'  => $arResult['photos'][0]['FULL'],
	),
	'PRODUCT'         => array(
		'ID'                           => $arResult['ID'],
		'ACTIVE'                       => $arResult['ACTIVE'],
		'PICT'                         => reset($arResult['MORE_PHOTO']),
		'NAME'                         => $arResult['~NAME'],
		'SUBSCRIPTION'                 => true,
		'ITEM_PRICE_MODE'              => $arResult['ITEM_PRICE_MODE'],
		'ITEM_PRICES'                  => $arResult['ITEM_PRICES'],
		'ITEM_PRICE_SELECTED'          => $arResult['ITEM_PRICE_SELECTED'],
		'ITEM_QUANTITY_RANGES'         => $arResult['ITEM_QUANTITY_RANGES'],
		'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
		'ITEM_MEASURE_RATIOS'          => $arResult['ITEM_MEASURE_RATIOS'],
		'ITEM_MEASURE_RATIO_SELECTED'  => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
		'ITEM_MEASURE_TITLE'           => $arResult['ITEM_MEASURE']['TITLE'],
		'SLIDER_COUNT'                 => $arResult['MORE_PHOTO_COUNT'],
		'SLIDER'                       => $arResult['MORE_PHOTO'],
		'CAN_BUY'                      => $arResult['CAN_BUY'],
		'CHECK_QUANTITY'               => $arResult['CHECK_QUANTITY'],
		'QUANTITY_FLOAT'               => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
		'MAX_QUANTITY'                 => $arResult['CATALOG_QUANTITY'],
		'STEP_QUANTITY'                => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
		'CATEGORY'                     => $arResult['CATEGORY_PATH'],
	),
	'BASKET'          => array(
		'QUANTITY'         => $arParams['PRODUCT_QUANTITY_VARIABLE'],
		'BASKET_URL'       => $arParams['BASKET_URL'],
		'SKU_PROPS'        => $arResult['OFFERS_PROP_CODES'],
		'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
		'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	),
	'OFFERS'          => $arResult['JS_OFFERS'],
	'OFFER_SELECTED'  => $arResult['OFFERS_SELECTED'],
	'TREE_PROPS'      => $skuProps,
);
?>

<div class="product-card" id="<?=$mainId?>">
	<div class="">
		<div class="row">
			<div class="col-4 pr-0">
				<div class="product-pic-block">
					<div class="big-pic-block">
						<div class="big-pic-wrapper" data-entity="images-container">
							<? foreach($arResult['photos'] as $key => $pic): ?>
								<div class="big-pic">
									<img src="<?=$pic['BIG']?>" alt="" class="big-image">
								</div>
							<? endforeach; ?>
						</div>

						<div class="product-sale akcija"></div>  <!--class=""akcija class="discont"-->
						<!-- button wishlist -->

						<? if($arResult['CAN_BUY'] && ((USER_ROLE !== $arResult['seller_data']['OWNER'] && USER_ROLE !== 'guest') || (USER_ROLE == 'guest' && $arResult['seller_data']['OWNER'] !== 'shopper'))): ?>
							<div>
								<?php if(!empty($arResult['BASKET_DELAYED'])): ?>
									<a href="javascript:" class="like wishbtn js-wishbtn-delete in_wishlist"
									   onclick="delete4wish('<?=$arResult['BASKET_DELAYED']['ID']?>', this)">
									</a>
								<?php else: ?>
									<a href="javascript:" class="like wishbtn js-wishbtn-add  <? if(isset($itInDelay)){
										echo 'in_wishlist';
									} ?>"
									   onclick="add2wish(
											   '<?=$arResult["ID"]                    //p_id  - PRODUCT_ID ?>',
											   '<?=$arResult["CATALOG_PRICE_ID_1"]    //pp_id - PRODUCT_PRICE_ID ?>',
											   '<?=$arResult["RATIO_PRICE"]['VALUE']    //p 	- PRICE ?>',
											   '<?=$arResult["NAME"]                    //name  - NAME ?>',
											   '<?=$arResult["DETAIL_PAGE_URL"]        //dpu   - DETAIL_PAGE_URL?>',
											   '<?=USER_ROLE?>',
											   this
											   )">
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<!-- button wishlist -->
					</div>
					<div class="gallary-block">
						<? foreach($arResult['photos'] as $key => $pic): ?>
							<div class="small-pic-item"><img src="<?=$pic['SMALL']?>" alt=""></div>
						<? endforeach; ?>
					</div>
				</div>
			</div>
			<div class="col-8 pl-0">
				<div class="product-info-block">
					<div class="product-fermer-info">
						<div class="product-fermer">
							<? if(empty($arResult['seller_data']['WORK_COMPANY'])): ?>
								<?=$arResult['seller_data']['LAST_NAME']?> <?=$arResult['seller_data']['NAME']?>
							<? else: ?>
								<?=$arResult['seller_data']['WORK_COMPANY']?>
							<? endif; ?>
						</div>
						<div class="goods-reviews">
							<div class="goods-count"><?=$arResult['all_count']?> товаров</div>
							<? /** TODO
							 * Сделать отзывы через отдельный ИБ.
							 * Добавление отзывов через iblock.element.add.
							 * Вывод через news.list. Пагинация списка отзывов через аякс.
							 */
							?>
							<div class="reviews-count">5 отзывов</div>
						</div>
						<a href="#" class="product-fermer-pic">
							<img src="<?=$arResult['seller_data']['photo']?>" alt="">
						</a>
					</div>
					<div class="in-stock">В наличии</div>
					<div class="product-card-name"><?=$arResult['NAME']?></div>
					<div class="product-raiting">
						<div class="rating-wrapper">
							<? $APPLICATION->IncludeComponent('bitrix:iblock.vote', 'stars', array(
								'CUSTOM_SITE_ID'    => isset($arParams['CUSTOM_SITE_ID'])?$arParams['CUSTOM_SITE_ID']:null,
								'IBLOCK_TYPE'       => $arParams['IBLOCK_TYPE'],
								'IBLOCK_ID'         => $arParams['IBLOCK_ID'],
								'ELEMENT_ID'        => $arResult['ID'],
								'ELEMENT_CODE'      => '',
								'MAX_VOTE'          => '5',
								'VOTE_NAMES'        => array('1', '2', '3', '4', '5'),
								'SET_STATUS_404'    => 'N',
								'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
								'CACHE_TYPE'        => $arParams['CACHE_TYPE'],
								'CACHE_TIME'        => $arParams['CACHE_TIME'],
							), $component, array('HIDE_ICONS' => 'Y'));
							?>
						</div>
						<span class="sales-count">52 покупки</span>
					</div>
					<div class="product-desc"><?=$arResult['DETAIL_TEXT']?></div>

					<? if(!empty($arResult['PROPERTIES']['CT_SL_PT_SET_PROPERTIES_GROUP']['VALUE'])): ?>
						<? foreach($arResult['PROPERTIES']['CT_SL_PT_SET_PROPERTIES_GROUP']['VALUE'] as $key => $val): ?>
							<div class="product-dop-desc">
								<div class="product-dop-desc-title"><?=$arResult['PROPERTIES']['CT_SL_PT_SET_PROPERTIES_GROUP']['DESCRIPTION'][$key]?></div>
								<?=$val;?>
							</div>
						<? endforeach; ?>
					<? endif; ?>

					<div class="product-card-price-block">
						<div class="card-price-wrap">
							<span class="price" id="<?=$itemIds['PRICE_ID']?>"><?=$arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?></span><span
									class="unit">/ за <?=$arResult['ITEM_MEASURE']['TITLE']?></span>
						</div>
						<? if($arResult['MIN_PRICE']['DISCOUNT_DIFF'] > 0): ?>
							<div class="product-old-price" id="<?=$itemIds['OLD_PRICE_ID']?>"><?=$arResult['MIN_PRICE']['PRINT_VALUE']?></div>
						<? endif; ?>
						<span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
						<div class="product-item-detail-economy-price mb-1" id="<?=$itemIds['DISCOUNT_PRICE_ID']?>"></div>
						<div class="product-item-label-ring" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>" style="display: none;"></div>
					</div>
					<div class="product-card-delivery-address">
						<? if (key_exists('DELIVERIES_ADDRESSES', $arResult)  &&  is_array($arResult['DELIVERIES_ADDRESSES'])  &&  !empty($arResult['DELIVERIES_ADDRESSES'])) {
							echo '<div class="address-title">Доставка в ';
							if ($arResult['DELIVERIES_ADDRESSES']['TITLE_IS_LINK']) {
								echo '<button type="button" data-toggle="popover" data-container="body" data-placement="bottom" data-content="'.$arResult['DELIVERIES_ADDRESSES']['LIST'].'">'.$arResult['DELIVERIES_ADDRESSES']['TITLE'].'</button>';
							} else {
								echo '<span>' . $arResult['DELIVERIES_ADDRESSES']['TITLE'] . '</span>';
							}
							echo '</div>';
						}?>
					</div>
					<? if($arResult['CAN_BUY'] && ((USER_ROLE !== $arResult['seller_data']['OWNER'] && USER_ROLE !== 'guest') || (USER_ROLE == 'guest' && $arResult['seller_data']['OWNER'] !== 'shopper'))): ?>
						<div class="product-in-cart-block">
							<div class="product-count-block">
								<div class="count-minus" id="<?=$itemIds['QUANTITY_DOWN_ID']?>" data-entity="basket-item-quantity-minus"></div>
								<input type="text" class="card-count-value" id="<?=$itemIds['QUANTITY_ID']?>" value="1" size="1"/>
								<div class="count-plus" id="<?=$itemIds['QUANTITY_UP_ID']?>" data-entity="basket-item-quantity-plus"></div>
							</div>
							<div id="<?=$itemIds['BASKET_ACTIONS_ID']?>">
								<a href="javascript:" class="product-in-cart-btn" id="<?=$itemIds['ADD_BASKET_LINK']?>">в корзину</a>
							</div>
						</div>
					<? endif; ?>
					<div class="soc-block">
						<div class="soc-head">поделиться</div>
						<div class="soc-btns"><img src="<?=SITE_TEMPLATE_PATH?>/image/soc-icons.jpg" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $(function () {
        $('div.address-title button').popover({
            container: 'body',
            html: true
        })
    });
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
	});
	//BX.Currency.setCurrencies(<?//=CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)?>//);
	let <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
