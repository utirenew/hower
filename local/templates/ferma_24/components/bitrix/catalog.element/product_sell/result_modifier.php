<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent  $component
 */

$component = $this->getComponent();
$arParams  = $component->applyTemplateModifications();

use Bitrix\Sale\Internals\BasketTable, Bitrix\Sale\Fuser;

if(CModule::includeModule('sale')){
	$rs = BasketTable::getList([
		'filter' => [
			"FUSER_ID"   => Fuser::getId(),
			"LID"        => SITE_ID,
			"ORDER_ID"   => "NULL",
			'DELAY'      => 'Y',
			"PRODUCT_ID" => $arResult['ID'],
		],
		'select' => [
			"ID",
		],
	]);
	while($ar = $rs->fetch()){
		$arResult['BASKET_DELAYED'] = $ar;
	}
}

// Ресайз главного и доп фото
$no_main_pic = false;
if(!empty($arResult['DETAIL_PICTURE'])){
	$file[0]['FULL']  = $arResult['DETAIL_PICTURE']['SRC'];
	$file[0]['BIG']   = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width' => 304, 'height' => 304), BX_RESIZE_IMAGE_EXACT)['src'];
	$file[0]['SMALL'] = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT)['src'];
}
else{
	$no_main_pic = true;

	$file[0]['FULL'] = $file[0]['BIG'] = $file[0]['SMALL'] = SITE_TEMPLATE_PATH . '/components/bitrix/catalog.element/product_sell/images/no_photo.png';
}

if(!empty($arResult['PROPERTIES']['CT_SL_PT_SET_MORE_PHOTO']['VALUE'])){
	if($no_main_pic) $file = array();
	foreach($arResult['PROPERTIES']['CT_SL_PT_SET_MORE_PHOTO']['VALUE'] as $val){
		$temp = array();

		$temp['FULL']  = CFile::GetFileArray($val)['SRC'];
		$temp['BIG']   = CFile::ResizeImageGet($val, array('width' => 304, 'height' => 304), BX_RESIZE_IMAGE_EXACT)['src'];
		$temp['SMALL'] = CFile::ResizeImageGet($val, array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT)['src'];

		$file[] = $temp;
		unset($temp);
	}
}

$arResult['photos'] = $file;

// Данные о продавце
$rsUser                  = CUser::GetByID($arResult['CREATED_BY']);
$arResult['seller_data'] = $rsUser->Fetch();
switch($arResult['seller_data']['UF_ORDER']){
	case 6:
		$arResult['seller_data']['OWNER'] = 'shopper';
		break;
	case 8:
		$arResult['seller_data']['OWNER'] = 'seller';
		break;
	default:
		$arResult['seller_data']['OWNER'] = 'guest';
		break;
}

// Ресайз фото продавца
if(!empty($arResult['seller_data']['PERSONAL_PHOTO'])){
	$arResult['seller_data']['photo'] = CFile::ResizeImageGet($arResult['seller_data']['PERSONAL_PHOTO'], array('width' => 70, 'height' => 70), BX_RESIZE_IMAGE_EXACT)['src'];
}
else{
	$arResult['seller_data']['photo'] = SITE_TEMPLATE_PATH . '/components/bitrix/catalog.element/product_sell/images/no_photo.png';
}

// Подсчёт общего количества товаров продавца
$arSelect              = array("CREATED_BY");
$arFilter              = array('IBLOCK_ID' => $arResult['IBLOCK_ID'], 'CREATED_BY' => $arResult['CREATED_BY'], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
$arResult['all_count'] = CIBlockElement::GetList(Array(), $arFilter, array(), false, $arSelect);

// Выведем точки доставки, если разрешено.
use \Bitrix\Main\Loader;
$arResult['DELIVERIES_ADDRESSES'] = '';
if (key_exists('VIEW_DELIVERY_ADDRESS', $arParams)  &&  $arParams['VIEW_DELIVERY_ADDRESS'] == 'Y') {
    if(Loader::includeModule('sepro.helper')) {
        $arResult['DELIVERIES_ADDRESSES'] = \Sepro\User::getDeliveriesPoints([$arResult['seller_data']['ID'] => $arResult['seller_data']['UF_DELIVERY_ID']])[$arResult['seller_data']['ID']];
    }
}


// Тегировать кеш
if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
{
    $cp =& $this->__component;
    if (strlen($cp->getCachePath()))
    {
        $GLOBALS['CACHE_MANAGER']->RegisterTag('catalog__item');
    }
}
