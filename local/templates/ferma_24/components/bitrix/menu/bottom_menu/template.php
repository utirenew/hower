<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if(!empty($arResult)): ?>
	<div class="footer-menu-block">
		<? foreach($arResult as $arItem): ?>
			<? if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
			<? if($arItem["SELECTED"]): ?>
				<span class="footer-menu-link active"><?=$arItem["TEXT"]?></span>
			<? else: ?>
				<a href="<?=$arItem["LINK"]?>" class="footer-menu-link"><?=$arItem["TEXT"]?></a>
			<? endif ?>
		<? endforeach ?>
	</div>
<? endif ?>
