<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/ferma_24/template/page/template/' . PAGE_TEMPLATE_NAME . '/content-after.php'; ?>
</div><!--content-->

<footer>
	<div class="container">
		<div class="row">
			<div class="col-4">
				<? $APPLICATION->IncludeComponent("bitrix:subscribe.form", ".default", Array(
					"CACHE_TIME"          => "3600",
					"CACHE_TYPE"          => "A",
					"PAGE"                => "#SITE_DIR#personal/subscribe/subscr_edit.php",
					"SHOW_HIDDEN"         => "N",
					"USE_PERSONALIZATION" => "Y",
				)); ?>
			</div>
			<div class="col-2">
				<div class="column-head menu-head">Мы в соцсетях</div>
				<div class="footer-menu-block">
					<a href="#" class="footer-menu-link soc-link icon-fb">facebook</a>
					<a href="#" class="footer-menu-link soc-link icon-vk">vkontakte</a>
					<a href="#" class="footer-menu-link soc-link icon-insta">instagam</a>
					<a href="#" class="footer-menu-link soc-link icon-tw">twitter</a>
				</div>
			</div>
			<div class="col-2">
				<div class="column-head menu-head">Предложения фермеров</div>
				<? $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
						"ALLOW_MULTI_SELECT"    => "N",
						"CHILD_MENU_TYPE"       => "sell_menu",
						"DELAY"                 => "N",
						"MAX_LEVEL"             => "1",
						"MENU_CACHE_GET_VARS"   => array(),
						"MENU_CACHE_TIME"       => "3600",
						"MENU_CACHE_TYPE"       => "A",
						"MENU_CACHE_USE_GROUPS" => "N",
						"ROOT_MENU_TYPE"        => "sell_menu",
						"USE_EXT"               => "Y",
						"COMPONENT_TEMPLATE"    => "bottom_menu",
					), false); ?>

			</div>
			<div class="col-2">
				<div class="column-head menu-head">Спрос покупателей</div>
				<? $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
					"ALLOW_MULTI_SELECT"    => "N",
					"CHILD_MENU_TYPE"       => "buy_menu",
					"DELAY"                 => "N",
					"MAX_LEVEL"             => "1",
					"MENU_CACHE_GET_VARS"   => array(),
					"MENU_CACHE_TIME"       => "3600",
					"MENU_CACHE_TYPE"       => "A",
					"MENU_CACHE_USE_GROUPS" => "N",
					"ROOT_MENU_TYPE"        => "buy_menu",
					"USE_EXT"               => "Y",
					"COMPONENT_TEMPLATE"    => "bottom_menu",
				), false); ?>

			</div>
			<div class="col-2">
				<div class="column-head menu-head">Информация</div>
				<? $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
					"ALLOW_MULTI_SELECT"    => "N",
					"CHILD_MENU_TYPE"       => "information",
					"DELAY"                 => "N",
					"MAX_LEVEL"             => "1",
					"MENU_CACHE_GET_VARS"   => array(),
					"MENU_CACHE_TIME"       => "3600",
					"MENU_CACHE_TYPE"       => "A",
					"MENU_CACHE_USE_GROUPS" => "N",
					"ROOT_MENU_TYPE"        => "information",
					"USE_EXT"               => "N",
					"COMPONENT_TEMPLATE"    => "bottom_menu",
				), false); ?>
			</div>
		</div>
		<div class="row align-items-center justify-content-between">
			<div class="col-12">
				<div class="footer-line"></div>
			</div>
			<div class="col-auto">
				<? if(IS_HOME): ?>
					<span class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/image/svg/logo_ferm_24.svg" alt="" class="logo-img"></span>
				<? else: ?>
					<a href="/" class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/image/svg/logo_ferm_24.svg" alt="" class="logo-img"></a>
				<? endif; ?>
			</div>
			<div class="col-auto">
				<span class="footer-text">© 2018 ферма24, Все права защищены.</span>
			</div>
			<div class="col-auto">
				<button type="button" class="underline-link" data-toggle="modal" data-target="#agree">Пользовательское соглашение</button>
			</div>
			<div class="col-auto">
				<button type="button" class="underline-link" data-toggle="modal" data-target="#policy">Политика конфиденциальности</button>
			</div>
			<div class="col-auto">
				<div id="semup">
					<span id="semup-text">made by</span>
					<a href="http://semup.pro/" target="_blank" id="semup-link"><img src="<?=SITE_TEMPLATE_PATH;?>/image/svg/semup.svg" alt="" id="semup-img"></a>
				</div>
			</div>
		</div>
	</div>
</footer>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW"   => "file",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE"    => "",
	"PATH"             => "/include/policy.php",
)); ?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW"   => "file",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE"    => "",
	"PATH"             => "/include/agree.php",
)); ?>
</div><!--wrapper-->

<script type="text/javascript">
	let LANG = {
		BASKET_ADDED: "<?=GetMessage("BASKET_ADDED")?>",
		WISHLIST_ADDED: "<?=GetMessage("WISHLIST_ADDED")?>",
		ADD_COMPARE_ADDED: "<?=GetMessage("ADD_COMPARE_ADDED")?>",
		ADD_CART_LOADING: "<?=GetMessage("ADD_CART_LOADING")?>",
		ADD_BASKET_DEFAULT_LABEL: "<?=GetMessage("ADD_BASKET_DEFAULT_LABEL")?>",
		ADDED_CART_SMALL: "<?=GetMessage("ADDED_CART_SMALL")?>",
		CATALOG_AVAILABLE: "<?=GetMessage("CATALOG_AVAILABLE")?>",
		GIFT_PRICE_LABEL: "<?=GetMessage("GIFT_PRICE_LABEL")?>",
		CATALOG_ON_ORDER: "<?=GetMessage("CATALOG_ON_ORDER")?>",
		CATALOG_NO_AVAILABLE: "<?=GetMessage("CATALOG_NO_AVAILABLE")?>",
		FAST_VIEW_PRODUCT_LABEL: "<?=GetMessage("FAST_VIEW_PRODUCT_LABEL")?>",
		CATALOG_ECONOMY: "<?=GetMessage("CATALOG_ECONOMY")?>",
		WISHLIST_SENDED: "<?=GetMessage("WISHLIST_SENDED")?>",
		REQUEST_PRICE_LABEL: "<?=GetMessage("REQUEST_PRICE_LABEL")?>",
		REQUEST_PRICE_BUTTON_LABEL: "<?=GetMessage("REQUEST_PRICE_BUTTON_LABEL")?>",
		ADD_SUBSCRIBE_LABEL: "<?=GetMessage("ADD_SUBSCRIBE_LABEL")?>",
		REMOVE_SUBSCRIBE_LABEL: "<?=GetMessage("REMOVE_SUBSCRIBE_LABEL")?>"
	};
</script>
</body>
</html>
