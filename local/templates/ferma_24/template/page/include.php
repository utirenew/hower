<?php
$url_page             = $APPLICATION->GetCurPage(false);
$url_page_explode     = explode('/', substr($url_page, 1, -1));
$name_section_level_0 = reset($url_page_explode);

if(in_array($name_section_level_0, ['sell', 'buy', 'catalog'])) $currentSection = '/' . $name_section_level_0 . '/';
else $currentSection = $url_page;

switch($currentSection){
	case '/':
		$page_template_name = 'main';
		break;

	case '/cabinet/seller/edit/catalog/':
	case '/cabinet/seller/edit/catalog/wishlist/':
	case '/cabinet/seller/edit/help/message/':
	case '/cabinet/seller/edit/info/address/':
	case '/cabinet/seller/edit/info/deliveri/':
	case '/cabinet/seller/edit/info/deliveris/':
	case '/cabinet/seller/edit/info/main/':
	case '/cabinet/seller/edit/info/payment/':
	case '/cabinet/seller/edit/info/social/':
	case '/cabinet/seller/edit/order/history/':
	case '/cabinet/seller/edit/user/message/':
	case '/cabinet/seller/edit/user/review/':
	case '/cabinet/seller/edit/user/wishlist/':
	case '/cabinet/seller/edit/subscription/':
		if(!isset($_SESSION['SESS_AUTH']['AUTHORIZED'])) $page_template_name = '.default';
		elseif(USER_ROLE === 'shopper') LocalRedirect(str_replace('seller', USER_ROLE, $currentSection));
		else $page_template_name = 'cabinet';
		break;

	case '/cabinet/shopper/edit/catalog/':
	case '/cabinet/shopper/edit/catalog/wishlist/':
	case '/cabinet/shopper/edit/help/message/':
	case '/cabinet/shopper/edit/info/address/':
	case '/cabinet/shopper/edit/info/main/':
	case '/cabinet/shopper/edit/info/payment/':
	case '/cabinet/shopper/edit/info/social/':
	case '/cabinet/shopper/edit/order/history/':
	case '/cabinet/shopper/edit/user/message/':
	case '/cabinet/shopper/edit/user/review/':
	case '/cabinet/shopper/edit/user/wishlist/':
		if(!isset($_SESSION['SESS_AUTH']['AUTHORIZED'])) $page_template_name = '.default';
		elseif(USER_ROLE === 'seller') LocalRedirect(str_replace('shopper', USER_ROLE, $currentSection));
		else $page_template_name = 'cabinet';
		break;

	default:
		$page_template_name = '.default';
		break;
}

define('PAGE_TEMPLATE_NAME', $page_template_name);