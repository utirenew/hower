<!--page template: custom/cabinet-->
<div class="cabinet content-inner">
	<div class="container">
		<div class="row">
			<div class="col-3">
				<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW"   => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE"    => "",
					"PATH"             => "/include/cabinet/" . USER_ROLE . "/edit/sidebar.php",
				)); ?>
			</div>
			<div class="col-9">