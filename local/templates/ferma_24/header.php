<?php

use Bitrix\Main\Page\Asset;

global $USER;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if(!defined('IS_HOME')) define('IS_HOME', false);

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/ferma_24/template/page/include.php';
?>

<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH . '/image/ico.png'?>"/>
	<?php
	$APPLICATION->ShowHead();
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-charset.css");
	Asset::getInstance()->addCss("/bitrix/css/main/font-awesome.css");
	//template
	//css
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/icons.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/slick/slick.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/slick/slick-theme.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/styles.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-legacy.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-header.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-catalog.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-cabinet.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-basket.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-global.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/upd-cart.css");
	//js
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-3.4.0.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/popper.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/slick/slick.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/wishlist.js");
	?>
	<title><? $APPLICATION->ShowTitle() ?></title>

	<script type="text/javascript">
		let ajaxPath = "<?=SITE_DIR?>ajax.php",
			SITE_DIR = "<?=SITE_DIR?>",
			SITE_ID = "<?=SITE_ID?>",
			TEMPLATE_PATH = "<?=SITE_TEMPLATE_PATH?>";
	</script>
</head>
<body>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div class="wrapper">
	<div class="content">
		<header>
			<div class="container">
				<div class="row header-row position-relative">
					<? $APPLICATION->IncludeComponent('bitrix:sale.location.selector.search', 'city_search', array(
						"COMPONENT_TEMPLATE"         => "city_search",
						"ID"                         => "",
						"CODE"                       => "",
						"INPUT_NAME"                 => "LOCATION",
						"PROVIDE_LINK_BY"            => "id",
						"JSCONTROL_GLOBAL_ID"        => "",
						"JS_CALLBACK"                => "",
						"FILTER_BY_SITE"             => "Y",
						"SHOW_DEFAULT_LOCATIONS"     => "Y",
						"CACHE_TYPE"                 => "A",
						"CACHE_TIME"                 => "36000000",
						"FILTER_SITE_ID"             => "s1",
						"INITIALIZE_BY_GLOBAL_EVENT" => "",
						"SUPPRESS_ERRORS"            => "N",
						'JS_CONTROL_GLOBAL_ID'       => 'as',
					)); ?>
					<div class="col-auto ml-auto">
						<a href="#" class="location"><?=$USER->GetParam('city')?></a>
					</div>
					<div class="col-12">
						<div id="bootstrap-4-dropdown-keep-open"></div>
						<div class="header">
							<div class="row h-100 align-items-center">
								<div class="col-auto h-100 d-flex align-items-center">
									<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
										"AREA_FILE_SHOW"   => "file",
										"AREA_FILE_SUFFIX" => "inc",
										"EDIT_TEMPLATE"    => "",
										"PATH"             => "/include/logo.php",
									)); ?>
								</div>
								<div class="col-auto h-100 p-0">
									<div class="phone-wrapper"><a href="tel:88005008179" class="phone">8 800 500 81 79</a></div>
								</div>

								<div class="col-auto ml-auto ml-0">
									<? if($USER->IsAuthorized()): ?>
										<a href="/cabinet/<?=URL_PART_USER_OF_ORDER?>/edit/info/main/" class="user-btn">
											<span class="user-name"><?=$USER->GetFirstName()?></span>
											<form action="<?=$arResult["AUTH_URL"]?>">
												<input type="hidden" name="logout" value="yes"/>
												<input id="btn-logout" type="submit" name="logout_butt" value="Выйти"/>
											</form>
										</a>
									<? else: ?>
										<div class="user-btn alter-width ml-lg-auto">
											<a class="user-btn-link" data-fancybox data-src="#enter" href="javascript:">Вход</a>
										</div>
										<div class="user-btn alter-width">
											<a class="user-btn-link" data-fancybox data-src="#register" href="javascript:">Регистрация</a>
										</div>
										<div class="modal-window small-modal" id="enter">
											<? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "", Array(
												"FORGOT_PASSWORD_URL" => "",
												"PROFILE_URL"         => "",
												"REGISTER_URL"        => "",
												"SHOW_ERRORS"         => "Y",
											)); ?>
										</div>
										<div class="modal-window small-modal" id="register">
											<? $APPLICATION->IncludeComponent("bitrix:main.register", "", Array(
												"AUTH"               => "Y",
												"REQUIRED_FIELDS"    => array("EMAIL", "NAME", "LAST_NAME"),
												"SET_TITLE"          => "Y",
												"SHOW_FIELDS"        => array("EMAIL", "NAME", "SECOND_NAME", "LAST_NAME", "PERSONAL_BIRTHDAY", "PERSONAL_PHONE"),
												"SUCCESS_PAGE"       => "/",
												"USER_PROPERTY"      => array("UF_ORDER", 'UF_SELLER_TYPE'),
												"USER_PROPERTY_NAME" => "",
												"USE_BACKURL"        => "Y",
											)); ?>
										</div>
										<div class="modal-window small-modal" id="forgot">
											<? $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "in_modal"); ?>
										</div>
									<? endif; ?>
								</div>


								<? if(CModule::includeModule("sale")): ?>
									<div class="col-auto favorites-wrapper">
										<div id="wishcount">

											<? $delaydBasketItems = CSaleBasket::GetList(array(), array(
												"FUSER_ID" => CSaleBasket::GetBasketUserID(),
												"LID"      => SITE_ID,
												"ORDER_ID" => "NULL",
												"DELAY"    => "Y",
											), array()); ?>
										</div>

										<?php
										$favoritesSpan = '<span class="have-favorites"></span>';
										$userId        = $_SESSION['SESS_AUTH']['USER_ID'];
										?>
										<?php if($_SESSION['SESS_AUTH']['AUTHORIZED'] === 'Y' && $delaydBasketItems !== 0): ?>
											<a href="/cabinet/<?=URL_PART_USER_OF_ORDER?>/edit/catalog/wishlist/"
											   class="favorites <?=$delaydBasketItems === 0?'empty':''?>">
												<?=$favoritesSpan?>
											</a>
										<?php else: ?>
											<span class="favorites empty"><?=$favoritesSpan?></span>
										<?php endif; ?>
									</div>
								<? endif; ?>
								<? if(CModule::includeModule("sale")){
									$basket_price_total = Bitrix\Sale\BasketComponentHelper::getFUserBasketPrice(CSaleBasket::GetBasketUserID(), SITE_ID);
									$basket_items_count = CSaleBasket::GetList(array(), array(
										"FUSER_ID" => CSaleBasket::GetBasketUserID(),
										"LID"      => SITE_ID,
										"ORDER_ID" => "NULL",
										"DELAY"    => "N",
									), array());
								} ?>
								<div class="col cart-header-wrapper dropdown" id="dropdownCartBodyWrapper">
									<!-- Basket content -->
									<div class="cart-header <?=$basket_items_count !== 0?'js-cart-header':''?>" id="dropdownCartBody" aria-haspopup="false" aria-expanded="false"
									     <?=$basket_items_count !== 0?'data-toggle="dropdown"':''?> data-flip="false">
										<span class="cart-name">Корзина</span>
										<div class="cart-body-wrapper">
                                            <span class="cart-price <?=$basket_price_total === 0?'d-none':''?>">
                                                <?=$basket_price_total !== 0?CurrencyFormat($basket_price_total, 'RUB'):''?>
                                            </span>
											<span class="cart-item-number <?=$basket_items_count === 0?'d-none':''?>"><?="$basket_items_count"?></span>
											<div class="cart-body dropdown-menu" aria-labelledby="dropdownCartBody">
												<? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "basket_link", Array(
													"HIDE_ON_BASKET_PAGES" => "N",
													"MAX_IMAGE_SIZE"       => "100",
													"PATH_TO_BASKET"       => "/cabinet/" . USER_ROLE . "/edit/catalog/cart/",
													"PATH_TO_ORDER"        => "/cabinet/" . USER_ROLE . "/edit/order/make/",
													"PATH_TO_PERSONAL"     => "/cabinet/" . USER_ROLE . "/edit/info/main/",
													"POSITION_FIXED"       => "N",
													"SHOW_AUTHOR"          => "N",
													"SHOW_DELAY"           => "N",
													"SHOW_EMPTY_VALUES"    => "N",
													"SHOW_IMAGE"           => "Y",
													"SHOW_NOTAVAIL"        => "Y",
													"SHOW_NUM_PRODUCTS"    => "Y",
													"SHOW_PERSONAL_LINK"   => "N",
													"SHOW_PRICE"           => "Y",
													"SHOW_PRODUCTS"        => "Y",
													"SHOW_REGISTRATION"    => "N",
													"SHOW_SUMMARY"         => "Y",
													"SHOW_TOTAL_PRICE"     => "Y",
												)); ?>
												<script type="text/javascript">
													BitrixSmallCart.prototype.setUserRole('<?=USER_ROLE?>');
												</script>
											</div>
										</div>
									</div>
									<!-- Basket content -->
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<? if(!IS_HOME): ?>
			<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", array(
				"START_FROM"         => "0",
				"PATH"               => "",
				"SITE_ID"            => "s1",
				"COMPONENT_TEMPLATE" => "breadcrumbs",
			), false); ?>
		<? endif; ?>
		<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/local/templates/ferma_24/template/page/template/' . PAGE_TEMPLATE_NAME . '/content-before.php'; ?>
