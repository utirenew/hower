<?php

//Подключаем ядро Битрикс и главный модуль
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use Bitrix\Sale\Fuser;
use Bitrix\Sale\Internals\BasketTable;

//Подключаем модуль sale
CModule::includeModule("sale");
$arBasketItemsFull = array();
//Получение полей для атрибута onclick
$rs = BasketTable::getList([
    'filter' => [
	    "FUSER_ID"   => Fuser::getId(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    ],
    'select' => [
        "ID",
        "PRODUCT_ID",
        "PRODUCT_PRICE_ID",
        "PRICE",
        "NAME",
        "DETAIL_PAGE_URL",
    ]
]);
while ($ar = $rs->fetch()) {
    $arBasketItemsFull[$ar["ID"]]  = $ar;
}
$itemData = $arBasketItemsFull[intval($_POST['p_id'])];
$itemData['USER_ROLE'] = USER_ROLE;

    //Получаем количество отложеных товаров
    if (CSaleBasket::Delete( intval($_POST['p_id']) )) {
        $arBasketItems = array();

        $dbBasketItems = CSaleBasket::GetList(
            array(
                "NAME" 	=> "ASC",
                "ID" 	=> "ASC"
            ),
            array(
                "FUSER_ID" 		=> CSaleBasket::GetBasketUserID(),
                "LID" 			=> SITE_ID,
                "ORDER_ID" 		=> "NULL",
                "DELAY" 		=> "Y",
            ),
            false,
            false,
            array("PRODUCT_ID")
        );

        while ($arItems = $dbBasketItems->Fetch()){
            $arBasketItems[] = $arItems["PRODUCT_ID"];
        }

        //Загоняем отложенне в переменную
        $inwished = count($arBasketItems);
    }

//Выводи количество отложенных товаров
// echo $inwished;
echo json_encode(array(
    'count' => $inwished,
    'itemData'    => $itemData,
));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
