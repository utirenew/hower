<?php

//Подключаем ядро Битрикс и главный модуль
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use Bitrix\Sale\Fuser;
use Bitrix\Sale\Internals\BasketTable;

//Подключаем модуль sale
Loader::includeModule("sale");

	//Получаем корзину текущего пользователя
    $fUserID = CSaleBasket::GetBasketUserID();

    //Создаем переменные для обработчика
    $arFields = array(
        "PRODUCT_ID" 			=> intval($_POST['p_id']),
        "PRODUCT_PRICE_ID" 		=> intval($_POST['pp_id']),
        "PRICE" 				=> floatval($_POST['p']),
        "CURRENCY" 				=> "RUB",
        "QUANTITY" 				=> 1,
        "LID" 					=> 's1',
        "DELAY" 				=> "Y",
        "CAN_BUY" 				=> "Y",
        "NAME" 					=> preg_replace( '/[^ \/ 0-9 а-яА-Я]+/u', '', $_POST['name'] ),
        "MODULE" 				=> "catalog",
        "NOTES" 				=> "Розничная цена",
        "DETAIL_PAGE_URL" 		=> preg_replace( '/[^ \/ 0-9 a-z]+/u', '', $_POST['dpu'] ),
        "FUSER_ID" 				=> $fUserID,
        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
        'PRODUCT_XML_ID'        => intval($_POST['p_id']),
    );

    //Получаем количество отложеных товаров
    if (CSaleBasket::Add($arFields)) {
        $arBasketItems  = array();
        $basketKeys     = array();
        $basket_item_id = null;

        $dbBasketItems = CSaleBasket::GetList(
            array(
                "NAME" 	=> "ASC",
                "ID" 	=> "ASC"
            ),
            array(
                "FUSER_ID" 		=> CSaleBasket::GetBasketUserID(),
                "LID" 			=> SITE_ID,
                "ORDER_ID" 		=> "NULL",
                "DELAY" 		=> "Y",
            ),
            false,
            false,
            array("PRODUCT_ID")
        );

        while ($arItems = $dbBasketItems->Fetch()){
            $arBasketItems[] = $arItems["PRODUCT_ID"];
        }

        //Загоняем отложенне в переменную
        $inwished = count($arBasketItems);


        //Получение id продукта в корзине
        Loader::includeModule('sale');
        $rs = BasketTable::getList([
            'filter' => [
	            "FUSER_ID"   => Fuser::getId(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ],
            'select' => [
                "ID",
                "PRODUCT_ID",
                "QUANTITY"
            ]
        ]);
        while ($ar = $rs->fetch()) {
            $ar["QUANTITY"]                 = (int) $ar["QUANTITY"];
            $basketKeys[$ar['PRODUCT_ID']]  = $ar;
        }
        if ( array_key_exists(intval($_POST['p_id']), $basketKeys) ) {
            $basket_item_id = $basketKeys[intval($_POST['p_id'])]['ID'];
        }
    }

//Выводи количество отложенных товаров
echo json_encode(array(
    'id'    => $basket_item_id,
    'count' => $inwished,
    'debug' => $_POST
));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
