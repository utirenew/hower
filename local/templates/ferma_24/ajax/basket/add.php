<?php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

Loader::includeModule("sale");

$resultCSaleBasket = CSaleBasket::Update(
    intval($_POST['basket_line_item_id']),
    array(
        'QUANTITY' => intval($_POST['basket_line_item_quantity'])
    )
);

$result = array(
    'resultCSaleBasket' => $resultCSaleBasket,
    // 'debug' => array(
    //     'id'        => intval($_POST['basket_line_item_id']),
    //     'quantity'  => intval($_POST['basket_line_item_quantity']),
    //     'post'      => $_POST
    // )
);

echo json_encode($result);
