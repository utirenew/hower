<?

use Bitrix\Main\Loader, Bitrix\Main\EventManager;

Loader::includeModule("sepro.helper");
Loader::includeModule('sale');
Loader::includeModule('socialnetwork');

$eventManager = EventManager::getInstance();

$eventManager->addEventHandler('sale', 'OnSaleOrderSaved', ['Sepro\\Events', 'SaleOrderSaved']);
//$eventManager->addEventHandler("sale", "OnBeforeBasketAdd", "checkPermissionOnBeforeBasketAdd");


function checkPermissionOnBeforeBasketAdd(&$arFields){
	global $USER;

	$userDataObj   = CUser::GetByID($USER->GetID());
	$userData      = $userDataObj->Fetch();
	$user_UF_ORDER = $userData["UF_ORDER"];     // 6 or 8, see more in b_user_field_enum table

	switch(intval($user_UF_ORDER)){
		case 6:
		case 8:
			// code...
			break;

		default:
			$arFields['PRODUCT_ID'] = null;
			break;
	}
}