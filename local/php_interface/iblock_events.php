<?

use Bitrix\Main\Loader,
	Bitrix\Main\EventManager;

Loader::includeModule("sepro.helper");
Loader::includeModule("sale");

$eventManager = EventManager::getInstance();

$eventManager->addEventHandler("iblock", "OnBeforeIBlockElementAdd", ['Sepro\\Events', 'IBlockBeforeIBlockElementAddHandler']);
$eventManager->addEventHandler("iblock", "OnAfterIBlockElementAdd", ['Sepro\\Events', 'IBlockAfterIBlockElementAddUpdateHandler']);
$eventManager->addEventHandler("iblock", "OnAfterIBlockElementUpdate", ['Sepro\\Events', 'IBlockAfterIBlockElementAddUpdateHandler']);
$eventManager->addEventHandler("iblock", "OnBeforeIBlockElementDelete", ['Sepro\\Events', 'IBlockBeforeIBlockElementDeleteHandler']);
