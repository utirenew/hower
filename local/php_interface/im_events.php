<?

use Bitrix\Main\Loader,
	Bitrix\Main\EventManager;

Loader::includeModule("sepro.helper");
Loader::includeModule('im');


$eventManager = EventManager::getInstance();

$eventManager->addEventHandler('im', 'OnBeforeMessageNotifyAdd', ['Sepro\\Events', 'ImBeforeMessageAdd']);