<?

use Bitrix\Main\EventManager;


$eventManager = EventManager::getInstance();

$eventManager->addEventHandler("currency", "CurrencyFormat", "Ferma_CurrencyFormat");


function Ferma_CurrencyFormat($fSum, $strCurrency){
	return number_format($fSum, 0, '.', ' ');
}