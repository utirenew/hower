<?

use Bitrix\Main\Loader,
	Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();

$eventManager->addEventHandler("main", "OnBeforeProlog", "checkUserPermissionOnBeforeExecTemplates");
$eventManager->addEventHandler("main", "OnAfterUserAdd", "OnAfterUserAddHandler");


function checkUserPermissionOnBeforeExecTemplates(){
	global $USER;

	$userDataObj   = CUser::GetByID($USER->GetID());
	$userData      = $userDataObj->Fetch();
	$user_UF_ORDER = $userData["UF_ORDER"];     // 6 or 8, see more in b_user_field_enum table

	switch(intval($user_UF_ORDER)){
		case 6:
			$url_part_user_of_order = 'shopper';
			$user_role              = 'shopper';
			break;
		case 8:
			$url_part_user_of_order = 'seller';
			$user_role              = 'seller';
			break;
		default:
			$url_part_user_of_order = 'guest';
			$user_role              = 'guest';
			break;
	}
	define('USER_ROLE', $user_role);
	define('URL_PART_USER_OF_ORDER', $url_part_user_of_order);
}

function OnAfterUserAddHandler($arFields){
	if($arFields["ID"] > 0){
		if($arFields["UF_ORDER"] == '8')  //Если поле UF_ORDER заполнено и не равно 7
		{
			$arGroups   = CUser::GetUserGroup($arFields["ID"]);
			$arGroups[] = 9; //ПРОДАВЦЫ
			CUser::SetUserGroup($arFields["ID"], $arGroups);
		}
		else{
			$arGroups   = CUser::GetUserGroup($arFields["ID"]);
			$arGroups[] = 10;  //ПОКУПАТЕЛИ
			CUser::SetUserGroup($arFields["ID"], $arGroups);
		}
	}
}