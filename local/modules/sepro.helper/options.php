<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$request = \Sepro\User::getRequest();
$rights = \Sepro\App::getInstance()->GetGroupRight("sepro.helper");
$arLanguages = array();

// Перехватываем AJAX
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($request->get('DIV')))
{
    \Sepro\App::getInstance()->RestartBuffer();

    $arErrors = array();
    $arResponse = array(
        /* DEFAULT VARIABLES */
        'DIV' => $request->get('DIV'),
        'FID' => $request->get('FID'),
        'COUNTER' => intval($request->get('COUNTER')),
        'LENGTH' => intval($request->get('LENGTH')),
        'STEP' => intval($request->get('STEP')) > 0 ? $request->get('STEP') : 1,

        /* IMPORT VARIABLES */
        'MODEL' => $request->get('MODEL'),
        'IBLOCK_CATALOG_ID' => $request->get('IBLOCK_CATALOG_ID'),
        'IBLOCK_OFFERS_ID' => $request->get('IBLOCK_OFFERS_ID'),
        'CATALOG_GROUP_ID' => $request->get('CATALOG_GROUP_ID'),

        /* SESSION VARIABLES */
        'autosave_id' => $request->get('autosave_id'),
        'sessid' => $request->get('sessid')
    );

    try
    {
        if($arResponse['LENGTH'] > 0 && $arResponse['COUNTER'] >= $arResponse['LENGTH'])
        {
            throw new Exception(implode('|', array(
                'Обработано: '.intval($arResponse['LENGTH']).' стр',
                'Завершено!',
                'OK')
            ));
        }

        if(check_bitrix_sessid())
        {
            foreach(array('IBLOCK_CATALOG_ID', 'IBLOCK_OFFERS_ID', 'CATALOG_GROUP_ID') as $key)
            {
                if(!empty($arResponse[$key]))
                {
                    define($key, $arResponse[$key]);
                    continue;
                }

                if(defined($key))
                {
                    $arResponse[$key] = constant($key);
                    continue;
                }

                switch($key)
                {
                    case 'IBLOCK_CATALOG_ID':

                        $value = \Sepro\IBlock::getIBCatalog();

                        break;
                    case 'IBLOCK_OFFERS_ID':

                        $value = \Sepro\IBlock::getIBOffers();

                        break;
                    case 'CATALOG_GROUP_ID':

                        $value = \Bitrix\Catalog\GroupTable::getList(array(
                            'filter' => array('=BASE' => 'Y'),
                            'select' => array('ID')
                        ))->fetch();

                        break;
                }

                $arResponse[$key] = $value['ID'];
                define($key, $value['ID']);
            }

            switch($arResponse['DIV'])
            {
                case 'UPDATE_DEPENDENCE':

                    if(!defined('IBLOCK_CATALOG_ID'))
                    {
                        throw new Exception('Не определена константа IBLOCK_CATALOG_ID');
                        break;
                    }

                    $arFilter = array(
                        '=IBLOCK_ID' => 1,
                        'ACTIVE' => 'Y'
                    );

                    if($arResponse['LENGTH'] <= 0)
                    {
                        $arResponse['LENGTH'] = \Sepro\ElementTable::GetList(array(
                            'filter' => $arFilter
                        ))->getSelectedRowsCount();
                    }

                    $arFilter['>ID'] = intval($arResponse['FID']);

                    $rsElements = \Sepro\ElementTable::GetList(array(
                        'order' => array('ID' => 'ASC'),
                        'filter' => $arFilter,
                        'select' => array('ID')
                    ));

                    $n = 1;
                    while($arElement = $rsElements->fetch())
                    {
                        \Sepro\Events::updateDependence($arElement['ID'], 'UPDATE');

                        $arResponse['FID'] = $arElement['ID'];
                        $arResponse['COUNTER']++;
                        $n++;

                        if($n > $arResponse['STEP']) break;
                    }

                    break;
            }

            $obMessage = new \CAdminMessage(array(
                'MESSAGE' => 'Процесс выполнения:',
                "DETAILS" => "#PROGRESS_BAR#",
                "HTML" => true,
                "TYPE" => "PROGRESS",
                "PROGRESS_TOTAL" => 100,
                "PROGRESS_VALUE" => round(100 / ($arResponse['LENGTH'] / $arResponse['COUNTER']), 2)
            ));

            $arResponse['HTML'] = $obMessage->Show();
        }
        else
        {
            $arErrors[] = 'Ваша сессия больше не актуальна';
        }

        if(!empty($arErrors))
        {
            throw new Exception(implode('<br>', $arErrors));
        }

        echo json_encode($arResponse);
    }
    catch(Exception $e)
    {
        list($detail, $message, $type) = explode('|', $e->getMessage());

        $message = new \CAdminMessage(array(
            'MESSAGE' => !empty($message) ? $message : 'Программная ошибка!',
            'DETAILS' => !empty($detail) ? $detail : 'Обратитесь к администраторам сайта',
            'TYPE' => $type == 'OK' ? 'OK' : 'ERROR'
        ));

        echo json_encode(array(
            'DIV' => $arResponse['DIV'],
            'HTML' => $message->Show(),
            'FINISH' => true
        ));
    }

    die();
}

if ($rights >= "R")
{
    $rsLanguages = \Bitrix\Main\Localization\LanguageTable::GetList(
        array(
            'order' => array('ID' => 'ASC'),
            'select' => array('ID', 'NAME')
        )
    );

    while($arLanguage = $rsLanguages->fetch())
    {
        $arLanguages[$arLanguage['ID']] = $arLanguage['NAME'];
    }

    $COptions = new CModuleOptions(
        "sepro.helper",
        array(
            array(
                'DIV' => 'general',
                'TYPE' => 'SETTING',
                'TAB' => 'Настройки',
                'TITLE' => 'Настройка параметров модуля',
                'GROUPS' => array(
                    array(
                        'TITLE' => 'Константы',
                        'OPTIONS' => array(
                            'SITE_NAME' => array(
                                'TITLE' => 'Основной домен сайта',
                                'TYPE' => 'TEXT',
                                'SORT' => '0',
                                'VALUE' => SITE_NAME,
                            ),
                            'DOCUMENT_ROOT' => array(
                                'TITLE' => 'Абсолютный путь к корню сайта',
                                'TYPE' => 'TEXT',
                                'SORT' => '1',
                                'VALUE' => DOCUMENT_ROOT
                            ),
                            'LANGUAGE_ID' => array(
                                'TITLE' => 'Основной язык сайта',
                                'TYPE' => 'SELECT',
                                'SORT' => '2',
                                'VALUE' => LANGUAGE_ID,
                                'OPTIONS' => $arLanguages
                            ),
                            'SYSTEM_LOG' => array(
                                'TITLE' => 'Путь к файлу логирования о системных ошибках',
                                'TYPE' => 'TEXT',
                                'SORT' => '4',
                                'VALUE' => SYSTEM_LOG
                            )
                        )
                    )
                )
            ),
            array(
                'DIV' => 'UPDATE_DEPENDENCE',
                'TYPE' => 'SPECIAL',
                'TAB' => 'Скрипт работы по шагам',
                'TITLE' => 'Обновление актуальности товаров',
                'GROUPS' => array(
                    array(
                        'OPTIONS' => array(
                            'STEP' => array(
                                'TITLE' => 'Диапазон одной итерации',
                                'TYPE' => 'INTEGER',
                                'SORT' => '0',
                                'VALUE' => 5
                            )
                        )
                    )
                )
            )
        ),
        true
    );

    $COptions->saveOptions();
    $COptions->showHTML();
}
