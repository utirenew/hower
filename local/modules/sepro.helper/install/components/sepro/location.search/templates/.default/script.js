function LocationSearch(opts)
{
    this.value = false;
    this.token = false;
    this.CVariants = false;
    this.timer = null;
    this.isMobile = !!$('.bx-retina') || !!$('.bx-android');
    this.arCache = {};
    this.LOCATION = {};
    this.opts = {};

    if(!$.isEmptyObject(opts))
    {
        this.opts = opts;

        if(!!opts.LOCATION)
        {
            this.value = opts.LOCATION.DISPLAY || false;
            this.LOCATION = opts.LOCATION;
        }

        if(!!opts.LOCATION_TOKEN)
        {
            this.token = opts.LOCATION_TOKEN;
        }
    }
}

LocationSearch.prototype.redirect = function(source)
{
    if(this.isMobile)
        document.location = source;
    else
        window.location.replace(source);
};

LocationSearch.prototype.click = function(element, event)
{
    event.preventDefault();

    var $element = $(element),
        value = +$element.data('value') || 0,
        code = $element.data('code') || 0,
        name = $element.data('name') || false,
        type = $element.data('type') || 5;

    $(this.CVariants).html('');

    this.LOCATION = {
        CODE: code,
        TYPE_ID: type,
        ID: value,
        DISPLAY: name
    };
};


LocationSearch.prototype.keyup = function(input)
{
    if(!!this.timer)
    {
        clearTimeout(this.timer);
    }

    this.timer = setTimeout(BX.delegate(function()
    {
        this.handler(input);
    }, this), 750);
};

LocationSearch.prototype.handler = function(input)
{
    var value = input.value.replace(/[^a-zA-Zа-яА-Я0-9- ]/gi, '');

    if(!value || this.value === value || value.length <= 2)
    {
        return;
    }

    this.CVariants = '.' + this.opts.JS_CLASS_VARIANTS;

    if(!!this.arCache[value])
    {
        $(this.CVariants).html(this.arCache[value]);
        return;
    }

    this.value = value;

    $.ajax({
        url: this.opts.SOURCE,
        data:
        {
            VALUE: this.value,
            LOCATION_TOKEN: this.token
        },
        type: 'post',
        dataType: 'html',
        success: BX.delegate(this.ajaxResult, this),
        error: function(xhr, st)
        {
            console.log(xhr);
        }
    });
};

LocationSearch.prototype.ajaxResult = function(result)
{
    $(this.CVariants).html(result);
    this.arCache[this.value] = result;
};