<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$request = \Sepro\User::getRequest();?>
<div class="header-city-popup">
    <a href="#" class="header-city-popup-close close"></a>
    <div class="header-city-popup-head">
        <div class="header-city-popup-title">Выбор города</div>
        <div class="header-city-popup-title-desc">Всего: <?=intval($arResult['COUNT']);?>&nbsp;<?=\Sepro\Helpers::num2word(intval($arResult['COUNT']), array('город', 'города', 'городов'));?></div>
    </div>
    <div class="header-city-popup-search">
        <input
            type="text"
            class="header-city-popup-search-input fuzzy-search"
            placeholder="Введите название города..."
            value="<?=$arResult['LOCATION']['DISPLAY'];?>"
            onkeyup="_<?=$arParams['JS_OBJECT_NAME'];?>.keyup(this);"
        >
        <i class="header-city-popup-search-submit search-btn"></i>
    </div>
    <?//=\Sepro\Helpers::printPre($arResult);?>
    <div class="<?=$arParams['JS_CLASS_VARIANTS'];?>">
        <?if(!empty($arResult['LOCATIONS'])):
            \Sepro\App::GetInstance()->RestartBuffer();?>
            <div class="header-city-popup-body">
                <?if(!empty($arResult['LOCATIONS']['ITEMS'])):?>
                    <ul class="header-city-popup-list list">
                        <?foreach($arResult['LOCATIONS']['ITEMS'] as $arLocation):?>
                            <li>
                                <a href="#" class="city"
                                    onclick="_<?=$arParams['JS_OBJECT_NAME'];?>.click(this, event);"
                                    data-code="<?=$arLocation['CODE'];?>"
                                    data-name="<?=$arLocation['DISPLAY'];?>"
                                    data-type="<?=$arLocation['TYPE_ID'];?>"
                                    data-value="<?=$arLocation['VALUE'];?>">
                                    <b><?=$arLocation['DISPLAY'];?></b><span class="delimeter">, </span>
                                    <?foreach($arLocation['PATH'] as $LID):
                                        if(empty($arResult['LOCATIONS']['ETC']['PATH_ITEMS'][$LID])) continue;?>
                                        <?=$arResult['LOCATIONS']['ETC']['PATH_ITEMS'][$LID]['DISPLAY'];?><span class="delimeter">, </span>
                                    <?endforeach;?>
                                </a>
                            </li>
                        <?endforeach;?>
                    </ul>
                <?else:?>
                    <div class="errors"><div class="error">К сожалению, ничего не найдено.</div></div>
                <?endif;?>
            </div>
        <?die(); endif;?>
    </div>
</div>
<script type="text/javascript">
    var _<?=$arParams['JS_OBJECT_NAME'];?> = new LocationSearch(<?=json_encode(array(
        'SOURCE' => \Sepro\App::GetCurPage(),
        'LOCATION_TOKEN' => $arParams['LOCATION_TOKEN'],
        'JS_CLASS_VARIANTS' => $arParams['JS_CLASS_VARIANTS'],
        'LOCATION' => $arResult['LOCATION']
    ))?>);
</script>

<?// EXAMPLE
/*$APPLICATION->IncludeComponent(
    'sepro:location.search',
    'top',
    array(
        'CACHE_ADDON' => array(
            'SOURCE' => \Sepro\App::GetInstance()->GetCurPage(false),
            'JS_OBJECT_NAME' => 'obLocationTop',
            'CURRENT_LOCATION' => !empty($GLOBALS['USER_LOCATION']['CITY']) ? $GLOBALS['USER_LOCATION']['CITY'] : 'Екатеринбург',
        ),
        'LOCATION_TOKEN' => 'location.search.top',
        'CURRENT_LOCATION' => !empty($GLOBALS['USER_LOCATION']['CITY']) ? $GLOBALS['USER_LOCATION']['CITY'] : 'Екатеринбург',
        'JS_CLASS_VARIANTS' => 'js-top-class-variants',
        'JS_OBJECT_NAME' => 'obLocationTop',
        'CACHE_TYPE' => 'Y',
        'CACHE_GROUPS' => 'Y'
    ),
    false
);*/
?>