<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Entity,
    \Bitrix\Main\UI,
    \Bitrix\Catalog\StoreProductTable,
    \Bitrix\Catalog\ProductTable,
    \Bitrix\Catalog\PriceTable,
    \Sepro\ElementTable,
    \Sepro\ORMFactory,
    \Sepro\IBlock,
    \Sepro\User,
    \Sepro\App;

class StandardElementsComponent extends CBitrixComponent
{
    protected $cacheKeys = array(); // кешируемые ключи arResult
    protected $cacheAddon = array(); // дополнительные параметры, от которых зависит кеш
    protected $nav = array(); // параметры постраничной навигации
    protected $sections = array(); // разделы инфоблоков
    protected $iblock = array(); // Данные инфоблока
    protected $obQuery; // Сущность запросов
    protected $limit = 0;
    protected $offset = 0;

    protected $checkParams = array(
        'IBLOCK_TYPE' => array('type' => 'string'),
        'COUNT' => array('type' => 'int')
    );

    public $_return = false;

    // ПОДКЛЮЧАЕМ ЯЗЫКОВЫЕ ФАЙЛЫ
    public function onIncludeComponentLang()
    {
        Loc::setCurrentLang(LANGUAGE_ID);
        $this->includeComponentLang(basename(__FILE__), LANGUAGE_ID);
        Loc::loadMessages(__FILE__);
    }

    // ПРОВЕРЯЕТ PARAMS
    public function onPrepareComponentParams($params)
    {
        $params = array_replace_recursive(
            $params,
            array(
                'IBLOCK_TYPE' => trim($params['IBLOCK_TYPE']),
                'IBLOCK_ID' => intval($params['IBLOCK_ID']),
                'SECTION_ID' => !empty($params['SECTION_ID']) && is_array($params['SECTION_ID']) ? array_unique($params['SECTION_ID']) : array(),
                'ITEM_PROPERTIES' => !empty($params['ITEM_PROPERTIES']) && is_array($params['ITEM_PROPERTIES']) ? array_unique($params['ITEM_PROPERTIES']) : array(),
                'OFFER_PROPERTIES' => !empty($params['OFFER_PROPERTIES']) && is_array($params['OFFER_PROPERTIES']) ? array_unique($params['OFFER_PROPERTIES']) : array(),
                'RELATION_PROPS' => !empty($params['RELATION_PROPS']) && is_array($params['RELATION_PROPS']) ? array_unique($params['RELATION_PROPS']) : array(),
                'SORT_FIELD1' => mb_strlen($params['SORT_FIELD1']) ? $params['SORT_FIELD1'] : 'ID',
                'SORT_DIRECTION1' => $params['SORT_DIRECTION1'] == 'ASC' ? 'ASC' : 'DESC',
                'SORT_FIELD2' => mb_strlen($params['SORT_FIELD2']) ? $params['SORT_FIELD2'] : 'ID',
                'SORT_DIRECTION2' => $params['SORT_DIRECTION2'] == 'ASC' ? 'ASC' : 'DESC',
                'OFFER_SORT_FIELD' => !empty($params['OFFER_SORT_FIELD']) ? $params['OFFER_SORT_FIELD'] : 'PRICE.PRICE',
                'OFFER_SORT_DIRECTION' => !empty($params['OFFER_SORT_DIRECTION']) ? $params['OFFER_SORT_DIRECTION'] : 'ASC',
                'CHECK_MODULE_CATALOG' => $params['CHECK_MODULE_CATALOG'] == 'Y' ? 'Y' : 'N',
                'SHOW_NAV' => $params['SHOW_NAV'] == 'Y' ? 'Y' : 'N',
                'VARIABLE_NAV' => !empty($params['VARIABLE_NAV']) ? $params['VARIABLE_NAV'] : 'pagen',
                'CATALOG_GROUPS' => is_array($params['CATALOG_GROUPS']) ? array_unique($params['CATALOG_GROUPS']) : array(),
                'CACHE_TYPE' => $params['CACHE_TYPE'] == 'Y' ? 'Y' : 'N',
                'CACHE_TIME' => intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600,
            )
        );

        $this->arResult['PARAMS'] = $params;
        return $params;
    }

    // ПРОВЕРЯЕТ ЧИТАТЬ ДАННЫЕ ИЗ КЕША
    protected function readDataFromCache()
    {
        if($this->arParams['CACHE_TYPE'] == 'N')
        {
            return true;
        }

        return $this->StartResultCache(false, $this->cacheAddon);
    }

    // КЭШИРУЕТ КЛЮЧИ МАССИВА
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && !empty($this->cacheKeys))
        {
            $this->arResult['CACHE_KEYS'] = $this->cacheKeys;
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    // ПРОВЕРЯЕТ ОБЯЗАТЕЛЬНЫЙ ВВОД ПАРАМЕТРОВ ИЗ $this->checkParams
    private function checkAutomaticParams()
    {
        foreach ($this->checkParams as $key => $params)
        {
            switch ($params['type'])
            {
                case 'int':

                    if (intval($this->arParams[$key]) <= 0)
                    {
                        throw new \Bitrix\Main\ArgumentTypeException($key, 'integer');
                    }
                    break;

                case 'string':

                    $this->arParams[$key] = htmlspecialchars(trim($this->arParams[$key]));
                    if (empty($this->arParams[$key]))
                    {
                        throw new \Bitrix\Main\ArgumentNullException($key);
                    }
                    break;

                case 'array':

                    if (!is_array($this->arParams[$key]))
                    {
                        throw new \Bitrix\Main\ArgumentTypeException($key, 'array');
                    }
                    break;

                default:
                    throw new \Bitrix\Main\ArgumentTypeException($key);
            }
        }
    }

    // ВЫПОЛНЯЕТ ДЕЙСТВИЯ НАД КЕШИРОВАНИЕМ
    protected function executeProlog()
    {
        $this->setFrameMode(true); // Голосуем за автокомпозит. Динамическая компонента
        if (intval($this->arParams['COUNT']) > 0)
        {
            $this->nav = new UI\PageNavigation($this->arParams['VARIABLE_NAV']);
            $this->nav->allowAllRecords(false)->setPageSize($this->arParams['COUNT'])->initFromUri();
            $this->limit = $this->nav->getLimit() + 1;
            $this->offset = $this->nav->getOffset();
        }

        if (!empty($this->arParams['CACHE_ADDON']))
        {
            $this->cacheAddon = $this->arParams['CACHE_ADDON'];
            $this->arResult['CACHE_ADDON'] = $this->cacheAddon;
        }
    }

    // ПОДГОТАВЛИВАЕМ SELECT
    protected function onPrepareSelect($check, $arSelect)
    {
        switch($check)
        {
            case 'ITEM':
            case 'OFFER':

                // НАДСТРОЙКА ДЛЯ ПОСТРАНИЧНОЙ НАВИГАЦИИ
                if($check === 'ITEM')
                {
                    $this->obQuery->setLimit($this->limit);
                    $this->obQuery->setOffset($this->offset);
                }

                // НАДСТРОЙКА ДЛЯ СВОЙСТВ
                if(!empty($this->arParams[$check.'_PROPERTIES']))
                {
                    $arProperties = array();
                    $arHLTables = array();
                    $rsProperties = \Bitrix\Iblock\PropertyTable::GetList(
                        array(
                            'order' => array('ID' => 'DESC'),
                            'filter' => array('=ID' => $this->arParams[$check.'_PROPERTIES']),
                            'select' => array(
                                'ID',
                                'NAME',
                                'SORT',
                                'CODE',
                                'HINT',
                                'PROPERTY_TYPE',
                                'LIST_TYPE',
                                'MULTIPLE',
                                'IS_REQUIRED',
                                'USER_TYPE',
                                'LINK_IBLOCK_ID',
                                'USER_TYPE_SETTINGS'
                            )
                        )
                    );

                    while($arProperty = $rsProperties->fetch())
                    {
                        if(!empty($arProperty['USER_TYPE_SETTINGS']))
                        {
                            $arProperty['USER_TYPE_SETTINGS'] = unserialize($arProperty['USER_TYPE_SETTINGS']);
                        }

                        switch($arProperty['USER_TYPE'])
                        {
                            case 'directory': // Если обнаружили своство типа справочник

                                if(strlen($arProperty['USER_TYPE_SETTINGS']['TABLE_NAME']) > 0)
                                {
                                    $arHLTables[$arProperty['USER_TYPE_SETTINGS']['TABLE_NAME']] = $arProperty['ID'];
                                }

                                break;
                        }

                        $arProperties[$arProperty['ID']] = $arProperty;
                    }

                    if(!empty($arHLTables))
                    {
                        $rsHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::GetList(array(
                            'filter' => array('=TABLE_NAME' => array_keys($arHLTables)),
                            'select' => array('*')
                        ));

                        while($arHLBlock = $rsHLBlock->fetch())
                        {
                            $arProperties[$arHLTables[$arHLBlock['TABLE_NAME']]]['HL_BLOCK'] = $arHLBlock;
                        }
                    }

                    $this->arResult['PROPERTIES'][$check] = $arProperties;
                }

                break;
        }

        $this->obQuery->setSelect($arSelect);
    }

    // ПОДГОТАВЛИВАЕМ FILTER
    protected function onPrepareFilter($check, $arFilter)
    {
        switch($check)
        {
            case 'ITEM':
            case 'OFFER':

                if($check == 'ITEM')
                {
                    if(!empty($GLOBALS[$this->arParams['FILTER_NAME']]))
                    {
                        $arGFilter = $GLOBALS[$this->arParams['FILTER_NAME']];

                        switch($this->arParams['FILTER_NAME'])
                        {
                            case 'arrFilter':

                                if(!empty($arGFilter['OFFERS']))
                                {
                                    $arGFilter['=ID'] = \CIBlockElement::SubQuery(
                                        'PROPERTY_'.$this->iblock['SKU_PROPERTY_ID'],
                                        array_merge(
                                            $arGFilter['OFFERS'],
                                            array(
                                                'IBLOCK_ID' => $this->iblock['SKU_IBLOCK_ID'],
                                                'ACTIVE_DATE' => 'Y',
                                                'ACTIVE' => 'Y',
                                                'CATALOG_AVAILABLE' => 'Y'
                                            )
                                        )
                                    );
                                    unset($arGFilter['OFFERS']);
                                }

                                $rsElements = \CIBlockElement::GetList(
                                    array(),
                                    array_merge($arFilter, $arGFilter),
                                    false,
                                    false,
                                    array('ID')
                                );

                                while($arElement = $rsElements->fetch())
                                {
                                    $arFilter['=ID'][] = $arElement['ID'];
                                }

                                break;

                            default:

                                $arFilter = array_merge($arFilter, $arGFilter);

                                break;
                        }
                    }

                    if($this->arParams['IBLOCK_ID'] > 0)
                    {
                        $arFilter['=IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];

                        if($this->arParams['CHECK_MODULE_CATALOG'] == 'Y')
                        {
                            $arFilter['CATALOG.AVAILABLE'] = 'Y';
                        }

                        if(!empty($this->sections[$this->arParams['IBLOCK_ID']]) && !empty($this->arParams['SECTION_ID']))
                        {
                            $arFilter['=IBLOCK_SECTION_ID'] = array();

                            foreach($this->arParams['SECTION_ID'] as $NID)
                            {
                                if(!empty($this->sections[$this->arParams['IBLOCK_ID']]['SECTIONS'][$NID]['CHILDRENS']))
                                {
                                    $arFilter['=IBLOCK_SECTION_ID'] = array_merge(
                                        explode(';', $this->sections[$this->arParams['IBLOCK_ID']]['SECTIONS'][$NID]['CHILDRENS']),
                                        $arFilter['=IBLOCK_SECTION_ID']
                                    );

                                    continue;
                                }

                                $arFilter['=IBLOCK_SECTION_ID'][] = $NID;
                            }
                        }
                    }
                }

                break;
        }
        $this->obQuery->setFilter($arFilter);
    }

    // КАСТОМИЗИРУЕМ GETLIST
    protected function GetListElements($check, $arSort, $arFilter, $arSelect)
    {
        switch($check)
        {
            case 'ITEM':
            case 'OFFER':

                $this->obQuery = new Entity\Query(ElementTable::getEntity());

                if($check == 'ITEM')
                {
                    $this->obQuery->countTotal(true);

                    // Регистрируем зависимости
                    if(!empty($GLOBALS[$this->arParams['RUNTIME_FIELDS']]))
                    {
                        foreach($GLOBALS[$this->arParams['RUNTIME_FIELDS']] as $field => $arRuntime)
                        {
                            $this->obQuery->registerRuntimeField($field, $arRuntime);
                            $this->arResult['RUNTIME'][$check][$field] = $arRuntime;
                        }
                    }

                    // Группируем по полям
                    if(!empty($GLOBALS[$this->arParams['GROUP_FIELDS']]) && is_array($GLOBALS[$this->arParams['GROUP_FIELDS']]))
                    {
                        $this->obQuery->setGroup($GLOBALS[$this->arParams['GROUP_FIELDS']]);
                        $this->arResult['GROUP'][$check] = $GLOBALS[$this->arParams['GROUP_FIELDS']];
                    }
                }

                break;
        }

        $this->obQuery->setOrder($arSort);
        $this->onPrepareFilter($check, $arFilter);
        $this->onPrepareSelect($check, $arSelect);

        $this->arResult['ORDER'][$check] = $this->obQuery->getOrder();
        $this->arResult['FILTER'][$check] = $this->obQuery->getFilter();
        $this->arResult['SELECT'][$check] = $this->obQuery->getSelect();

        return $this->obQuery->exec();
    }

    // СБОР ДАННЫХ
    protected function getResult()
    {
        $this->sections = IBlock::getSections();
        $ORMIEPTable = ORMFactory::compile('b_iblock_element_property');
        $nItemCount = 0;

        if($this->arParams['IBLOCK_ID'] > 0)
        {
            $this->iblock = IBlock::getIBlock($this->arParams['IBLOCK_ID']);
            $this->arResult['IBLOCK'] = $this->iblock;
        }

        // СОБИРАЕМ ЭЛЕМЕНТЫ
        $rsElements = $this->GetListElements(
            'ITEM',
            array(
                $this->arParams['SORT_FIELD1'] => $this->arParams['SORT_DIRECTION1'],
                $this->arParams['SORT_FIELD2'] => $this->arParams['SORT_DIRECTION2']
            ),
            array(
                'ACTIVE' => 'Y'
            ),
            array(
                'ID',
                'IBLOCK_ID',
                'NAME',
                'CODE',
                'PREVIEW_PICTURE',
                'DETAIL_PICTURE',
                'DATE_CREATE',
                'TIMESTAMP_X',
                'PREVIEW_TEXT',
                'PREVIEW_TEXT_TYPE',
                'DETAIL_TEXT',
                'DETAIL_TEXT_TYPE'
            )
        );

        $this->arResult['TOTAL_ELEMENTS'] = $rsElements->getCount();

        if($this->arParams['SHOW_NAV'] == 'Y' && $this->arParams['COUNT'] > 0 && $this->arResult['TOTAL_ELEMENTS'] > 0)
        {
            $this->nav->setRecordCount($this->arResult['TOTAL_ELEMENTS']);
        }

        while ($arElement = $rsElements->fetch())
        {
            $nItemCount++;
            if($this->arParams['COUNT'] > 0 && $nItemCount > $this->arParams['COUNT'])
            {
                break;
            }

            $this->arResult['ITEMS'][$arElement['ID']] = $arElement;
            $this->arResult['NID_ELEMENTS'][$arElement['ID']] = $arElement['ID'];
        }

        // ЗАПИСЫВАЕМ КОРРЕКТНЫЕ РАЗДЕЛЫ ДЛЯ ЭЛЕМЕНТОВ
        if(!empty($this->arResult['NID_ELEMENTS']))
        {
            $rsESections = \Bitrix\Iblock\SectionElementTable::GetList(
                array(
                    'order' => array('IBLOCK_SECTION_ID' => 'DESC'),
                    'filter' => array('IBLOCK_ELEMENT_ID' => $this->arResult['NID_ELEMENTS']),
                    'select' => array(
                        'SID' => 'IBLOCK_SECTION_ID',
                        'EID' => 'IBLOCK_ELEMENT_ID'
                    )
                )
            );

            while($arESection = $rsESections->fetch())
            {
                $this->arResult['ITEMS'][$arESection['EID']]['IBLOCK_SECTION_ID'][$arESection['SID']] = $arESection['SID'];
                $this->arResult['NID_SECTIONS'][$arESection['SID']] = $arESection['SID'];
            }
        }

        // УСТАНАВЛИВАЕМ ЗНАЧЕНИЯ СВОЙСТВ ЭЛЕМЕНТОВ
        if(!empty($this->arResult['NID_ELEMENTS']) && !empty($this->arResult['PROPERTIES']['ITEM']))
        {
            $rsEProperties = $ORMIEPTable::GetList(
                array(
                    'order' => array('IBLOCK_ELEMENT_ID' => 'ASC'),
                    'filter' => array(
                        '=IBLOCK_ELEMENT_ID' => $this->arResult['NID_ELEMENTS'],
                        '=IBLOCK_PROPERTY_ID' => array_keys($this->arResult['PROPERTIES']['ITEM'])
                    ),
                    'select' => array(
                        'ID',
                        'IBLOCK_ELEMENT_ID',
                        'IBLOCK_PROPERTY_ID',
                        'DESCRIPTION',
                        'VALUE'
                    )
                )
            );

            while($arValue = $rsEProperties->fetch())
            {
                $value = $arValue['VALUE'];

                if(strlen($arValue['DESCRIPTION']) > 0)
                {
                    $value = array(
                        'VALUE' => $arValue['VALUE'],
                        'DESCRIPTION' => $arValue['DESCRIPTION'],
                    );
                }

                $this->arResult['ITEMS'][$arValue['IBLOCK_ELEMENT_ID']]['PROPERTIES'][$arValue['IBLOCK_PROPERTY_ID']][$arValue['ID']] = $value;
                $this->arResult['PROPERTIES']['ITEM'][$arValue['IBLOCK_PROPERTY_ID']]['VALUES'][$arValue['ID']] = $arValue['VALUE'];

                if(in_array($this->arResult['PROPERTIES']['ITEM'][$arValue['IBLOCK_PROPERTY_ID']]['PROPERTY_TYPE'], array('E')))
                {
                    $this->arResult['PROPERTIES']['ITEM'][$arValue['IBLOCK_PROPERTY_ID']]['ELEMENTS'][$arValue['VALUE']][$arValue['IBLOCK_ELEMENT_ID']] = $arValue['IBLOCK_ELEMENT_ID'];
                }
            }
        }

        // ЗАПИСЫВАЕМ ОСТАТКИ НА СКЛАДАХ ЕСЛИ ИНФОБЛОК ЯВЛЯЕТСЯ ТОРГОВЫМ КАТАЛОГОМ И УСТАНАВЛИВАЕМ ЦЕНЫ
        if($this->arParams['CHECK_MODULE_CATALOG'] == 'Y')
        {
            // УСТАНАВЛИВАЕМ ЦЕНЫ
            if(!empty($this->arParams['CATALOG_GROUPS']))
            {
                $rsPrices = PriceTable::GetList(
                    array(
                        'order' => array('PRICE' => 'ASC'),
                        'filter' => array(
                            '=PRODUCT_ID' => $this->arResult['NID_ELEMENTS'],
                            '=CATALOG_GROUP_ID' => $this->arParams['CATALOG_GROUPS']
                        ),
                        'select' => array(
                            'ID',
                            'PRODUCT_ID',
                            'CATALOG_GROUP_ID',
                            'PRICE',
                            'CURRENCY',
                            'TIMESTAMP_X'
                        )
                    )
                );

                while($arPrice = $rsPrices->fetch())
                {
                    $arFields = array(
                        'ID' => intval($arPrice['ID']),
                        'TIMESTAMP_X' => $arPrice['TIMESTAMP_X'],
                        'PRODUCT_ID' => intval($arPrice['PRODUCT_ID']),
                        'IBLOCK_ID' => $this->arResult['ITEMS'][$arPrice['PRODUCT_ID']]['IBLOCK_ID'],
                        'CATALOG_GROUP_ID' => intval($arPrice['CATALOG_GROUP_ID']),
                        'PRICE' => $arPrice['PRICE'],
                        'CURRENCY' => $arPrice['CURRENCY']
                    );

                    if($arFields['DISCOUNT_PRICE'] = $this->getDiscountPrice($arFields))
                    {
                        $this->arResult['ITEMS'][$arPrice['PRODUCT_ID']]['DISCOUNT_PRICES'][$arPrice['PRODUCT_ID']] = floatval($arFields['DISCOUNT_PRICE']);
                    }

                    $this->arResult['ITEMS'][$arPrice['PRODUCT_ID']]['PRICES'][$arPrice['CATALOG_GROUP_ID']][$arPrice['ID']] = $arFields;
                    $this->arResult['ITEMS'][$arPrice['PRODUCT_ID']]['CURRENT_PRICES'][$arPrice['PRODUCT_ID']] = floatval($arPrice['PRICE']);
                }
            }

            // ЗАПИСЫВАЕМ ОСТАТКИ ПО СКЛАДАМ
            $rsStores = StoreProductTable::GetList(
                array(
                    'runtime' => array(
                        'PRICE' => array(
                            'data_type' => '\Bitrix\Catalog\PriceTable',
                            'reference' => array(
                                '=ref.PRODUCT_ID' => 'this.PRODUCT_ID',
                            )
                        )
                    ),
                    'order' => array(
                        'PRICE.PRICE' => 'ASC',
                        'PRODUCT_ID' => "ASC"
                    ),
                    'filter' => array(
                        '=STORE.ACTIVE' => 'Y',
                        '=PRODUCT_ID' => $this->arResult['NID_ELEMENTS'],
                        '=PRICE.CATALOG_GROUP_ID' => $this->arParams['CATALOG_GROUPS'],
                        '>AMOUNT' => 0
                    ),
                    'select' => array(
                        'ID',
                        'PRODUCT_ID',
                        'STORE_ID',
                        'AMOUNT'
                    )
                )
            );

            while($arStore = $rsStores->fetch())
            {
                $this->arResult['ITEMS'][$arStore['PRODUCT_ID']]['STORES'][$arStore['STORE_ID']][$arStore['ID']] = array(
                    'ID' => intval($arStore['ID']),
                    'STORE_ID' => intval($arStore['STORE_ID']),
                    'AMOUNT' => intval($arStore['AMOUNT'])
                );

                $this->arResult['ITEMS'][$arStore['PRODUCT_ID']]['AMOUNTS'][$arStore['STORE_ID']] += $arStore['AMOUNT'];
            }

            // ЗАПИСЫВАЕМ ОСТАТКИ КАК ЕСТЬ ИЗ КАТАЛОГА ТОВАРОВ
            $rsProduct = ProductTable::GetList(
                array(
                    'filter' => array(
                        '=ID' => $this->arResult['NID_ELEMENTS'],
                    ),
                    'select' => array(
                        'ID',
                        'QUANTITY'
                    )
                )
            );

            while($arProduct = $rsProduct->fetch())
            {
                $this->arResult['ITEMS'][$arProduct['ID']]['TOTAL'] = $arProduct['QUANTITY'];
            }

            // ПРОДОЛЖАЕМ РАБОТУ С ТОРГОВЫМИ ПРЕДЛОЖЕНИЯМИ
            if(intval($this->iblock['SKU_PROPERTY_ID']))
            {
                $arProducts = array();
                $rsEProperties = $ORMIEPTable::GetList(
                    array(
                        'order' => array('VALUE' => 'ASC'),
                        'filter' => array(
                            '=IBLOCK_PROPERTY_ID' => $this->iblock['SKU_PROPERTY_ID'],
                            '=VALUE' => $this->arResult['NID_ELEMENTS']
                        ),
                        'select' => array('VALUE', 'IBLOCK_ELEMENT_ID')
                    )
                );

                while($SKU = $rsEProperties->fetch())
                {
                    $this->arResult['NID_OFFERS'][$SKU['IBLOCK_ELEMENT_ID']] = $SKU['IBLOCK_ELEMENT_ID'];
                    $arProducts[$SKU['IBLOCK_ELEMENT_ID']] = $SKU['VALUE'];
                }

                // ДОСТАЕМ ТОРГОВЫЕ ПРЕДЛОЖЕНИЯ
                if(!empty($this->arResult['NID_OFFERS']))
                {
                    $rsOffers = $this->GetListElements(
                        'OFFER',
                        array(
                            $this->arParams['OFFER_SORT_FIELD'] => $this->arParams['OFFER_SORT_DIRECTION'],
                            'ID' => 'ASC'
                        ),
                        array(
                            'ACTIVE' => 'Y',
                            '=ID' => $this->arResult['NID_OFFERS']
                        ),
                        array(
                            'ID',
                            'IBLOCK_ID',
                            'NAME',
                            'CODE',
                            'DETAIL_PICTURE',
                            'PREVIEW_PICTURE'
                        )
                    );

                    while($arOffer = $rsOffers->fetch())
                    {
                        $this->arResult['ITEMS'][$arProducts[$arOffer['ID']]]['OFFERS'][$arOffer['ID']] = $arOffer;
                    }

                    // УСТАНАВЛИВАЕМ ЗНАЧЕНИЯ СВОЙСТВ ТОРГОВЫХ ПРЕДЛОЖЕНИЙ
                    if(!empty($this->arResult['PROPERTIES']['OFFER']))
                    {
                        $rsEProperties = $ORMIEPTable::GetList(
                            array(
                                'order' => array('IBLOCK_ELEMENT_ID' => 'ASC'),
                                'filter' => array(
                                    '=IBLOCK_ELEMENT_ID' => $this->arResult['NID_OFFERS'],
                                    '=IBLOCK_PROPERTY_ID' => array_keys($this->arResult['PROPERTIES']['OFFER'])
                                ),
                                'select' => array(
                                    'ID',
                                    'NID' => 'IBLOCK_ELEMENT_ID',
                                    'PID' => 'IBLOCK_PROPERTY_ID',
                                    'VALUE'
                                )
                            )
                        );

                        while($arValue = $rsEProperties->fetch())
                        {
                            $SKU_ID = $arProducts[$arValue['NID']];

                            // ТОРГОВОЕ ПРЕДЛОЖЕНИЕ НЕ АКТИВНО
                            if(empty($this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arValue['NID']]))
                            {
                                continue;
                            }

                            $this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arValue['NID']]['PROPERTIES'][$arValue['PID']][$arValue['ID']] = $arValue['VALUE'];
                            $this->arResult['PROPERTIES']['OFFER'][$arValue['PID']]['VALUES'][$arValue['ID']] = $arValue['VALUE'];

                            // ЗАВИСИМА ЦЕНА ОТ СВОЙСТВА
                            if(in_array($arValue['PID'], $this->arParams['RELATION_PROPS']))
                            {
                                $value = $this->arResult['ITEMS'][$SKU_ID]['OFFER_PROPERTIES'][$arValue['PID']][$arValue['NID']];

                                if(!empty($value)) // ЕСЛИ СВОЙСТВО МНОЖЕСТВЕННОЕ
                                {
                                    $arValue['VALUE'] = $value.';'.$arValue['VALUE'];
                                }

                                // УСТАНАВЛИВАЕМ СВОЙСТВА ТОРГОВЫХ ПРЕДЛОЖЕНИЙ ДЛЯ СВЯЗКИ С ТОВАРАМИ
                                $this->arResult['ITEMS'][$SKU_ID]['OFFER_PROPERTIES'][$arValue['PID']][$arValue['NID']] = $arValue['VALUE'];

                                // УСТАНАВЛИВАЕМ СВОЙСТВА ТОРГОВЫХ ПРЕДЛОЖЕНИЙ ДЛЯ ОПРЕДЕЛЕНИЯ В JS
                                if(
                                    $this->arResult['PROPERTIES']['OFFER'][$arValue['PID']]['PROPERTY_TYPE'] == 'L' ||
                                    $this->arResult['PROPERTIES']['OFFER'][$arValue['PID']]['USER_TYPE'] == 'directory'
                                )
                                {
                                    $this->arResult['ITEMS'][$SKU_ID]['OFFERS_TO_JS'][$arValue['NID']][$arValue['PID']] = $arValue['VALUE'];
                                }
                            }
                        }
                    }

                    // УСТАНАВЛИВАЕМ ЦЕНЫ ДЛЯ ТОРГОВЫХ ПРЕДЛОЖЕНИЙ
                    if(!empty($this->arParams['CATALOG_GROUPS']))
                    {
                        $rsPrices = PriceTable::GetList(
                            array(
                                'order' => array('PRICE' => 'ASC'),
                                'filter' => array(
                                    '=PRODUCT_ID' => $this->arResult['NID_OFFERS'],
                                    '=CATALOG_GROUP_ID' => $this->arParams['CATALOG_GROUPS']
                                ),
                                'select' => array(
                                    'ID',
                                    'PRODUCT_ID',
                                    'CATALOG_GROUP_ID',
                                    'PRICE',
                                    'CURRENCY',
                                    'TIMESTAMP_X'
                                )
                            )
                        );

                        while($arPrice = $rsPrices->fetch())
                        {
                            $SKU_ID = $arProducts[$arPrice['PRODUCT_ID']];

                            // ТОРГОВОЕ ПРЕДЛОЖЕНИЕ НЕ АКТИВНО
                            if(empty($this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arPrice['PRODUCT_ID']]))
                            {
                                continue;
                            }

                            $arFields = array(
                                'ID' => intval($arPrice['ID']),
                                'TIMESTAMP_X' => $arPrice['TIMESTAMP_X'],
                                'PRODUCT_ID' => intval($arPrice['PRODUCT_ID']),
                                'IBLOCK_ID' => $this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arPrice['PRODUCT_ID']]['IBLOCK'],
                                'CATALOG_GROUP_ID' => intval($arPrice['CATALOG_GROUP_ID']),
                                'PRICE' => $arPrice['PRICE'],
                                'CURRENCY' => $arPrice['CURRENCY']
                            );

                            if($arFields['DISCOUNT_PRICE'] = $this->getDiscountPrice($arFields))
                            {
                                $this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arPrice['PRODUCT_ID']]['DISCOUNT_PRICES'][$arPrice['PRODUCT_ID']] = floatval($arFields['DISCOUNT_PRICE']);
                            }

                            $this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arPrice['PRODUCT_ID']]["PRICES"][$arPrice['CATALOG_GROUP_ID']][$arPrice['ID']] = $arFields;
                            $this->arResult['ITEMS'][$SKU_ID]['OFFERS'][$arPrice['PRODUCT_ID']]["CURRENT_PRICES"][$arPrice['PRODUCT_ID']] = floatval($arPrice['PRICE']);

                            $this->arResult['ITEMS'][$SKU_ID]['PRICES'][$arPrice['CATALOG_GROUP_ID']][$arPrice['ID']] = $arFields;
                            $this->arResult['ITEMS'][$SKU_ID]['CURRENT_PRICES'][$arPrice['PRODUCT_ID']] = floatval($arPrice['PRICE']);
                        }
                    }

                    // ЗАПИСЫВАЕМ ОСТАТКИ
                    $rsStores = StoreProductTable::GetList(
                        array(
                            'runtime' => array(
                                'PRICE' => array(
                                    'data_type' => '\Bitrix\Catalog\PriceTable',
                                    'reference' => array(
                                        '=ref.PRODUCT_ID' => 'this.PRODUCT_ID',
                                    )
                                )
                            ),
                            'order' => array(
                                'PRICE.PRICE' => 'ASC',
                                'PRODUCT_ID' => "ASC"
                            ),
                            'filter' => array(
                                '=STORE.ACTIVE' => 'Y',
                                '=PRODUCT_ID' => $this->arResult['NID_OFFERS'],
                                '=PRICE.CATALOG_GROUP_ID' => $this->arParams['CATALOG_GROUPS'],
                                '>AMOUNT' => 0
                            ),
                            'select' => array(
                                'ID',
                                'PRODUCT_ID',
                                'STORE_ID',
                                'AMOUNT'
                            )
                        )
                    );

                    while($arStore = $rsStores->fetch())
                    {
                        // ТОРГОВОЕ ПРЕДЛОЖЕНИЕ НЕ АКТИВНО
                        if(empty($this->arResult['ITEMS'][$arProducts[$arStore['PRODUCT_ID']]]['OFFERS'][$arStore['PRODUCT_ID']]))
                        {
                            continue;
                        }

                        $this->arResult['ITEMS'][$arProducts[$arStore['PRODUCT_ID']]]['OFFERS'][$arStore['PRODUCT_ID']]['STORES'][$arStore['STORE_ID']][$arStore['ID']] = array(
                            'ID' => intval($arStore['ID']),
                            'STORE_ID' => intval($arStore['STORE_ID']),
                            'AMOUNT' => intval($arStore['AMOUNT'])
                        );

                        // ОСТАТКИ ТОРГОВЫЙ ПРЕДЛОЖЕНИЙ
                        $this->arResult['ITEMS'][$arProducts[$arStore['PRODUCT_ID']]]['OFFERS'][$arStore['PRODUCT_ID']]['AMOUNTS'][$arStore['STORE_ID']] += $arStore['AMOUNT'];
                        $this->arResult['ITEMS'][$arProducts[$arStore['PRODUCT_ID']]]['CURRENT_STORES'][$arStore['STORE_ID']][$arStore['PRODUCT_ID']] += $arStore['AMOUNT'];

                        // ОБЩИЕ ОСТАТКИ ТОВАРА
                        $this->arResult['ITEMS'][$arProducts[$arStore['PRODUCT_ID']]]['AMOUNTS'][$arStore['STORE_ID']] += $arStore['AMOUNT'];
                    }

                    // ЗАПИСЫВАЕМ ОСТАТКИ КАК ЕСТЬ ИЗ КАТАЛОГА ТОВАРОВ
                    $rsProduct = ProductTable::GetList(
                        array(
                            'filter' => array(
                                '=ID' => $this->arResult['NID_OFFERS'],
                            ),
                            'select' => array(
                                'ID',
                                'QUANTITY'
                            )
                        )
                    );

                    while($arProduct = $rsProduct->fetch())
                    {
                        $this->arResult['ITEMS'][$arProducts[$arProduct['ID']]]['OFFERS'][$arProduct['ID']]['TOTAL'] = $arProduct['QUANTITY'];
                        $this->arResult['ITEMS'][$arProducts[$arProduct['ID']]]['TOTAL'] += $arProduct['QUANTITY'];
                    }
                }
            }
        }


        // ПОЛУЧАЕМ ВСЕ ОПИСАНИЯ ЗНАЧЕНИЙ СВОЙСТВ ТОВАРОВ И ТОРГОВЫХ ПРЕДЛОЖЕНИЙ
        if (!empty($this->arResult['PROPERTIES']))
        {
            foreach ($this->arResult['PROPERTIES'] as $check => &$arProps)
            {
                switch($check)
                {
                    case 'ITEM':
                    case 'OFFER':

                        foreach($arProps as $NID => &$arProp)
                        {
                            if(empty($arProp['VALUES']) || !is_array($arProp['VALUES']))
                            {
                                continue;
                            }

                            $arProp = $this->setValueProperties($arProp);
                        }

                        break;
                }
            }
        }

        // ПОСТРАНИЧКА
        if ($this->arParams['SHOW_NAV'] == 'Y' && $this->arParams['COUNT'] > 0 && $this->arResult['TOTAL_ELEMENTS'] > 0)
        {
            ob_start();

            App::getInstance()->IncludeComponent(
                "bitrix:main.pagenavigation",
                "modern",
                array(
                    "NAV_OBJECT" => $this->nav,
                    "SEF_MODE" => "Y",
                    "SHOW_COUNT" => "N",
                    "SHOW_ALL" => "N"
                ),
                false
            );

            $this->arResult['NAV_STRING'] = @ob_get_clean();
            $this->arResult['NAV_OBJECT'] = $this->nav;
        }
    }

    public function getDiscountPrice(&$arFields)
    {
        if(
            !intval($arFields['PRODUCT_ID']) ||
            !intval($arFields['IBLOCK_ID']) ||
            !intval($arFields['CATALOG_GROUP_ID']) ||
            !floatval($arFields['PRICE']) ||
            empty($arFields['CURRENCY'])
        ){
            return false;
        }

        if(\Bitrix\Catalog\Product\Price\Calculation::isAllowedUseDiscounts())
        {
            $arDiscounts = \CCatalogDiscount::GetDiscount(
                $arFields['PRODUCT_ID'],
                $arFields['IBLOCK_ID'],
                $arFields['CATALOG_GROUP_ID'],
                User::getInstance()->GetUserGroupArray(),
                'N',
                false,
                array()
            );

            if(!empty($arDiscounts))
            {
                $arFields['DISCOUNT'] = \CCatalogDiscount::applyDiscountList(
                    $arFields['PRICE'],
                    $arFields['CURRENCY'],
                    $arDiscounts
                );
            }
        }

        return !empty($arFields['DISCOUNT']['DISCOUNT_LIST']) ? $arFields['DISCOUNT']['PRICE'] : false;
    }

    public function setValueProperties($arProp)
    {
        switch($arProp['PROPERTY_TYPE'])
        {
            case 'L':

                $rsElementEnumProperties = \Bitrix\Iblock\PropertyEnumerationTable::getList(
                    array(
                        'order' => array('VALUE' => 'ASC'),
                        'filter' => array(
                            '=ID' => array_unique($arProp['VALUES']),
                            '=PROPERTY_ID' => $arProp['ID']
                        )
                    )
                );

                while($arElementEnumProperty = $rsElementEnumProperties->fetch())
                {
                    $arProp['DISPLAY_VALUES'][$arElementEnumProperty['ID']] = $arElementEnumProperty['VALUE'];
                    $arProp['DISPLAY_VALUES_XML_ID'][$arElementEnumProperty['ID']] = $arElementEnumProperty['XML_ID'];
                }

                break;

            case 'S':

                switch($arProp['USER_TYPE'])
                {
                    case 'HTML':

                        foreach($arProp['VALUES'] as $NID => $value)
                        {
                            $arProp['DISPLAY_VALUES'][$NID] = unserialize($value);
                        }

                        break;

                    case 'directory':

                        if(!empty($arProp['HL_BLOCK']))
                        {
                            $HLDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arProp['HL_BLOCK'])->getDataClass();

                            if (class_exists($HLDataClass))
                            {
                                $rsHLValues = $HLDataClass::GetList(array(
                                    'order' => array('ID' => 'ASC'),
                                    'filter' => array(
                                        '=UF_XML_ID' => array_unique($arProp['VALUES'])
                                    ),
                                    'select' => array('*')
                                ));

                                while ($arHLValue = $rsHLValues->fetch())
                                {
                                    $arProp['DISPLAY_VALUES_ROW_ID'][$arHLValue['UF_XML_ID']] = $arHLValue['ID'];
                                    $arProp['DISPLAY_VALUES_XML_ID'][$arHLValue['UF_XML_ID']] = $arHLValue['UF_XML_ID'];
                                    $arProp['DISPLAY_VALUES'][$arHLValue['UF_XML_ID']] = $arHLValue;
                                }
                            }
                        }

                        break;

                    default:

                        $ORMIEPTable = ORMFactory::compile('b_iblock_element_property');
                        $rsEPTable = $ORMIEPTable::GetList(array(
                            'filter' => array(
                                '=ID' => array_keys($arProp['VALUES'])
                            ),
                            'select' => array(
                                'ID',
                                'IBLOCK_ELEMENT_ID',
                                'VALUE',
                                'DESCRIPTION'
                            )
                        ));

                        while($arEPTable = $rsEPTable->fetch())
                        {
                            $arProp['DISPLAY_VALUES'][$arEPTable['IBLOCK_ELEMENT_ID']][$arEPTable['ID']] = $arEPTable;

                            if($arProp['CODE'] == 'CML2_ATTRIBUTES' && !empty($arEPTable['DESCRIPTION']))
                            {
                                $arProp['ATTRIBUTES_VALUES'][$arEPTable['IBLOCK_ELEMENT_ID']][$arEPTable['DESCRIPTION']] = $arEPTable;
                            }
                        }

                        break;
                }

                break;

            case 'F':

                foreach($arProp['VALUES'] as $NID => $value)
                {
                    $NIDFile = intval($value);
                    $path = CFile::GetPath($NIDFile);

                    if(!is_file(DOCUMENT_ROOT.$path))
                        continue;

                    $arProp['DISPLAY_VALUES'][$NIDFile]['PROPERTY_VALUE_ID'] = $NID;
                    $arProp['DISPLAY_VALUES'][$NIDFile]['PATH'] = $path;
                }

                break;

            case 'E':

                $arSelect = array(
                    'ID',
                    'IBLOCK_ID',
                    'NAME',
                    'CODE',
                    'DETAIL_TEXT',
                    'PREVIEW_TEXT',
                    'IBLOCK_SECTION_ID',
                    'PREVIEW_PICTURE',
                    'DETAIL_PICTURE'
                );

                $arProp['LINK_IBLOCK'] = IBlock::getIBlock($arProp['LINK_IBLOCK_ID']);
                foreach($arProp['LINK_IBLOCK']['PROPERTIES'] as $arProperty)
                {
                    if($arProperty)
                        $arSelect[] = 'IBLOCK_'.$arProp['LINK_IBLOCK_ID'].'_PROPERTY_'.$arProperty['CODE'].'.VALUE';
                }

                $rsElements = ElementTable::GetList(
                    array(
                        'order' => array('ID' => 'ASC'),
                        'filter' => array(
                            '=ID' => $arProp['VALUES'],
                            '=IBLOCK_ID' => $arProp['LINK_IBLOCK_ID']
                        ),
                        'select' => $arSelect
                    )
                );

                while($arElement = $rsElements->fetch())
                {
                    $arProp['DISPLAY_VALUES'][$arElement['ID']] = $arElement;
                }

                break;
        }

        return $arProp;
    }

    // ИНИЦИАЛИЗАЦИЯ КОМПОНЕНТЫ
    public function executeComponent()
    {
        try
        {
            $this->checkAutomaticParams();
            $this->executeProlog();

            if ($this->readDataFromCache())
            {
                $this->getResult();
                $this->putDataToCache();
                $this->includeComponentTemplate();
            }

            if($this->_return)
            {
                // For example. In template.php set $this->__component->_return = $variable; and return false;
                return $this->_return;
            }
        }
        catch (Exception $e)
        {
            $this->AbortResultCache();
            \Sepro\Log::add2log($e->getMessage());
            echo $e->getMessage();
        }

        return true;
    }
}
