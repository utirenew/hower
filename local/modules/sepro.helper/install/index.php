<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\IO\Directory,
    Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

class sepro_helper extends CModule
{
    var $MODULE_ID = 'sepro.helper';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    public $errors = array();

    function __construct()
    {
        /** @var array $arModuleVersion */
        include(__DIR__ . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage("SEPRO_HELPER_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("SEPRO_HELPER_MODULE_DESC");
        $this->PARTNER_NAME = Loc::getMessage("SEPRO_HELPER_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("SEPRO_HELPER_PARTNER_URI");
    }

    public function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        $eventManager = EventManager::getInstance();

        $eventManager->registerEventHandlerCompatible('main', 'OnBeforeProlog', $this->MODULE_ID, '\Sepro\Events', 'OnBeforePrologHandler');
        $eventManager->registerEventHandlerCompatible('main', 'OnAfterUserAuthorize', $this->MODULE_ID, '\Sepro\Events', 'OnAfterUserAuthorizeHandler');
        $eventManager->registerEventHandlerCompatible('main', 'OnEpilog', $this->MODULE_ID, '\Sepro\Events', '_Check404Error');
        $eventManager->registerEventHandlerCompatible('main', 'OnEndBufferContent', $this->MODULE_ID, '\Sepro\Events', 'OnEndBufferContentHandler');

        $eventManager->registerEventHandlerCompatible('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Sepro\Properties\UserTypeTags', 'GetUserTypeDescription');

        $eventManager->registerEventHandlerCompatible('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, '\Sepro\Events', 'OnBeforeIBlockElementAddHandler');
        $eventManager->registerEventHandlerCompatible('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Sepro\Events', 'OnAfterIBlockElementAddHandler');
        $eventManager->registerEventHandlerCompatible('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID, '\Sepro\Events', 'OnBeforeIBlockElementUpdateHandler');
        $eventManager->registerEventHandlerCompatible('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, '\Sepro\Events', 'OnAfterIBlockElementUpdateHandler');
        $eventManager->registerEventHandlerCompatible('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID, '\Sepro\Events', 'OnBeforeIBlockElementDeleteHandler');

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/php_interface", $_SERVER["DOCUMENT_ROOT"]."/local/php_interface", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/components", $_SERVER["DOCUMENT_ROOT"]."/local/components", true, true);

        return true;
    }

    public function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);
        $eventManager = EventManager::getInstance();

        $eventManager->unRegisterEventHandler('main', 'OnBeforeProlog', $this->MODULE_ID, '\Sepro\Events', 'OnBeforePrologHandler');
	    $eventManager->unRegisterEventHandler('main', 'OnAfterUserAuthorize', $this->MODULE_ID, '\Sepro\Events', 'OnAfterUserAuthorizeHandler');
        $eventManager->unRegisterEventHandler('main', 'OnEpilog', $this->MODULE_ID, '\Sepro\Events', '_Check404Error');
        $eventManager->unRegisterEventHandler('main', 'OnEndBufferContent', $this->MODULE_ID, '\Sepro\Events', 'OnEndBufferContentHandler');

        $eventManager->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID, '\Sepro\Properties\UserTypeTags', 'GetUserTypeDescription');

        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, '\Sepro\Events', 'OnBeforeIBlockElementAddHandler');
        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, '\Sepro\Events', 'OnAfterIBlockElementAddHandler');
        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID, '\Sepro\Events', 'OnBeforeIBlockElementUpdateHandler');
        $eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, '\Sepro\Events', 'OnAfterIBlockElementUpdateHandler');
        $eventManager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID, '\Sepro\Events', 'OnBeforeIBlockElementDeleteHandler');

        Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"]."/local/components/sepro");

        return true;
    }

    private function ShowErrors()
    {
        if(empty($this->errors))
        {
            return false;
        }

        $keys = array_keys($GLOBALS);
        for($i=0, $intCount = count($keys); $i < $intCount; $i++) {
            if($keys[$i]!='i' && $keys[$i]!='GLOBALS' && $keys[$i]!='strTitle' && $keys[$i]!='filepath') {
                global ${$keys[$i]};
            }
        }

        $PathInstall = str_replace('\\', '/', __FILE__);
        $PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen('/index.php'));
        IncludeModuleLangFile($PathInstall.'/install.php');

        $APPLICATION->SetTitle('Sepro helper');
        include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');
        echo CAdminMessage::ShowMessage(array(
            'MESSAGE' => 'Ошибка установки',
            'TYPE' => 'ERROR',
            'DETAILS' => implode('<br>', $this->errors)
        ));
        ?>
        <form action="<?=$APPLICATION->GetCurPage();?>" method="get">
            <p>
                <input type="hidden" name="lang" value="<?=LANGUAGE_ID;?>"/>
                <input type="submit" value="<?=Loc::getMessage('MOD_BACK');?>"/>
            </p>
        </form>
        <?include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
        die();
    }
}
?>