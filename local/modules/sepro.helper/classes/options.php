<?
use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Application,
    \Sepro\ORMFactory,
    \Sepro\User,
    \Sepro\Form,
    \Sepro\DOM,
    \Sepro\App;

class CModuleOptions
{
    private $mid; // SEPRO.HELPER
    private $arCUROptions = array();
    private $arOptions = array();
    private $arFields = array();
    private $arGroups = array();

    private static $AExist = false;

    public final function __construct($mid, $arFields)
    {
        $this->mid = $mid;

        $ORMOption = ORMFactory::compile('b_option');

        $rsOptions = $ORMOption::GetList(array(
            'filter' => array(
                '=MODULE_ID' => $this->mid
            ),
            'select' => array(
                'NAME',
                'VALUE'
            )
        ));

        while($arOption = $rsOptions->fetch())
        {
            $this->arCUROptions[$arOption['NAME']] = $arOption['VALUE'];
        }

        foreach($arFields as $arTab)
        {
            $this->arFields[$arTab['DIV']] = array(
                "DIV" => $arTab['DIV'],
                "TYPE" => $arTab['TYPE'],
                "TAB" => $arTab['TAB'],
                "TITLE" => $arTab['TITLE']
            );

            if($arTab['TYPE'] !== 'SPECIAL')
            {
                foreach($arTab['GROUPS'] as &$arGroup)
                {
                    foreach($arGroup['OPTIONS'] as $name => &$arOption)
                    {
                        $this->arOptions[$name] = $arOption['VALUE'];
                    }
                }
            }

            $this->arGroups[$arTab['DIV']] = $arTab['GROUPS'];
        }
    }

    public function saveOptions()
    {
        $request = User::getRequest();

        if($request->get('update') !== 'Y') return false;

        $connection = Application::getConnection();
        $helper = $connection->getSqlHelper();
        $delete = $helper->prepareAssignment('b_option', 'MODULE_ID', $this->mid);

        $connection->queryExecute("DELETE FROM ".$helper->quote("b_option")." WHERE ".$delete);

        foreach($this->arOptions as $name => $value)
        {
            if(empty($request->get($name))) continue;

            Option::Set($this->mid, $name, $request->get($name));

            $this->arCUROptions[$name] = $request->get($name);
        }

        return true;
    }

    public function showHTML()
    {
        if(!static::$AExist)
        {
            \CJSCore::RegisterExt('options', array('js' => SEPRO_TOOLS_JS.'step_by_step.js'));
            \CJSCore::Init(array('jquery', 'options'));

            static::$AExist = true;
        }

        foreach($this->arFields as $arTab)
        {
            $obTab = new CAdminTabControl($arTab['DIV'], array($arTab));
            $obTab->Begin();

            $arForm = array('id' => $arTab['DIV'].'_form');

            if($arTab['TYPE'] == 'SPECIAL')
            {
                $arForm['onsubmit'] = 'Sepro.ajaxProgress(this); return false;';
            }

            echo Form::start(
                App::getInstance()->GetCurPageParam(
                    'mid='.htmlspecialchars($this->mid).'&lang='.LANGUAGE_ID.'&mid_menu=1',
                    array('mid', 'lang', 'mid_menu')
                ), 'post', $arForm).Form::input('hidden', 'sessid', bitrix_sessid());

            $obTab->BeginNextTab();

            foreach($this->arGroups[$arTab['DIV']] as $arGroup)
            {
                if(strlen($arGroup['TITLE']) > 0)
                {
                    echo DOM::addContainer(
                        'tr',
                        DOM::addContainer(
                            'td',
                            $arGroup['TITLE'],
                            array('colspan' => 2)
                        ),
                        array('class' => 'heading')
                    );
                }

                foreach($arGroup['OPTIONS'] as $name => $arOption)
                {
                    $input = '';
                    $attr = array(
                        'PLACEHOLDER' => strlen($arOption['PLACEHOLDER']) > 0 ? htmlspecialchars($arOption['PLACEHOLDER']) : ''
                    );

                    if(!empty($this->arCUROptions[$name]))
                    {
                        $arOption['VALUE'] = $this->arCUROptions[$name];
                    }

                    switch($arOption['TYPE'])
                    {
                        case 'INTEGER':
                        case 'TEXT':

                            $attr = array_merge($attr, array(
                                'size' => 50,
                                'maxlength' => 255
                            ));

                            $input = Form::input(
                                $arOption['TYPE'] == 'INTEGER' ? 'number' : 'text',
                                $name,
                                $arOption['VALUE'],
                                false,
                                $attr
                            );

                            break;

                        case 'CHECKBOX':

                            if($arOption['VALUE'] == 'Y')
                            {
                                $attr['checked'] = 'checked';
                            }

                            $input = Form::input(
                                'checkbox',
                                $name,
                                $arOption['VALUE'],
                                false,
                                $attr
                            );

                            break;
                        case 'SELECT':

                            $input = Form::select($name, $arOption['OPTIONS'], $arOption['VALUE']);

                            break;

                        case 'TEXTAREA':

                            $attr = array_merge($attr, array(
                                'name' => $name,
                                'cols' => 70,
                                'rows' => 7
                            ));

                            $input = DOM::addContainer('textarea', $arOption['VALUE'], $attr);

                            break;
                    }

                    $row = DOM::addContainer('tr', '#OPTION#');
                    $title = false;
                    $arCVFields = array(
                        'valign' => 'top',
                        'nowrap' => ''
                    );

                    if(strlen($arOption['TITLE']) > 0)
                    {
                        $title = DOM::addContainer(
                            'td',
                            $arOption['TITLE'],
                            array(
                                'valign' => 'middle',
                                'width' => '50%'
                            )
                        );
                    }
                    else
                    {
                        $arCVFields['colspan'] = 2;
                        $arCVFields['align'] = 'center';
                    }

                    $colValue = DOM::addContainer('td', $input, $arCVFields);

                    echo str_replace('#OPTION#', $title.$colValue, $row);
                }
            }

            $obTab->Buttons();

            $submit = 'Сохранить';

            switch($arTab['TYPE'])
            {
                case 'SPECIAL':

                    $submit = "Запустить";

                    break;

                case 'SETTING':

                    echo Form::input('hidden', 'update', 'Y');

                    break;
            }

            echo Form::input('submit', 'submit', $submit);

            if($arTab['TYPE'] == 'SPECIAL')
            {
                echo Form::input(
                    'button',
                    'abort',
                    'Сбросить',
                    false,
                    array(
                        'disabled' => 'disabled',
                        'class' => 'js-abort',
                        'onclick' => 'Sepro.abortProgress(this);'
                    )
                );

                echo Form::input('hidden', 'DIV', $arTab['DIV']);
            }

            echo Form::end();

            $obTab->End();

            echo DOM::addLoner('br');
        }
    }
}