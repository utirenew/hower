<?
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
$SERVER_NAME = $_SERVER['SERVER_NAME'];
$RESULT_LANG = 'ru';

if(empty($DOCUMENT_ROOT))
{
    $DOCUMENT_ROOT = realpath(dirname(__FILE__).'/../../..');
    $_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT;
}

if(empty($SERVER_NAME))
{
    $SERVER_NAME = \Bitrix\Main\Config\Option::get('main', 'server_name', gethostname(), 's1');
}

define('DOCUMENT_ROOT', $DOCUMENT_ROOT);
define('LOG_FILENAME', DOCUMENT_ROOT.'/system_log.txt');
define("SYSTEM_LOG", LOG_FILENAME);
define('SITE_NAME', $SERVER_NAME);
define('LANGUAGE_ID', $RESULT_LANG);
define('EMAIL_ADMIN', 'egor.baklach@gmail.com');

define('SEPRO_TOOLS_JS', '/local/modules/sepro.helper/tools/js/');
define('SEPRO_TOOLS_CSS', '/local/modules/sepro.helper/tools/css/');

\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::IncludeModule("pull");

\Bitrix\Main\Loader::registerAutoLoadClasses(
    'sepro.helper',
    array(
        // CLASSES
        '\Sepro\App' => 'lib/app.php',
        '\Sepro\ECommerce' => 'lib/commerce.php',
        '\Sepro\Payment' => 'lib/payment.php',
        '\Sepro\User' => 'lib/user.php',
        '\Sepro\IBlock' => 'lib/iblock.php',
        '\Sepro\Basket' => 'lib/basket.php',
        '\Sepro\Log' => 'lib/log.php',
        '\Sepro\Events' => 'lib/events.php',
        '\Sepro\Helpers' => 'lib/helpers.php',
        '\Sepro\Binary' => 'lib/binary.php',
        '\Sepro\Dom' => 'lib/dom.php',
        '\Sepro\Form' => 'lib/form.php',
        '\Sepro\Socialnetwork' => 'lib/socialnetwork.php',

        // ORM FACTORY AND TABLE ELEMENT
        '\Sepro\ORMFactory' => 'lib/orm.php',
        '\Sepro\ElementTable' => 'lib/element.php',

        // TRAITS
        '\Sepro\Traits\IBlockElementEvents' => 'lib/traits/iblockelementevents.php',
        '\Sepro\Traits\SystemEvents' => 'lib/traits/systemevents.php',
        '\Sepro\Traits\BufferEvents' => 'lib/traits/bufferevents.php',
        '\Sepro\Traits\AjaxEvents' => 'lib/traits/ajaxevents.php',

        // REST
        '\Sepro\Rest\Client' => 'lib/rest/client.php',
        '\Sepro\Rest\Request' => 'lib/rest/request.php',
        '\Sepro\Rest\Response' => 'lib/rest/response.php',

        // USER PROPERTIES
        '\Sepro\Properties\UserTypeTags' => 'lib/properties/usertypetags.php',

        // IMPORT
        '\Sepro\Import\Autoload' => 'lib/import/autoload.php',

        // OPTIONS
        'CModuleOptions' => 'classes/options.php'
    )
);