Sepro = window.Sepro || {asynchrony: false};
Sepro.xhr = false;

Sepro.abortProgress = function(btn)
{
    let $this = $(btn);

    if($this.hasClass('active'))
    {
        $this.attr('disabled', 'disabled');
        $this.removeClass('active');

        CloseWaitWindow();
    }

    Sepro.xhr.abort();
};

Sepro.ajaxProgress = function(request)
{
    let data = request || {};

    if(request instanceof HTMLElement)
    {
        let $form = $(request),
            query = $form.serializeArray(),
            obj = {};

        for (let i in query)
        {
            obj[query[i]['name']] = query[i]['value'];
        }

        data = obj;
    }

    let $special = $('#'+data.DIV+'_layout'),
        $abort = $special.find('.js-abort'),
        $errors = $('.adm-info-message-red'),
        $progress = $special.next('.js-progress'),
        counter = data.COUNTER || 0;

    ShowWaitWindow();

    if(!$abort.hasClass('active'))
    {
        $abort.attr('disabled', false);
        $abort.addClass('active');
    }

    if($errors.length > 0)
    {
        $errors.remove();
    }

    if(data.hasOwnProperty('HTML'))
    {
        if(data.hasOwnProperty('FINISH'))
        {
            $special.before(data.HTML);
            $progress.remove();

            $abort.removeClass('active');
            $abort.attr('disabled', 'disabled');

            Sepro.xhr.abort();
            CloseWaitWindow();

            return false;
        }

        if($progress.length > 0)
        {
            $progress.html(data.HTML);
        }
        else
        {
            let OPBar = document.createElement('div');

            OPBar.className = 'js-progress';
            OPBar.innerHTML = data.HTML;

            $special.after(OPBar);
        }
    }

    console.log(counter);

    Sepro.xhr = $.ajax({
        data: data,
        type: 'post',
        dataType: 'json',
        success: Sepro.ajaxProgress,
        error: function(xhr, st)
        {
            console.log(xhr.responseCode);
        }
    });

    return false;
};