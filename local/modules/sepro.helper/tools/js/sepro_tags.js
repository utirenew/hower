Sepro = window.Sepro || {
    asynchrony: false
};

function Sepro_tags(pid, values, request)
{
    this.timer = null;
    this.value = null;

    this.input = null;
    this.tags = null;
    this.variants = null;

    this.pid = pid;
    this.values = values;
    this.value = Object.keys(values).join(', ');
    this.request = request;
}

Sepro_tags.prototype.keyup = function(input)
{
    let value = input.value.replace(/[^a-zA-Zа-яА-Я0-9-+\(\)*\/., ]/gi, ''),
        values = value.split(',');

    if(this.value === value || Sepro.asynchrony)
    {
        return true;
    }

    this.value = value;

    if(!this.input)
    {
        this.input = $(input);
        this.tags = this.input.closest('.js-sepro-tags');
        this.variants = this.tags.find('.js-sepro-tags-variants');
    }

    this.handler(values);

    if(!!this.timer)
    {
        clearTimeout(this.timer);
    }

    this.timer = setTimeout($.proxy(function()
    {
        this.send(values.pop().trim());
    }, this), 500);

    return true;
};

Sepro_tags.prototype.handler = function(values)
{
    let name = 'PROP' + '[' + this.pid + ']',
        extValues = [],
        n = 0;

    this.tags.find('.js-sepro-tags-hidden').remove();

    $.each(values, $.proxy(function(k, value)
    {
        value = value.trim();

        if(!value.length || extValues.includes(value)) return true;

        let input = document.createElement("input");

        input.type = 'hidden';
        input.className = 'js-sepro-tags-hidden';
        input.value = value;

        extValues.push(value);

        if(!!this.values[value])
        {
            input.name = name + '[' + this.values[value] + ']';

            this.tags.append(input);

            return true;
        }

        input.name = name + '[n' + n + ']';

        this.tags.append(input);
        n++;

    }, this));
};

Sepro_tags.prototype.send = function(end)
{
    if(Sepro.asynchrony) return true;

    $.ajax({
        url: 'iblock_element_edit.php?' + $.param(this.request),
        data: {
            PID: this.pid,
            AJAX: 'tags',
            VALUE: end
        },
        type: 'post',
        dataType: 'html',
        success: $.proxy(function (response)
        {
            if(!!response)
            {
                this.variants.html(response);

                $(document).bind('click', $.proxy(function(event)
                {
                    this.variants.html('');
                    $(this).unbind(event);
                }, this));
            }

            Sepro.asynchrony = false;
        }, this),
        error: function(xhr, st)
        {
            console.log(xhr);

            Sepro.asynchrony = false;
        }
    });

    Sepro.asynchrony = true;
};

Sepro_tags.prototype.choose = function(value)
{
    let values = this.value.split(',');

    values.pop();

    values.push((values.length ? ' ' : '') + value);

    this.handler(values);

    values = values.join(',');

    this.input.val(values).focus();
    this.value = values;

    return false;
};