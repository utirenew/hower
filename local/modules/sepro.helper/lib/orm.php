<?
namespace Sepro;

use \Bitrix\Main\Application;

class ORMFactory
{
    protected $connection;
    protected $sqlHelper;
    protected $entityName;

    private static $ORMEntity = array();

    private function __construct($table_name, $connectionName)
    {
        try
        {
            $this->connection = Application::getConnection($connectionName);
            $this->sqlHelper = $this->connection->getSqlHelper();

            if(!$this->getTableName($table_name))
            {
                throw new \Exception("Table $connectionName:$table_name not exists");
            }

            $className = $this->getClassName($table_name);
            $this->entityName = "\\Sepro\\".$className;

            if(!class_exists($this->entityName))
            {
                $entity = '
                namespace Sepro;
                
                class '.$className.' extends \Bitrix\Main\Entity\DataManager
                {
                    public static function getClassName()
                    {
                        return __CLASS__;
                    }
                
                    public static function getFilePath()
                    {
                        return __FILE__;
                    }
                
                    public static function getTableName()
                    {
                        return \''.$table_name.'\';
                    }

                    public static function getConnectionName()
                    {
                        return \''.$connectionName.'\';
                    }
                    
                    public static function getMap()
                    {
                        return '.var_export($this->setMap($table_name), true).';
                    }
                }';

                eval($entity);
            }
        }
        catch(\Exception $e)
        {
            die($e->getMessage());
        }
    }

    final public static function compile($table_name, $connectionName = 'default')
    {
        if(empty($table_name)) return false;

        if(empty(static::$ORMEntity[$table_name]))
        {
            $ORMEntity = new self($table_name, $connectionName);
            static::$ORMEntity[$table_name] = $ORMEntity->getEntityName();
        }

        return static::$ORMEntity[$table_name];
    }

    private function getEntityName()
    {
        return $this->entityName;
    }

    private function getTableName($table_name)
    {
        $arTable = $this->connection->query(
            sprintf(
                "SHOW TABLES LIKE '%s'",
                $this->sqlHelper->forSql($table_name)
            )
        )->fetch();

        return is_array($arTable) && $table_name == array_shift($arTable);
    }

    private function getClassName($table_name, $prefix = 'ORM', $suffix = 'Table')
    {
        $className = explode('_', $table_name);
        $className = array_map('ucfirst', $className);
        $className = $prefix.implode('', $className).$suffix;

        return $className;
    }

    private function setMap($table_name)
    {
        $arMap = array();
        $arIndexes = array();

        $rsIndexes = $this->connection->query(
            sprintf(
                'SHOW INDEXES FROM %s',
                $this->sqlHelper->forSql($table_name)
            )
        );

        while($arIndex = $rsIndexes->fetch())
        {
            if (intval($arIndex["Non_unique"])) continue;

            $arIndexes[$arIndex["Column_name"]] = $arIndex;
        }

        $rsResult = $this->connection->query(
            sprintf(
                'DESCRIBE %s',
                $this->sqlHelper->forSql($table_name)
            )
        );

        while($arColumn = $rsResult->fetch())
        {
            $arField = array();

            if($arColumn['Key'] === 'PRI' || !empty($arIndexes[$arColumn['Field']]))
            {
                $arField['primary'] = true;
            }

            if(!empty($arColumn["Default"]))
            {
                $arField['default_value'] = $arField["Default"];
            }

            if($arColumn["Null"] === "NO" && !$arField['primary'])
            {
                $arField['required'] = true;
            }

            if($arColumn['Extra'] === 'auto_increment')
            {
                $arField['autocomplete'] = true;
            }

            switch(true)
            {
                case preg_match("/^(varchar|char|varbinary)/", $arColumn["Type"]):

                    $arField["data_type"] = "string";

                    break;

                case preg_match("/^(text|longtext|mediumtext|longblob)/", $arColumn["Type"]):

                    $arField["data_type"] = "text";

                    break;

                case preg_match("/^(datetime|timestamp)/", $arColumn["Type"]):

                    $arField["data_type"] = "datetime";

                    break;

                case preg_match("/^(date)/", $arColumn["Type"]):

                    $arField["data_type"] = "date";

                    break;

                case preg_match("/^(int|smallint|bigint|tinyint)/", $arColumn["Type"]):

                    $arField["data_type"] = "integer";

                    break;

                case preg_match("/^(float|double|decimal)/", $arColumn["Type"]):

                    $arField["data_type"] = "float";

                    break;

                default:

                    $arField["data_type"] = "UNKNOWN";
            }

            if($arField['data_type'] === 'string')
            {
                preg_match("/\((\d+)\)$/", $arColumn["Type"], $match);

                if($match[1] == 1 && in_array($arColumn["Default"], array('N', 'Y')))
                {
                    $arField["data_type"] = "boolean";
                    $arField["values"] = array('N', 'Y');
                }
            }

            $arMap[$arColumn['Field']] = $arField;

        }

        return $arMap;
    }
}