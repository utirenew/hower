<? namespace Sepro;

use Sepro\User as SUser,
	Sepro\Socialnetwork as SSoc,
	Bitrix\Main\UserTable as MUser,
	Bitrix\Sale\Internals\DiscountTable,
	Bitrix\Sale\Internals\DiscountGroupTable,
	Bitrix\Sale\Discount,
	CCatalogDiscount,
	CSaleDiscount,
	Cutil;


class Events{
	use Traits\IBlockElementEvents, Traits\SystemEvents, Traits\BufferEvents, Traits\AjaxEvents;

	public const TYPE_PRODUCT = 1;
	public const TYPE_SET = 2;
	public const TYPE_SKU = 3;
	public const TYPE_OFFER = 4;
	public const TYPE_FREE_OFFER = 5;
	public const TYPE_EMPTY_SKU = 6;
	protected static $type_sales_values = [
		TYPE_SELL_SALE => [
			'1' => 'Perc',
			'2' => 'CurEach',
			'3' => 'CurAll',
			'4' => '',
		],
		TYPE_BUY_SALE  => [
			'1' => 'Perc',
			'2' => 'CurEach',
			'3' => 'CurAll',
			'4' => '',
		],
	];
	protected static $arDeafultParams = [
		'LID'                 => 's1',
		'NAME'                => '',
		'ACTIVE_FROM'         => '',
		'ACTIVE_TO'           => '',
		'ACTIVE'              => 'Y',
		'SORT'                => '100',
		'PRIORITY'            => '10',
		'LAST_DISCOUNT'       => 'N',
		'LAST_LEVEL_DISCOUNT' => 'N',
		'CURRENCY'            => '',
		'XML_ID'              => '',
		'CONDITIONS'          => [
			'CLASS_ID' => 'CondGroup',
			'DATA'     => [
				'All'  => 'AND',
				'True' => 'True',
			],
			'CHILDREN' => [],
		],
		'ACTIONS'             => [
			'CLASS_ID' => 'CondGroup',
			'DATA'     => ['ALL' => 'AND'],
			'CHILDREN' => [
				[
					'CLASS_ID' => 'ActSaleBsktGrp',
					'DATA'     => [
						'Type'  => 'Discount',
						'Value' => 0,
						'Unit'  => 'Perc',
						'Max'   => 0,
						'All'   => 'AND',
						'True'  => 'True',
					],
					'CHILDREN' => [],
				],
			],
		],
		'USER_GROUPS'         => [2],
	];


	public function OnBeforePrologHandler(){
		global $USER;

		if(!$USER->GetParam('city')){
			if($USER->IsAuthorized()){
				$uid     = $USER->GetID();
				$oneUser = $USER->GetList($by = "personal_country", $order = "desc", array('ID' => $uid));
				while($userData = $oneUser->NavNext()){
					if(!empty($userData['PERSONAL_CITY'])){
						$USER->SetParam('city', $userData['PERSONAL_CITY']);
					}
					else{
						$USER->Update($uid, array('PERSONAL_CITY' => User::getLocation()['city']), false);
						$USER->SetParam('city', User::getLocation()['city']);
					}
				}
			}
			else{
				$tempCity = User::getLocation()['city'];
				if(empty($tempCity)) $tempCity = 'Москва';
				$USER->SetParam('city', $tempCity);
			}

		}

		return true;
	}

	public function OnAfterUserAuthorizeHandler($arUser){
		unset($_SESSION['SESS_AUTH']['city']);
	}

	public function SaleOrderSaved($order){
		global $USER;

		if($order->getParameter("IS_NEW")){
			$USER_ID     = $USER->GetID();
			$order_data  = $order->getParameter("ENTITY");
			$items       = $order_data->getBasket()->getBasketItems();
			$product_ids = [];

			foreach($items as $item) $product_ids[] = $item->getField('PRODUCT_ID');

			$users   = array_diff(SUser::getUsersByProductsID($product_ids), [$USER_ID]);
			$friends = SSoc::getNoFriends($USER_ID, $users);

			if(!empty($friends)) SSoc::setNewFriends($USER_ID, $friends);
		}
	}

	public function ImBeforeMessageAdd($arFields){
		$arr_group_id = MUser::getUserGroupIds($arFields['FROM_USER_ID']);
		$pattern      = '/\w+@\w+\.\w{2,4}|\+?[\d\(\)\-\s]{5,20}|email|телефон/ui';

		// Проверка исключения блокировки по id пользователя, по группе пользователя и регулярке текста сообщения
		if((!in_array($arFields['FROM_USER_ID'], [1]) || empty(array_intersect([1, 7, 11], $arr_group_id))) &&
		   preg_match_all($pattern, $arFields['MESSAGE']) === 1){
			return [
				'result' => false,
				'reason' => 'Недопустимое содержание',
			];
		}

		return $arFields;
	}

	public function IBlockBeforeIBlockElementAddHandler(&$arFields){
		$name             = $arFields["NAME"];
		$arParams         = array("replace_space" => "-", "replace_other" => "-");
		$trans            = Cutil::translit($name, "ru", $arParams);
		$arFields["CODE"] = $trans;
	}

	public function IBlockBeforeIBlockElementDeleteHandler($id){
		$res      = \CIBlockElement::GetByID($id);
		$arFields = $res->GetNext();

		$arDiscount = CCatalogDiscount::GetDiscountForProduct(array(
			'ID'        => $arFields['ID'],
			'IBLOCK_ID' => $arFields['IBLOCK_ID'],
		), array('SITE_ID' => 's1'));

		$id = key($arDiscount);
		if(!empty($arDiscount)) DiscountTable::delete($id);
	}

	public function IBlockAfterIBlockElementAddUpdateHandler(&$arFields){
		if($arFields['RESULT'] && in_array($arFields['IBLOCK_ID'], [SELL_IBLOCK, BUY_IBLOCK])){
			$prop_sale = PROP_SELL_SALE;
			$type_sale = TYPE_SELL_SALE;

			if($arFields['IBLOCK_ID'] == BUY_IBLOCK){
				$prop_sale = PROP_BUY_SALE;
				$type_sale = TYPE_BUY_SALE;
			}

			self::updateDiscount($arFields, $prop_sale, $type_sale);
		}
	}

	protected function updateDiscount($arFields, $prop_sale, $type_sale){
		if(isset($arFields['PROPERTY_VALUES'][$prop_sale]) && isset($arFields['PROPERTY_VALUES'][$type_sale])){
			$sale       = array_column($arFields['PROPERTY_VALUES'][$prop_sale], 'VALUE')[0];
			$type       = array_column($arFields['PROPERTY_VALUES'][$type_sale], 'VALUE')[0];
			$arDiscount = CCatalogDiscount::GetDiscountForProduct(array(
				'ID'        => $arFields['ID'],
				'IBLOCK_ID' => $arFields['IBLOCK_ID'],
			), array('SITE_ID' => 's1'));

			$id = key($arDiscount);

			if(!empty($sale) && !empty($type) && !empty(self::$type_sales_values[$type_sale][$type])){
				if(empty($arDiscount)){
					// ADD
					$arrUpdate            = self::$arDeafultParams;
					$arrUpdate['NAME']    = "Скидка на {$arFields['NAME']} ({$arFields['ID']}). Пользователь ({$arFields['MODIFIED_BY']})";
					$arrUpdate['ACTIONS'] = [
						'CLASS_ID' => 'CondGroup',
						'DATA'     => ['All' => 'AND'],
						'CHILDREN' => [
							[
								'CLASS_ID' => 'ActSaleBsktGrp',
								'DATA'     => [
									'Type'  => 'Discount',
									'Value' => $sale,
									'Unit'  => self::$type_sales_values[$type_sale][$type],
									'Max'   => 0,
									'All'   => 'AND',
									'True'  => 'True',
								],
								'CHILDREN' => [
									[
										'CLASS_ID' => 'CondIBElement',
										'DATA'     => [
											'logic' => 'Equal',
											'value' => [$arFields['ID']],
										],
									],
								],
							],
						],
					];

					CSaleDiscount::Add($arrUpdate);
				}
				else{
					// UPDATE
					$arrUpdate = [
						'NAME'         => "Скидка на {$arFields['NAME']} ({$arFields['ID']}). Пользователь ({$arFields['MODIFIED_BY']})",
						'ACTIONS_LIST' => [
							'CLASS_ID' => 'CondGroup',
							'DATA'     => ['All' => 'AND'],
							'CHILDREN' => [
								[
									'CLASS_ID' => 'ActSaleBsktGrp',
									'DATA'     => [
										'Type'  => 'Discount',
										'Value' => $sale,
										'Unit'  => self::$type_sales_values[$type_sale][$type],
										'Max'   => 0,
										'All'   => 'AND',
										'True'  => 'True',
									],
									'CHILDREN' => [
										[
											'CLASS_ID' => 'CondIBElement',
											'DATA'     => [
												'logic' => 'Equal',
												'value' => [$arFields['ID']],
											],
										],
									],
								],
							],
						],
					];

					CSaleDiscount::Update($id, $arrUpdate);
				}
			}
			elseif(!empty($arDiscount)){
				// DELETE
				DiscountTable::delete($id);
			}
		}
	}
}