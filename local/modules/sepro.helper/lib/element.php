<?
namespace Sepro;

use \Bitrix\Main\DB;

class ElementTable extends \Bitrix\Iblock\ElementTable
{
    private static $base_cg_id = false; // Базовый тип цены

    public static function getMap()
    {
        $arMap = parent::getMap();
        $ORMProperties = ORMFactory::compile('b_iblock_element_property');

        if(class_exists('\Bitrix\Catalog\ProductTable'))
        {
            $arMap['CATALOG'] = array(
                'data_type' => '\Bitrix\Catalog\ProductTable',
                'reference' => array(
                    '=ref.ID' => 'this.ID',
                )
            );
        }

        if(class_exists('\Bitrix\Catalog\PriceTable'))
        {
            if(intval(static::$base_cg_id) <= 0)
            {
                $arGTable = \Bitrix\Catalog\GroupTable::getList(array(
                    'filter' => array('=BASE' => 'Y'),
                    'select' => array('ID')
                ))->fetch();

                static::$base_cg_id = intval($arGTable['ID']) > 0 ? $arGTable['ID'] : 1;
            }

            $arMap['PRICE'] = array(
                'data_type' => '\Bitrix\Catalog\PriceTable',
                'reference' => array(
                    '=ref.PRODUCT_ID' => 'this.ID',
                    '=ref.CATALOG_GROUP_ID' => new DB\SqlExpression('?i', static::$base_cg_id)
                )
            );
        }

        foreach(IBlock::getIBlocks() as $IBLOCK)
        {
            foreach($IBLOCK['PROPERTIES'] as $CODE => $arProperty)
            {
                $arMap['IBLOCK_'.$IBLOCK['ID'].'_PROPERTY_'.$CODE] = array(
                    'data_type' => $ORMProperties::getClassName(),
                    'reference' => array(
                        '=ref.IBLOCK_ELEMENT_ID' => 'this.ID',
                        '=ref.IBLOCK_PROPERTY_ID' => new DB\SqlExpression('?i', $arProperty['ID'])
                    )
                );
            }
        }

        return $arMap;
    }
}