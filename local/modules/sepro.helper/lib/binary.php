<?namespace Sepro;

class Binary
{
    public $first;
    public $last;
    public $mid;

    private $array;
    private $value;

    private function __construct(array $array, $value)
    {
        $this->first = 0;
        $this->last = count($array) - 1;
        $this->array = $array;
        $this->value = (integer) $value;
    }

    public static function search($array, $value)
    {
        if(empty($array))
        {
            return false;
        }

        $b = new self($array, $value);

        return $b->recursiveSearch();
    }

    private function recursiveSearch()
    {
        $this->mid = intval($this->first + ($this->last - $this->first) / 2);

        if ($this->array[$this->first] == $this->value)
        {
            return $this->first;
        }

        if($this->first >= $this->last)
        {
            return -(1 + $this->first);
        }

        if($this->array[$this->mid] == $this->value)
        {
            if ($this->mid == $this->first + 1)
            {
                return $this->mid;
            }
            else
            {
                $this->last = $this->mid + 1;

                return $this->recursiveSearch();
            }
        }
        else if($this->array[$this->mid] > $this->value)
        {
            $this->last = $this->mid;
            return $this->recursiveSearch();
        }

        $this->first = $this->mid + 1;
        return $this->recursiveSearch();
    }
}