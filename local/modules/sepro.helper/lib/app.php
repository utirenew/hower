<?
namespace Sepro;

class App
{
    private static $application = false;
    private static $curPage = false;

    public static function GetInstance()
    {
        if(!static::$application)
        {
            static::$application = $GLOBALS['APPLICATION'];
        }

        return static::$application;
    }

    public static function GetCurPage()
    {
        if(empty(static::$curPage))
        {
            static::$curPage = self::GetInstance()->GetCurPage(false);
        }

        return static::$curPage;
    }

    /**
     * @param $property_id
     * @return bool
     *
     * Example:
     * \Sepro\App::GetInstance()->AddBufferContent(Array("Sepro\App", "GetParameter"), 'PAGE_CLASS');
     * Property must have two parameters "Y" or "N"
     */
    public static function GetParameter($property_id)
    {
        $value = self::getInstance()->GetPageProperty($property_id);

        if(empty($value))
        {
            return false;
        }

        return $value;
    }

    public static function getTitle()
    {
        $title = '<h1 class="page-title">'.self::GetInstance()->GetTitle().'</h1>';

        return self::GetParameter('HIDE_TITLE') !== 'Y' ? $title : '';
    }

    public function getPageClass()
    {
        $return = '';
        $arClasses = array();

        if(!empty(self::getParameter('PAGE_CLASS')))
        {
            $arClasses = array_filter(explode(' ', self::getParameter('PAGE_CLASS')));
        }

        if(self::GetParameter('IS_ARTICLE') == 'Y')
        {
            $arClasses = array_merge($arClasses, array('article-page', 'js-article-page'));
        }

        if(!empty($arClasses))
        {
            $return = ' class="'.(count($arClasses) > 1 ? implode(' ', $arClasses) : array_shift($arClasses)).'"';
        }

        return $return;
    }

    public static function deleteCache()
    {
        $arItems = array(
            array(
                'path' => '/bitrix/managed_cache/',
                'type' => 'Управляемый кеш',
                'delete' => true
            ),
            array(
                'path' => '/bitrix/stack_cache/',
                'type' => 'Управляемый кеш',
                'delete' => true
            ),
            array(
                'path' => '/bitrix/cache/',
                'type' => 'Неуправляемый кеш'
            ),
            array(
                'path' => '/bitrix/html_pages/',
                'type' => 'HTML кеш'
            )
        );

        if(class_exists('Memcache'))
        {
            $memcache_obj = new \Memcache();
            $memcache_obj->connect('memcache_host', 11211);
            $memcache_obj->flush();
        }

        BXClearCache(true);
        $GLOBALS['CACHE_MANAGER']->CleanAll();
        $GLOBALS['stackCacheManager']->CleanAll();
        $staticHtmlCache = \Bitrix\Main\Data\StaticHtmlCache::getInstance();
        $staticHtmlCache->deleteAll();

        foreach ($arItems as $arItem)
        {
            if ($arItem['delete'])
            {
                DeleteDirFilesEx($arItem['path']);
            }
        }
    }
}