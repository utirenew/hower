<?
namespace Sepro\Import;

class Autoload
{
    private static $classes = array();

    private function __construct($class)
    {
        if(class_exists($class) || strpos($class, __NAMESPACE__) !== 0)
        {
            return;
        }

        $class = str_replace(__NAMESPACE__, '', $class);

        require_once __DIR__.str_replace('\\', '/', strtolower($class)).'.php';
    }

    public static function init($class)
    {
        if(empty(self::$classes[$class]))
        {
            self::$classes[$class] = new self($class);
        }

        return self::$classes[$class];
    }
}