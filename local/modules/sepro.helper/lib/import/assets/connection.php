<?namespace Sepro\Import\Assets;

use Bitrix\Main\Application;
use Sepro\Helpers;
use Sepro\User;

class Connection
{
    protected $params = array();

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function __get($name)
    {
        return $this->params[$name];
    }

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function __isset($name)
    {
        return !empty($this->params[$name]);
    }

    public function request($arRequest)
    {
        foreach($arRequest as $code => $request)
        {
            switch($code)
            {
                case 'FID':
                case 'LENGTH':
                    $this->$code = $request;
                    break;
                case 'STEP':
                    $this->$code = intval($request) ? $request : 1;
                    break;
                case 'COUNTER':
                    $this->$code = intval($request) ? $request : 0;
                    break;
            }
        }
    }

    public function checkRequest()
    {
        $arErrors = array();

        foreach(array('FID', 'LENGTH', 'STEP', 'COUNTER') as $code)
        {
            if(!array_key_exists($code, $this->params))
            {
                $arErrors[] = "Недоступен параметр: $code";
            }
        }

        if(!empty($arErrors))
        {
            throw new \Exception(implode('<br>', $arErrors));
        }

        return true;
    }

    public function response()
    {
        $arResponse = array(
            'FID' => $this->FID,
            'STEP' => $this->STEP,
            'LENGTH' => $this->LENGTH,
            'COUNTER' => $this->COUNTER
        );

        return $arResponse;
    }

    public function parse($arValue)
    {
        $cache = Application::getInstance()->getManagedCache();
        $hash = implode('#', array('IMPORT_DATA_PARSE', serialize(array($arValue['EXTENSION'], $arValue['PATH']))));
        $parse = array();

        if(User::getRequest()->get('clear_cache') == 'Y')
        {
            $cache->clean($hash);
        }

        if($cache->read(86400, $hash))
        {
            $parse = $cache->get($hash);
        }
        else
        {
            switch($arValue['EXTENSION'])
            {
                case 'xml':

                    $parse = Helpers::parseXml($arValue['PATH']);

                    $parse = !empty($arValue['CALLBACK']) ? call_user_func($arValue['CALLBACK'], $parse) : Helpers::xml2array($parse);

                    break;

                case 'csv':

                    $reader = new Csv();
                    if(!empty($arValue['ENCODING']))
                    {
                        $reader->setInputEncoding($arValue['ENCODING']);
                    }
                    $reader->setDelimiter(';');
                    $reader->setEnclosure("");
                    $reader->setSheetIndex(0);

                    mb_internal_encoding('latin1');
                    $spreadsheet = $reader->load($arValue['PATH']);
                    mb_internal_encoding('UTF-8');

                    $parse = $spreadsheet->getActiveSheet()->toArray(null, false, false, true);

                    break;

                case 'xls':
                case 'xlsx':

                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                    $filter = new XLSFilter($arValue);

                    $reader->setReadFilter($filter)->setReadDataOnly(true);

                    mb_internal_encoding('latin1');
                    $spreadsheet = $reader->load($arValue['PATH']);
                    mb_internal_encoding('UTF-8');

                    $parse = $spreadsheet->getActiveSheet()->rangeToArray(
                        $filter->getRange(),
                        null,
                        false,
                        false,
                        true
                    );

                    break;
                default:

                    break;
            }

            $cache->setImmediate($hash, $parse);
        }

        return $parse;
    }
}
