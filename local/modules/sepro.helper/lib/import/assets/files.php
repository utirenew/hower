<?namespace Sepro\Import\Assets;

use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class Files extends Connection
{
    public $start = 0;
    public $finish = 1000000;

    public function checkFiles($arValues)
    {
        $arErrors = array();

        if(!is_array($arValues) || empty($arValues))
        {
            $arErrors[] = 'Массив прайс-листов пуст';
        }

        foreach($arValues as $name => $arValue)
        {
            if(!is_file($arValue["PATH"]))
            {
                $arErrors[] = "Прайс-лист $name: Файл ".$arValue["PATH"]." не доступен";
            }

            if(empty($arValue['EXTENSION']))
            {
                $arErrors[] = "Не указано расширение EXTENSION для прайс-листа: $name";
            }

            switch($arValue['EXTENSION'])
            {
                case 'xls':
                case 'xlsx':

                    if(empty($arValue['RANGE']['START']))
                    {
                        $arErrors[] = "Прайс-лист $name: Не указано начало столбца";
                    }

                    if(empty($arValue['RANGE']['FINISH']))
                    {
                        $arErrors[] = "Прайс-лист $name: Не указан конец столбца";
                    }

                    break;
            }
        }

        if(!empty($arErrors))
        {
            throw new \Exception(implode('<br>', $arErrors));
        }

        return true;
    }

    public function request($arRequest)
    {
        parent::request($arRequest);

        if(!empty($arRequest['MODEL']))
        {
            $this->MODEL = $arRequest['MODEL'];
        }

        $length = 0;
        foreach($this->files as $name => $arValues)
        {
            if(empty($this->MODEL))
            {
                $this->MODEL = $name;
            }

            $length += count($arValues['PARSE']);

            if($this->COUNTER >= $length && $this->MODEL == $name)
            {
                $this->MODEL = false;
                $this->FID = false;
            }
        }

        if(!intval($this->LENGTH))
        {
            $this->LENGTH = $length;
        }

        if(empty($this->FID))
        {
            foreach($this->files[$this->MODEL]['PARSE'] as $key => $arValue)
            {
                $this->FID = $key;
                break;
            }
        }

        return true;
    }

    public function checkRequest()
    {
        parent::checkRequest();

        if(!array_key_exists('MODEL', $this->params))
        {
            throw new \Exception('Недоступен параметр: MODEL');
        }
    }

    public function response()
    {
        $arResponse = parent::response();

        $arResponse['MODEL'] = $this->MODEL;

        return $arResponse;
    }
}

class XLSFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public $params = array();

    public function __construct($arValue)
    {
        $this->startRow = $arValue['START'];
        $this->endRow = $arValue['FINISH'];
        $this->startCol = $arValue['RANGE']['START'];
        $this->endCol = $arValue['RANGE']['FINISH'];
        $this->columns = range($arValue['RANGE']['START'], $arValue['RANGE']['FINISH']);
    }

    public function getRange()
    {
        return $this->startCol.$this->startRow.':'.$this->endCol.$this->endRow;
    }

    public function __set($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function __get($name)
    {
        return $this->params[$name];
    }

    public function readCell($column, $row, $worksheetName = '')
    {
        if ($row >= $this->startRow && $row <= $this->endRow)
        {
            if(!empty($this->columns))
            {
                if(in_array($column, $this->columns))
                {
                    return true;
                }

                return false;
            }

            return true;
        }

        return false;
    }
}