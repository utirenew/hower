<?
namespace Sepro\Import\Assets;

class Api extends Connection
{
    public function request($arRequest)
    {
        parent::request($arRequest);

        if(!empty($arRequest['MODEL']))
        {
            $this->MODEL = $arRequest['MODEL'];
        }

        $length = 0;
        foreach($this->files as $name => $arValues)
        {
            if(empty($this->MODEL))
            {
                $this->MODEL = $name;
            }

            $length += count($arValues['PARSE']);

            if($this->COUNTER >= $length && $this->MODEL == $name)
            {
                $this->MODEL = false;
                $this->FID = false;
            }
        }

        if(!intval($this->LENGTH))
        {
            $this->LENGTH = $length;
        }

        if(empty($this->FID))
        {
            foreach($this->files[$this->MODEL]['PARSE'] as $key => $arValue)
            {
                $this->FID = $key;
                break;
            }
        }

        return true;
    }

    public function checkRequest()
    {
        parent::checkRequest();

        if(!array_key_exists('MODEL', $this->params))
        {
            throw new \Exception('Недоступен параметр: MODEL');
        }
    }

    public function response()
    {
        $arResponse = parent::response();

        $arResponse['MODEL'] = $this->MODEL;

        return $arResponse;
    }
}