<?
namespace Sepro\Import\Traits;

trait Csv
{
    public function init()
    {
        $arKeys = array_keys($this->files[$this->connection->MODEL]['PARSE']);

        $CPoint = 0;
        for($n = 0; $n <= count($arKeys); $n++)
        {
            if($arKeys[$n] == $this->connection->FID)
            {
                $CPoint = $n;
                break;
            }
        }

        $arParse = array_slice(
            $this->files[$this->connection->MODEL]['PARSE'],
            $CPoint,
            $this->connection->STEP+1,
            true
        );

        $N = 0;
        foreach($arParse as $key => $arValue)
        {
            $this->connection->FID = $key;
            if($N >= $this->connection->STEP) break;

            if(!empty($this->headers) && is_array($this->headers))
            {
                $arValue = array_combine($this->headers, $arValue);
            }

            $this->import($arValue);

            $this->connection->COUNTER++;
            $N++;
        }

        return true;
    }
}