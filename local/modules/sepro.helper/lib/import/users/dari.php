<?namespace Sepro\Import\Users;

use Bitrix\Main\Application;
use Sepro\ElementTable;
use Sepro\Helpers;
use Sepro\IBlock;
use Sepro\Import\Base;
use Sepro\Import\Traits;
use Sepro\Log;
use Sepro\User;

class Dari extends Base
{
    use Traits\Xml;

    public $user = 'dari';
    public $type = 'API';

    public $files = array(
        'IMPORT' => array(
            'PATH' => 'http://dari66.ru/sitemap.xml',
            'EXTENSION' => 'xml'
        )
    );

    private function getContentByCache($url)
    {
        $cache = Application::getInstance()->getManagedCache();
        $hash = implode('#', array('GET_DATA_BY_URL', $url));
        $dom = false;

        if(User::getRequest()->get('clear_cache') == 'Y')
        {
            $cache->clean($hash);
        }

        if($cache->read(86400, $hash))
        {
            $dom = $cache->get($hash);
        }
        else
        {
            $dom = file_get_contents($url);

            $cache->setImmediate($hash, $dom);
        }

        return $dom;
    }

    private function parseCatalogSection($url, $page, &$arServices)
    {
        $dom = $this->getContentByCache($url);

        if(empty($dom))
        {
            return false;
        }

        $oDom = new \HtmlParser\ParserDom($dom);
        $services = $oDom->find('.content-list', 0);
        $arSection = array(
            'XML_ID' => $page['path'],
            'TITLE' => $oDom->find('h1', 0)
        );

        // TITLE
        if(!$arSection['TITLE'] || !$services)
        {
            return false;
        }

        $arSection['TITLE'] = $arSection['TITLE']->getPlainText();

        if(empty($arSection['TITLE']))
        {
            return false;
        }

        // DESCRIPTION
        $arSection['DESCRIPTION'] = $oDom->find('div.right-col div.content', 0);

        if($arSection['DESCRIPTION'])
        {
            $arSection['DESCRIPTION'] = $arSection['DESCRIPTION']->innerHtml();

            foreach($oDom->find('.breadcrumb, h1') as $oChild)
            {
                if(!$oChild) continue;
                $arSection['DESCRIPTION'] = str_replace($oChild->outerHtml(), '', $arSection['DESCRIPTION']);
            }

            $arSection['DESCRIPTION'] = str_replace($services->outerHtml(), '', $arSection['DESCRIPTION']);
        }

        // SERVICES
        foreach($services->find('li') as $service)
        {
            $arService = array(
                'TITLE' => $service->find('.co-title', 0)
            );

            if($arService['TITLE'])
            {
                $arService['XML_ID'] = end(array_filter(explode('/', $arService['TITLE']->getAttr('href'))));
            }

            if(empty($arService['XML_ID'])) continue;

            if(!empty($arServices[$arService['XML_ID']]))
            {
                $arServices[$arService['XML_ID']]['SECTIONS'][$arSection['XML_ID']] = $arSection;
                continue;
            }

            $arService['TITLE'] = $arService['TITLE']->getPlainText();

            $arService['SECTIONS'][$arSection['XML_ID']] = $arSection;

            $arServices[$arService['XML_ID']] = $arService;
        }

        return true;
    }

    private function parseCatalogItem($url, $page)
    {
        $dom = $this->getContentByCache($url);

        if(empty($dom))
        {
            return false;
        }

        $cache = Application::getInstance()->getManagedCache();
        $hash = implode('#', array('GET_PRODUCT_BY_URL', $url));
        $arElement = array();

        if(User::getRequest()->get('clear_cache') == 'Y')
        {
            $cache->clean($hash);
        }

        if($cache->read(86400, $hash))
        {
            $arElement = $cache->get($hash);
        }
        else
        {
            $oDom = new \HtmlParser\ParserDom($dom);

            $arMulties = array();
            $arPrices = array();
            $arElement = array(
                'PICTURE' => $oDom->find('#slider', 0),
                'PREVIEW_TEXT' => $oDom->find('.ar-col-r div'),
                'ARTICLE' => $oDom->find('.article', 0)
            );

            if($arElement['PICTURE'])
            {
                $arElement['PICTURE'] = $arElement['PICTURE']->find('.in-modal', 0);
            }

            if($arElement['PICTURE'])
            {
                $arElement['PICTURE'] = $page['scheme'].'://'.$page['host'].'/'.$arElement['PICTURE']->getAttr('href');
            }

            if(!empty($arElement['PREVIEW_TEXT']))
            {
                $arElement['PREVIEW_TEXT'] = array_shift($arElement['PREVIEW_TEXT']);
                $arElement['PREVIEW_TEXT'] = $arElement['PREVIEW_TEXT']->getPlainText();
            }

            if($arElement['ARTICLE'])
            {
                $arElement['DETAIL_TEXT'] = end(explode('Описание</h2>', $arElement['ARTICLE']->innerHtml()));
                $arElement['PRICES'] = $arElement['ARTICLE']->find('.ar-price', 0);
                $arElement['MULTI'] = $arElement['ARTICLE']->find('.multi-list .m-list-li');
            }

            if($arElement['PRICES'])
            {
                foreach($arElement['PRICES']->find('option') as $price)
                {
                    $value = +$price->getAttr('value');

                    if(!intval($value)) continue;

                    $arPrices[$value] = array(
                        'VALUE' => $value,
                        'PROPERTY' => $price->getAttr('data-property')
                    );
                }

                if(empty($arPrices))
                {
                    $arElement['PRICES'] = $arElement['PRICES']->find('input[name=price]', 0);

                    if($arElement['PRICES'])
                    {
                        $value = +$arElement['PRICES']->getAttr('value');

                        if(intval($value))
                        {
                            $arPrices[$value] = array(
                                'VALUE' => $value
                            );
                        }
                    }
                }

                $arElement['PRICES'] = $arPrices;
            }

            foreach($arElement['MULTI'] as $multi) // IS MULTI WITHOUT OFFERS
            {
                $arMulti = array(
                    'NAME' => $multi->find('.ml-text', 0),
                    'PICTURE' => $multi->find('.ml-img img', 0),
                    'DESCRIPTION' => $multi->find('.multi-hide-content', 0)
                );

                if($arMulti['NAME'])
                {
                    $arMulti['XML_ID'] = trim($arMulti['NAME']->getPlainText());
                    $arMulti['NAME'] = $arMulti['NAME']->find('strong', 0);

                    if($arMulti['NAME'])
                    {
                        $arMulti['NAME'] = $arMulti['NAME']->getPlainText();
                        $arMulti['PROPERTY'] = trim(str_replace($arMulti['NAME'], '', $arMulti['XML_ID']));
                        $arMulti['XML_ID'] = \CUtil::translit(
                            $arMulti['XML_ID'],
                            "ru",
                            ["replace_space" => "-", "replace_other" => "-"]
                        );
                    }
                }

                if(empty($arMulti['XML_ID'])) continue;

                if($arMulti['PICTURE'])
                {
                    $arMulti['PICTURE'] = $arMulti['PICTURE']->getAttr('src');
                }

                if($arMulti['DESCRIPTION'])
                {
                    $arMulti['DESCRIPTION'] = $arMulti['DESCRIPTION']->innerHtml();
                }

                $arMulties[$arMulti['XML_ID']] = $arMulti;
            }

            $arElement['MULTI'] = $arMulties;

            if(!empty($arElement['DETAIL_TEXT']))
            {
                $arElement['DETAIL_TEXT'] = current(preg_split('/(<div class="text-center ar-price|<h2>Программы на выбор)/i', $arElement['DETAIL_TEXT']));
            }

            unset($arElement['ARTICLE']);

            $cache->setImmediate($hash, $arElement);
        }

        return $arElement;
    }

    public function join()
    {
        $this->files['IMPORT']['CALLBACK'] = function($parse)
        {
            $arServices = array();
            $arExtServices = array();

            foreach($parse as $loc)
            {
                $page = (string) $loc->loc;

                if(strpos($page, '/catalog/') === false) continue;

                $page = parse_url($page);

                if(!empty($page['query'])) continue;

                $page['path'] = end(array_filter(explode('/', $page['path'])));
                $url = $page['scheme'].'://'.$page['host'].'/catalog/'.$page['path'].'/';

                switch(true)
                {
                    case intval(+$page['path']): // SECTION

                        $this->parseCatalogSection($url, $page, $arServices);

                        break;
                    case strpos($page['path'], 'item') !== false: // SERVICE

                        if(empty($arServices[$page['path']]))
                        {
                            break;
                        }

                        if(!$arElement = $this->parseCatalogItem($url, $page))
                        {
                            break;
                        }

                        $arExtServices[] = $page['path'];

                        $arServices[$page['path']] = array_merge($arServices[$page['path']], $arElement);

                        break;
                }
            }

            if(!empty($arExtServices))
            {
                foreach(array_diff(array_keys($arServices), $arExtServices) as $xml_id)
                {
                    $page = array(
                        'scheme' => 'http',
                        'host' => 'dari66.ru',
                        'path' => $xml_id
                    );

                    $arServices[$xml_id] = array_merge(
                        $arServices[$xml_id],
                        $this->parseCatalogItem(
                            $page['scheme'].'://'.$page['host'].'/catalog/'.$page['path'].'/',
                            $page
                        )
                    );
                }
            }

            return $arServices;
        };

        parent::join();
    }

    public function import($arValue)
    {
        // CHECK REQUIRE FIELDS
        foreach(array('TITLE', 'XML_ID', 'SECTIONS', 'PRICES') as $code)
        {
            if(empty($arValue[$code]))
            {
                $arErrors[] = "Значение $code пусто";
            }
        }

        if(!empty($arErrors))
        {
            Log::add2log(array_merge(
                ['Ошибки при проверке обязательных полей'],
                $arErrors
            ));

            return false;
        }

        $arValue['CODE'] = \CUtil::translit(
            trim(implode('-', array($arValue['TITLE'], $arValue['XML_ID']))),
            "ru",
            ["replace_space" => "-", "replace_other" => "-"]
        );

        // DELETE ELEMENT IF EXIST
        $arFields = ElementTable::GetList(array(
            'filter' => array('=CODE' => $arValue['CODE'], '=IBLOCK_ID' => IBLOCK_CATALOG_ID),
            'select' => array('ID')
        ))->fetch();

        if(intval($arFields['ID']))
        {
            if(!\CIBlockElement::Delete($arFields['ID']))
            {
                Log::add2log(array_merge(
                    ['Ошибка удаления товара'],
                    $arValue
                ));

                return false;
            }
        }

        $obElement = new \CIBlockElement();
        $arSections = array();
        $arIBlock = IBlock::getIBlock(IBLOCK_CATALOG_ID);

        // GET OR ADD SECTIONS
        foreach($arValue['SECTIONS'] as $arSection)
        {
            $arSections[] = IBlock::addSection(array(
                'NAME' => trim($arSection['TITLE']),
                'XML_ID' => $arSection['XML_ID'],
                'CODE' => \CUtil::translit(
                    trim($arSection['TITLE']),
                    "ru",
                    [
                        "replace_space" => "-",
                        "replace_other" => "-"
                    ]
                ),
                'DESCRIPTION' => $arSection['DESCRIPTION'],
                'DESCRIPTION_TYPE' => 'html',
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => IBLOCK_CATALOG_ID
            ), true);
        }

        if(empty($arSections))
        {
            return false;
        }

        // ADD ELEMENT
        $arFields = array(
            'DATE_CREATE' => date('d.m.Y H:i:s'),
            'TIMESTAMP_X' => date('d.m.Y H:i:s'),
            'XML_ID' => $arValue['XML_ID'],
            'NAME' => $arValue['TITLE'],
            'CODE' => $arValue['CODE'],
            'ACTIVE' => 'Y',
            'IBLOCK_SECTION' => $arSections,
            'IBLOCK_ID' => IBLOCK_CATALOG_ID
        );

        if(!empty($arValue['PICTURE']))
        {
            $arFields['DETAIL_PICTURE'] = \CFile::MakeFileArray($arValue['PICTURE']);
        }

        if(!empty($arValue['PREVIEW_TEXT']))
        {
            $arFields['PREVIEW_TEXT'] = $arValue['PREVIEW_TEXT'];
            $arFields['PREVIEW_TEXT_TYPE'] = 'html';
        }

        if(!empty($arValue['DETAIL_TEXT']))
        {
            $arFields['DETAIL_TEXT'] = $arValue['DETAIL_TEXT'];
            $arFields['DETAIL_TEXT_TYPE'] = 'html';
        }

        foreach($arValue['MULTI'] as $code => $arMulti)
        {
            $arHLValues = array(
                'UF_XML_ID' => $code,
                'UF_NAME' => $arMulti['NAME']
            );

            if(!empty($arMulti['DESCRIPTION']))
            {
                $arHLValues['UF_FULL_DESCRIPTION'] = $arMulti['DESCRIPTION'];
            }

            if(!empty($arMulti['PICTURE']))
            {
                $arMulti['PICTURE'] = \CFile::MakeFileArray($arMulti['PICTURE']);

                if(!empty($arMulti['PICTURE']))
                {
                    $arHLValues['UF_FILES'] = \Sepro\IBlock::addFile($arMulti['PICTURE'], 'uf', true);
                }
            }

            if(!empty($arMulti['PROPERTY']))
            {
                $arHLValues['UF_PARAMETERS'] = $arMulti['PROPERTY'];
            }

            $arFields['PROPERTY_VALUES'][$arIBlock['PROPERTIES']['MULTI']['ID']][] = IBlock::setHighloadValue(
                'b_hlbd_multistaff',
                $arHLValues
            );
        }

        if(!$EID = $obElement->Add($arFields))
        {
            Log::add2log(array_merge(
                array(
                    'Ошибка добавления элемента',
                    $obElement->LAST_ERROR
                ),
                $arFields,
                $arValue
            ));

            return false;
        }

        $arFields = array('ID' => $EID);

        if(!\CCatalogProduct::Add($arFields))
        {
            Log::add2log(array_merge(
                array('Ошибка добавления товара в торговый каталог'),
                $arFields
            ));

            return false;
        }

        switch(count($arValue['PRICES']))
        {
            case 1: // IS PRODUCT

                $price = array_shift($arValue['PRICES']);

                $arFields = array(
                    "PRODUCT_ID" => $EID,
                    "CATALOG_GROUP_ID" => CATALOG_GROUP_ID,
                    "PRICE" => $price['VALUE'],
                    "CURRENCY" => 'RUB'
                );

                if(!\CPrice::Add($arFields))
                {
                    Log::add2log(array_merge(
                        array('Ошибка добавления стоимости товара'),
                        $arFields
                    ));

                    return false;
                }

                break;
            default: // IS OFFERS SET

                $arIBOffers = \Sepro\IBlock::getIBOffers(IBLOCK_OFFERS_ID);

                foreach($arValue['PRICES'] as $arOffer)
                {
                    if(empty($arOffer['PROPERTY'])) continue;

                    $arFields = array(
                        'DATE_CREATE' => date('d.m.Y H:i:s'),
                        'TIMESTAMP_X' => date('d.m.Y H:i:s'),
                        'NAME' => trim(implode(' - ', array($arValue['TITLE'], $arOffer['PROPERTY']))),
                        'CODE' => \CUtil::translit(
                            trim(implode('-', array($arValue['TITLE'], $arValue['XML_ID'], $arOffer['VALUE'], $arOffer['PROPERTY']))),
                            "ru",
                            ["replace_space" => "-", "replace_other" => "-"]
                        ),
                        'ACTIVE' => 'Y',
                        'IBLOCK_ID' => IBLOCK_OFFERS_ID,
                        'PROPERTY_VALUES' => array(
                            $arIBOffers['PROPERTIES']['CML2_LINK']['ID'] => $EID,
                            $arIBOffers['PROPERTIES']['PARAMETERS']['ID'] => $arOffer['PROPERTY']
                        )
                    );

                    $arFields['XML_ID'] = $arFields['CODE'];

                    // ADD OFFER
                    if(!$OID = $obElement->Add($arFields))
                    {
                        Log::add2log(array_merge(
                            array(
                                'Ошибка добавления элемента',
                                $obElement->LAST_ERROR
                            ),
                            $arFields
                        ));

                        continue;
                    }

                    $arFields = array('ID' => $OID);

                    if(!\CCatalogProduct::Add($arFields))
                    {
                        Log::add2log(array_merge(
                            array('Ошибка добавления товара в торговый каталог'),
                            $arFields
                        ));

                        continue;
                    }

                    $arFields = array(
                        "PRODUCT_ID" => $OID,
                        "CATALOG_GROUP_ID" => CATALOG_GROUP_ID,
                        "PRICE" => $arOffer['VALUE'],
                        "CURRENCY" => 'RUB'
                    );

                    if(!\CPrice::Add($arFields))
                    {
                        Log::add2log(array_merge(
                            array('Ошибка добавления стоимости товара'),
                            $arFields
                        ));
                    }
                }

                break;
        }

        return true;
    }
}