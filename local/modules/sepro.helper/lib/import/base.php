<?
namespace Sepro\Import;

use Sepro\Import\Assets;

class Base
{
    protected $type;
    protected $user;
    protected $files = array();

    public $connection;

    public function __construct()
    {
        $this->getAssets($this->type);
        $this->connected();
    }

    public function getAssets($type)
    {
        switch($type)
        {
            case 'API':
                $this->connection = new Assets\Api($this->user);
                break;
            case 'FILES':
                $this->connection = new Assets\Files($this->user);
                break;
        }
    }

    public function setRequest($arRequest)
    {
        if(method_exists($this->connection, 'request'))
        {
            $this->connection->request($arRequest);
        }

        if(method_exists($this->connection, 'checkRequest'))
        {
            $this->connection->checkRequest();
        }
    }

    public function getResponses()
    {
        if(method_exists($this->connection, 'response'))
        {
            return $this->connection->response();
        }

        return false;
    }

    public function connected()
    {
        switch($this->type)
        {
            case 'FILES':

                foreach($this->files as &$arValue)
                {
                    if(!intval($arValue['START']))
                    {
                        $arValue['START'] = $this->connection->start;
                    }

                    if(!intval($arValue['FINISH']))
                    {
                        $arValue['FINISH'] = $this->connection->finish;
                    }
                }

                $this->connection->checkFiles($this->files);

                break;
        }

        return true;
    }

    public function join()
    {
        foreach($this->files as $name => &$arValue)
        {
            $arValue['PARSE'] = $this->connection->parse($arValue);

            if(empty($arValue['PARSE']))
            {
                throw new \Exception("Ошибка парсинга: $name");
            }
        }

        $this->connection->files = $this->files;

        return true;
    }

    public function init()
    {
        return true;
    }

    public function import($arElement)
    {
        return true;
    }
}
?>