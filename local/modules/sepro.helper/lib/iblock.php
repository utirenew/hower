<?
namespace Sepro;

use \Bitrix\Main\Application;

class IBlock
{
    static $catalog = array(); // IBLOCK каталога товаров
    static $sku = array(); // IBLOCK торговых предложений

    static $iblocks;
    static $iblocksByType;

    public static function addSection($arFields, $check_section = false)
    {
        $obSection = new \CIBlockSection();
        $arSection = false;

        if(is_bool($check_section) && $check_section)
        {
            $arSection = \Bitrix\Iblock\SectionTable::GetList(array(
                'filter' => array(
                    'CODE' => $arFields['CODE'],
                    'IBLOCK_ID' => $arFields['IBLOCK_ID']
                ),
                'select' => array(
                    'ID'
                )
            ))->fetch();

            if(intval($arSection['ID']))
            {
                return $arSection['ID'];
            }
        }

        if(!$arSection['ID'] = $obSection->Add($arFields))
        {
            Log::add2log(array_merge(
                ['Ошибка добавления категории', $obSection->LAST_ERROR],
                $arFields
            ));

            return false;
        }

        return $arSection['ID'];
    }

    public static function addProperty($arFields, $check_property = false)
    {
        $obProperty = new \CIBlockProperty();
        $arProperty = false;

        if(is_bool($check_property) && $check_property)
        {
            $arProperty = \Bitrix\Iblock\PropertyTable::GetList(array(
                'filter' => array(
                    'CODE' => $arFields['CODE'],
                    'IBLOCK_ID' => $arFields['IBLOCK_ID']
                ),
                'select' => array('ID')
            ))->fetch();

            if(intval($arProperty['ID']))
            {
                return $arProperty['ID'];
            }
        }

        if(!$arProperty['ID'] = $obProperty->Add($arFields))
        {
            Log::add2log(array_merge(
                ['Ошибка добавления свойства', $obProperty->LAST_ERROR],
                $arFields
            ));

            return false;
        }

        return $arProperty['ID'];
    }

    public static function getIBCatalog($iblock_id = false)
    {
        if(empty(static::$catalog))
        {
            if(intval($iblock_id) > 0)
            {
                static::$catalog = self::getIBlock($iblock_id);
            }

            if(empty(static::$catalog))
            {
                if(!class_exists('\Bitrix\Catalog\CatalogIblockTable')) return false;

                $rsCatalog = \Bitrix\Catalog\CatalogIblockTable::GetList(array(
                    'select' => array('IBLOCK_ID', 'PRODUCT_IBLOCK_ID')
                ));

                while($arCatalog = $rsCatalog->fetch())
                {
                    if(!empty($arCatalog['PRODUCT_IBLOCK_ID']))
                    {
                        static::$catalog = self::getIBlock($arCatalog['PRODUCT_IBLOCK_ID']);

                        break;
                    }

                    static::$catalog = self::getIBlock($arCatalog['IBLOCK_ID']);

                    break;
                }
            }
        }

        return !empty(static::$catalog) ? static::$catalog : false;
    }

    public static function getIBOffers($iblock_id = false)
    {
        if(empty(static::$sku))
        {
            if(intval($iblock_id) > 0)
            {
                static::$sku = self::getIBlock($iblock_id);
            }

            if(empty(static::$sku))
            {
                self::getIBCatalog();

                if(intval(static::$catalog['SKU_IBLOCK_ID']) > 0)
                {
                    static::$sku = self::getIBlock(static::$catalog['SKU_IBLOCK_ID']);
                }
            }
        }

        return !empty(static::$sku) ? static::$sku : false;
    }

    public static function setHighloadValue($table_name, $arFields)
    {
        $ORMTable = ORMFactory::compile($table_name);

        $arValue = $ORMTable::GetList(array(
            'filter' => array('UF_XML_ID' => $arFields['UF_XML_ID']),
            'select' => array('ID')
        ))->fetch();

        if(!intval($arValue['ID']))
        {
            $arRow = $ORMTable::GetList(array(
                'order' => array('UF_SORT' => 'DESC'),
                'select' => array('UF_SORT')
            ))->fetch();

            if(!empty($arRow) && is_array($arRow))
            {
                $arFields = array_merge($arFields, $arRow);
            }

            $arFields['UF_SORT'] = intval($arFields['UF_SORT']) + 10;

            $obResult = $ORMTable::add($arFields);

            if (!$obResult->isSuccess())
            {
                Log::add2log(array_merge(
                    array('Ошибка добавления новой записи в справочник: '.$table_name),
                    $obResult->getErrorMessages(),
                    $arFields
                ));

                return false;
            }
        }

        return array(
            'VALUE' => $arFields['UF_XML_ID']
        );
    }

    public static function getProperties(array $arFilter, array $arSelect)
    {
        $cache = Application::getInstance()->getManagedCache();
        $arProperties = array();

        if(empty($arFilter))
        {
            $arFilter = array(
                'ACTIVE' => 'Y'
            );
        }

        $arSelect = array_merge($arSelect, array('ID'));

        $hash = implode('#', array("IBLOCK_PROPERTIES_ID", serialize($arFilter), serialize($arSelect)));

        if(User::getRequest()->get('clear_cache') == 'Y')
        {
            $cache->clean($hash);
        }

        if($cache->read(86400, $hash))
        {
            $arProperties = $cache->get($hash);
        }
        else
        {
            $rsProperties = \Bitrix\Iblock\PropertyTable::GetList(array(
                'order' => array('SORT' => 'ASC', 'ID' => 'ASC'),
                'filter' => $arFilter,
                'select' => $arSelect
            ));

            while($arProperty = $rsProperties->fetch())
            {
                $arProperties[$arProperty['ID']] = $arProperty;
            }

            $cache->setImmediate($hash, $arProperties);
        }

        return $arProperties;
    }

    public static function getIBlocksByType($iblock_type)
    {
        if(empty(static::$iblocksByType[$iblock_type]))
        {
            self::getIBlocks();
        }

        return !empty(static::$iblocksByType[$iblock_type]) ? static::$iblocksByType[$iblock_type] : false;
    }

    public static function getIBlock($id)
    {
        if(empty(static::$iblocks[$id]))
        {
            self::getIBlocks();
        }

        return !empty(static::$iblocks[$id]) ? static::$iblocks[$id] : false;
    }

    public static function getIblockSections($IBLOCK_ID, $arFilter = array(), $UFExist = false, $clearCache = false)
    {
        if(intval($IBLOCK_ID) <= 0) return false;

        $arIBSections = self::getSections($arFilter, $UFExist, $clearCache);

        return !empty($arIBSections[$IBLOCK_ID]) ? $arIBSections[$IBLOCK_ID] : false;
    }

    public static function getPropertyEnums($arRuntime = array(), $arFilter = array(), $clearCache = false)
    {
        $cache = Application::getInstance()->getManagedCache();
        $hash = implode('#', array("IBLOCK_PROPERTY_ENUM", serialize($arRuntime), serialize($arFilter)));
        $arPEnums = array();

        if($clearCache)
        {
            $cache->clean($hash);
        }

        if($cache->read(86400, $hash))
        {
            $arPEnums = $cache->get($hash);
        }
        else
        {
            $rsPEnums = \Bitrix\Iblock\PropertyEnumerationTable::GetList(array(
                'order' => array(
                    'PROPERTY_ID' => 'ASC',
                    'SORT' => 'ASC',
                    'ID' => 'ASC'
                ),
                'runtime' => $arRuntime,
                'filter' => $arFilter,
                'select' => array(
                    'ID',
                    'PROPERTY_ID',
                    'VALUE',
                    'XML_ID'
                )
            ));

            while($arPEnum = $rsPEnums->fetch())
            {
                $arPEnums[$arPEnum['PROPERTY_ID']][$arPEnum['ID']] = $arPEnum;
            }

            $cache->setImmediate($hash, $arPEnums);
        }

        return $arPEnums;
    }

    public static function getIBlocks()
    {
        if(empty(static::$iblocks))
        {
            $rsIblocks = \Bitrix\Iblock\IblockTable::GetList(
                array(
                    'order' => array('ID' => 'ASC'),
                    'select' => array('*')
                )
            );

            while($arIBlock = $rsIblocks->fetch())
            {
                static::$iblocks[$arIBlock['ID']] = $arIBlock;
            }

            $rsProperties = \Bitrix\Iblock\PropertyTable::GetList(array(
                'filter' => array(
                    '=IBLOCK_ID' => array_keys(static::$iblocks)
                ),
                'select' => array(
                    'ID',
                    'IBLOCK_ID',
                    'NAME',
                    'CODE',
                    'SORT',
                    'MULTIPLE',
                    'LINK_IBLOCK_ID',
                    'PROPERTY_TYPE'
                )
            ));

            while($arProperty = $rsProperties->fetch())
            {
                static::$iblocks[$arProperty['IBLOCK_ID']]['PROPERTIES'][$arProperty['CODE']] = $arProperty;

                if(in_array($arProperty['CODE'], array('CML2_LINK')))
                {
                    // Записываем в инфоблок предложений связку с каталогом
                    static::$iblocks[$arProperty['IBLOCK_ID']]['IS_SKU'] = true;
                    static::$iblocks[$arProperty['IBLOCK_ID']]['CATALOG_IBLOCK_ID'] = $arProperty['LINK_IBLOCK_ID'];
                    static::$iblocks[$arProperty['IBLOCK_ID']]['SKU_PROPERTY_ID'] = $arProperty['ID'];

                    // Записываем в кататог связку с инфоблоком предложений
                    static::$iblocks[$arProperty['LINK_IBLOCK_ID']]['SKU_IBLOCK_ID'] = $arProperty['IBLOCK_ID'];
                    static::$iblocks[$arProperty['LINK_IBLOCK_ID']]['SKU_PROPERTY_ID'] = $arProperty['ID'];
                }
            }

            foreach(static::$iblocks as $arIBlock)
            {
                static::$iblocksByType[$arIBlock['IBLOCK_TYPE_ID']][$arIBlock['ID']] = $arIBlock;
            }
        }

        return static::$iblocks;
    }

    private static function setSectionRecursive($array)
    {
        if(!is_array($array) || empty($array))
        {
            return false;
        }

        $arSections = array_reverse($array, true);

        foreach(array_keys($arSections) as $NID)
        {
            $curSection = $arSections[$NID];

            $arSections[$NID]['SECTION_PATH'] = $array[$NID]['SECTION_PATH'] =  '/'.$curSection['CODE'];
            $arSections[$NID]['SUBKEYS'] = $curSection['ID'];
            $arSections[$NID]['AR_SUBKEYS'] = array($curSection['ID']);

            if(intval($curSection['IBLOCK_SECTION_ID']) > 0)
            {
                $arSections[$NID]['SECTION_PATH'] = $array[$NID]['SECTION_PATH'] = $arSections[$curSection['IBLOCK_SECTION_ID']]['SECTION_PATH'].'/'.$curSection['CODE'];
                $arSections[$NID]['SUBKEYS'] = $arSections[$curSection['IBLOCK_SECTION_ID']]['SUBKEYS'].';'.$curSection['ID'];
                $arSections[$NID]['AR_SUBKEYS'] = array_merge($arSections[$curSection['IBLOCK_SECTION_ID']]['AR_SUBKEYS'], array($curSection['ID']));
            }
        }

        foreach(array_keys($array) as $NID)
        {
            $curSection = $arSections[$NID];

            if(intval($curSection['IBLOCK_SECTION_ID']) > 0)
            {
                $array[$curSection['IBLOCK_SECTION_ID']]['CHILD'][$NID] = $array[$NID];

                if(!empty($curSection['CHILDRENS']))
                {
                    $arSections[$curSection['IBLOCK_SECTION_ID']]['CHILDRENS'] .= $curSection['CHILDRENS'];
                }

                if(!empty($curSection['ELEMENTS']))
                {
                    if(!empty($arSections[$curSection['IBLOCK_SECTION_ID']]['ELEMENTS']))
                    {
                        $curSection['ELEMENTS'] = array_merge($curSection['ELEMENTS'], $arSections[$curSection['IBLOCK_SECTION_ID']]['ELEMENTS']);
                    }

                    $arSections[$curSection['IBLOCK_SECTION_ID']]['ELEMENTS'] = $curSection['ELEMENTS'];
                }

                $arSections[$curSection['IBLOCK_SECTION_ID']]['CHILDRENS'] .= $NID.';';

                unset($array[$NID]);
            }
            else
            {
                $arSections[$NID]['CHILDRENS'] .= $NID;
            }
        }

        return array(
            'TREE' => array_reverse($array, true),
            'SECTIONS' => $arSections
        );
    }

    public static function getSections($arFilter = array(), $UFExist = false, $clearCache = false)
    {
        $cache = Application::getInstance()->getManagedCache();
        $hash = implode('#', array("IBLOCK_SECTION_ELEMENTS", serialize($arFilter), $UFExist));
        $arIBSections = array();
        $arSElements = array();

        if($clearCache)
        {
            $cache->clean($hash);
        }

        if($cache->read(86400, $hash))
        {
            $arIBSections = $cache->get($hash);
        }
        else
        {
            if(empty($arFilter))
            {
                $arFilter = array(
                    '=ELEMENT.ACTIVE' => 'Y'
                );
            }

            $rsSElements = \Bitrix\Iblock\SectionElementTable::GetList(array(
                'runtime' => array(
                    'ELEMENT' => array(
                        'data_type' => '\Sepro\ElementTable',
                        'reference' => array(
                            '=ref.ID' => 'this.IBLOCK_ELEMENT_ID'
                        )
                    ),
                    'SECTION' => array(
                        'data_type' => '\Bitrix\Iblock\SectionTable',
                        'reference' => array(
                            '=ref.ID' => 'this.IBLOCK_SECTION_ID'
                        )
                    )
                ),
                'filter' => $arFilter,
                'select' => array(
                    'IBLOCK_ELEMENT_ID',
                    'IBLOCK_SECTION_ID'
                )
            ));

            while($arSElement = $rsSElements->fetch())
            {
                $arSElements[$arSElement['IBLOCK_SECTION_ID']][$arSElement['IBLOCK_ELEMENT_ID']] = $arSElement['IBLOCK_ELEMENT_ID'];
            }

            $rsSections = \Bitrix\Iblock\SectionTable::GetList(
                array(
                    'order' => array(
                        'LEFT_MARGIN' => 'DESC',
                        'SORT' => 'ASC',
                        'ID' => 'ASC'
                    ),
                    'filter' => array(
                        'ACTIVE' => 'Y'
                    ),
                    'select' => array(
                        'ID',
                        'XML_ID',
                        'IBLOCK_ID',
                        'ACTIVE',
                        'NAME',
                        'CODE',
                        'PICTURE',
                        'DEPTH_LEVEL',
                        'IBLOCK_SECTION_ID',
                        'DESCRIPTION'
                    )
                )
            );

            while($arSection = $rsSections->fetch())
            {
                if(!empty($arSElements[$arSection['ID']]))
                {
                    $arSection['ELEMENTS'] = $arSElements[$arSection['ID']];
                }

                $arIBSections[$arSection['IBLOCK_ID']][$arSection['ID']] = $arSection;
            }

            foreach($arIBSections as $IBLOCK_ID => &$arSections)
            {
                $arSections = self::setSectionRecursive($arSections);

                if($UFExist)
                {
                    $ORMUFSections = ORMFactory::compile('b_uts_iblock_'.$IBLOCK_ID.'_section');
                    $rsUFSections = $ORMUFSections::GetList(array(
                        'select' => array('*')
                    ));

                    while($arUFSection = $rsUFSections->fetch())
                    {
                        $SECTION_ID = array_shift($arUFSection);

                        foreach(array_keys($arUFSection) as $UFCode)
                        {
                            if(empty($arUFSection[$UFCode])) continue;

                            $arSections['UF_FIELDS'][$UFCode][$SECTION_ID] = $arUFSection[$UFCode];
                        }
                    }
                }
            }

            $cache->setImmediate($hash, $arIBSections);
        }

        return $arIBSections;
    }
}