<?php

namespace Sepro;

use Bitrix\Main\Type\DateTime;
use Bitrix\Socialnetwork\UserRelationsTable as Friends;

class Socialnetwork{

	public static function getNoFriends(int $USER_ID, array $users){
		$alias   = 'friend';
		$friends = [];

		$friends_res = Friends::query()
			->addSelect('SECOND_USER_ID', $alias)
			->where('FIRST_USER_ID', $USER_ID)
			->whereIn('SECOND_USER_ID', $users)
			->union(Friends::query()
				->addSelect('FIRST_USER_ID', $alias)
				->where('SECOND_USER_ID', $USER_ID)
				->whereIn('FIRST_USER_ID', $users)
			)->exec();

		while($one = $friends_res->fetch()) $friends[] = $one[$alias];

		return array_diff($users, $friends);
	}

	public static function setNewFriends(int $USER_ID, array $users){
		foreach($users as $id){
			Friends::add([
				'FIRST_USER_ID'  => $USER_ID,
				'SECOND_USER_ID' => $id,
				'RELATION'       => 'F',
				'DATE_CREATE'    => new DateTime(),
				'DATE_UPDATE'    => new DateTime(),
				'MESSAGE'        => '',
				'INITIATED_BY'   => 'F',
			]);
		}
	}
}