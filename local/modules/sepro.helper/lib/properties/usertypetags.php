<?namespace Sepro\Properties;

use \Bitrix\Iblock,
    \Bitrix\Main\Page\Asset,
    \Sepro\User;

class UserTypeTags
{
    static $AExist = false;

    const USER_TYPE = 'sepro_tags';

    function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => Iblock\PropertyTable::TYPE_STRING,
            "USER_TYPE" => self::USER_TYPE,
            "CLASS_NAME" => "\Sepro\Properties\UserTypeTags",
            "DESCRIPTION" => "Теги",

            "GetPropertyFieldHtmlMulty" => array(__CLASS__, "GetPropertyFieldHtmlMulty"),
            "PrepareSettings" => array(__CLASS__, "PrepareSettings")
        );
    }

    function GetPropertyFieldHtmlMulty($arProperty, $values, $strHTMLControlName)
    {
        if($strHTMLControlName['MODE'] == 'iblock_element_admin')
        {
            return implode(' / ', array_column($values, 'VALUE'));
        }

        if(!static::$AExist)
        {
            \CJSCore::RegisterExt('tags', array(
                'js' => SEPRO_TOOLS_JS.self::USER_TYPE.'.js',
                'css' => SEPRO_TOOLS_CSS.self::USER_TYPE.'.css'
            ));

            \CJSCore::Init(array('jquery', 'tags'));

            static::$AExist = true;
        }

        $request = User::getRequest();
        $values = array_combine(array_keys($values), array_column($values, 'VALUE'));
        $arContainers = array(
            '#INPUTS#' => \Sepro\Form::input(
                'text',
                false,
                implode(', ', $values),
                false,
                array(
                    'SIZE' => 30,
                    'onkeyup' => 'Sepro.Tags_'.$arProperty['ID'].'.keyup(this);'
                )
            ),
            '#VARIANTS#' => \Sepro\DOM::addContainer('div', false, array('class' => 'sepro-tags-variants js-sepro-tags-variants')),
            '#PID#' => $arProperty['ID'],
            '#VALUES#' => json_encode(array_flip($values)),
            '#REQUEST#' => json_encode($request->getQueryList()->toArray(), JSON_HEX_TAG)
        );

        foreach($values as $VID => $value)
        {
            $arContainers['#INPUTS#'] .= \Sepro\Form::input(
                'hidden',
                $strHTMLControlName['VALUE'].'['.$VID.']',
                $value,
                false,
                array('class' => 'js-sepro-tags-hidden')
            );
        }

        echo str_replace(
            array_keys($arContainers),
            $arContainers,
            implode('', array(
                \Sepro\DOM::addContainer('div', '#INPUTS##VARIANTS#', array('class' => 'sepro-tags js-sepro-tags')),
                \Sepro\DOM::addContainer(
                    'script',
                    'Sepro.Tags_'.$arProperty['ID'].' = new Sepro_tags(#PID#, #VALUES#, #REQUEST#);'
                )
            ))
        );

        return true;
    }

    function PrepareSettings($arFields)
    {
        $arFields['MULTIPLE'] = 'Y';

        return $arFields;
    }
}