<?namespace Sepro;

class DOM
{
    public static function addLoner($tag, array $attr = array())
    {
        return '<'.$tag.self::attributes($attr).'/>';
    }

    public static function addContainer($tag, $content = '', array $attr = array())
    {
        return '<'.$tag.self::attributes($attr).'>'.$content.'</'.$tag.'>';
    }

    private static function attributes(array $attributes = array())
    {
        if (null !== $attributes)
        {
            $data = array();

            foreach ($attributes as $name => $value)
            {
                $data[] = ' '.htmlspecialchars($name).'="'.htmlspecialchars($value).'"';
            }

            return implode('', $data);
        }

        return false;
    }
}