<?namespace Sepro\Traits;

use \Sepro\User;

trait SystemEvents
{
    function OnBeforePrologHandler()
    {
        // CRONTAB EXIST $_SERVER is empty;
        if(!User::getUserIP())
        {
            return true;
        }

        define('PULL_USER_ID', -1);

        if(class_exists('CPullWatch'))
        {
            //\CPullWatch::Add(PULL_USER_ID, 'GENERAL_CHAT');
        }

        $request = User::getRequest();

        if($request->isAjaxRequest() && !empty($request->get('AJAX')))
        {
            $GLOBALS['APPLICATION']->RestartBuffer();
            self::checkAjax($request);
            \CMain::FinalActions();
            die();
        }

        return true;
    }

    function _Check404Error()
    {
        if(defined('ERROR_404') || strpos(\CHTTP::GetLastStatus(), '404') !== false)
        {
            global $APPLICATION;

            $APPLICATION->RestartBuffer();

            $GLOBALS['_SERVER']["REAL_FILE_PATH"] = "/404.php";

            require($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/header.php');
            require($_SERVER['DOCUMENT_ROOT'].'/404.php');
            require($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/footer.php');
        }
    }
}