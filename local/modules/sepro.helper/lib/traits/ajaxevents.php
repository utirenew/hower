<?namespace Sepro\Traits;

use \Sepro\Properties\UserTypeTags,
    \Sepro\ORMFactory,
    \Sepro\DOM;

trait AjaxEvents
{
    public static function checkAjax($request)
    {
        switch($request->get('AJAX'))
        {
            case 'tags': // ONLY FOR ADMIN PANEL

                $VALUE = $request->get('VALUE');
                $PID = $request->get('PID');
                $arVariants = array();

                if(empty($VALUE))
                {
                    break;
                }

                $ORMEPTable = ORMFactory::compile('b_iblock_element_property');
                $variants = DOM::addContainer('ul', '#VARIANTS#');

                $rsValues = $ORMEPTable::GetList(array(
                    'order' => array(
                        'VALUE' => 'ASC'
                    ),
                    'runtime' => array(
                        'COUNT_VALUE' => array(
                            'data_type' => 'integer',
                            'expression' => array('COUNT(DISTINCT %s)', 'VALUE'),
                        ),
                        'PROPERTY' => array(
                            'data_type' => '\Bitrix\Iblock\PropertyTable',
                            'reference' => array(
                                '=ref.ID' => 'this.IBLOCK_PROPERTY_ID'
                            )
                        )
                    ),
                    'filter' => array(
                        'PROPERTY.USER_TYPE' => UserTypeTags::USER_TYPE,
                        '=%VALUE' => $VALUE.'%'
                    ),
                    'limit' => 10,
                    'select' => array(
                        'VALUE',
                        'COUNT_VALUE'
                    )
                ));

                while($arValue = $rsValues->fetch())
                {
                    if($arValue['VALUE'] == $VALUE) continue;

                    $arValue['HTML_VALUE'] = preg_replace('/('.preg_quote($VALUE).')/ui', '<b class="gotcha">$1</b>', $arValue['VALUE'], 1);
                    $arVariants[] = DOM::addContainer(
                        'li',
                        DOM::addContainer(
                            'a',
                            $arValue['HTML_VALUE'],
                            array(
                                'href' => '#',
                                'onclick' => "Sepro.Tags_$PID.choose('".$arValue['VALUE']."'); return false;"
                            )
                        )
                    );
                }

                echo str_replace('#VARIANTS#', implode('', $arVariants), $variants);

                break;

                // YOU CAN ADD NEW CONDITIONS BY AJAX
        }

        return true;
    }
}