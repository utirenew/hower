<?namespace Sepro\Traits;

use \Sepro\Properties\UserTypeTags,
    \Sepro\App,
    \Sepro\User,
    \Sepro\DOM;

trait BufferEvents
{
    function OnEndBufferContentHandler(&$content)
    {
        switch(App::GetCurPage())
        {
            case '/bitrix/admin/iblock_element_edit.php':

                $request = User::getRequest();
                $IBLOCK_ID = $request->get('IBLOCK_ID');

                if(
                    !empty($request->get('bxsender')) ||
                    !empty($_SERVER['HTTP_X_REQUESTED_WITH']) ||
                    !$GLOBALS['USER']->IsAuthorized()
                ){
                    break;
                }

                $rsProperties = \Bitrix\Iblock\PropertyTable::GetList(array(
                    'filter' => array(
                        '=IBLOCK_ID' => $IBLOCK_ID,
                        '=USER_TYPE' => array(
                            UserTypeTags::USER_TYPE
                        )
                    ),
                    'select' => array('ID')
                ));

                if(intval($rsProperties->getSelectedRowsCount()))
                {
                    $DOM = new \DOMDocument();

                    @$DOM->loadHTML(DOM::addLoner('meta', array(
                            'http-equiv' => 'Content-Type',
                            'content' => 'text/html; charset=utf-8'
                        )).$content);

                    while($arProperty = $rsProperties->fetch())
                    {
                        $PROP = $DOM->getElementById('tr_PROPERTY_'.$arProperty['ID']);

                        if(!$PROP) continue;

                        $TD = $PROP->getElementsByTagName('td')->item(1);
                        $TABLE = $TD->getElementsByTagName('table')->item(0);

                        $TD->removeChild($TABLE);
                    }

                    $content = $DOM->saveHTML();
                }

                break;
        }
    }


}