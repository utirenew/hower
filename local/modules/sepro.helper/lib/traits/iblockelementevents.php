<?namespace Sepro\Traits;

use \Sepro\App,
    \Sepro\IBlock,
    \Sepro\Log,
    \Sepro\ORMFactory,
    \Sepro\Properties\UserTypeTags,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

trait IBlockElementEvents
{
    private static $base_cg = array(); // Базовый тип цены
    private static $catalog_iblock = array();

    // перед добавлением элемента
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        return self::checkFields($arFields, 'ADD');
    }

    // после добавления элемента
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        // В случае если есть ошибка при добавлении
        if(intval($arFields['ID']) <= 0) return true;

        self::updateDependence($arFields['ID'], 'ADD');
        App::deleteCache();

        return true;
    }

    // перед обновлением элемента
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        return self::checkFields($arFields, 'UPDATE');
    }

    // после обновления элемента
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        self::updateDependence($arFields['ID'], 'UPDATE');
        App::deleteCache();
    }

    // перед удалением элемента
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        self::updateDependence($ID, 'DELETE');
        App::deleteCache();
    }

    public static function checkFields($arFields, $action)
    {
        $arErrors = array();

        switch(App::GetCurPage())
        {
            case '/bitrix/admin/iblock_element_edit.php':

                $rsProperties = \Bitrix\Iblock\PropertyTable::GetList(array(
                    'filter' => array(
                        '=IBLOCK_ID' => $arFields['IBLOCK_ID'],
                        '=IS_REQUIRED' => 'Y',
                        '=USER_TYPE' => array(
                            UserTypeTags::USER_TYPE
                        )
                    ),
                    'select' => array('ID', 'NAME')
                ));

                while($arProperty = $rsProperties->fetch())
                {
                    if(empty($arFields['PROPERTY_VALUES'][$arProperty['ID']]))
                    {
                        $arErrors[] = Loc::getMessage('SEPRO_HELPER_ERROR_EVENT_USER_FIELD_EXIST', array('#PROPERTY_NAME#' => $arProperty['NAME']));
                    }
                }

                break;
        }

        if(!empty($arErrors))
        {
            App::GetInstance()->ThrowException(implode('<br>', $arErrors));
            return false;
        }

        return true;
    }

    public static function updateDependence($EID, $action)
    {
        $arProduct = array();

        if(class_exists('\Bitrix\Catalog\ProductTable'))
        {
            $arProduct = \Bitrix\Catalog\ProductTable::GetList(array(
                'filter' => array(
                    'ID' => $EID,
                    'TYPE' => array(
                        self::TYPE_PRODUCT,
                        self::TYPE_SET,
                        self::TYPE_SKU,
                        self::TYPE_OFFER
                    )
                ),
                'select' => array('TYPE')
            ))->fetch();
        }

        if(empty($arProduct))
        {
            return true;
        }

        if($action == 'DELETE' && in_array($arProduct['TYPE'], array(self::TYPE_PRODUCT, self::TYPE_SET, self::TYPE_SKU)))
        {
            return true;
        }

        // Записываем базовую цену
        if(empty(static::$base_cg))
        {
            static::$base_cg = \Bitrix\Catalog\GroupTable::getList(array(
                'filter' => array('=BASE' => 'Y'),
                'select' => array('ID')
            ))->fetch();
        }

        // Записываем каталог товаров
        if(empty(static::$catalog_iblock))
        {
            static::$catalog_iblock = IBlock::getIBCatalog(defined('IBLOCK_CATALOG_ID') ? IBLOCK_CATALOG_ID : false);
        }

        if(
            !intval(static::$catalog_iblock['PROPERTIES']['MINIMUM_PRICE']['ID']) ||
            !intval(static::$catalog_iblock['PROPERTIES']['MAXIMUM_PRICE']['ID']) ||
            !intval(static::$catalog_iblock['SKU_PROPERTY_ID'])
        )
        {
            return true;
        }

        self::updateMinMaxPrice(
            $EID,
            $arProduct['TYPE'],
            static::$catalog_iblock['PROPERTIES']['MINIMUM_PRICE']['ID'],
            static::$catalog_iblock['PROPERTIES']['MAXIMUM_PRICE']['ID'],
            static::$catalog_iblock['SKU_PROPERTY_ID']
        );

        return true;
    }

    public static function updateMinMaxPrice($EID, $type, $PID_MIN, $PID_MAX, $PID_SKU)
    {
        $ORMEPTable = ORMFactory::compile('b_iblock_element_property');
        $arOLDPrices = array();
        $arPrices = array();

        if(in_array($type, array(self::TYPE_PRODUCT, self::TYPE_SET)))
        {
            $arEPrice = \Bitrix\Catalog\PriceTable::GetList(array(
                'filter' => array(
                    '=PRODUCT_ID' => $EID,
                    'CATALOG_GROUP_ID' => static::$base_cg['ID']
                ),
                'select' => array(
                    'PRODUCT_ID',
                    'PRICE'
                )
            ))->fetch();

            if(!empty($arEPrice))
            {
                $arPrices[$arEPrice['PRODUCT_ID']] = $arEPrice['PRICE'];
            }
        }

        if($type == self::TYPE_OFFER)
        {
            $arEPTable = $ORMEPTable::GetList(array(
                'filter' => array(
                    '=IBLOCK_ELEMENT_ID' => $EID,
                    '=IBLOCK_PROPERTY_ID' => $PID_SKU
                ),
                'select' => array('VALUE')
            ))->fetch();

            if(!empty($arEPTable['VALUE']))
            {
                $EID = $arEPTable['VALUE'];
            }
        }

        if(empty($arPrices))
        {
            $rsEPTable = $ORMEPTable::GetList(array(
                'runtime' => array(
                    'PRICE' => array(
                        'data_type' => '\Bitrix\Catalog\PriceTable',
                        'reference' => array(
                            '=ref.PRODUCT_ID' => 'this.IBLOCK_ELEMENT_ID',
                            '=ref.CATALOG_GROUP_ID' => new \Bitrix\Main\DB\SqlExpression('?i', static::$base_cg['ID']),
                            '=ref.CURRENCY' => new \Bitrix\Main\DB\SqlExpression('?', 'RUB'),
                        )
                    )
                ),
                'filter' => array(
                    '=VALUE' => $EID,
                    '=IBLOCK_PROPERTY_ID' => $PID_SKU,
                    '!PRICE.PRICE' => false
                ),
                'select' => array(
                    'IBLOCK_ELEMENT_ID',
                    'PRICE_VALUE' => 'PRICE.PRICE'
                )
            ));

            while($arEPTable = $rsEPTable->fetch())
            {
                $arPrices[$arEPTable['IBLOCK_ELEMENT_ID']] = $arEPTable['PRICE_VALUE'];
            }
        }

        /////////////////////////
        // GET CUR DATA MIN AND MAX
        /////////////////////////

        $rsEPTable = $ORMEPTable::GetList(array(
            'filter' => array(
                '=IBLOCK_ELEMENT_ID' => $EID,
                '=IBLOCK_PROPERTY_ID' => array(
                    $PID_MIN,
                    $PID_MAX
                )
            ),
            'select' => array(
                'ID',
                'IBLOCK_PROPERTY_ID'
            )
        ));

        while($arEPTable = $rsEPTable->fetch())
        {
            $arOLDPrices[$arEPTable['IBLOCK_PROPERTY_ID']] = $arEPTable['ID'];
        }

        /////////////////////////
        // ADD OR UPDATE MIN PRICE
        /////////////////////////

        if(!empty($arOLDPrices[$PID_MIN]))
        {
            $obResult = $ORMEPTable::update($arOLDPrices[$PID_MIN], array(
                'VALUE' => min($arPrices)
            ));
        }
        else
        {
            $obResult = $ORMEPTable::add(array(
                'IBLOCK_PROPERTY_ID' => $PID_MIN,
                'IBLOCK_ELEMENT_ID' => $EID,
                'VALUE' => min($arPrices),
                'VALUE_NUM' => floatval(min($arPrices)),
                'VALUE_TYPE' => 'text'
            ));
        }

        if(!$obResult->isSuccess())
        {
            Log::add2log(array_merge(
                array(Loc::getMessage('SEPRO_HELPER_ERROR_EVENT_MIN_PRICE', array('#PRODUCT_ID#' => $EID, '#VALUE#' => min($arPrices)))),
                $obResult->getErrorMessages()
            ));

            return false;
        }

        /////////////////////////
        // ADD OR UPDATE MAX PRICE
        /////////////////////////

        if(!empty($arOLDPrices[$PID_MAX]))
        {
            $obResult = $ORMEPTable::update($arOLDPrices[$PID_MAX], array(
                'VALUE' => max($arPrices)
            ));
        }
        else
        {
            $obResult = $ORMEPTable::add(array(
                'IBLOCK_PROPERTY_ID' => $PID_MAX,
                'IBLOCK_ELEMENT_ID' => $EID,
                'VALUE' => max($arPrices),
                'VALUE_NUM' => floatval(max($arPrices)),
                'VALUE_TYPE' => 'text'
            ));
        }

        if(!$obResult->isSuccess())
        {
            Log::add2log(array_merge(
                array(Loc::getMessage('SEPRO_HELPER_ERROR_EVENT_MAX_PRICE', array('#PRODUCT_ID#' => $EID, '#VALUE#' => max($arPrices)))),
                $obResult->getErrorMessages()
            ));

            return false;
        }

        return true;
    }
}