<?
namespace Sepro;

class Basket
{
    private static $basket = false;

    public static function getBasket(array $arSelect = array())
    {
        if(empty(static::$basket))
        {
            $arSKUIBlock = IBlock::getIBOffers();
            $arBasket = array();

            $arFields = array(
                'runtime' => array(
                    'ELEMENT' => array(
                        'data_type' => '\Sepro\ElementTable',
                        'reference' => array(
                            '=ref.ID' => 'this.PRODUCT_ID'
                        )
                    )
                ),
                'filter' => array(
                    '=FUSER_ID' => \CSaleBasket::GetBasketUserID(),
                    '=ORDER_ID' => NULL,
                    '=CAN_BUY' => 'Y',
                    '=DELAY' => 'N'
                ),
                'select' => array_merge($arSelect, array(
                    'ID',
                    'PRODUCT_ID',
                    'QUANTITY'
                ))
            );

            if(!empty($arSKUIBlock))
            {
                $arFields['runtime']['CML2_LINK'] = array(
                    'data_type' => '\Sepro\ElementTable',
                    'reference' => array(
                        '=ref.ID' => 'this.ELEMENT.IBLOCK_'.$arSKUIBlock['ID'].'_PROPERTY_CML2_LINK.VALUE'
                    )
                );
            }

            $rsBasket = \Bitrix\Sale\Internals\BasketTable::GetList($arFields);

            while($arProduct = $rsBasket->fetch())
            {
                $arBasket[$arProduct['PRODUCT_ID']] = $arProduct;
            }

            static::$basket = $arBasket;
        }

        return static::$basket;
    }
}