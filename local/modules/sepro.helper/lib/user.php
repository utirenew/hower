<?

namespace Sepro;

use Bitrix\{
	Main\Localization\Loc,
	Sale\Location\LocationTable as Location,
	Sale\Location\Name\LocationTable as Name,
	Main\Service\GeoIp,
	Main\UserTable,
	Iblock\ElementTable};

Loc::loadMessages(__FILE__);

class User{
	private static $user = false;
	private static $location = false;
	private static $requests = array();
	private static $assets = array();
	private static $ip = false;
	private static $city = false;
	private static $geoCode = '69MH6XT6';

	public static function getInstance(){
		if(!static::$user){
			static::$user = $GLOBALS['USER'];
		}

		return static::$user;
	}

	public static function addUser($arFields, $check_user = false){
		$obUser = new \CUser;
		$arUser = array();

		if(is_bool($check_user) && $check_user){
			$arUser = \Bitrix\Main\UserTable::GetList(array(
				'filter' => array(
					'XML_ID' => $arFields['XML_ID'],
				),
				'select' => array(
					'ID',
				),
			))->fetch();

			if(intval($arUser['ID'])){
				return $arUser['ID'];
			}
		}

		$password = rand(100000, 999999);

		$arFields['PASSWORD']         = $password;
		$arFields['CONFIRM_PASSWORD'] = $password;
		$arFields['UF_PASSWORD']      = $password;
		$arFields['UF_IP']            = self::getUserIP();

		foreach(explode(' ', $arFields['NAME']) as $key => $value){
			switch($key){
				case 0:
					$arFields['NAME'] = $value;
					break;
				case 1:
					$arFields['LAST_NAME'] = $value;
					break;
				case 2:
					$arFields['SECOND_NAME'] = $value;
					break;
			}
		}

		if(!$arUser['ID'] = $obUser->Add($arFields)){
			Log::add2log(array_merge(array(
				'Ошибка добавления нового пользователя',
				$obUser->LAST_ERROR,
			), $arFields));

			return false;
		}

		return $arUser['ID'];
	}

	public static function getRequest(){
		if(empty(static::$requests)){
			static::$requests = \Bitrix\Main\Context::getCurrent()->getRequest();
		}

		return static::$requests;
	}

	public static function getLocation(){
		if(empty(static::$location)){
			$arLocation = array('city' => '', 'region' => '', 'country' => '');
			$next_try   = true;

			self::getUserIP();

			if(empty(static::$ip))
				return false;

			// GET LOCATION BY BITRIX
			if(class_exists('\Bitrix\Main\Service\GeoIp\Manager')){
				$obData = GeoIp\Manager::getDataResult(static::$ip, 'ru');

				foreach(
					array(
						'city'    => (string)$obData->getGeoData()->cityName,
						'region'  => (string)$obData->getGeoData()->regionName,
						'country' => (string)$obData->getGeoData()->countryName,
					) as $code => $value
				){
					if(empty($value)){
						$arLocation = array('city' => '', 'region' => '', 'country' => '');
						$next_try   = true;
						break;
					}

					$arLocation[$code] = $value;
					$next_try          = false;
				}
			}

			// GET LOCATION BY MOD GEOIP
			// REQUIRED INSTALL GEOIP-DATA see more in /user/share/GeoIP
			if($next_try && function_exists('geoip_record_by_name')){
				$obData = geoip_record_by_name(static::$ip);

				foreach(
					array(
						'city'    => (string)$obData['city'],
						'region'  => (string)geoip_region_name_by_code($obData['country_code'], $obData['region']),
						'country' => (string)$obData['country_name'],
					) as $code => $value
				){
					if(empty($value)){
						$arLocation = array('city' => '', 'region' => '', 'country' => '');
						$next_try   = true;
						break;
					}

					$arLocation[$code] = $value;
				}
			}

			// GET LOCATION BY MOD GEOIP APACHE
			if($next_try){
				foreach(
					array(
						'city'    => $_SERVER['GEOIP_CITY'],
						'region'  => $_SERVER['GEOIP_REGION_NAME'],
						'country' => $_SERVER['GEOIP_COUNTRY_NAME'],
					) as $code => $value
				){
					if(empty($value)){
						$arLocation = array('city' => '', 'region' => '', 'country' => '');
						$next_try   = true;
						break;
					}

					$arLocation[$code] = $value;
				}
			}

			if($next_try && function_exists('curl_init')){
				$Xml = Helpers::dataCurl("http://ipgeobase.ru:7020/geo/?ip=" . static::$ip);

				if(!$Xml){
					$Xml = Helpers::dataCurl("http://geoip.elib.ru/cgi-bin/getdata.pl?sid=" . static::$geoCode . "&ip=" . static::$ip . "&hex=3ffd");
				}

				$obData = Helpers::parseXml($Xml);

				if($obData){
					foreach($obData->ip as $obValue){
						foreach(
							array(
								'city'    => !empty($obValue->city)?(string)$obValue->city:(string)$obValue->Town,
								'region'  => !empty($obValue->region)?(string)$obValue->region:(string)$obValue->Region,
								'country' => !empty($obValue->country)?(string)$obValue->country:(string)$obValue->Country,
							) as $code => $value
						){
							if(empty($value)){
								$arLocation = array('city' => '', 'region' => '', 'country' => '');
								$next_try   = true;
								break;
							}

							$arLocation[$code] = $value;
						}
					}
				}
			}

			static::$location = $arLocation;
		}

		return !empty(static::$location)?static::$location:false;
	}

	public static function addAssets($link, $type, $sort){
		if(!in_array($type, array('css', 'js')) || !intval($sort))
			return false;

		if(in_array($link, static::$assets[$type]))
			return true;

		static::$assets[$type][$sort] = $link;

		return true;
	}

	public static function getAssets($type){
		if(empty(static::$assets[$type]) || !is_array(static::$assets[$type])){
			return false;
		}

		ksort(static::$assets[$type]);

		return static::$assets[$type];
	}

	public static function getUserIP(){
		if(!static::$ip){
			static::$ip = class_exists('\Bitrix\Main\Service\GeoIp\Manager')?GeoIp\Manager::getRealIp():self::getUserHostAddress('REMOTE_ADDR');
		}

		return static::$ip;
	}

	public static function getUserHostAddress($ip_param_name = false){
		$arTrustedIPS = array(
			'GEOIP_ADDR',
			'HTTP_X_REAL_IP',
			'HTTP_CLIENT_IP',
			'HTTP_X_FORWARDED_FOR',
			'REMOTE_ADDR',
		);

		if(!empty($_SERVER[$ip_param_name]) && filter_var($_SERVER[$ip_param_name], FILTER_VALIDATE_IP))
			return $_SERVER[$ip_param_name];

		foreach($arTrustedIPS as $ipParamName){
			if($ip_param_name === $ipParamName)
				continue;

			if(!empty($_SERVER[$ipParamName]) && filter_var($_SERVER[$ipParamName], FILTER_VALIDATE_IP))
				return $_SERVER[$ipParamName];
		}

		return false;
	}

	/**
	 * @param bool $city
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function setCity($city = false){
		global $USER;

		if(!$city)
			return false;

		$uid = $USER->GetID();

		if(is_integer($city)){
			$arLoc = Location::getList(array(
				'select' => array('CITY_ID'),
				'filter' => array(
					'ID' => $city,
				),
			))->fetch();

			if($arLoc['CITY_ID'] !== null){
				$arLocName    = Name::getList(array(
					'select' => array('NAME'),
					'filter' => array(
						'LOCATION_ID' => $city,
						'LANGUAGE_ID' => 'ru',
					),
				))->fetch();
				static::$city = $arLocName['NAME'];
				$USER->SetParam('city', static::$city);
				if($USER->IsAuthorized())
					$USER->Update($uid, array('PERSONAL_CITY' => static::$city), false);
			}
		}
		elseif(is_string($city)){
			$arLocName = Name::getList(array(
				'select' => array('NAME', 'LOCATION_ID'),
				'filter' => array(
					'NAME_UPPER'  => strtoupper($city),
					'LANGUAGE_ID' => 'ru',
				),
			))->fetch();

			if(!empty($arLocName['LOCATION_ID'])){
				$arLoc = Location::getList(array(
					'select' => array('CITY_ID'),
					'filter' => array(
						'ID' => $arLocName['LOCATION_ID'],
					),
				))->fetch();

				if($arLoc['CITY_ID'] !== null){
					static::$city = $arLocName['NAME'];
					$USER->SetParam('city', static::$city);
					if($USER->IsAuthorized())
						$USER->Update($uid, array('PERSONAL_CITY' => static::$city), false);
				}
			}
		}

		return static::$city;
	}

	public static function setShop($action, $shop = false, $key = ''){
		global $USER;

		if(!$shop)
			return false;

		$uid = $USER->GetID();
		$res = UserTable::getList([
			'select' => ['UF_SHOPS'],
			'filter' => ['ID' => $uid],
		])->fetch();

		$update = $res['UF_SHOPS'];

		switch($action){
			case 'del':
				unset($update[$key]);
				break;
			case 'add':
				$update[] = $shop;
				break;
			case 'update':
				$update[$key] = $shop;
		}
		if($USER->Update($uid, array('UF_SHOPS' => $update), false)){
			$response = false;
		}
		else{
			$response = $USER->LAST_ERROR;
		}

		return $response;
	}


	/**
	 * @param array $ids Ключи sellers ID, значения - ид адресов доставки
	 *
	 * @return array Ключи ид продавцов, значения - наименования адресов доставки
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getDeliveriesPoints(array $ids){
		$return = [];

		if(!empty($ids)){

			$points_ids = [];
			foreach($ids as $elid){
				$points_ids = array_merge($points_ids, $elid);
			}

			$result = Name::getList([
				'select' => array('NAME', 'LOCATION_ID', 'ID'),
				'filter' => array(
					'LOCATION_ID' => array_unique($points_ids),
					'LANGUAGE_ID' => 'ru',
				),
			])->fetchAll();


			if(!empty($result)){
				$points = [];
				foreach($result as $item){
					$points[$item['NAME']] = $item['LOCATION_ID'];
				}
			}


			foreach($ids as $seller_id => $seller_points){

				$count = count($seller_points);

				if($count == 1){
					$return[$seller_id]['TITLE']         = ' ' . array_pop(array_keys($points));
					$return[$seller_id]['TITLE_IS_LINK'] = 0;
					$return[$seller_id]['LIST']          = '';
				}
				else{
					$return[$seller_id]['TITLE']         = ' ' . $count . ' ' . self::end_word_define($count);
					$return[$seller_id]['TITLE_IS_LINK'] = 1;
					$return[$seller_id]['LIST']          = implode('<br>', array_keys(array_intersect($points, $seller_points)));
				}
			}
		}

		return $return;
	}


	protected function end_word_define($value = 0){

		if(($value % 10 >= 5 and $value % 10 <= 9) or $value % 10 == 0 or ($value >= 11 and $value <= 14)){
			$return = 'регионов';
		}
		elseif($value % 10 == 1){
			$return = 'регион';
		}
		elseif($value % 10 >= 2 && $value % 10 <= 4){
			$return = 'региона';
		}

		return $return;
	}

	public static function getUsersByProductsID($ids = []){
		$user_ids = [];

		if(!empty($ids) && is_array($ids)){
			$user_ids_result = ElementTable::getList([
				'select' => ['CREATED_BY'],
				'group'  => ['CREATED_BY'],
				'filter' => ['ID' => $ids],
			])->fetchAll();

			foreach($user_ids_result as $one){
				$user_ids[] = $one['CREATED_BY'];
			}
		}

		return $user_ids;
	}
}