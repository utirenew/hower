<?
$MESS['SEPRO_HELPER_ERROR_EVENT_USER_FIELD_EXIST'] = 'Обязательное свойство #PROPERTY_NAME# не заполнено';
$MESS['SEPRO_HELPER_ERROR_EVENT_MIN_PRICE'] = 'Ошибка записи минимальной стоимости товара. PRODUCT_ID: #PRODUCT_ID#; VALUE: #VALUE#';
$MESS['SEPRO_HELPER_ERROR_EVENT_MAX_PRICE'] = 'Ошибка записи максимальной стоимости товара. PRODUCT_ID: #PRODUCT_ID#; VALUE: #VALUE#';
