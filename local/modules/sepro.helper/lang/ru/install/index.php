<?
$MESS['SEPRO_HELPER_MODULE_NAME'] = 'Sepro helper';
$MESS['SEPRO_HELPER_MODULE_DESC'] = 'Bitrix обертка';
$MESS['SEPRO_HELPER_PARTNER_NAME'] = 'sepro.su';
$MESS['SEPRO_HELPER_PARTNER_URI'] = 'https://sepro.su/';

$MESS['SEPRO_HELPER_MODULE_INSTALL_ERROR_INIT'] = 'Файл init.php доступен по пути - #PATH#';
$MESS['SEPRO_HELPER_MODULE_INSTALL_ERROR_INIT_DESC'] = 'Необходимо удалить файл на время установки';
?>