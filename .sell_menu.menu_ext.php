<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;
$aMenuLinksExt = $APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
	"IBLOCK_TYPE"      => "catalog",
	"IBLOCK_ID"        => "5",
	"DEPTH_LEVEL"      => "1",
	"CACHE_TYPE"       => "A",
	"CACHE_TIME"       => "3600",
	"IS_SEF"           => "Y",
	"SEF_BASE_URL"     => "/sell/",
	"SECTION_PAGE_URL" => "#SECTION_CODE_PATH#/",
	"DETAIL_PAGE_URL"  => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
), false, array('HIDE_ICONS' => true));
$aMenuLinks    = array_merge($aMenuLinks, $aMenuLinksExt);
