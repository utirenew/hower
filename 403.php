<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("403 Forbidden");
@define("ERROR_403","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доступ запрещён");
?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Ошибка 403. Доступ запрещён</h2>
            </div>
        </div>
    </div>
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
